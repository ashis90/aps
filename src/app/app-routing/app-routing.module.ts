import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule, PreloadAllModules } from "@angular/router";
import { HomeComponent } from "../frontend/home/home.component";
import { AboutComponent } from "../frontend/about/about.component";
import { ServicesComponent } from "../frontend/services/services.component";
import { ContactComponent } from "../frontend/contact/contact.component";
import { LoginComponent } from "../frontend/login/login.component";
import { PageNotFoundComponent } from "../frontend/page-not-found/page-not-found.component";
import { AuthGuard } from "../auth.guard";
import { DashHomeComponent } from "../dashboard/side-menu/dash-home/dash-home.component";
import { MyaccountComponent } from "../dashboard/myaccount/myaccount.component";
import { RegionalComponent } from "../dashboard/regional-manager/regional-list/regional.component";
import { SalesComponent } from "../dashboard/sales-manager/sales-list/sales.component";
import { PhysicianComponent } from "../dashboard/physician/physician-list/physician.component";
import { RegionalViewComponent } from "../dashboard/regional-manager/regional-view/regional-view.component";
import { RegionalEditComponent } from "../dashboard/regional-manager/regional-edit/regional-edit.component";
import { SalesEditComponent } from "../dashboard/sales-manager/sales-edit/sales-edit.component";
import { SalesViewComponent } from "../dashboard/sales-manager/sales-view/sales-view.component";
import { SalesRepsPhysiciansComponent } from "../dashboard/physician/sales-reps-physicians/sales-reps-physicians.component";
import { PhysicianEditComponent } from "../dashboard/physician/physician-edit/physician-edit.component";
import { PhysicianViewComponent } from "../dashboard/physician/physician-view/physician-view.component";
import { CommissionComponent } from "../dashboard/sales-manager/commission/commission.component";
import { CommissionHistoryComponent } from "../dashboard/sales-manager/commission-history/commission-history.component";
import { RegionalAddComponent } from "../dashboard/regional-manager/regional-add/regional-add.component";
import { SalesAddComponent } from "../dashboard/sales-manager/sales-add/sales-add.component";
import { PhysicianAddComponent } from "../dashboard/physician/physician-add/physician-add.component";
import { TrainingComponent } from "../dashboard/side-menu/sales-training/training.component";
import { PhysicianSpecimensComponent } from "../dashboard/physician/physician-specimens/physician-specimens.component";
import { AnnouncementsComponent } from "../dashboard/side-menu/announcements/announcement-list/announcements.component";
import { AnnouncementEditComponent } from "../dashboard/side-menu/announcements/announcement-edit/announcement-edit.component";
import { AnnouncementAddComponent } from "../dashboard/side-menu/announcements/announcement-add/announcement-add.component";
import { PopupMessageComponent } from "../dashboard/side-menu/popup-message/popup-message.component";
import { AnalyticsComponent } from "../dashboard/side-menu/analytics/analytics.component";
import { NewsletterComponent } from "../dashboard/side-menu/newsletter/newsletter.component";
import { NotificationsComponent } from "../dashboard/notifications/notifications.component";
import { Training2Component } from "../dashboard/side-menu/training/training2.component";
import { SuppliesComponent } from "../dashboard/supplies/supplies.component";
import { SubmittedReportsComponent } from "../dashboard/submitted-reports/submitted-reports.component";
import { SurveyComponent } from "../frontend/survey/survey.component";
import { CommissionDataEntryComponent } from "../dashboard/commission-data-entry/commission-data-entry.component";
import { ForgotPasswordComponent } from "../frontend/forgot-password/forgot-password.component";
import { CollectionsComponent } from "../frontend/collections/collections.component";
import { AnnouncementsHomeComponent } from "../dashboard/side-menu/announcements-home/announcements-home.component";
import { PartnersCompanySubmittedReportsComponent } from "../dashboard/partners-company-submitted-reports/partners-company-submitted-reports.component";
import { PartnersCompanyComponent } from "../dashboard/partners-company/partners-company.component";
import { PartnersCompanyManagerComponent } from "../dashboard/partners-company-manager/partners-company-manager.component";
import { BillerComponent } from "../dashboard/biller/biller.component";
import { PhysicianAnalyticsComponent } from "../dashboard/side-menu/physician-analytics/physician-analytics.component";
import { AbilityDiagnosticsPartnersComponent } from "../dashboard/ability-diagnostics-partners/ability-diagnostics-partners.component";
import { PartnerPhysiciansComponent } from "../dashboard/ability-diagnostics-partners/partner-physicians/partner-physicians.component";
import { AbilityPartnerEditComponent } from "../dashboard/ability-diagnostics-partners/ability-partner-edit/ability-partner-edit.component";
import { SalesAnalyticsComponent } from '../dashboard/side-menu/sales-analytics/sales-analytics.component';
import { WoundSubmittedReportsComponent } from "../dashboard/wound-submitted-reports/wound-submitted-reports.component";

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "home" },
  { path: "home", component: HomeComponent },
  { path: "about", component: AboutComponent },
  { path: "services", component: ServicesComponent },
  { path: "contact", component: ContactComponent },
  { path: "login", component: LoginComponent },
  { path: "forgot-password", component: ForgotPasswordComponent },
  { path: "survey", component: SurveyComponent },

  /////////// regional manager//////////
  {
    path: "regional-manager",
    component: RegionalComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager"
      ]
    }
  },
  {
    path: "view-regional-manager/:id",
    component: RegionalViewComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager"
      ]
    }
  },
  {
    path: "edit-regional-manager/:id",
    component: RegionalEditComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager"
      ]
    }
  },
  {
    path: "add-regional-manager",
    component: RegionalAddComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager"
      ]
    }
  },

  // Ability Diagnostics Partners
  {
    path: "ability-partners",
    component: AbilityDiagnosticsPartnersComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },

  // Ability Diagnostics Partners Edit
  {
    path: "edit-ability-partner/:id",
    component: AbilityPartnerEditComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },

  // Ability Diagnostics Partners Physician
  {
    path: "ability-partner-physicians/:id",
    component: PartnerPhysiciansComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },

  /////////// sales representative//////////
  {
    path: "sales-representative/:id",
    component: SalesComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager"
      ]
    }
  },

  {
    path: "sales-representative",
    component: SalesComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager"
      ]
    }
  },
  {
    path: "add-sales-representative",
    component: SalesAddComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager"
      ]
    }
  },
  {
    path: "edit-sales-representative/:id",
    component: SalesEditComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager"
      ]
    }
  },
  {
    path: "view-sales-representative/:id",
    component: SalesViewComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager"
      ]
    }
  },

  /////////// commission //////////

  {
    path: "fees/:id",
    component: PageNotFoundComponent
    // component: CommissionComponent,
    // canActivate: [AuthGuard],
    // data: {
    //   roles: [
    //     "aps_sales_manager",
    //     "sales",
    //     "clinic",
    //     "partner",
    //     "team_regional_Sales",
    //     "team_sales_manager",
    //     "regional_manager",
    //     "sales_regional_manager"
    //   ]
    // }
  },
  {
    path: "fee-history/:id",
    component: PageNotFoundComponent
    // component: CommissionHistoryComponent,
    // canActivate: [AuthGuard],
    // data: {
    //   roles: [
    //     "aps_sales_manager",
    //     "regional_manager",
    //     "sales_regional_manager",
    //     "team_regional_Sales",
    //     "partner",
    //     "sales"
    //   ]
    // }
  },

  /////////// physician //////////
  {
    path: "sales-physician-listing/:id",
    component: SalesRepsPhysiciansComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager"
      ]
    }
  },
  {
    path: "physician-specimens-details/:id",
    component: PhysicianSpecimensComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "sales_regional_manager",
        "regional_manager",
        "sales",
        "clinic",
        "partner",
        "combine_physicians_accounts",
        "data_entry_oparator"
      ]
    }
  },
  {
    path: "collections",
    component: CollectionsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },

  {
    path: "physician",
    component: PhysicianComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager",
        "sales",
        "clinic",
        "partner",
        "combine_physicians_accounts"
      ]
    }
  },
  {
    path: "add-physician",
    component: PhysicianAddComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "sales",
        "data_entry_oparator",
        "team_regional_Sales",
        "team_sales_manager",
        "sales_regional_manager"
      ]
    }
  },
  {
    path: "view-physician/:id",
    component: PhysicianViewComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager",
        "sales",
        "clinic",
        "partner",
        "combine_physicians_accounts"
      ]
    }
  },
  {
    path: "edit-physician/:id",
    component: PhysicianEditComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager",
        "sales",
        "clinic",
        "partner",
        "combine_physicians_accounts"
      ]
    }
  },

  {
    path: "dashboard",
    component: DashHomeComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager",
        "sales",
        "clinic",
        "partner",
        "combine_physicians_accounts",
        "physician",
        "partners_company_manager",
        "biller",
        "pharmacist"
      ]
    }
  },
  {
    path: "myaccount",
    component: MyaccountComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "team_regional_Sales",
        "team_manager",
        "team_sales_manager",
        "data_entry_oparator",
        "regional_manager",
        "sales_regional_manager",
        "sales",
        "clinic",
        "partner",
        "combine_physicians_accounts",
        "physician",
        "partners_company_manager",
        "biller",
        "pharmacist"
      ]
    }
  },
  {
    path: "sales-training",
    component: TrainingComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager", "regional_manager", "sales","sales_regional_manager"] }
  },
  {
    path: "announcements",
    component: AnnouncementsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },
  {
    path: "announcements-edit/:id",
    component: AnnouncementEditComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },
  {
    path: "announcements-add",
    component: AnnouncementAddComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },
  {
    path: "popup-message",
    component: PopupMessageComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },
  {
    path: "analytics",
    component: AnalyticsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },
  {
    path: "physician-analytics",
    component: PhysicianAnalyticsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },
  {
    path: 'sales-analytics',
    component: SalesAnalyticsComponent,
    canActivate: [AuthGuard],
    data: { roles: ['aps_sales_manager'] }
  },
  {
    path: "news-letter",
    component: NewsletterComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },
  {
    path: "fees",
    component: PageNotFoundComponent
    // component: CommissionComponent,
    // canActivate: [AuthGuard],
    // data: {
    //   roles: [
    //     "aps_sales_manager",
    //     "sales",
    //     "team_regional_Sales",
    //     "team_sales_manager",
    //     "sales_regional_manager",
    //     "regional_manager",
    //     "partner",
    //     "clinic"
    //   ]
    // }
  },
  {
    path: "notifications",
    component: NotificationsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "aps_sales_manager",
        "sales_regional_manager",
        "data_entry_oparator",
        "team_regional_Sales",
        "team_sales_manager",
        "sales"
      ]
    }
  },
  {
    path: "training",
    component: Training2Component,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "combine_physicians_accounts",
        "physician",
        "clinic",
        "team_regional_Sales",
        "regional_manager",
        "team_sales_manager",
        "sales_regional_manager"
      ]
    }
  },
  {
    path: "supplies",
    component: SuppliesComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [
        "combine_physicians_accounts",
        "physician",
        "sales",
        "clinic",
        "team_regional_Sales",
        "regional_manager",
        "team_manager",
        "team_sales_manager",
        "sales_regional_manager",
        "partner"
      ]
    }
  },
  {
    path: "submitted-reports",
    component: SubmittedReportsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["combine_physicians_accounts", "physician", "pharmacist"] }
  },
  {
    path: "partners-submitted-reports",
    component: PartnersCompanySubmittedReportsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["partners_company_manager"] }
  },
  {
    path: "partners-submitted-reports/:id",
    component: PartnersCompanySubmittedReportsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },
  {
    path: "submitted-reports/:id",
    component: SubmittedReportsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: ["aps_sales_manager", "combine_physicians_accounts", "physician"]
    }
  },
  {
    path: "wound-submitted-reports",
    component: WoundSubmittedReportsComponent,
    canActivate: [AuthGuard],
    data: { roles: ["pharmacist"] }
  },
  {
    path: "wound-submitted-reports/:id",
    component: WoundSubmittedReportsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: ["pharmacist"]
    }
  },
  {
    path: "commission-data-entry",
    component: PageNotFoundComponent
    // component: CommissionDataEntryComponent,
    // canActivate: [AuthGuard],
    // data: { roles: ["aps_sales_manager"] }
  },
  {
    path: "announcements-home",
    component: AnnouncementsHomeComponent,
    canActivate: [AuthGuard],
    data: { roles: ["sales"] }
  },
  {
    path: "partners",
    component: PartnersCompanyComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },
  {
    path: "partners-company-manager/:id",
    component: PartnersCompanyManagerComponent,
    canActivate: [AuthGuard],
    data: { roles: ["aps_sales_manager"] }
  },
  {
    path: "biller-submitted-reports",
    component: BillerComponent,
    canActivate: [AuthGuard],
    data: { roles: ["biller"] }
  },

  { path: "page-not-found", component: PageNotFoundComponent },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  //imports: [CommonModule, RouterModule.forRoot(routes, { useHash: true })],
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
