import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
// import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public login_url = environment.baseUrl + 'Login/';
  checklogin;
  headers: Headers = new Headers;
  options: any;
  constructor(private http: HttpClient) {
    // constructor() {
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append( 'Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }
  postsArray: any = [] ;

      // isLogin(sendData) {
      //  return this.http.post(this.login_url + 'login', JSON.stringify(sendData), this.options);
      // }

}
