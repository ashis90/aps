import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  NgZone
} from "@angular/core";
import { AnnouncementsService } from "src/app/dashboard/side-menu/announcements/announcements.service";
import { SalesManagerService } from "src/app/dashboard/sales-manager/sales-manager.service";
import { PhysicianService } from "src/app/dashboard/physician/physician.service";
import { Router, ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { PopupComponent } from "../../../frontend/popup/popup.component";
import { environment } from "src/environments/environment";
import * as FusionCharts from "fusioncharts";
import { NgxSpinnerService } from "ngx-spinner";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder,
  NgForm
} from "@angular/forms";
import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators"; //add this
export interface StateGroup {
  ids: Number[];
  names: string[];
}
const schemaUrl = [
  {
    name: "Time",
    type: "date",
    format: "%Y-%m-%d"
  },
  {
    name: "Number of Specimens",
    type: "number"
  }
];

const newschemaUrl = [
  {
    name: "Time",
    type: "date",
    format: "%Y-%m-%d"
  },
  {
    name: "Collection Amount",
    type: "number"
  }
];

// const newschemaUrl = [
// 	{
// 		"name": "Time",
// 		"type": "date",
// 		"format": "%Y-%m-%d"
// 	},
// 	{
// 		"name": "Type",
// 		"type": "string"
// 	},
// 	{
// 		"name": "Sales Value",
// 		"type": "number"
// 	}
// ];

@Component({
  selector: "app-dash-home",
  templateUrl: "./dash-home.component.html",
  styleUrls: ["./dash-home.component.css"]
})
export class DashHomeComponent implements OnInit {
  @ViewChild("videoPlayer") videoplayer: ElementRef;
  @ViewChild("phyChart") phyChart: ElementRef;
  public assetsUrl = environment.assetsUrl;
  username: string;
  userfullname: string;
  userrole: string;
  message: any;
  sendData: any;
  data: any;
  result: any;
  modalRef: BsModalRef;
  userid: string;
  popup_message: any;
  comm_specimen_list: any;
  phy_highest_specimen_list: any;
  cal_month: string;
  total_specimen_NF: any;
  total_specimen_W: any;
  dataSource: any;
  type: string;
  width: string;
  height: string;
  nwheight: string;
  dataUrl: any;
  sales_representative_data: any;
  phySearchForm: FormGroup;
  salesSearchForm: FormGroup;
  physician_data: any;
  phySpecimenCountData: any;
  phyChartData: any;

  sales_rep: any;
  physician_list: any;
  phy_from_date: any;
  phy_to_date: any;
  sale_from_date: any;
  sale_to_date: any;
  blankErr: boolean = false;
  dateRangeErr: boolean = false;
  cnt: any = 0;
  chart: any;
  month_list: any = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];
  cur_year: number = new Date().getFullYear();
  start_year: number = 2017;
  year_arr: any = [];
  phy_year: any;
  form_month: any;
  to_month: any;
  salesCommData: any;
  salesChartData: any;
  monthRangeErr: boolean = false;
  salesBlankErr: boolean = false;
  filteredOptions: Observable<StateGroup[]>;
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  public stateGroups: StateGroup[] = [
    {
      ids: [],
      names: []
    }
  ]; //test
  public searchData: StateGroup[] = [
    {
      ids: [],
      names: []
    }
  ];
  allNames: any = [];
  allIds: any = [];

  newfilteredOptions: Observable<StateGroup[]>;
  newoptions: any = [];
  newids: any = [];
  newfilterVal: any = [];
  public newstateGroups: StateGroup[] = [
    {
      ids: [],
      names: []
    }
  ]; //test
  public newsearchData: StateGroup[] = [
    {
      ids: [],
      names: []
    }
  ];
  newallNames: any = [];
  newallIds: any = [];
  constructor(
    private modalService: BsModalService,
    private itemsService: AnnouncementsService,
    private routes: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private SalesService: SalesManagerService,
    private formBuilder: FormBuilder,
    private PhysicianService: PhysicianService,
    private spinner: NgxSpinnerService
  ) {
    this.phySearchForm = this.formBuilder.group({
      physician_list: [""],
      physician_name_list: [""],
      phy_from_date: [""],
      phy_to_date: [""]
    });
    this.salesSearchForm = this.formBuilder.group({
      sales_rep: [""],
      sales_rep_name: [""],
      form_month: [""],
      to_month: [""],
      phy_year: [""]
    });
    this.username = localStorage.getItem("username");
    this.userrole = localStorage.getItem("userrole");
    this.userid = localStorage.getItem("userid");

    this.type = "timeseries";
    this.width = "100%";
    this.height = "600";
    this.nwheight = "350";
    this.dataSource = {
      data: null,
      yAxis: {
        plot: [{ value: "Sales", type: "area" }]
      },
      caption: {
        text: "Days"
      }
    };

    this.phySpecimenCountData = {
      data: null,
      yAxis: {
        plot: [{ value: "Sales", type: "area" }]
      },
      caption: {
        text: "Days"
      }
    };
    this.salesCommData = {
      data: null,
      yAxis: {
        plot: [
          {
            value: "Sales",
            type: "area"
          }
        ]
      },
      caption: {
        text: "Days"
      }
    };
    // this.salesCommData = {
    // 	// Initially data is set as null
    // 	data: null,
    // 	caption: {
    // 		text: 'Sales Analysis'
    // 	},
    // 	subcaption: {
    // 		text: 'Grocery & Footwear'
    // 	},
    // 	series: 'Type',
    // 	yAxis: [
    // 		{
    // 			plot: 'Sales Value',
    // 			title: 'Sale Value',
    // 			format: {
    // 				prefix: '$'
    // 			}
    // 		}
    // 	]

    // };
    while (this.start_year <= this.cur_year) {
      this.year_arr.push(this.start_year);
      this.start_year++;
    }
  }

  ngOnInit() {
    if(localStorage.getItem('userrole') === 'sales'){
      if(localStorage.getItem('view_popup') !== 'Yes'){
        console.log(localStorage.getItem('view_popup'))
        this.routes.navigate(["/sales-training"]);
        return false;
      }
    }
    
    this.sendData = {
      username: this.username,
      userrole: this.userrole,
      userid: this.userid
    };
    this.itemsService.fetch_annoucement(this.sendData).subscribe(data => {
      this.message = data;
      this.data = this.message.role;
      this.result = this.message.result;
      this.userfullname = this.message.fullname;

      if (this.message.popup_data.popup_text !== "") {
        var roleArray: Array<string> = [
          "physician",
          "sales",
          "sales_regional_manager",
          "team_sales_manager",
          "team_regional_Sales",
          "clinic",
          "partner"
        ];
        if (
          //this.data === 'physician' ||
          this.data === "sales"
          //this.data === 'sales_regional_manager' ||
          //this.data === 'team_sales_manager' ||
          //this.data === 'team_regional_Sales' ||
          //this.data === 'clinic' ||
          //this.data === 'partner'
        ) {
          this.modalRef = this.modalService.show(PopupComponent, {
            initialState: {
              title: "popup",
              text_message: this.message.popup_data.popup_text
            }
          });
        }
      }
    });
    let sendData = {
      user_role: this.userrole,
      user_id: this.userid
    };
    let salesData;
    let sales_reps_data;
    this.spinner.show();
    this.SalesService.getMonthlySalesSummary(sendData).subscribe(data => {
      salesData = data;

      if (salesData.status === "1") {
        this.comm_specimen_list = salesData.sales_reps_data;
        this.phy_highest_specimen_list =
          salesData.physician_specimen_count_data;
        this.cal_month = salesData.current_month;

        if (0 in salesData.prev_month_total_specimen_count_data) {
          this.total_specimen_NF =
            salesData.prev_month_total_specimen_count_data[0].total_specimen;
        } else {
          this.total_specimen_NF = "0";
        }
        if (1 in salesData.prev_month_total_specimen_count_data) {
          this.total_specimen_W =
            salesData.prev_month_total_specimen_count_data[1].total_specimen;
        } else {
          this.total_specimen_W = "0";
        }

        this.dataUrl = salesData.past_seven_days_specimen_data;
        this.sales_representative_data = salesData.sales_representative_data;
        this.physician_data = salesData.physician_data;
        this.sales_representative_data.map(option =>
          this.allNames.push(option.name)
        );
        this.sales_representative_data.map(val => this.allIds.push(val.id));
        this.sales_representative_data.map(data => this.filterVal.push(data));
        this.physician_data.map(option =>
          this.newallNames.push(option.physician_name)
        );
        this.physician_data.map(val => this.newallIds.push(val.physician_id));
        this.physician_data.map(data => this.newfilterVal.push(data));
        this.fetchData();
      }
      this.spinner.hide();
    });

    this.stateGroups = [
      {
        ids: this.allIds,
        names: this.allNames
      }
    ];

    this.filteredOptions = this.salesSearchForm
      .get("sales_rep_name")
      .valueChanges.pipe(
        startWith(""),
        map(value => this._filterGroup(value ? value.toLowerCase() : ""))
      );

    this.newstateGroups = [
      {
        ids: this.newallIds,
        names: this.newallNames
      }
    ];

    this.newfilteredOptions = this.phySearchForm
      .get("physician_name_list")
      .valueChanges.pipe(
        startWith(""),
        map(value => this._newfilterGroup(value ? value.toLowerCase() : ""))
      );
  }

  private _filterGroup(value: string): StateGroup[] {
    console.log(value);
    if (value) {
      let itemCount: number = this.filterVal.length;
      this.options = [];
      this.ids = [];
      for (let i = 0; i < itemCount; i++) {
        if (this.filterVal[i].name.toLowerCase().indexOf(value) !== -1) {
          this.options.push(this.filterVal[i].name);
          this.ids.push(this.filterVal[i].id);
        }
      }
      this.searchData = [
        {
          ids: this.ids,
          names: this.options
        }
      ];

      return this.searchData;
    }

    return this.stateGroups;
  }

  private _newfilterGroup(value: string): StateGroup[] {
    if (value) {
      let itemCount: number = this.newfilterVal.length;
      this.newoptions = [];
      this.newids = [];
      for (let i = 0; i < itemCount; i++) {
        if (
          this.newfilterVal[i].physician_name.toLowerCase().indexOf(value) !==
          -1
        ) {
          this.newoptions.push(this.newfilterVal[i].physician_name);
          this.newids.push(this.newfilterVal[i].physician_id);
        }
      }
      this.newsearchData = [
        {
          ids: this.newids,
          names: this.newoptions
        }
      ];
      return this.newsearchData;
    }

    return this.newstateGroups;
  }

  callSomeFunction(phy_id: any) {
    this.salesSearchForm.controls["sales_rep"].setValue(phy_id);
  }

  onEnter(evt: any) {
    this.salesSearchForm.controls["sales_rep"].setValue("");
  }

  newcallSomeFunction(phy_id: any) {
    this.phySearchForm.controls["physician_list"].setValue(phy_id);
  }

  newonEnter(evt: any) {
    this.phySearchForm.controls["physician_list"].setValue("");
  }

  getSalesData() {
    this.sales_rep = this.salesSearchForm.value.sales_rep;
    this.form_month = this.salesSearchForm.value.form_month;
    this.to_month = this.salesSearchForm.value.to_month;
    this.phy_year = this.salesSearchForm.value.phy_year;
    this.spinner.show();
    if (
      this.sales_rep &&
      this.phy_year &&
      this.form_month &&
      this.to_month &&
      this.form_month < this.to_month
    ) {
      let serviceData;
      let param = {
        sales_rep: this.sales_rep,
        phy_year: this.phy_year,
        form_month: this.form_month,
        to_month: this.to_month,
        user_role: this.userrole
      };
      this.monthRangeErr = false;
      this.salesBlankErr = false;
      this.SalesService.getSpecimenCountForSales(param).subscribe(data => {
        serviceData = data;
        this.spinner.hide();
        this.salesChartData = serviceData.sales_comm_arr;
        this.fetchSalesChartData();
      });
    } else if (this.form_month > this.to_month) {
      this.monthRangeErr = true;
      this.salesBlankErr = false;
    } else {
      this.salesBlankErr = true;
      this.monthRangeErr = false;
    }
  }

  getPhysicianData() {
    this.physician_list = this.phySearchForm.value.physician_list;
    this.phy_from_date = this.convertDate(
      this.phySearchForm.value.phy_from_date
    );
    this.phy_to_date = this.convertDate(this.phySearchForm.value.phy_to_date);
    this.spinner.show();
    if (
      this.physician_list &&
      this.phy_from_date &&
      this.phy_to_date &&
      this.phy_from_date <= this.phy_to_date
    ) {
      let serviceData;
      this.blankErr = false;
      this.dateRangeErr = false;
      let param = {
        physician_list: this.physician_list,
        phy_from_date: this.phy_from_date,
        phy_to_date: this.phy_to_date,
        user_role: this.userrole
      };
      this.PhysicianService.getSpecimenCountForPhysicians(param).subscribe(
        data => {
          serviceData = data;
          this.spinner.hide();
          this.phyChartData = serviceData.physician_specimen_data;
          this.phySpecimenCountData.data = "";
          this.fetchPhyChartData();
        }
      );
    } else if (this.phy_from_date > this.phy_to_date) {
      this.dateRangeErr = true;
      this.blankErr = false;
    } else {
      this.blankErr = true;
      this.dateRangeErr = false;
    }
  }

  modal_hide() {
    this.modalRef.hide();
  }

  updatechk() {
    this.userid = localStorage.getItem("userid");
    this.itemsService.updatechk(this.userid).subscribe(data => {
      this.popup_message = data;
      if (this.popup_message.status === "1") {
        window.location.reload();
      }
    });
  }
  closeVideo() {
    this.videoplayer.nativeElement.pause();
  }

  fetchData() {
    let jsonify = res => res.json();
    let dataFetch = this.dataUrl;
    let schemaFetch = schemaUrl;
    Promise.all([dataFetch, schemaFetch]).then(res => {
      let data = res[0];
      let schema = res[1];
      if (data !== "No data found.") {
        let fusionTable = new FusionCharts.DataStore().createDataTable(
          data,
          schema
        ); // Instance of DataTable to be passed as data in dataSource
        this.dataSource.data = fusionTable;
      }
    });
  }

  initialized($event) {
    this.chart = $event.chart; // saving chart instance
  }

  fetchPhyChartData() {
    let dataFetch = this.phyChartData;
    let schemaFetch = schemaUrl;

    if (this.chart) {
    }
    Promise.all([dataFetch, schemaFetch]).then(res => {
      let data = res[0];
      let schema = res[1];

      if (data !== "No data found.") {
        let fusionDataStore = new FusionCharts.DataStore();
        // After that we are creating a DataTable by passing our data and schema as arguments
        let fusionTable = fusionDataStore.createDataTable(data, schema);
        this.phySpecimenCountData.data = fusionTable;
      }
    });
  }

  fetchSalesChartData() {
    let dataFetch = this.salesChartData;
    let schemaFetch = newschemaUrl;

    if (this.chart) {
    }
    Promise.all([dataFetch, schemaFetch]).then(res => {
      let data = res[0];
      let schema = res[1];

      let fusionDataStore = new FusionCharts.DataStore();
      // After that we are creating a DataTable by passing our data and schema as arguments
      let fusionTable = fusionDataStore.createDataTable(data, schema);
      this.salesCommData.data = fusionTable;
    });
  }

  updateSalesChart() {
    this.salesCommData.data = "";
    this.salesSearchForm.reset();
  }

  updatePhyChart() {
    this.phySearchForm.reset();

    setTimeout(() => {
      this.phySpecimenCountData.data = "";
    }, 500);
  }

  getRandomSpan() {
    return Math.floor(Math.random() * 6 + 1);
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = "";
    } else {
      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }
}
