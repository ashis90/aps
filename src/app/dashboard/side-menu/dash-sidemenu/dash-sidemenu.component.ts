import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { LoginService } from "src/app/frontend/login/login.service";

@Component({
  selector: "app-dash-sidemenu",
  templateUrl: "./dash-sidemenu.component.html",
  styleUrls: ["./dash-sidemenu.component.css"]
})
export class DashSidemenuComponent implements OnInit {
  user_role: string;
  user_id: any;
  postsArray: any;
  usertoken: string;
	show_hide: boolean = false;

  constructor(private routes: Router, private service: LoginService) {
    this.user_role = localStorage.getItem("userrole");
    this.user_id = localStorage.getItem("userid");
  }

  ngOnInit() {
    this.usertoken = localStorage.getItem("usertoken");
    console.log(this.user_id)
  }

  logout() {
    this.service.islogout(this.usertoken).subscribe(data => {
      this.postsArray = data;
      if (this.postsArray.status === "1") {
        localStorage.removeItem("usertoken");
        localStorage.clear();
        this.routes.navigate(["/login"]);
      } else {
        this.routes.navigate(["/login"]);
      }
    });
 }
	openDropDown1() {
		if (this.show_hide == false) {
			this.show_hide = true;
		} else {
			this.show_hide = false;
		}


	}
 

}
