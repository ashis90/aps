import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  public baseUrl = environment.baseUrl + 'Analytics/';
  headers: Headers = new Headers;
  options: any;

  constructor(private http: HttpClient) {
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  get_data() {
    return this.http.post(this.baseUrl + 'get_data', this.options);
  }
  getPhysicianAnalyticsData(sendData): Observable<any> {
    console.log(sendData)
    return this.http.post(this.baseUrl + 'get_physician_analytics_data', JSON.stringify(sendData), this.options);
  }

  searchtPhysicianAnalyticsData(sendData): Observable<any> {
    console.log(sendData)
    return this.http.post(this.baseUrl + 'searcht_physician_analytics_data', JSON.stringify(sendData), this.options);
  }

  getSalesAnalyticsData(sendData): Observable<any> {
    console.log(sendData)
    return this.http.post(this.baseUrl + 'get_sales_analytics_data', JSON.stringify(sendData), this.options);
  }

  searchtSalesAnalyticsData(sendData): Observable<any> {
    console.log(sendData)
    return this.http.post(this.baseUrl + 'searcht_sales_analytics_data', JSON.stringify(sendData), this.options);
  }

}
