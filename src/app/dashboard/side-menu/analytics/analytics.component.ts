import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from 'src/app/dashboard/side-menu/analytics/analytics.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { setTheme } from 'ngx-bootstrap/utils';


@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {

   dataSource: any;
   dataSource2: any;
  details: any;
  details2: any;
  errMessage: string;
  alldetails: any;

  constructor(private service: AnalyticsService, private routes: Router,
    private http: HttpClient, private spinner: NgxSpinnerService,
     private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) {

     }


    ngOnInit() {
      this.spinner.show();
      this.service.get_data().subscribe( data => {
        this.alldetails = data;
        if (this.alldetails.status === '1') {
          this.details = this.alldetails.result;
          this.details2 = this.alldetails.result2;

      this.dataSource = {
        'chart': {
          'caption': 'Physicians Specialization Chart',
          'subCaption' : '',
          'showLegend': '1',
          'showPercentValues': '1',
          'legendPosition': 'bottom',
          'useDataPlotColorForLabels': '1',
          'enablemultislicing': '0',
          'showlegend': '0',
          'theme': 'fusion',
        },
        'data': [{
          'label': 'DPM',
          'value':  this.details.DPM
        }, {
          'label': 'PCP',
          'value': this.details.PCP
        }, {
          'label': 'DO',
          'value': this.details.DO
        }, {
          'label': 'PA',
          'value': this.details.PA
        }, {
          'label': 'Nurse Practitioner',
          'value': this.details.Nurse_Practitioner
        }]
    };

    this.dataSource2 = {
      'chart': {
        'caption': 'Specialization by Samples Sent',
        'subCaption' : '',
        'showLegend': '1',
        'showPercentValues': '1',
        'legendPosition': 'bottom',
        'useDataPlotColorForLabels': '1',
        'enablemultislicing': '0',
        'showlegend': '0',
        'theme': 'fusion',
      },
      'data': [{
        'label': 'DPM',
        'value':  this.details2.DPM
      }, {
        'label': 'PCP',
        'value': this.details2.PCP
      }, {
        'label': 'DO',
        'value': this.details2.DO
      }, {
        'label': 'PA',
        'value': this.details2.PA
      }, {
        'label': 'Nurse Practitioner',
        'value': this.details2.Nurse_Practitioner
      }]
  };
  this.spinner.hide();
  } else {
  this.errMessage = 'Something is worng!.';
 }
});
    }



}
