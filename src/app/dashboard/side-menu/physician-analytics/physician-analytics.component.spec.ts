import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicianAnalyticsComponent } from './physician-analytics.component';

describe('PhysicianAnalyticsComponent', () => {
  let component: PhysicianAnalyticsComponent;
  let fixture: ComponentFixture<PhysicianAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicianAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicianAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
