import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from 'src/app/dashboard/side-menu/analytics/analytics.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { setTheme } from 'ngx-bootstrap/utils';
import { ToastrService } from 'ngx-toastr';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-sales-analytics',
  templateUrl: './sales-analytics.component.html',
  styleUrls: ['./sales-analytics.component.css']
})
export class SalesAnalyticsComponent implements OnInit {
  statusSelect = '';
  monthSelect = '';
  yearSelect = '';
  limit: number;
  loadmorehide: boolean;
  sales_list: any = [];
  public data: any;
  notFount: boolean;
  Year: Date;
  currentYear: any;
  allyear: any = [];
  searchForm: FormGroup;
  sendData: any;
  succMessage: any;
  total_physician: number;
  bsInlineValue = new Date();
  todayDate = '';
  maxDate: Date;
  statusMessage: string = 'loading...';
  datePickerConfig: { containerClass: string; dateInputFormat: string };

  // for comfirmation message
  public popoverTitle: string = '<i class="fa fa-trash-o"></i> Delete Physician';
  public popoverMessage: string = 'Are you really <b>sure</b> you want to do this?';
  public cancelClicked: boolean = false;
  searchText: string;
  user_role: string;
  user_id: string;
  total_showing_users: any;
  response: any;
  filterData: any;
  avgDateDiff: any;
  usrCount: any;
  // user_access: boolean = false;
  constructor(
    private service: AnalyticsService,
    private routes: Router,
    private http: HttpClient,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private fb: FormBuilder
  ) {
    this.searchForm = this.fb.group({
      status: [''],
      state: [''],
      fname: [''],
      from_date: [''],
      to_date: [''],
      searchText: ['']
    });
    setTheme('bs3');
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate());
    this.datePickerConfig = Object.assign(
      {},
      {
        containerClass: 'theme-blue',
        dateInputFormat: 'MM/DD/YYYY'
      }
    );

    this.user_role = localStorage.getItem('userrole');
    this.user_id = localStorage.getItem('userid');
    // if (this.user_role === 'aps_sales_manager') {
    // } else {
    //   this.routes.navigate(['/page-not-found']);
    // }
  }

  // get data from service
  resetForm() {
    this.spinner.show();
    this.searchForm.reset();
    this.get_data();
  }
  get_data() {
    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id
    };

    this.service.getSalesAnalyticsData(this.sendData).subscribe(
      (data) => {
        this.sales_list = data;

        if (this.sales_list.status === '0') {
          this.loadmorehide = true;
          this.statusMessage = 'No Data Found.';
          this.notFount = true;
          this.spinner.hide();
        } else {
          this.data = this.sales_list.salesDetails;
          console.log(this.data);
          this.total_showing_users = this.data.length;
          this.notFount = false;
          this.avgDateDiff = this.sales_list.avgDateDiff;
          this.usrCount = this.sales_list.usrCount;
          this.spinner.hide();

        }
      },
      (error) => {
        this.statusMessage =
          'Something want wrong. Please Try again after sometime OR Contact with administrator.';
        this.spinner.hide();
      }
    );
  }

  // show 10 data in view (first time load)
  ngOnInit() {
    this.spinner.show();
    this.loadmorehide = true;

    this.limit = 0;
    this.Year = new Date();
    this.currentYear = this.Year.getFullYear();
    for (let i = 2017; i <= this.currentYear; i++) {
      this.allyear.push(i);
    }
    this.get_data();
  }

  search_filter(term: string) {
    let allData: any;
    allData = this.sales_list.salesDetails;
    if (!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter((x) =>
        x.full_name.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim().toLowerCase();
  }

  // search data
  searchData() {
    this.loadmorehide = true;
    if (
      this.searchForm.value.status === '' &&
      this.searchForm.value.from_date === '' &&
      this.searchForm.value.to_date === '' &&
      this.searchForm.value.fname === ''
    ) {
      this.showError('Please enter any value.');
      return;
    }
    this.spinner.show();
    this.sendData = {
      status: this.searchForm.value.status,
      fname: this.searchForm.value.fname,
      from_date: this.convertDate(this.searchForm.value.from_date),
      to_date: this.convertDate(this.searchForm.value.to_date),
      user_role: this.user_role,
      user_id: this.user_id,
    };
    localStorage.setItem('search_from_date', this.convertDate(this.searchForm.value.from_date));
    localStorage.setItem('search_to_date', this.convertDate(this.searchForm.value.to_date));

    this.service.searchtSalesAnalyticsData(this.sendData).subscribe(
      (data) => {
        this.sales_list = data;
        if (this.sales_list.status === '0') {
          this.total_showing_users = 0;
          this.notFount = true;
          this.statusMessage = 'No Data Found.';
          this.spinner.hide();
        } else {
          this.data = this.sales_list.salesDetails;
          this.total_showing_users = this.data.length;
          this.avgDateDiff = this.sales_list.avgDateDiff;
          this.usrCount = this.sales_list.usrCount;
          this.notFount = false;
          this.spinner.hide();
        }
      },
      (error) => {
        this.statusMessage =
          'Something want wrong. Please Try again after sometime OR Contact with administrator';
        this.spinner.hide();
      }
    );
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {
      var date = new Date(str),
        mnth = ('0' + (date.getMonth() + 1)).slice(-2),
        day = ('0' + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join('-');
    }
    return newDate;
  }

  showSuccess(message) {
    this.toastr.success(message, 'Success');
  }

  showError(message) {
    this.toastr.error(message, 'Oops!');
  }
}
