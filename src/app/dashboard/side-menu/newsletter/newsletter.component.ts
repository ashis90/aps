import { Component, OnInit } from '@angular/core';
import { NewsletterService } from 'src/app/dashboard/side-menu/newsletter/newsletter.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {
  FormData: FormGroup;
  popup_message: any;
  details: any;
  role: any = '';
  succMessage: string;
  errMessage: string;
  sendData: any;
  all_phy_sales: any;
  physician: any;
  sales: any;
  sales_represent: boolean;
  physician_list: boolean;
  err: string;
  selected: boolean;
  toggle: boolean = false;
  toggle2: boolean = false;
  data: any;
  emailFormArray: Array<any> = [];

  constructor(private service: NewsletterService, private routes: Router,
    private http: HttpClient, private spinner: NgxSpinnerService,
     private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private toastr: ToastrService) {

      this.physician_list = false;
      this.sales_represent = false;

     this.FormData = formBuilder.group({
      news_content: ['', Validators.required],
      role: ['', Validators.required],
      seleted_sal: [''],
      seleted_phy: [''],
     });
     }

     get_phy_and_sales() {
      this.service.get_phy_and_sales().subscribe( data => {
        this.all_phy_sales = data;
        if (this.all_phy_sales.status === '1') {
          this.physician = this.all_phy_sales.physician;
          this.sales = this.all_phy_sales.sales;
          this.spinner.hide();
          } else {
            this.errMessage = 'Something is worng!.';
          }
     });
     }

     get f() { return this.FormData.controls; }

  ngOnInit() {
    this.spinner.show();
    this.err = '';
    this.get_phy_and_sales();
  }



  show_phy_list(role) {
    this.spinner.show();
    if (role === 1) {
      this.physician_list = true;
      this.sales_represent = false;
      this.spinner.hide();
    } else if (role === 2) {
      this.sales_represent = true;
      this.physician_list = false;
      this.spinner.hide();
    } else {
      this.err = 'Please select role.';
      this.sales_represent = false;
      this.physician_list = false;
      this.spinner.hide();
    }
  }

//   onChange(id: string, isChecked: boolean) {
//     if (isChecked) {
//       this.emailFormArray.push(id);
//     } else {
//       let index = this.emailFormArray.indexOf(id);
//       this.emailFormArray.splice(index, 1);
//     }
// }


  togglePhysican(item) {
    item.checked = !item.checked;
    this.toggle = this.physician.every(item => item.checked);
  }

  toggleAllPhy() {
    this.toggle = !this.toggle;
    this.physician.forEach(item => item.checked = this.toggle);
  }


  toggleSales(item) {
    item.checked = !item.checked;
    this.toggle2 = this.sales.every(item => item.checked);
  }

  toggleAllSales() {
    this.toggle2 = !this.toggle2;
    this.sales.forEach(item => item.checked = this.toggle2);
  }

  send_newsletter() {
    if (this.FormData.invalid) {
      return;
  }
    this.succMessage = '';
    this.errMessage = '';
    this.sendData = {
      'news_content': this.FormData.value.news_content,
      'physician_id': this.physician,
      'sales_id': this.sales,
    };

    
    this.service.send_newsletter(this.sendData).subscribe( data => {
      this.popup_message = data;
      if (this.popup_message.status === '1') {
        this.showSuccess(this.popup_message.message);
        this.spinner.hide();
        } else if (this.popup_message.status === '3') {
          this.showError(this.popup_message.message);
        } else {
          this.showError(this.popup_message.message);
        }
   });

  }

  showSuccess(message) {
    this.toastr.success(message, 'Success');
  }

  showError(message) {
    this.toastr.error(message, 'Oops!');
  }


}
