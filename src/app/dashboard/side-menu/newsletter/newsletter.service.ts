import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsletterService {


  public baseUrl = environment.baseUrl + 'Newsletter/';
  headers: Headers = new Headers;
  options: any;

    constructor(private http: HttpClient) {
      this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
      this.headers.append( 'Access-Control-Allow-Origin', '*');
      this.headers.append('Accept', 'application/json');
      this.options = new RequestOptions({ headers: this.headers });
    }

    get_phy_and_sales() {
      return this.http.post(this.baseUrl + 'get_phy_and_sales', this.options);
      }
    send_newsletter(sendData) {
    return this.http.post(this.baseUrl + 'send_newsletter', JSON.stringify(sendData), this.options);
    }





}
