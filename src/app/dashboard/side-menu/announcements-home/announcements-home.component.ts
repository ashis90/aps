import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { AnnouncementsService } from 'src/app/dashboard/side-menu/announcements/announcements.service';
import { SalesManagerService } from 'src/app/dashboard/sales-manager/sales-manager.service';
import { PhysicianService } from 'src/app/dashboard/physician/physician.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { PopupComponent } from '../../../frontend/popup/popup.component';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder,
  NgForm
} from '@angular/forms';
import { Observable } from "rxjs";
import { map, startWith } from 'rxjs/operators'; //add this

@Component({
  selector: 'app-announcements-home',
  templateUrl: './announcements-home.component.html',
  styleUrls: ['./announcements-home.component.css']
})
export class AnnouncementsHomeComponent implements OnInit {
  @ViewChild('videoPlayer') videoplayer: ElementRef;
  public assetsUrl = environment.assetsUrl;
  username: string;
  userfullname: string;
  userrole: string;
  message: any;
  sendData: any;
  data: any;
  result: any;
  modalRef: BsModalRef;
  userid: string;
  popup_message: any;
  comm_specimen_list: any;
  phy_highest_specimen_list: any;
  cal_month: string;
  total_specimen: any;
  dataSource: any;
  type: string;
  width: string;
  height: string;
  dataUrl: any;
  sales_representative_data: any;
  phySearchForm: FormGroup;
  salesSearchForm: FormGroup;
  physician_data: any;
  phySpecimenCountData: any;
  phyChartData: any;

  constructor(
    private modalService: BsModalService,
    private itemsService: AnnouncementsService,
    private routes: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private SalesService: SalesManagerService,
    private formBuilder: FormBuilder,
    private PhysicianService: PhysicianService,
    private spinner: NgxSpinnerService,
  ) {

    this.username = localStorage.getItem('username');
    this.userrole = localStorage.getItem('userrole');
    this.userid = localStorage.getItem('userid');
  }

  ngOnInit() {
    if(localStorage.getItem('userrole') === 'sales'){
      if(localStorage.getItem('view_popup') !== 'Yes'){
        console.log(localStorage.getItem('view_popup'))
        this.routes.navigate(["/sales-training"]);
        return false;
      }
    }
    this.spinner.show();
    this.sendData = { username: this.username, userrole: this.userrole, userid: this.userid };
    this.itemsService.fetch_annoucement(this.sendData).subscribe((data) => {
      this.message = data;
      this.data = this.message.role;
      this.result = this.message.result;
      this.userfullname = this.message.fullname;
      this.spinner.hide();
      if (this.message.popup_data.popup_text !== '') {
        var roleArray: Array<string> = [
          'physician',
          'sales',
          'sales_regional_manager',
          'team_sales_manager',
          'team_regional_Sales',
          'clinic',
          'partner'
        ];
        if (
          //this.data === 'physician' ||
          this.data === 'sales'
          //this.data === 'sales_regional_manager' ||
          //this.data === 'team_sales_manager' ||
          //this.data === 'team_regional_Sales' ||
          //this.data === 'clinic' ||
          //this.data === 'partner'
        ) {
          this.modalRef = this.modalService.show(PopupComponent, {
            initialState: {
              title: 'popup',
              text_message: this.message.popup_data.popup_text
            }
          });
        }
      }

    });
    let sendData = {
      user_role: this.userrole,
      user_id: this.userid
    };
    let salesData;
    let sales_reps_data;

    // this.SalesService.getMonthlySalesSummary(sendData).subscribe((data) => {
    //   salesData = data;
    //   if (salesData.status === '1') {
    //     this.comm_specimen_list = salesData.sales_reps_data;
    //     this.phy_highest_specimen_list = salesData.physician_specimen_count_data;
    //     this.cal_month = salesData.current_month;
    //     this.total_specimen = salesData.prev_month_total_specimen_count_data.total_specimen;
    //     this.dataUrl = salesData.past_seven_days_specimen_data;
    //     this.sales_representative_data = salesData.sales_representative_data;
    //     this.physician_data = salesData.physician_data;
    //   }
    //   this.spinner.hide();
    // });

  }


  modal_hide() {
    this.modalRef.hide();
  }

  updatechk() {
    this.userid = localStorage.getItem('userid');
    this.itemsService.updatechk(this.userid).subscribe((data) => {
      this.popup_message = data;
      if (this.popup_message.status === '1') {
        window.location.reload();
      }
    });
  }
  closeVideo() {
    this.videoplayer.nativeElement.pause();
  }

  getRandomSpan() {
    return Math.floor((Math.random() * 6) + 1);
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }
}
