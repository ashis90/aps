import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnouncementsHomeComponent } from './announcements-home.component';

describe('AnnouncementsHomeComponent', () => {
  let component: AnnouncementsHomeComponent;
  let fixture: ComponentFixture<AnnouncementsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnouncementsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnouncementsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
