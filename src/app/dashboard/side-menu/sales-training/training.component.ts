import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AnnouncementsService } from 'src/app/dashboard/side-menu/announcements/announcements.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {
  @ViewChild('videoPlayer') videoplayer: ElementRef
  @ViewChild('videoPlayer1') videoplayer1: ElementRef
  @ViewChild('chkDisabled') chkDisabled: ElementRef
  userid: any;
  popup_message: any;
  postArray:any;
  view_popup_status:boolean=false;
  popupChecked:boolean=false;
  constructor(private itemsService: AnnouncementsService, private routes: Router,
    private http: HttpClient, private activatedRoute: ActivatedRoute,private spinner: NgxSpinnerService,private toastr: ToastrService,) { }

  ngOnInit() {

    this.getTrainingStatus();
    this.chkDisabled.nativeElement.disabled = true;
  }

  // ngAfterViewInit(): void {
  //   document.onclick = (args: any) : void => {
  //     console.log(args.target.tagName)
  //         if(args.target.tagName !== 'SPAN' || args.target.tagName !== 'IMG' || args.target.tagName !== 'A') {
  //             this.closeVideo();
  //             this.closeVideo1();
  //         }
  //     }
  // }

  getTrainingStatus(){
    let param = {'user_id':localStorage.getItem('userid')};
    this.itemsService.getTrainingStatus(param).subscribe( data => {
      console.log(data)
      this.postArray = data;
      if (this.postArray.status === '1') {
        if(this.postArray.view_popup == 'Yes'){
          localStorage.removeItem('view_popup');
          localStorage.removeItem('training_timestamp');
          localStorage.setItem("view_popup", this.postArray.view_popup);
          localStorage.setItem("training_timestamp", this.postArray.training_timestamp);
          this.view_popup_status = true;
          if(this.popupChecked){
            this.routes.navigate(["/dashboard"]);
          }
          
        }else{
          this.toastr.warning("Please watch the HIPPA training session and check the checkbox, to get full access of your account.", "Notice", {
            timeOut: 4000
          });
          this.view_popup_status = false;
          let popElem: HTMLElement = document.getElementById('btnu') as HTMLElement;
            popElem.click();
            return false;

        }
      }

    });
  }

  toggleVideo() {
    let natHTML = this.chkDisabled.nativeElement;
    this.videoplayer.nativeElement.play();

     if(!this.view_popup_status){
      this.videoplayer.nativeElement.onended = function(){
        natHTML.disabled = false;
      } 
     
      
    }
  }

  toggleVideo1() {
  this.videoplayer1.nativeElement.play();
  }

  closeVideo() {
    this.videoplayer.nativeElement.pause();
  }


  closeVideo1() {
    this.videoplayer1.nativeElement.pause();
  }

  updatechk() {
    this.spinner.show();
    this.userid = localStorage.getItem('userid');
    this.itemsService.updatechk(this.userid).subscribe( data => {
      this.popup_message = data;
      this.spinner.hide();
      if (this.popup_message.status === '1') {
        //window.location.reload();
        let elem: HTMLElement = document.getElementById('myVideoClose') as HTMLElement;
        elem.click();
        this.getTrainingStatus();
        this.popupChecked = true;

        // this.getTrainingStatus();
        // 
        
      }

    });
  }


}
