import { Component, OnInit } from '@angular/core';
import { AnnouncementsService } from 'src/app/dashboard/side-menu/announcements/announcements.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-popup-message',
  templateUrl: './popup-message.component.html',
  styleUrls: ['./popup-message.component.css']
})
export class PopupMessageComponent implements OnInit {

  FormData: FormGroup;
  popup_message: any;
  details: any;
  role: any;
  succMessage: string;
  errMessage: string;
  sendData: any;


  constructor(private service: AnnouncementsService, private routes: Router,
    private http: HttpClient, private spinner: NgxSpinnerService, private activatedRoute: ActivatedRoute, private toastr: ToastrService) {

     this.FormData = new FormGroup({
      descriptions: new FormControl(),
      physician: new FormControl(),
      sales: new FormControl(),
      sales_regional_manager: new FormControl(),
      team_sales_manager: new FormControl(),
      team_regional_Sales: new FormControl(),
      clinic: new FormControl(),
      partner: new FormControl(),
     });
     }

     get_data() {
      this.service.popup_message().subscribe(data => {
          this.popup_message = data;
          if (this.popup_message.status === '0') {
              this.spinner.hide();
          } else {
            this.details = this.popup_message.details;
            this.role = this.popup_message.all_role;

    this.FormData.controls['descriptions'].setValue(this.details.popup_text);

    this.FormData.controls['physician'].setValue(this.role.physician);
    this.FormData.controls['sales'].setValue(this.role.sales);
    this.FormData.controls['sales_regional_manager'].setValue(this.role.sales_regional_manager);
    this.FormData.controls['team_sales_manager'].setValue(this.role.team_sales_manager);
    this.FormData.controls['team_regional_Sales'].setValue(this.role.team_regional_Sales);
    this.FormData.controls['clinic'].setValue(this.role.clinic);
    this.FormData.controls['partner'].setValue(this.role.partner);

            this.spinner.hide();
          }
      });
    }



    ngOnInit() {
      this.spinner.show();
      this.get_data();
    }



 update_popup() {
    this.succMessage = '';
    this.errMessage = '';
    this.sendData = {
      'descriptions': this.FormData.value.descriptions,

      'physician': this.FormData.value.physician,
      'sales': this.FormData.value.sales,
      'sales_regional_manager': this.FormData.value.sales_regional_manager,
      'team_sales_manager': this.FormData.value.team_sales_manager,
      'team_regional_Sales': this.FormData.value.team_regional_Sales,
      'clinic': this.FormData.value.clinic,
      'partner': this.FormData.value.partner,
    };

    this.service.update_popup_message(this.sendData).subscribe( data => {
      this.popup_message = data;
      if (this.popup_message.status === '1') {
        this.showSuccess(this.popup_message.message);
        this.get_data();
        } else {
          this.showError(this.popup_message.message);
        }
   });

  }

  showSuccess(message) {
    this.toastr.success(message, 'Success');
  }

  showError(message) {
    this.toastr.error(message, 'Oops!');
  }
}
