import { Component, OnInit } from '@angular/core';
import { AnnouncementsService } from 'src/app/dashboard/side-menu/announcements/announcements.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { setTheme } from 'ngx-bootstrap/utils';

@Component({
  selector: 'app-announcement-edit',
  templateUrl: './announcement-edit.component.html',
  styleUrls: ['./announcement-edit.component.css']
})
export class AnnouncementEditComponent implements OnInit {


  announcement: any = [];
  details: any;
  id: any;
  sendData: any;
  announcement_name: any;
  descriptions: any;
  is_active: any;
  announcements_for: any;
  FormData: FormGroup;
  succMessage: string;
  errMessage: string;
  config:any;
  constructor(private service: AnnouncementsService, private routes: Router,
     private http: HttpClient, private spinner: NgxSpinnerService, private activatedRoute: ActivatedRoute) {

      this.FormData = new FormGroup({
        announcement_name: new FormControl(''),
        descriptions: new FormControl(''),
        is_active: new FormControl(''),
        announcements_for: new FormControl(''),
      });
      this.id = this.activatedRoute.snapshot.paramMap.get('id');
      //this.config.allowedContent = true;
      }



  get_data() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.sendData = { 'id': this.id, };
    this.service.announcement_edit(this.sendData).subscribe(data => {
        this.announcement = data;
        if (this.announcement.status === '0') {
            this.spinner.hide();
        } else {
          this.details = this.announcement.announcements[0];
          if (this.details.announcement_name) {
            this.announcement_name = this.details.announcement_name;
          } else { this.announcement_name = ''; }

        // this.FormData.controls['descriptions'].setValue(this.details.descriptions);
          if (this.details.descriptions) {
            this.descriptions = this.details.descriptions;
          } else { this.descriptions = ''; }

          if (this.details.is_active) {
            this.is_active = this.details.is_active;
          } else { this.is_active = ''; }

          if (this.details.announcements_for) {
            this.announcements_for = this.details.announcements_for;
          } else { this.announcements_for = ''; }

          this.spinner.hide();
        }
    });
  }


  ngOnInit() {
    this.spinner.show();
    this.get_data();
  }

  update_announcement() {
    this.succMessage = '';
    this.errMessage = '';
    this.sendData = {
      'id': this.id,
      'announcement_name': this.FormData.value.announcement_name,
      'descriptions': this.FormData.value.descriptions,
      'announcements_for': this.FormData.value.announcements_for,
      'is_active': this.FormData.value.is_active,
    };
    this.service.update_announcement(this.sendData).subscribe( data => {
      this.announcement = data;
      if (this.announcement.status === '1') {
        this.succMessage = this.announcement.message;
        this.get_data();
        } else {
          this.errMessage = this.announcement.message;
        }
   });

  }


}
