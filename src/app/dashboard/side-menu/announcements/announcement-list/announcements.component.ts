import { Component, OnInit } from '@angular/core';
import { AnnouncementsService } from 'src/app/dashboard/side-menu/announcements/announcements.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { setTheme } from 'ngx-bootstrap/utils';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.css']
})
export class AnnouncementsComponent implements OnInit {

  announcement: any = [];
  data: any;
  notFount: boolean;
  succMessage: string;
  errMessage: string;
  sendData: any;

  // for comfirmation message
  public popoverTitle: string = '<i class="fa fa-trash-o"></i> Delete Announcements';
  public popoverMessage: string = 'Are you really <b>sure</b> you want to do this?';
  public cancelClicked: boolean = false;

  constructor(private service: AnnouncementsService, private routes: Router,
     private http: HttpClient, private spinner: NgxSpinnerService, private toastr: ToastrService) { }


  get_data() {
    this.service.announcement().subscribe(data => {
        this.announcement = data;
        if (this.announcement.status === '0') {
          this.notFount = true;
            this.spinner.hide();
        } else {
          this.data = this.announcement.announcements;
          this.notFount = false;
          this.spinner.hide();
        }
    });
}





  ngOnInit() {
    this.spinner.show();
    this.get_data();
  }

  delete_announcement(id) {
    if (id) {
    this.succMessage = '';
    this.errMessage = '';
    this.sendData = {
      'id': id,
    };

    this.service.delete_announcement(this.sendData).subscribe( data => {
      this.announcement = data;
      if (this.announcement.status === '1') {
        this.showSuccess(this.announcement.message);
        this.ngOnInit();
        } else {
          this.showError(this.announcement.message);
        }
   });
  }

  }

  trackByFn(index, item) {
    return item ? item.team_manager_name : undefined; // or item.id
  }

  showSuccess(message) {
    this.toastr.success(message, 'Success');
  }

  showError(message) {
    this.toastr.error(message, 'Oops!');
  }

}
