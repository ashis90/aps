import { Component, OnInit } from '@angular/core';
import { AnnouncementsService } from 'src/app/dashboard/side-menu/announcements/announcements.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-announcement-add',
  templateUrl: './announcement-add.component.html',
  styleUrls: ['./announcement-add.component.css']
})
export class AnnouncementAddComponent implements OnInit {

  id: any;
  sendData: any;
  announcement_name: any;
  descriptions: any;
  is_active: any = 'Active';
  announcements_for: any = '1';
  FormData: FormGroup;
  succMessage: string;
  errMessage: string;
  announcement: any;
  constructor(private service: AnnouncementsService, private routes: Router,
    private http: HttpClient, private spinner: NgxSpinnerService, private activatedRoute: ActivatedRoute, private toastr: ToastrService) {
    this.FormData = new FormGroup({
      announcement_name: new FormControl('', [Validators.required]),
      descriptions: new FormControl(''),
      is_active: new FormControl(''),
      announcements_for: new FormControl(''),
    });
    this.id = localStorage.getItem('userid');
  }

  ngOnInit() {
  }


  add_announcement() {
    this.succMessage = '';
    this.errMessage = '';
    this.sendData = {
      'user_id': this.id,
      'announcement_name': this.FormData.value.announcement_name,
      'descriptions': this.FormData.value.descriptions,
      'announcements_for': this.FormData.value.announcements_for,
      'is_active': this.FormData.value.is_active,
    };
    if (this.FormData.status === 'VALID') {
      this.service.add_announcement(this.sendData).subscribe( data => {
        this.announcement = data;
        if (this.announcement.status === '1') {
          this.showSuccess(this.announcement.message);
          this.routes.navigate(['/announcements']);
          } else {
            this.showError(this.announcement.message);
          }
     });
    } else {
      this.validateAllFormFields(this.FormData);
    }


  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.FormData.get(field).valid && (this.FormData.get(field).touched || this.FormData.get(field).dirty);
  }

  validateAllFormFields(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }
  });
  }

  showSuccess(message) {
    this.toastr.success(message, 'Success');
  }

  showError(message) {
    this.toastr.error(message, 'Oops!');
  }


}
