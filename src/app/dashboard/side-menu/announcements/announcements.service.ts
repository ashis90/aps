import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AnnouncementsService {

  public baseUrl = environment.baseUrl + 'announcements/';
  headers: Headers = new Headers;
  options: any;

              constructor(private http: HttpClient) {
                this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
                this.headers.append( 'Access-Control-Allow-Origin', '*');
                this.headers.append('Accept', 'application/json');
                this.options = new RequestOptions({ headers: this.headers });
              }

  announcement() {
    return this.http.post(this.baseUrl + 'all_announcement', this.options);
  }
  announcement_edit(sendData) {
    return this.http.post(this.baseUrl + 'announcement_edit', JSON.stringify(sendData), this.options);
  }
  update_announcement(sendData) {
    return this.http.post(this.baseUrl + 'update_announcement', JSON.stringify(sendData), this.options);
  }

  add_announcement(sendData) {
    return this.http.post(this.baseUrl + 'add_announcement', JSON.stringify(sendData), this.options);
  }

  delete_announcement(sendData) {
    return this.http.post(this.baseUrl + 'delete_announcement', JSON.stringify(sendData), this.options);
  }

  popup_message() {
    return this.http.post(this.baseUrl + 'popup_message', this.options);
  }

  update_popup_message(sendData) {
    return this.http.post(this.baseUrl + 'update_popup_message', JSON.stringify(sendData), this.options);
  }
  updatechk(data) {
    return this.http.post(this.baseUrl + 'update_chk', JSON.stringify(data), this.options);
  }

  getTrainingStatus(data) {
    return this.http.post(this.baseUrl + 'get_training_status', JSON.stringify(data), this.options);
  }

  fetch_annoucement(data) {
    return this.http.post(this.baseUrl + 'get_announcement', JSON.stringify(data), this.options);
  }

}
