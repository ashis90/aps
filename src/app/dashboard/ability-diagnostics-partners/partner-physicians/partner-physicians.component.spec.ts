import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerPhysiciansComponent } from './partner-physicians.component';

describe('PartnerPhysiciansComponent', () => {
  let component: PartnerPhysiciansComponent;
  let fixture: ComponentFixture<PartnerPhysiciansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerPhysiciansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerPhysiciansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
