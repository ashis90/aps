import { TestBed } from '@angular/core/testing';

import { PartnerPhysiciansService } from './partner-physicians.service';

describe('PartnerPhysiciansService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PartnerPhysiciansService = TestBed.get(PartnerPhysiciansService);
    expect(service).toBeTruthy();
  });
});
