import { Component, OnInit } from "@angular/core";
import { PhysicianService } from "src/app/dashboard/physician/physician.service";
import { Router, ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  FormBuilder
} from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { setTheme } from "ngx-bootstrap/utils";
import { ToastrService } from "ngx-toastr";
import { map, startWith } from "rxjs/operators";

@Component({
  selector: "app-partner-physicians",
  templateUrl: "./partner-physicians.component.html",
  styleUrls: ["./partner-physicians.component.css"]
})
export class PartnerPhysiciansComponent implements OnInit {
  statusSelect = "";
  monthSelect = "";
  yearSelect = "";
  physician_list: any = [];
  public data: any;
  notFount: boolean;
  Year: Date;
  currentYear: any;
  allyear: any = [];
  searchForm: FormGroup;
  sendData: any;
  succMessage: any;
  total_physician: number;
  bsInlineValue = new Date();
  todayDate = "";
  maxDate: Date;
  statusMessage: string = "loading...";
  datePickerConfig: { containerClass: string; dateInputFormat: string };

  // for comfirmation message
  partner_id: string;
  searchText: string;
  user_role: string;
  user_id: string;
  total_showing_users: any;
  response: any;
  filterData: any;
  // user_access: boolean = false;
  constructor(
    private service: PhysicianService,
    private routes: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private fb: FormBuilder
  ) {
    this.partner_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.searchForm = this.fb.group({
      status: [""],
      state: [""],
      fname: [""],
      from_date: [""],
      to_date: [""],
      searchText: [""]
    });
    setTheme("bs3");
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate());
    this.datePickerConfig = Object.assign(
      {},
      {
        containerClass: "theme-blue",
        dateInputFormat: "MM/DD/YYYY"
      }
    );

    this.user_role = localStorage.getItem("userrole");
    this.user_id = localStorage.getItem("userid");
    if (this.user_role === "aps_sales_manager") {
    } else {
      this.routes.navigate(["/page-not-found"]);
    }
  }
  ngOnInit() {
    this.spinner.show();
    this.Year = new Date();
    this.currentYear = this.Year.getFullYear();
    for (let i = 2017; i <= this.currentYear; i++) {
      this.allyear.push(i);
    }
    this.get_data();
  }

  get_data() {
    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id,
      partner_id: this.partner_id
    };
    this.service.partnerPhysicianList(this.sendData).subscribe(
      data => {
        this.physician_list = data;
        if (this.physician_list.status === "0") {
          this.statusMessage = "No Data Found.";
          this.notFount = true;
          this.spinner.hide();
        } else {
          this.data = this.physician_list.physicians_details;
          this.total_showing_users = this.data.length;
          this.notFount = false;
          this.spinner.hide();
        }
      },
      error => {
        this.statusMessage =
          "Something want wrong. Please Try again after sometime OR Contact with administrator.";
        this.spinner.hide();
      }
    );
  }

  // search data
  searchData() {
    if (
      this.searchForm.value.status === "" &&
      this.searchForm.value.from_date === "" &&
      this.searchForm.value.to_date === "" &&
      this.searchForm.value.fname === "" &&
      this.searchForm.value.state === ""
    ) {
      this.showError("Please enter any value.");
      return;
    }
    this.spinner.show();
    this.sendData = {
      status: this.searchForm.value.status,
      fname: this.searchForm.value.fname,
      state: this.searchForm.value.state,
      from_date: this.convertDate(this.searchForm.value.from_date),
      to_date: this.convertDate(this.searchForm.value.to_date),
      user_role: this.user_role,
      user_id: this.user_id,
      partner_id: this.partner_id
    };
    localStorage.setItem(
      "search_from_date",
      this.convertDate(this.searchForm.value.from_date)
    );
    localStorage.setItem(
      "search_to_date",
      this.convertDate(this.searchForm.value.to_date)
    );

    this.service.search_partner_physician_list(this.sendData).subscribe(
      data => {
        this.physician_list = data;
        if (this.physician_list.status === "0") {
          this.total_showing_users = 0;
          this.notFount = true;
          this.statusMessage = "No Data Found.";
          this.spinner.hide();
        } else {
          this.data = this.physician_list.physicians_details;
          this.total_showing_users = this.data.length;
          this.notFount = false;
          this.spinner.hide();
        }
      },
      error => {
        this.statusMessage =
          "Something want wrong. Please Try again after sometime OR Contact with administrator";
        this.spinner.hide();
      }
    );
  }
  //Quick Search
  search_filter(term: string) {
    let allData: any;
    allData = this.physician_list.physicians_details;
    if (!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x =>
        x.full_name
          .trim()
          .toLowerCase()
          .includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }
  }

  // change user status
  active_inactive(status, user_id) {
    if (confirm("Are you sure to change status ??")) {
      this.spinner.show();
      this.succMessage = "";
      this.sendData = {
        status: status,
        user_id: user_id
      };

      this.service.change_user_status(this.sendData).subscribe(data => {
        this.physician_list = data;
        if (this.physician_list.status === "0") {
          this.ngOnInit();
          this.showError(this.physician_list.message);
          this.notFount = true;
        } else {
          this.data = this.physician_list.physicians_details;
          this.ngOnInit();
          this.showSuccess(this.physician_list.message);
          this.notFount = false;
          this.spinner.hide();
        }
      });
    }
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = "";
    } else {
      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
