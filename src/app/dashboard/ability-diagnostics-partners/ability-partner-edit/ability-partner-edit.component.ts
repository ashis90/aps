import { Component, OnInit } from "@angular/core";
import { AbilityDiagnosticsPartnersService } from "src/app/dashboard/ability-diagnostics-partners/ability-diagnostics-partners.service";
import { Router, ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder
} from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { getLocale } from "ngx-bootstrap/chronos/public_api";
import { ToastrService } from "ngx-toastr";
@Component({
  selector: "app-ability-partner-edit",
  templateUrl: "./ability-partner-edit.component.html",
  styleUrls: ["./ability-partner-edit.component.css"]
})
export class AbilityPartnerEditComponent implements OnInit {
  id: any;
  sendData: any;
  partner_details: any = [];
  notFount: boolean;
  userDetails: any;
  regionalList: any = [];
  edit_partner: FormGroup;
  fileToUpload: File = null;
  file: any;
  // store data in this variable
  user_email: any;
  succMessage: any;
  errMessage: any;
  company_logo_file: any;
  image_url: any = "";
  user_status: any;
  userRole: any;
  company_logo: any;
  constructor(
    private service: AbilityDiagnosticsPartnersService,
    private routes: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private formbuilder: FormBuilder,
    private toastr: ToastrService
  ) {
    this.edit_partner = this.formbuilder.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      user_login: [""],
      user_email: [
        "",
        Validators.pattern("[a-zA-Z-0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$")
      ],
      edit_user_pass: ["", Validators.minLength(6)],
      edit_con_pass: ["", [this.equalto("edit_user_pass")]],
      mobile: [""],
      user_url: [""],
      address: [""],
      fax: [""],
      regional_manager: [""],
      user_status: [""]
    });
    this.userRole = localStorage.userrole;
  }

  equalto(repassword): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[repassword] == input;
      if (!isValid) {
        return { equalTo: { isValid } };
      } else {
        return null;
      }
    };
  }

  handleFileInput(event) {
    this.file = <File>event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = event => {
        // called once readAsDataURL is completed
        this.image_url = (<FileReader>event.target).result;
      };
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.get_data();
  }

  get_data() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    this.sendData = { id: this.id };

    this.service
      .abilityDiagnosticsPartnersDetails(this.sendData)
      .subscribe(data => {
        this.partner_details = data;
        if (this.partner_details.status === "1") {
          this.userDetails = this.partner_details.details;
          this.regionalList = this.partner_details.assign_regional_list;

          this.edit_partner.patchValue({
            first_name: this.userDetails.fname,
            last_name: this.userDetails.lname,
            user_login: this.userDetails.user_login,
            user_email: this.userDetails.email,
            mobile: this.userDetails.mob,
            fax: this.userDetails.fax,
            user_url: this.userDetails.url,
            address: this.userDetails.add
          });

          this.company_logo = this.userDetails.company_logo;

          if (this.userDetails.user_status) {
            this.user_status = this.userDetails.user_status;
          } else {
            this.user_status = "";
          }

          this.notFount = false;
          this.spinner.hide();
        } else {
          this.notFount = true;
          this.spinner.hide();
        }
      });
  }

  update_data() {
    this.succMessage = "";
    this.errMessage = "";
    const formData = new FormData();
    var id = this.id;

    formData.append("user_id", id);
    formData.append("first_name", this.edit_partner.value.first_name);
    formData.append("last_name", this.edit_partner.value.last_name);

    formData.append("user_login", this.edit_partner.value.user_login);
    formData.append("user_email", this.edit_partner.value.user_email);

    formData.append("edit_user_pass", this.edit_partner.value.edit_user_pass);
    formData.append("edit_con_pass", this.edit_partner.value.edit_con_pass);

    formData.append("mobile", this.edit_partner.value.mobile);
    formData.append("fax", this.edit_partner.value.fax);

    formData.append("user_url", this.edit_partner.value.user_url);
    formData.append("address", this.edit_partner.value.address);

    formData.append("user_status", this.edit_partner.value.user_status);

    if (this.file) {
      var fileSplit = this.file["name"].split(".");
      var fileExt = "";
      if (fileSplit.length > 1) {
        fileExt = fileSplit[fileSplit.length - 1];
      }
      if (fileExt === "jpeg" || fileExt === "jpg" || fileExt === "png") {
        formData.append("name", this.file["name"]);
        formData.append("file_data", this.file);
      } else {
        this.toastr.info(
          "Sorry, File type is not allowed. Only png, jpg and jpeg!",
          "Error",
          {
            timeOut: 3000
          }
        );
      }
    }

    if (this.edit_partner.status === "VALID") {
      this.spinner.show();
      if (this.edit_partner.value.user_email !== "") {
        this.service
          .abilityDiagnosticsPartnersEdit(formData)
          .subscribe(data => {
            this.partner_details = data;
            if (this.partner_details.status === "1") {
              this.showSuccess(this.partner_details.message);
              this.ngOnInit();
              this.notFount = true;
            } else {
              this.showError(this.partner_details.message);
              this.ngOnInit();
              this.notFount = false;
            }
          });
      } else {
        this.toastr.info("Please enter a Email.", "Error", {
          timeOut: 3000
        });
      }
    } else {
      this.toastr.info("Please fill all required fields.", "Error", {
        timeOut: 3000
      });
    }
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
