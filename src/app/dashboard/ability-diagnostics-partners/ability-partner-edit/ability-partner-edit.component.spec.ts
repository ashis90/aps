import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbilityPartnerEditComponent } from './ability-partner-edit.component';

describe('AbilityPartnerEditComponent', () => {
  let component: AbilityPartnerEditComponent;
  let fixture: ComponentFixture<AbilityPartnerEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbilityPartnerEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbilityPartnerEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
