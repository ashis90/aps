import { TestBed } from '@angular/core/testing';

import { AbilityDiagnosticsPartnersService } from './ability-diagnostics-partners.service';

describe('AbilityDiagnosticsPartnersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AbilityDiagnosticsPartnersService = TestBed.get(AbilityDiagnosticsPartnersService);
    expect(service).toBeTruthy();
  });
});
