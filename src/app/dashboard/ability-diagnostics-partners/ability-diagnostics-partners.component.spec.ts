import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbilityDiagnosticsPartnersComponent } from './ability-diagnostics-partners.component';

describe('AbilityDiagnosticsPartnersComponent', () => {
  let component: AbilityDiagnosticsPartnersComponent;
  let fixture: ComponentFixture<AbilityDiagnosticsPartnersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbilityDiagnosticsPartnersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbilityDiagnosticsPartnersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
