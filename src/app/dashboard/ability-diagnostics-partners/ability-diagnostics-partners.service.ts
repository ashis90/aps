import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { Headers, RequestOptions } from "@angular/http";
import { HttpClient } from "@angular/common/http";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AbilityDiagnosticsPartnersService {
  public baseUrl = environment.baseUrl + "AbilityPartners/";
  headers: Headers = new Headers();
  options: any;

  constructor(private http: HttpClient) {
    this.headers.append(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    this.headers.append("Access-Control-Allow-Origin", "*");
    this.headers.append("Accept", "application/json");
    this.options = new RequestOptions({ headers: this.headers });
  }

  abilityDiagnosticsPartnersList(sendData): Observable<any> {
    return this.http.post(
      this.baseUrl + "abilityDiagnosticsPartnersList",
      JSON.stringify(sendData),
      this.options
    );
  }

  abilityDiagnosticsPartnersDetails(sendData): Observable<any> {
    return this.http.post(
      this.baseUrl + "partnerDetails",
      JSON.stringify(sendData),
      this.options
    );
  }

  abilityDiagnosticsPartnersEdit(sendData): Observable<any> {
    return this.http.post(
      this.baseUrl + "updatePartnerDetails",
      sendData,
      this.options
    );
  }
}
