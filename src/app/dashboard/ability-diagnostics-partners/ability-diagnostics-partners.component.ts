import { Component, OnInit } from "@angular/core";
import { AbilityDiagnosticsPartnersService } from "src/app/dashboard/ability-diagnostics-partners/ability-diagnostics-partners.service";
import { Router, ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { getLocale } from "ngx-bootstrap/chronos/public_api";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { setTheme } from "ngx-bootstrap/utils";

@Component({
  selector: "app-ability-diagnostics-partners",
  templateUrl: "./ability-diagnostics-partners.component.html",
  styleUrls: ["./ability-diagnostics-partners.component.css"]
})
export class AbilityDiagnosticsPartnersComponent implements OnInit {
  statusSelect = "";
  partners_list: any = [];
  partner_id: any;
  public data: any;
  notFount: boolean;
  sendData: any;
  succMessage: any;
  user_role: any;
  user_id: any;
  response: any;
  total_showing_users: any;
  last_data_entry_date: any;
  filterData: any;

  constructor(
    private service: AbilityDiagnosticsPartnersService,
    private routes: Router,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    this.user_role = localStorage.getItem("userrole");
    this.user_id = localStorage.getItem("userid");
  }

  ngOnInit() {
    this.spinner.show();
    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id
    };
    this.service
      .abilityDiagnosticsPartnersList(this.sendData)
      .subscribe(data => {
        this.partners_list = data;
        if (this.partners_list.status === "0") {
          this.notFount = true;
          this.spinner.hide();
        } else {
          this.data = this.partners_list.partners_data;
          this.total_showing_users = this.partners_list.total_partners;
          this.notFount = false;
          this.spinner.hide();
        }
      });
  }

  search_filter(term: string) {
    let allData: any;
    allData = this.partners_list.partners_data;
    if (!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x =>
        x.partner_name
          .trim()
          .toLowerCase()
          .includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }
  }
}
