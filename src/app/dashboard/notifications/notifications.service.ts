import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  public baseUrl = environment.baseUrl + 'Notifications/';
  headers: Headers = new Headers;
  options: any;
  constructor(private http: HttpClient) {
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append( 'Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  // for Notification
  get_notifications(sendData) {
    return this.http.post(this.baseUrl + 'get_notifications', JSON.stringify(sendData), this.options);
    }
    get_unread_notifications(sendData) {
      return this.http.post(this.baseUrl + 'get_unread_notifications', JSON.stringify(sendData), this.options);
      }

  read_unread(sendData) {
      return this.http.post(this.baseUrl + 'read_unread', JSON.stringify(sendData), this.options);
      }

  change_status(sendData) {
        return this.http.post(this.baseUrl + 'change_status', JSON.stringify(sendData), this.options);
        }

  sales_notification(userdata) {
          return this.http.post(this.baseUrl + 'sales_notification', JSON.stringify(userdata), this.options);
          }

}
