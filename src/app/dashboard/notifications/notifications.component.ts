import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
// import { LoginService } from 'src/app/frontend/login/login.service';
import { NotificationsService } from 'src/app/dashboard/notifications/notifications.service';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl, AbstractControl, ValidatorFn, FormBuilder, NgForm } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  user_role: string;
  user_id: string;
  sendData: any;
  reponse: any;
  incom_notification: any;
  com_notification: any;
  total_incom_notification: string;
  total_com_notification: string;


  constructor(private service: NotificationsService, private routes: Router,
    private http: HttpClient, private activatedRoute: ActivatedRoute,
     private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private toastr: ToastrService) {
      this.user_role = localStorage.getItem('userrole');
      this.user_id = localStorage.getItem('userid');
     }

     get_data() {
      this.sendData = {
        'user_role': this.user_role,
        'user_id': this.user_id,
    };
           this.service.get_notifications(this.sendData).subscribe(data => {
               this.reponse = data;
               if (this.reponse.status === '0') {
                this.total_incom_notification = this.reponse.count_incom;
                this.total_com_notification = this.reponse.count_com;
                 this.spinner.hide();
               } else {
                 this.incom_notification = this.reponse.incom_notification;
                 this.com_notification = this.reponse.com_notification;
                 this.total_incom_notification = this.reponse.count_incom;
                 this.total_com_notification = this.reponse.count_com;
                 this.spinner.hide();
               }
           });
      }



  ngOnInit() {
    if(localStorage.getItem('userrole') === 'sales'){
      if(localStorage.getItem('view_popup') !== 'Yes'){
        console.log(localStorage.getItem('view_popup'))
        this.routes.navigate(["/sales-training"]);
        return false;
      }
    }
    this.spinner.show();
    this.get_data();
  }

  read_unread(id: string) {
    this.spinner.show();
    this.sendData = { 'note_id': id, 'user_id': this.user_id };
         this.service.read_unread(this.sendData).subscribe(data => {
             this.reponse = data;
             if (this.reponse.status === '0') {
               this.spinner.hide();
             } else {
               this.ngOnInit();
               this.spinner.hide();
             }
         });
  }


  change_status(id: string) {
    this.spinner.show();
    this.sendData = { 'note_id': id, 'user_id': this.user_id };
    this.service.change_status(this.sendData).subscribe(data => {
        this.reponse = data;
        if (this.reponse.status === '0') {
          this.spinner.hide();
          this. showError('Something is worn. Please try again!');
        } else {
          this.ngOnInit();
          this.spinner.hide();
          this. showSuccess('You are completed a notification.');
        }
    });
  }

  trackByFn(index, item) {
    return item ? item.team_manager_name : undefined; // or item.id
  }


  showSuccess(message) {
    this.toastr.success(message, 'Success');
  }

  showError(message) {
    this.toastr.error(message, 'Oops!');
  }


}
