import { TestBed } from '@angular/core/testing';

import { SubmittedReportsService } from './submitted-reports.service';

describe('SubmittedReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubmittedReportsService = TestBed.get(SubmittedReportsService);
    expect(service).toBeTruthy();
  });
});
