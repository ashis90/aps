import { Component, OnInit } from "@angular/core";
import { MyaccountService } from "src/app/dashboard/myaccount/myaccount.service";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  NgForm,
  Validators,
  AbstractControl,
  ValidatorFn
} from "@angular/forms";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-myaccount",
  templateUrl: "./myaccount.component.html",
  styleUrls: ["./myaccount.component.css"]
})
export class MyaccountComponent implements OnInit {
  userid: any;
  response: any = [];
  sendData;
  userdata: any = [];
  message: string;
  FormData: FormGroup;
  user_role: string;

  constructor(
    private service: MyaccountService,
    private routes: Router,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {
    this.FormData = this.formBuilder.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      user_login: [""],
      user_email: [""],
      user_pass: [
        "",
        [
          Validators.minLength(8),
          Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}/)
        ]
      ],
      // user_pass: ['', Validators.minLength(6)],
      con_pass: ["", this.equalto("user_pass")],
      mobile: [""],
      user_url: [""],
      address: [""],

      brn: [""],
      ban: [""],
      nbaccount: [""],
      clinic_addrs: [""],
      fax: [""],
      cell_phone: [""],
      manager_contact_name: [""],
      manager_cell_number: [""],
      npi: [""],
      wound_positive_send: [""],
      pharmd_notification_type: [""]
    });
    this.user_role = localStorage.getItem("userrole");
  }

  ngOnInit() {
    if (localStorage.getItem("userrole") === "sales") {
      if (localStorage.getItem("view_popup") !== "Yes") {
        console.log(localStorage.getItem("view_popup"));
        this.routes.navigate(["/sales-training"]);
        return false;
      }
    }

    this.userid = localStorage.getItem("userid");
    this.sendData = { userid: this.userid };
    this.service.get_userdata(this.sendData).subscribe(data => {
      this.response = data;
      if (this.response.status === "1") {
        this.userdata = this.response.details;
        this.FormData.patchValue({
          first_name: this.userdata.fname,
          last_name: this.userdata.lname,
          user_login: this.userdata.user_login,
          user_email: this.userdata.email,
          mobile: this.userdata.mob,
          user_url: this.userdata.url,
          address: this.userdata.add,
          brn: this.userdata.bank_routing_num,
          ban: this.userdata.bank_account_num,
          nbaccount: this.userdata.nbaccount,
          clinic_addrs: this.userdata.clinic,
          fax: this.userdata.fax,
          cell_phone: this.userdata.cell,
          manager_contact_name: this.userdata.manager,
          manager_cell_number: this.userdata.manager_cell,
          npi: this.userdata.npi_api,
          wound_positive_send: this.userdata.wound_positive_send,
          pharmd_notification_type: this.userdata.pharmd_notification_type
        });
      } else {
        this.message = "No data found";
      }
    });
  }

  update_user_deatails() {
    this.sendData = {
      user_id: this.userid,
      fname: this.FormData.value.first_name,
      lname: this.FormData.value.last_name,
      user_login: this.FormData.value.user_login,
      user_email: this.FormData.value.user_email,
      user_pass: this.FormData.value.user_pass,
      con_pass: this.FormData.value.con_pass,
      mobile: this.FormData.value.mobile,
      user_url: this.FormData.value.user_url,
      add: this.FormData.value.address,

      brn: this.FormData.value.brn,
      ban: this.FormData.value.ban,
      nbaccount: this.FormData.value.nbaccount,
      clinic: this.FormData.value.clinic_addrs,
      fax: this.FormData.value.fax,
      cell: this.FormData.value.cell_phone,
      manager: this.FormData.value.manager_contact_name,
      manager_cell: this.FormData.value.manager_cell_number,
      npi_api: this.FormData.value.npi,
      wound_positive_send: this.FormData.value.wound_positive_send,
      pharmd_notification_type: this.FormData.value.pharmd_notification_type
    };
    // console.log(this.sendData);return;
    this.service.update_userdata(this.sendData).subscribe(data => {
      this.response = data;
      if (this.response.status === "1") {
        this.showSuccess(this.response.message);
      } else {
        this.showError(this.response.message);
      }
    });
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }

  equalto(repassword): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[repassword] === input;
      if (!isValid) {
        return { equalTo: { isValid } };
      } else {
        return null;
      }
    };
  }
}
