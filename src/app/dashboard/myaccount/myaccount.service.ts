import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MyaccountService {

  public login_url = environment.baseUrl + 'Login/';
  headers: Headers = new Headers;
  options: any;
  constructor(private http: HttpClient) {
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append( 'Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }
  postsArray: any = [] ;

      get_userdata(userid) {
       return this.http.post(this.login_url + 'user_details', JSON.stringify(userid), this.options);
      }

      update_userdata(userdata) {
        return this.http.post(this.login_url + 'update_user_details', JSON.stringify(userdata), this.options);
       }
}
