import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder
} from '@angular/forms';
import { BillerService } from '../../dashboard/biller/biller.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-biller',
  templateUrl: './biller.component.html',
  styleUrls: ['./biller.component.css']
})
export class BillerComponent implements OnInit {

  user_role: string;
  user_id: string;
  FormData: FormGroup;
  sendData: any;
  response: any;
	data: any;
	count:any;
  notFount: boolean;
  id: string;
  select_test_type : any ='';

  constructor(
    private service: BillerService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private formbuilder: FormBuilder,
    private toastr: ToastrService
  ) {
    this.user_role = localStorage.getItem('userrole');
   
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.FormData = this.formbuilder.group({
      p_name: [''],
      accessioning_num: [''],
			test_type: [ '' ],
			physician_name: ['']
    
    });
  }

  report_list(){
      this.sendData = { user_role: this.user_role };
      this.service.get_submitted_reports(this.sendData).subscribe((data) => {
			this.response = data;
			if (this.response.status === '1') {
				this.data = this.response.submitted_rep;
				this.count = this.response.count;
				this.notFount = false;
				this.spinner.hide();
			} else {
				this.notFount = true;
				this.count = this.response.count;
				this.spinner.hide();
			}
    });
    
  }
    
	chanage_color(specimen_id) {
		this.sendData = {
			specimen_id: specimen_id
		};

		this.service.change_submitted_reports_color(this.sendData).subscribe((data) => {
			this.response = data;
			if (this.response.status === '1') {
				this.ngOnInit();
			} else {
				this.ngOnInit();
			}
		});
	}

	chanage_color_for_pcr(accessioning_num) {
		this.sendData = {
			accessioning_num: accessioning_num
		};

		this.service.change_color_for_pcr(this.sendData).subscribe((data) => {
			this.response = data;
			if (this.response.status === '1') {
				this.ngOnInit();
			} else {
				this.ngOnInit();
			}
		});
	}

	color(value) {
		if (value === '1') {
			return 'gray';
		} else if (value === '0') {
			return '#424242 ';
		} else {
			return '#424242 ';
		}
	}


 

  ngOnInit() {
    this.spinner.show();
    this.report_list();
  }

  search_data() {
		this.spinner.show();
		this.sendData = {
			user_role : this.user_role,
			p_name : this.FormData.value.p_name,
			physician_name : this.FormData.value.physician_name,
			accessioning_num : this.FormData.value.accessioning_num,
			test_type : this.FormData.value.test_type
		};
		if (this.FormData.value.p_name === '' && this.FormData.value.accessioning_num === '' && this.FormData.value.physician_name === '' && this.FormData.value.test_type === '') {
			this.showError('Please enter value on search field');
			this.spinner.hide();
			return;
		}
		this.service.search_submitted_reports(this.sendData).subscribe((data) => {
			this.response = data;
			if (this.response.status === '1') {
				this.data = this.response.submitted_rep;
				this.notFount = false;
				this.spinner.hide();
			} else {
				this.notFount = true;
				this.spinner.hide();
			}
		});
	}
	showSuccess(message) {
		this.toastr.success(message, 'Success');
	}

	showError(message) {
		this.toastr.error(message, 'Oops!');
	}
}
