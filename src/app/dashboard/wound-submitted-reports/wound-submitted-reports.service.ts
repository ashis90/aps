import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class WoundSubmittedReportsService {
	public baseUrl = environment.baseUrl + 'SubmittedReport/';
	headers: Headers = new Headers();
	options: any;

	constructor(private http: HttpClient) {
		this.headers.append(
			'Access-Control-Allow-Headers',
			'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		);
		this.headers.append('Access-Control-Allow-Origin', '*');
		this.headers.append('Accept', 'application/json');
		this.options = new RequestOptions({ headers: this.headers });
	}

	get_submitted_reports(sendData) {
		return this.http.post(this.baseUrl + 'get_submitted_reports_for_wounds', JSON.stringify(sendData), this.options);
	}

	search_submitted_reports(sendData) {
		return this.http.post(this.baseUrl + 'search_submitted_reports_for_wounds', JSON.stringify(sendData), this.options);
	}

	change_submitted_reports_color(sendData) {
		return this.http.post(this.baseUrl + 'change_submitted_reports_color_for_wounds', JSON.stringify(sendData), this.options);
	}

	change_color_for_pcr(sendData) {
		return this.http.post(this.baseUrl + 'change_color_for_pcr_for_wounds', JSON.stringify(sendData), this.options);
	}
	// get_partners(sendData) {
	// 	return this.http.post(this.baseUrl + 'get_partners', JSON.stringify(sendData), this.options);
	// }


}
