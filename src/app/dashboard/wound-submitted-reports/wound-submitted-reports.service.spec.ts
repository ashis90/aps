import { TestBed } from '@angular/core/testing';

import { WoundSubmittedReportsService } from './wound-submitted-reports.service';

describe('SubmittedReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WoundSubmittedReportsService = TestBed.get(WoundSubmittedReportsService);
    expect(service).toBeTruthy();
  });
});
