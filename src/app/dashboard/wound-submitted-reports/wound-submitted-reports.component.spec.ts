import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WoundSubmittedReportsComponent } from './wound-submitted-reports.component';

describe('SubmittedReportsComponent', () => {
  let component: WoundSubmittedReportsComponent;
  let fixture: ComponentFixture<WoundSubmittedReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WoundSubmittedReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WoundSubmittedReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
