import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { WoundSubmittedReportsService } from 'src/app/dashboard/wound-submitted-reports/wound-submitted-reports.service';
import { HttpClient } from '@angular/common/http';
import {
	FormGroup,
	Validators,
	FormArray,
	FormControl,
	AbstractControl,
	ValidatorFn,
	FormBuilder
} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
	selector: 'app-submitted-reports',
	templateUrl: './wound-submitted-reports.component.html',
	styleUrls: ['./wound-submitted-reports.component.css']
})
export class WoundSubmittedReportsComponent implements OnInit {
	user_role: string;
	user_id: string;
	FormData: FormGroup;
	sendData: any;
	response: any;
	data: any;
	notFount: boolean;
	id: string;
	total_pcr_report_count: any = 0;
	total_histo_report_count: any = 0;
	public url = environment.assetsUrl + 'assets/uploads/';

	constructor(
		private service: WoundSubmittedReportsService,
		private routes: Router,
		private http: HttpClient,
		private activatedRoute: ActivatedRoute,
		private spinner: NgxSpinnerService,
		private formbuilder: FormBuilder,
		private toastr: ToastrService
	) {
		this.user_role = localStorage.getItem('userrole');
		this.user_id = localStorage.getItem('userid');
		this.id = this.activatedRoute.snapshot.paramMap.get('id');
		this.FormData = this.formbuilder.group({
			p_lastname: [''],
			assessioning_num: [''],
			test_type: [''],
			dataType: ['genaral']
		});
	}

	get_data() {
		if (this.id) {
			this.sendData = { user_id: this.id };
		} else {
			this.sendData = { user_id: this.user_id };
		}
		this.service.get_submitted_reports(this.sendData).subscribe((data) => {
			this.response = data;
			if (this.response.status === '1') {
				this.data = this.response.submitted_rep;
				this.notFount = false;
				this.total_histo_report_count = this.response.total_histo_report_count;
				this.total_pcr_report_count = this.response.total_pcr_report_count;
				this.spinner.hide();
			} else {
				this.notFount = true;
				this.spinner.hide();
			}
		});
	}

	ngOnInit() {
		this.spinner.show();
		this.get_data();
	}

	resetForm() {
		this.FormData.reset();
		this.get_data();
	}

	search_data() {
		this.spinner.show();
		this.sendData = {
			user_id: this.user_id,
			p_lastname: this.FormData.value.p_lastname,
			assessioning_num: this.FormData.value.assessioning_num,
			test_type: this.FormData.value.test_type,
			dataType: this.FormData.value.dataType
		};
		if (this.FormData.value.p_lastname === '' && this.FormData.value.assessioning_num === '' && this.FormData.value.test_type === '' && this.FormData.value.data_type === '') {
			this.showError('Please enter Patient Last Name OR Assessioning number');
			this.spinner.hide();
			return;
		}
		this.service.search_submitted_reports(this.sendData).subscribe((data) => {
			this.response = data;
			if (this.response.status === '1') {
				this.data = this.response.submitted_rep;
				this.notFount = false;
				this.total_histo_report_count = this.response.total_histo_report_count;
				this.total_pcr_report_count = this.response.total_pcr_report_count;
				this.spinner.hide();
			} else {
				this.notFount = true;
				this.spinner.hide();
			}
		});
	}

	chanage_color(specimen_id) {
		this.sendData = {
			specimen_id: specimen_id
		};

		this.service.change_submitted_reports_color(this.sendData).subscribe((data) => {
			this.response = data;
			if (this.response.status === '1') {
				this.ngOnInit();
			} else {
				this.ngOnInit();
			}
		});
	}

	chanage_color_for_pcr(accessioning_num) {
		this.sendData = {
			accessioning_num: accessioning_num
		};

		this.service.change_color_for_pcr(this.sendData).subscribe((data) => {
			this.response = data;
			if (this.response.status === '1') {
				this.ngOnInit();
			} else {
				this.ngOnInit();
			}
		});
	}

	color(value) {
		if (value === '1') {
			return 'gray';
		} else if (value === '0') {
			return '#424242 ';
		} else {
			return '#424242 ';
		}
	}

	trackByFn(index, item) {
		return item ? item.team_manager_name : undefined; // or item.id
	}

	showSuccess(message) {
		this.toastr.success(message, 'Success');
	}

	showError(message) {
		this.toastr.error(message, 'Oops!');
	}
}
