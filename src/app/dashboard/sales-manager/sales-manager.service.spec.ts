import { TestBed } from '@angular/core/testing';

import { SalesManagerService } from './sales-manager.service';

describe('SalesManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SalesManagerService = TestBed.get(SalesManagerService);
    expect(service).toBeTruthy();
  });
});
