import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { SalesManagerService } from "src/app/dashboard/sales-manager/sales-manager.service";
import { RegionalService } from "src/app/dashboard/regional-manager/regional-list/regional.service";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder,
  NgForm
} from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-sales-add",
  templateUrl: "./sales-add.component.html",
  styleUrls: ["./sales-add.component.css"]
})
export class SalesAddComponent implements OnInit {
  sendData: any;
  FormData: FormGroup;
  response: any;
  user_role: string;
  user_id: string;
  sales_list: any;
  sales_rep: any;
  ein: boolean;
  user_status: string;
  llc: string;
  RegionalManagerList: any;
  public mask: string = "(999) 000-00-00-00";
  constructor(
    private service: SalesManagerService,
    private regionalservice: RegionalService,
    private routes: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    this.FormData = this.formBuilder.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      user_login: ["", Validators.required],
      user_email: [
        "",
        [
          Validators.required,
          Validators.pattern("[a-zA-Z-0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$")
        ]
      ],
      user_pass: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}/)
        ]
      ],
      //user_pass: ['', [Validators.required, Validators.minLength(6)]],
      con_pass: ["", [Validators.required, this.equalto("user_pass")]],
      mobile: [""],
      user_url: [""],
      address: [""],
      regional_manager: [""],
      llc: [""],
      ein_num: [""],
      user_status: [""],
      brn: [""],
      ban: [""],
      nbaccount: [""],
      commission_percentage: [""]
    });

    this.user_role = localStorage.getItem("userrole");
    this.user_id = localStorage.getItem("userid");
  }

  totalRegionalManagers() {
    this.sendData = {
      user_id: this.user_id
    };

    this.regionalservice
      .totalRegionalManagersList(this.sendData)
      .subscribe(data => {
        this.response = data;
        if (this.response.status === "0") {
        } else {
          this.RegionalManagerList = this.response.reg_man_details;
        }
      });
  }

  get_all_sales_rep() {
    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id
    };
    this.service.salesRepList(this.sendData).subscribe(data => {
      this.sales_list = data;
      this.spinner.hide();
      if (this.sales_list.status === "0") {
      } else {
        this.sales_rep = this.sales_list.sales_reps_data;
      }
    });
  }

  ngOnInit() {
    this.user_status = "Active";
    this.spinner.show();
    this.get_all_sales_rep();
    this.totalRegionalManagers();
  }

  add_data() {
    this.sendData = {
      first_name: this.FormData.value.first_name,
      last_name: this.FormData.value.last_name,
      user_login: this.FormData.value.user_login,
      user_email: this.FormData.value.user_email,
      user_pass: this.FormData.value.user_pass,
      con_pass: this.FormData.value.con_pass,
      mobile: this.FormData.value.mobile,
      user_url: this.FormData.value.user_url,
      address: this.FormData.value.address,
      regional_manager: this.FormData.value.regional_manager,
      llc: this.FormData.value.llc,
      ein_num: this.FormData.value.ein_num,
      user_status: this.FormData.value.user_status,
      brn: this.FormData.value.brn,
      ban: this.FormData.value.ban,
      nbaccount: this.FormData.value.nbaccount,
      commission_percentage: this.FormData.value.commission_percentage,
      user_id: this.user_id
    };
    this.service.add_sales_rep(this.sendData).subscribe(data => {
      this.response = data;
      if (this.response.status === "1") {
        this.showSuccess(this.response.message);
        this.routes.navigate(["/sales-representative"]);
      } else {
        this.showError(this.response.message);
      }
    });
    // this.FormData.reset();
  }

  equalto(repassword): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[repassword] === input;
      if (!isValid) {
        return { equalTo: { isValid } };
      } else {
        return null;
      }
    };
  }

  ein_no(val) {
    if (val === "llc") {
      this.ein = true;
    } else {
      this.ein = false;
    }
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
