import { Component, OnInit } from '@angular/core';
import { SalesManagerService } from 'src/app/dashboard/sales-manager/sales-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { getLocale } from 'ngx-bootstrap/chronos/public_api';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-sales',
	templateUrl: './sales.component.html',
	styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {
	// all variables
	statusSelect = '';
	selectState = '';
	monthSelect: any;
	yearSelect: any;
	sales_list: any = [];
	public data: any;
	notFount: boolean;
	Year: Date;
	currentYear: any;
	allyear: any = [];
	searchForm: FormGroup;
	sendData: any;
	succMessage: any;
	user_role: any;
	user_id: any;
	reg_id: any;
	searchText: string;
	total_paid_amount: number;
	total_charged_amount: number;
	response: any;
	TotalSalesManager: any;
	limit: number;
	total_showing_users: any;
	loadmorehide: boolean;
	last_data_entry_date: any;
	filterData: any;

	constructor(
		private service: SalesManagerService,
		private routes: Router,
		private activatedRoute: ActivatedRoute,
		private http: HttpClient,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService,
		private fb: FormBuilder
	) {
		this.searchForm = this.fb.group({
			status: [''],
			from_month: [''],
			from_year: [''],
			search_value: [''],
			state: [''],
			searchText: ['']
		});
		this.user_role = localStorage.getItem('userrole');
		this.user_id = localStorage.getItem('userid');
		this.reg_id = this.activatedRoute.snapshot.paramMap.get('id');
	}

	// get data from service
	get_data() {
		this.sendData = {
			user_role: this.user_role,
			user_id: this.user_id
		};

		this.service.salesManagersList(this.sendData).subscribe((data) => {
			this.sales_list = data;
			if (this.sales_list.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.data = this.sales_list.sales_reps_data;
				this.last_data_entry_date = this.sales_list.last_data_entry_date;
				this.total_showing_users = this.data.length;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}
	/*
 get_sales_reps_data_by_reg_id
*/
	get_sales_reps_data_by_reg_id() {
		this.sendData = {
			user_role: this.user_role,
			user_id: this.user_id,
			reg_id: this.reg_id
		};
		this.service.sales_managers_list_reg_id(this.sendData).subscribe((data) => {
			this.sales_list = data;
			if (this.sales_list.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.data = this.sales_list.sales_reps_data;
				this.total_showing_users = this.data.length;
				this.last_data_entry_date = this.sales_list.last_data_entry_date;
				this.total_paid_amount = this.sales_list.total_paid_amt;
				this.total_charged_amount = this.sales_list.total_charged_amt;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}

	// show 10 data in view (first time load)
	ngOnInit() {
		this.loadmorehide = false;
		this.limit = 0;
		this.spinner.show();
		this.Year = new Date();
		this.currentYear = this.Year.getFullYear();
		this.yearSelect = this.Year.getFullYear();
		this.monthSelect = this.Year.getMonth() + 1;
		for (let i = 2017; i <= this.currentYear; i++) {
			this.allyear.push(i);
		}
		if (this.reg_id) {
			this.get_sales_reps_data_by_reg_id();
		} else {
			this.showNumOfSalesManager();
			this.get_data();
		}
	}

	// show total number of sales manager
	showNumOfSalesManager() {
		this.sendData = {
			user_role: this.user_role,
			user_id: this.user_id
		};

		this.service.showNumOfSalesManager(this.sendData).subscribe((data) => {
			this.response = data;
			if (this.response.status === '0') {
			} else {
				this.TotalSalesManager = this.response.total;
				this.total_paid_amount = this.response.total_paid_amt;
				this.total_charged_amount = this.response.total_charged_amt;
			}
		});
	}

	search_filter(term: string) {
		let allData: any;
		allData = this.sales_list.sales_reps_data;
		if (!term) {
			this.filterData = this.data;
			this.data = allData;
		} else {
			this.filterData = allData.filter((x) =>
				x.sales_reps_name.trim().toLowerCase().includes(term.trim().toLowerCase())
			);
			this.data = this.filterData;
		}
	}

	// show load more data in view
	loadMore() {
		this.spinner.show();
		this.Year = new Date();
		this.currentYear = this.Year.getFullYear();
		for (let i = 2017; i <= this.currentYear; i++) {
			this.allyear.push(i);
		}
		if (this.TotalSalesManager === this.total_showing_users) {
			this.showError('No more data found');
			this.loadmorehide = true;
			this.spinner.hide();
			return false;
		}
		this.sendData = {
			user_role: this.user_role,
			user_id: this.user_id,
			limit: (this.limit += 10)
		};

		this.service.salesManagersListAll(this.sendData).subscribe((data) => {
			this.sales_list = data;
			if (this.sales_list.status === '1') {
				this.sales_list.sales_reps_data.forEach((element) => {
					this.data.push(element);
				});
				this.total_showing_users = this.data.length;
				this.notFount = false;
				this.spinner.hide();
			} else {
				this.notFount = true;
				this.spinner.hide();
			}
		});
	}

	// search data
	searchData() {
		if (
			this.searchForm.value.status === '' &&
			this.searchForm.value.from_month === '' &&
			this.searchForm.value.from_year === '' &&
			this.searchForm.value.state === '' &&
			this.searchForm.value.search_value === ''
		) {
			this.showError('Please enter any value.');
			return;
		}

		this.spinner.show();
		this.sendData = {
			status: this.searchForm.value.status,
			state: this.searchForm.value.state,
			from_month: this.searchForm.value.from_month,
			from_year: this.searchForm.value.from_year,
			search_value: this.searchForm.value.search_value,
			user_role: this.user_role,
			user_id: this.user_id,
			reg_id: this.reg_id
		};

		this.service.search_sales_list(this.sendData).subscribe((data) => {
			this.sales_list = data;
			if (this.sales_list.status === '0') {
				this.spinner.hide();
				this.total_showing_users = 0;
				this.loadmorehide = true;
				this.notFount = true;
			} else {
				this.data = this.sales_list.sales_reps_data;
				this.total_showing_users = this.data.length;
				this.total_paid_amount = this.sales_list.total_paid_amt;
				this.total_charged_amount = this.sales_list.total_charged_amt;
				this.notFount = false;
				this.loadmorehide = true;
				this.spinner.hide();
			}
		});
	}

	// change user status
	active_inactive(status, user_id) {
		this.spinner.show();
		this.succMessage = '';
		this.sendData = {
			status: status,
			user_id: user_id
		};

		this.service.change_user_status(this.sendData).subscribe((data) => {
			this.sales_list = data;
			if (this.sales_list.status === '0') {
				this.ngOnInit();
				this.succMessage = this.sales_list.message;
				this.notFount = true;
			} else {
				this.data = this.sales_list.sales_reps_data;
				this.ngOnInit();
				this.succMessage = this.sales_list.message;
				this.notFount = false;
			}
		});
	}

	trackByFn(index, item) {
		return item ? item.team_manager_name : undefined; // or item.id
	}

	showSuccess(message) {
		this.toastr.success(message, 'Success');
	}

	showError(message) {
		this.toastr.error(message, 'Oops!');
	}
}
