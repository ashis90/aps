import { Component, OnInit } from '@angular/core';
import { SalesManagerService } from 'src/app/dashboard/sales-manager/sales-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { getLocale } from 'ngx-bootstrap/chronos/public_api';
import {ExcelService} from '../../../excel.service';

@Component({
  selector: 'app-commission-history',
  templateUrl: './commission-history.component.html',
  styleUrls: ['./commission-history.component.css']
})
export class CommissionHistoryComponent implements OnInit {
  sendData: any;
  user_id: any;
  commission_list: any = [];
  notFount: boolean;
  data: any;
  yearSelect = '';
  Year: Date;
 currentYear: any;
 allyear: any = [];
  searchForm: FormGroup;
  
constructor(private excelService:ExcelService,private service: SalesManagerService, private routes: Router,
   private http: HttpClient, private spinner: NgxSpinnerService,  private activatedRoute: ActivatedRoute) {
    this.searchForm = new FormGroup({
      from_year : new FormControl(),
  });
  this.user_id = this.activatedRoute.snapshot.paramMap.get('id');
   }

   get_data() {
    this.sendData = {'user_id': this.user_id, };

         this.service.commission_history(this.sendData).subscribe(data => {
             this.commission_list = data;
             if (this.commission_list.status === '0') {
               this.notFount = true;
             } else {
               this.data = this.commission_list.history_view;
               this.notFount = false;
             }
         });

         setTimeout(() => {
          this.spinner.hide();
         }, 500);
   }

   search_data() {
    this.spinner.show();
    this.sendData = {'user_id': this.user_id, 'year': this.searchForm.value.from_year, };

         this.service.search_commission_history(this.sendData).subscribe(data => {
             this.commission_list = data;
             if (this.commission_list.status === '0') {
               this.notFount = true;
             } else {
               this.data = this.commission_list.history_view;
               this.notFount = false;
             }
         });
         setTimeout(() => {
          this.spinner.hide();
         }, 500);
   }

  ngOnInit() {
    this.spinner.show();
   this.Year = new Date();
   this.currentYear = this.Year.getFullYear();
   for (let i = 2017; i <= this.currentYear; i++) {
     this.allyear.push(i);
   }
   this.get_data();

  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.data, 'sample');
  }

}
