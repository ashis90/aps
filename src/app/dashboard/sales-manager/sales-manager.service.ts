import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalesManagerService {

  public baseUrl = environment.baseUrl + 'SalesRepresentative/';
  headers: Headers = new Headers;
  options: any;



  constructor(private http: HttpClient) {
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }


  salesManagersList(sendData): Observable<any> {
    return this.http.post(this.baseUrl + 'salesManagersList', JSON.stringify(sendData), this.options);
  }

  sales_managers_list_reg_id(sendData): Observable<any> {
    return this.http.post(this.baseUrl + 'sale_reps_by_reg_mgr_id', JSON.stringify(sendData), this.options);
  }


  salesManagersListAll(sendData): Observable<any> {
    return this.http.post(this.baseUrl + 'salesManagersListAll', JSON.stringify(sendData), this.options);
  }

  showNumOfSalesManager(sendData): Observable<any> {
    return this.http.post(this.baseUrl + 'showNumOfSalesManager', JSON.stringify(sendData), this.options);
  }

  search_sales_list(sendData): Observable<any> {
    return this.http.post(this.baseUrl + 'search_sales_list', JSON.stringify(sendData), this.options);
  }

  sales_man_details(sendData) {
    return this.http.post(this.baseUrl + 'sales_man_details', JSON.stringify(sendData), this.options);
  }

  update_data(sendData) {
    return this.http.post(this.baseUrl + 'update_sales_man_details', JSON.stringify(sendData), this.options);
  }

  change_user_status(sendData) {
    return this.http.post(this.baseUrl + 'change_user_status', JSON.stringify(sendData), this.options);
  }

  commissions(sendData) {
    return this.http.post(this.baseUrl + 'commissions', JSON.stringify(sendData), this.options);
  }

  realData(sendData) {
    return this.http.post(this.baseUrl + 'realData', JSON.stringify(sendData), this.options);
  }

  commission_history(sendData) {
    return this.http.post(this.baseUrl + 'commission_history', JSON.stringify(sendData), this.options);
  }

  add_sales_rep(sendData) {
    return this.http.post(this.baseUrl + 'add_sales_rep', JSON.stringify(sendData), this.options);
  }

  salesRepList(sendData) {
    return this.http.post(this.baseUrl + 'salesRepList', JSON.stringify(sendData), this.options);
  }


  search_commission_history(sendData) {
    return this.http.post(this.baseUrl + 'search_commission_history', JSON.stringify(sendData), this.options);
  }

  getMonthlySalesSummary(sendData) {
    return this.http.post(this.baseUrl + 'get_monthly_sales_summary', JSON.stringify(sendData), this.options);
  }
  getSpecimenCountForSales(sendData) {
    return this.http.post(this.baseUrl + 'getSpecimenCountForSales', JSON.stringify(sendData), this.options);
  }
}
