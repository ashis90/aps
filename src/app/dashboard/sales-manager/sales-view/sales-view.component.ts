import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { SalesManagerService } from 'src/app/dashboard/sales-manager/sales-manager.service';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl, AbstractControl, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-sales-view',
  templateUrl: './sales-view.component.html',
  styleUrls: ['./sales-view.component.css']
})
export class SalesViewComponent implements OnInit {
  id: any;
  sendData: any;
  regional_details: any = [];
  notFount: boolean;
  userDetails: any;

  constructor(private service: SalesManagerService, private routes: Router,
    private http: HttpClient, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.sendData = { 'id': this.id, };

    this.service.sales_man_details(this.sendData).subscribe(data => {
      this.regional_details = data;
      if (this.regional_details.status === '1') {
        this.userDetails = this.regional_details.details;

        this.notFount = false;
      } else {
        this.notFount = true;
      }
    });
  }

}
