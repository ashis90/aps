import { Component, OnInit } from '@angular/core';
import { SalesManagerService } from 'src/app/dashboard/sales-manager/sales-manager.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { getLocale } from 'ngx-bootstrap/chronos/public_api';
import { ExcelService } from '../../../excel.service';

@Component({
	selector: 'app-commission',
	templateUrl: './commission.component.html',
	styleUrls: [ './commission.component.css' ]
})
export class CommissionComponent implements OnInit {
	sendData: any;
	user_role: string;
	user_id: string;
	sales_id: string = '';
	commissions: any;
	notFount: boolean;
	data: any;
	sales_rep: any;
	Year: Date;
	currentYear: any;
	allyear: any = [];
	sales_list: any;
	total_char_am: any;
	total_paid_am: any;
	total_physicians: any;
	searchForm: FormGroup;
	// keyword = '';
	monthSelect: any;
	yearSelect: any;
	sales_repre = '';
	last_data_entry_date: any;
	filterData: any;

	constructor(
		private excelService: ExcelService,
		private service: SalesManagerService,
		private routes: Router,
		private activatedRoute: ActivatedRoute,
		private http: HttpClient,
		private spinner: NgxSpinnerService
	) {
		this.searchForm = new FormGroup({
			monthSelect: new FormControl(),
			yearSelect: new FormControl(),
			sales_repre: new FormControl(),
			keyword: new FormControl()
		});

		this.user_role = localStorage.getItem('userrole');
		this.user_id = localStorage.getItem('userid');

		this.sales_id = this.activatedRoute.snapshot.paramMap.get('id');
		if (this.sales_id) {
			this.user_id = this.sales_id;
		}
	}

	// get data from service
	get_data() {
		this.sendData = {
			user_id: this.user_id,
			user_role: this.user_role
		};

		this.service.commissions(this.sendData).subscribe((data) => {
			this.commissions = data;
			if (this.commissions.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.data = this.commissions.physicians_details;
				this.last_data_entry_date = this.commissions.last_data_entry_date;
				this.total_char_am = this.commissions.total_char;
				this.total_paid_am = this.commissions.total_paid;
				this.total_physicians = this.commissions.physicians_count;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}

	get_all_sales_rep() {
		this.sendData = {
			user_role: this.user_role,
			user_id: this.user_id
		};
		this.service.salesRepList(this.sendData).subscribe((data) => {
			this.sales_list = data;
			if (this.sales_list.status === '0') {
			} else {
				this.sales_rep = this.sales_list.sales_reps_data;
			}
		});
	}

	ngOnInit() {
		if(localStorage.getItem('userrole') === 'sales'){
			if(localStorage.getItem('view_popup') !== 'Yes'){
			  console.log(localStorage.getItem('view_popup'))
			  this.routes.navigate(["/sales-training"]);
			  return false;
			}
		  }
		this.spinner.show();
		this.Year = new Date();
		this.currentYear = this.Year.getFullYear();
		for (let i = 2017; i <= this.currentYear; i++) {
			this.allyear.push(i);
		}
		this.yearSelect = this.currentYear;
		this.monthSelect = this.Year.getMonth() + 1;
		if (this.sales_id) {
			this.get_sales_commission();
		} else {
			this.get_all_sales_rep();
			this.get_data();
		}
	}

	get_sales_commission() {
		this.spinner.show();
		this.sendData = {
			user_role: this.user_role,
			user_id: this.user_id,
			month: this.searchForm.value.monthSelect,
			year: this.searchForm.value.yearSelect,
			keyword: this.searchForm.value.keyword
		};

		this.service.commissions(this.sendData).subscribe((data) => {
			this.commissions = data;
			if (this.commissions.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.data = this.commissions.physicians_details;
				this.last_data_entry_date = this.commissions.last_data_entry_date;
				this.total_char_am = this.commissions.total_char;
				this.total_paid_am = this.commissions.total_paid;
				this.total_physicians = this.commissions.physicians_count;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}

	search_filter(term: string) {
		let allData: any;
		allData = this.commissions.physicians_details;
		if (!term) {
			this.filterData = this.data;
			this.data = allData;
		} else {
			this.filterData = allData.filter((x) =>
				x.physician_name.trim().toLowerCase().includes(term.trim().toLowerCase())
			);
			this.data = this.filterData;
		}
	}

	searchData() {
		this.spinner.show();

		this.sendData = {
			month: this.searchForm.value.monthSelect,
			year: this.searchForm.value.yearSelect,
			user_id: this.user_id,
			keyword: this.searchForm.value.keyword,
			sales_id: this.searchForm.value.sales_repre,
			user_role: this.user_role
		};

		this.service.commissions(this.sendData).subscribe((data) => {
			this.commissions = data;
			if (this.commissions.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.data = this.commissions.physicians_details;
				this.total_char_am = this.commissions.total_char;
				this.total_paid_am = this.commissions.total_paid;
				this.total_physicians = this.commissions.physicians_count;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}

	realData() {
		this.spinner.show();

		this.sendData = {
			month: this.searchForm.value.monthSelect,
			year: this.searchForm.value.yearSelect,
			sales_id: this.searchForm.value.sales_repre,
			keyword: this.searchForm.value.keyword,
			user_id: this.user_id,
			user_role: this.user_role
		};

		this.service.realData(this.sendData).subscribe((data) => {
			this.commissions = data;
			if (this.commissions.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.total_physicians = this.commissions.physicians_count;
				if (this.total_physicians === 0) {
					this.notFount = true;
					this.data = '';
				} else {
					this.data = this.commissions.physicians_details;
					this.total_char_am = this.commissions.total_char;
					this.total_paid_am = this.commissions.total_paid;
					this.notFount = false;
				}
				this.spinner.hide();
			}
		});
	}

	trackByFn(index, item) {
		return item ? item.team_manager_name : undefined; // or item.id
	}

	exportAsXLSX(): void {
		this.excelService.exportAsExcelFile(this.data, 'sample');
	}
}
