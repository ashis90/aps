import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { SalesManagerService } from "src/app/dashboard/sales-manager/sales-manager.service";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder
} from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { getLocale } from "ngx-bootstrap/chronos/public_api";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-sales-edit",
  templateUrl: "./sales-edit.component.html",
  styleUrls: ["./sales-edit.component.css"]
})
export class SalesEditComponent implements OnInit {
  id: any;
  sendData: any;
  sales_details: any = [];
  notFount: boolean;
  userDetails: any;
  regionalList: any = [];
  FormData: FormGroup;

  // store data in this variable
  user_email: any;
  succMessage: any;
  errMessage: any;
  llc: any;
  ein: boolean;
  regional_manager = "";
  user_status: any;
  userRole: any;
  view_popup: any;

  constructor(
    private service: SalesManagerService,
    private routes: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private formbuilder: FormBuilder,
    private toastr: ToastrService
  ) {
    this.FormData = this.formbuilder.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      user_login: [""],
      user_email: [
        "",
        Validators.pattern("[a-zA-Z-0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$")
      ],
      edit_user_pass: [
        "",
        [
          Validators.minLength(8),
          Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}/)
        ]
      ],
      //edit_user_pass: ['', Validators.minLength(6)],
      edit_con_pass: ["", [this.equalto("edit_user_pass")]],
      mobile: [""],
      user_url: [""],
      address: [""],
      brn: [""],
      ban: [""],
      nbaccount: [""],
      commission_percentage: [""],

      regional_manager: [""],
      llc: [""],
      corporation: [""],
      user_status: [""]
    });
    this.userRole = localStorage.userrole;
  }

  equalto(repassword): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[repassword] == input;
      if (!isValid) {
        return { equalTo: { isValid } };
      } else {
        return null;
      }
    };
  }

  ngOnInit() {
    this.spinner.show();
    this.ein = false;
    this.get_data();
  }

  get_data() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    this.sendData = { id: this.id };

    this.service.sales_man_details(this.sendData).subscribe(data => {
      this.sales_details = data;
      if (this.sales_details.status === "1") {
        this.userDetails = this.sales_details.details;
        this.regionalList = this.sales_details.assign_regional_list;
        this.view_popup = this.userDetails.view_popup;
        this.FormData.patchValue({
          first_name: this.userDetails.fname,
          last_name: this.userDetails.lname,
          user_login: this.userDetails.user_login,
          user_email: this.userDetails.email,
          mobile: this.userDetails.mob,
          user_url: this.userDetails.url,
          address: this.userDetails.add,
          brn: this.userDetails.bank_routing_num,
          ban: this.userDetails.bank_account_num,
          nbaccount: this.userDetails.name_on_bank,
          commission_percentage: this.userDetails.commission_percentage,
          corporation: this.userDetails.ein_no,
          regional_manager: this.userDetails.assign_to
        });

        if (this.userDetails.user_status) {
          this.user_status = this.userDetails.user_status;
        } else {
          this.user_status = "";
        }

        if (this.userDetails.llc) {
          this.llc = this.userDetails.llc;
          if (this.llc === "llc") {
            this.ein = true;
          }
        } else {
          this.llc = "";
        }

        this.notFount = false;
        this.spinner.hide();
      } else {
        this.notFount = true;
        this.spinner.hide();
      }
    });
  }

  update_data() {
    this.succMessage = "";
    this.errMessage = "";
    this.sendData = {
      user_id: this.id,
      first_name: this.FormData.value.first_name,
      last_name: this.FormData.value.last_name,
      user_login: this.FormData.value.user_login,
      user_email: this.FormData.value.user_email,
      edit_user_pass: this.FormData.value.edit_user_pass,
      edit_con_pass: this.FormData.value.edit_con_pass,
      mobile: this.FormData.value.mobile,
      user_url: this.FormData.value.user_url,
      brn: this.FormData.value.brn,
      ban: this.FormData.value.ban,
      nbaccount: this.FormData.value.nbaccount,
      commission_percentage: this.FormData.value.commission_percentage,
      address: this.FormData.value.address,
      user_status: this.FormData.value.user_status,
      ein_no: this.FormData.value.corporation,
      regional_manager: this.FormData.value.regional_manager,
      llc: this.FormData.value.llc
    };

    if (this.FormData.status === "VALID") {
      this.spinner.show();
      if (this.FormData.value.user_email !== "") {
        this.service.update_data(this.sendData).subscribe(data => {
          this.sales_details = data;
          if (this.sales_details.status === "1") {
            this.showSuccess(this.sales_details.message);
            this.ngOnInit();
            this.notFount = true;
          } else {
            this.showError(this.sales_details.message);
            this.ngOnInit();
            this.notFount = false;
          }
        });
      } else {
        this.toastr.info("Please enter a email.", "Error", {
          timeOut: 3000
        });
      }
    } else {
      this.toastr.info("Please fill all required fields.", "Error", {
        timeOut: 3000
      });
    }
  }

  ein_no(val) {
    if (val === "llc") {
      this.ein = true;
    } else {
      this.ein = false;
    }
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
