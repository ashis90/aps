import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnersCompanySubmittedReportsComponent } from './partners-company-submitted-reports.component';

describe('PartnersCompanySubmittedReportsComponent', () => {
  let component: PartnersCompanySubmittedReportsComponent;
  let fixture: ComponentFixture<PartnersCompanySubmittedReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnersCompanySubmittedReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnersCompanySubmittedReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
