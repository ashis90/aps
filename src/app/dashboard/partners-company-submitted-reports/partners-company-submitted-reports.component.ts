import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { FormGroup } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder
} from "@angular/forms";
import { PartnersCompanySubmittedReportsService } from "./partners-company-submitted-reports.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { environment } from "../../../environments/environment";

@Component({
  selector: "app-partners-company-submitted-reports",
  templateUrl: "./partners-company-submitted-reports.component.html",
  styleUrls: ["./partners-company-submitted-reports.component.css"]
})
export class PartnersCompanySubmittedReportsComponent implements OnInit {
  select_test_type: any = "";
  user_role: string;
  user_id: string;
  FormData: FormGroup;
  sendData: any;
  response: any;
  data: any;
  notFount: boolean;
  id: string;
  public url = environment.assetsUrl + "assets/uploads/";

  constructor(
    private service: PartnersCompanySubmittedReportsService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private formbuilder: FormBuilder,
    private toastr: ToastrService
  ) {
    this.user_role = localStorage.getItem("userrole");
    this.user_id = localStorage.getItem("userid");
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    this.FormData = this.formbuilder.group({
      p_lastname: [""],
      assessioning_num: [""],
      external_id: [""],
      test_type: [""]
    });
  }

  get_data() {
    if (this.id) {
      this.sendData = { partner_id: this.id };
    } else {
      this.sendData = { user_id: this.user_id };
    }
    this.service.get_submitted_reports(this.sendData).subscribe(data => {
      this.response = data;
      if (this.response.status === "1") {
        this.data = this.response.submitted_rep;
        this.notFount = false;
        this.spinner.hide();
      } else {
        this.notFount = true;
        this.spinner.hide();
      }
    });
  }

  ngOnInit() {
    this.spinner.show();
    //this.select_test_type = '';
    this.get_data();
  }
  resetForm() {
    this.FormData.reset();
    this.spinner.show();
    this.get_data();
  }

  search_data() {
    this.spinner.show();

    if (this.id) {
      this.sendData = {
        partner_id: this.id,
        p_lastname: this.FormData.value.p_lastname,
        external_id: this.FormData.value.external_id,
        assessioning_num: this.FormData.value.assessioning_num,
        test_type: this.FormData.value.test_type
      };
    } else {
      this.sendData = {
        user_id: this.user_id,
        p_lastname: this.FormData.value.p_lastname,
        external_id: this.FormData.value.external_id,
        assessioning_num: this.FormData.value.assessioning_num,
        test_type: this.FormData.value.test_type
      };
    }
    if (
      this.FormData.value.test_type === "" &&
      this.FormData.value.p_lastname === "" &&
      this.FormData.value.assessioning_num === "" &&
      this.FormData.value.external_id === ""
    ) {
      this.showError("Please enter value on search field.");
      this.spinner.hide();
      return;
    }
    this.service.search_submitted_reports(this.sendData).subscribe(data => {
      this.response = data;
      if (this.response.status === "1") {
        this.data = this.response.submitted_rep;
        this.notFount = false;
        this.spinner.hide();
      } else {
        this.notFount = true;
        this.spinner.hide();
      }
    });
  }

  chanage_color(specimen_id) {
    if (this.user_role !== "aps_sales_manager") {
      this.sendData = {
        specimen_id: specimen_id
      };

      this.service
        .change_submitted_reports_color(this.sendData)
        .subscribe(data => {
          this.response = data;
          if (this.response.status === "1") {
            this.ngOnInit();
          } else {
            this.ngOnInit();
          }
        });
    }
  }

  chanage_color_for_pcr(accessioning_num) {
    if (this.user_role !== "aps_sales_manager") {
      this.sendData = {
        accessioning_num: accessioning_num
      };

      this.service.change_color_for_pcr(this.sendData).subscribe(data => {
        this.response = data;
        if (this.response.status === "1") {
          this.ngOnInit();
        } else {
          this.ngOnInit();
        }
      });
    }
  }

  color(value) {
    if (value === "1") {
      return "gray";
    } else if (value === "0") {
      return "#424242 ";
    } else {
      return "#424242 ";
    }
  }

  trackByFn(index, item) {
    return item ? item.team_manager_name : undefined; // or item.id
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
