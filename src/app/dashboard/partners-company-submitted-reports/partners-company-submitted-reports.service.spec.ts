import { TestBed } from '@angular/core/testing';

import { PartnersCompanySubmittedReportsService } from './partners-company-submitted-reports.service';

describe('PartnersCompanySubmittedReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PartnersCompanySubmittedReportsService = TestBed.get(PartnersCompanySubmittedReportsService);
    expect(service).toBeTruthy();
  });
});
