import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { PhysicianService } from "src/app/dashboard/physician/physician.service";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder,
  NgForm
} from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-physician-add",
  templateUrl: "./physician-add.component.html",
  styleUrls: ["./physician-add.component.css"]
})
export class PhysicianAddComponent implements OnInit {
  sendData: any;
  FormData: FormGroup;
  response: any;
  user_role: string;
  user_id: string;
  sales_list: any;
  ein: boolean;
  user_status: string;
  state = "";
  assign_clinic = "";
  combine_phy_acc = "";
  sales_rep = "";
  assign_partner = "";
  assign_specialty = "";
  report_recive_way_fax: string;
  report_recive_way_email: string;
  physician_details: any;
  userDetails: any;
  all_assign_clinic: any;
  all_sales_rep: any;
  all_partner_name: any;
  all_com_physicians_name: any;
  public mask: string = "(999) 000-00-00-00";

  constructor(
    private service: PhysicianService,
    private routes: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    // this.toastr.setRootViewContainerRef(vcr);
    this.FormData = this.formBuilder.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      user_login: ["", Validators.required],
      user_email: [
        "",
        [
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
        ]
      ],
      user_pass: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}/)
        ]
      ],
      //user_pass: ['', [Validators.required, Validators.minLength(6)]],
      con_pass: ["", [Validators.required, this.equalto("user_pass")]],
      mobile: [""],
      user_url: [""],
      address: [""],
      state: [""],
      clinic_add: [""],
      fax: [""],
      anthr_fax: [""],
      cell_phone: [""],
      manager_contact_name: [""],
      manager_cell_number: [""],
      npi_api: [""],
      assign_clinic: [""],
      combine_phy_acc: [""],
      sales_rep: [""],
      assign_partner: [""],
      clinic_name: [""],
      assign_specialty: [""],
      report_recive_way_fax: [""],
      report_recive_way_email: [""],
      user_status: [""],
      wound_positive_send: ["No"],
      account_id: [""],
    });

    this.user_role = localStorage.getItem("userrole");
    this.user_id = localStorage.getItem("userid");
  }

  get_data() {
    this.sendData = {
      user_id: this.user_id
    };
    this.service.get_all_dropdown_data(this.sendData).subscribe(data => {
      this.response = data;
      if (this.response.status === "1") {
        this.userDetails = this.response.details;
        this.all_assign_clinic = this.response.assign_clinic;
        this.all_sales_rep = this.response.all_sales_rep;
        this.all_partner_name = this.response.all_partner_name;
        this.all_com_physicians_name = this.response.all_com_physicians_name;
      } else {
      }
    });
  }

  ngOnInit() {
    if (localStorage.getItem("userrole") === "sales") {
      if (localStorage.getItem("view_popup") !== "Yes") {
        console.log(localStorage.getItem("view_popup"));
        this.routes.navigate(["/sales-training"]);
        return false;
      }
    }
    this.user_status = "Active";
    this.report_recive_way_fax = "Fax";
    this.report_recive_way_email = "Email";
    this.get_data();
  }

  add_data() {
    this.sendData = {
      first_name: this.FormData.value.first_name,
      last_name: this.FormData.value.last_name,
      user_login: this.FormData.value.user_login,
      user_email: this.FormData.value.user_email,
      user_pass: this.FormData.value.user_pass,
      con_pass: this.FormData.value.con_pass,
      mobile: this.FormData.value.mobile,
      user_url: this.FormData.value.user_url,
      address: this.FormData.value.address,

      state: this.FormData.value.state,
      clinic_add: this.FormData.value.clinic_add,
      fax: this.FormData.value.fax,
      anthr_fax: this.FormData.value.anthr_fax,
      cell_phone: this.FormData.value.cell_phone,
      manager_contact_name: this.FormData.value.manager_contact_name,
      manager_cell_number: this.FormData.value.manager_cell_number,
      npi_api: this.FormData.value.npi_api,
      assign_clinic: this.FormData.value.assign_clinic,
      combine_phy_acc: this.FormData.value.combine_phy_acc,
      sales_rep: this.FormData.value.sales_rep,
      assign_partner: this.FormData.value.assign_partner,
      clinic_name: this.FormData.value.clinic_name,
      assign_specialty: this.FormData.value.assign_specialty,
      report_recive_way_fax: this.FormData.value.report_recive_way_fax,
      report_recive_way_email: this.FormData.value.report_recive_way_email,
      user_status: this.FormData.value.user_status,
      wound_positive_send: this.FormData.value.wound_positive_send,
      user_id: this.user_id, 
      account_id: this.FormData.value.account_id,
    };

    this.service.add_physician(this.sendData).subscribe(
      data => {
        this.response = data;
        if (this.response.status === "1") {
          this.showSuccess(this.response.message);
          this.routes.navigate(["/physician"]);
        } else {
          this.showError(this.response.message);
        }
      },
      error => {
        this.showError(
          "Something want wrong. Please Try again after sometime OR Contact with administrator."
        );
      }
    );
  }

  equalto(repassword): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[repassword] === input;
      if (!isValid) {
        return { equalTo: { isValid } };
      } else {
        return null;
      }
    };
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
