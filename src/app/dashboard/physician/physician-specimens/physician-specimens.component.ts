import { Component, OnInit, OnDestroy } from "@angular/core";
import { PhysicianService } from "src/app/dashboard/physician/physician.service";
import { Router, ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { getLocale } from "ngx-bootstrap/chronos/public_api";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { setTheme } from "ngx-bootstrap/utils";

@Component({
  selector: "app-physician-specimens",
  templateUrl: "./physician-specimens.component.html",
  styleUrls: ["./physician-specimens.component.css"]
})
export class PhysicianSpecimensComponent implements OnInit, OnDestroy {
  [x: string]: any;

  specimen_list: any = [];
  public data: any;
  total_physician: any;
  total_charged_amt: any;
  total_paid_amt: any;
  notFount: boolean;

  searchForm: FormGroup;
  sendData: any;
  succMessage: any;

  user_role: any;
  user_id: any;
  physician_id: any;
  physician_name: any;
  countData: any;

  constructor(
    private service: PhysicianService,
    private activatedRoute: ActivatedRoute,
    private routes: Router,
    private http: HttpClient,
    private spinner: NgxSpinnerService
  ) {
    this.searchForm = new FormGroup({
      from: new FormControl(""),
      to: new FormControl(""),
      test_type: new FormControl("")
    });
    setTheme("bs3");
    this.user_role = localStorage.getItem("userrole");
    this.user_id = localStorage.getItem("userid");
    this.physician_id = this.activatedRoute.snapshot.paramMap.get("id");
  }
  search_from_date: any;
  from_date: any;
  search_to_date: any;
  to_date: any;

  ngOnInit() {
    this.spinner.show();
    this.physician_specimens();
  }

  physician_specimens() {
    this.search_from_date = localStorage.getItem("search_from_date");
    this.search_to_date = localStorage.getItem("search_to_date");

    if (this.search_from_date != "") {
      this.from_date = this.search_from_date;
    } else {
      this.from_date = "";
    }
    if (this.search_to_date != "") {
      this.to_date = this.search_to_date;
    } else {
      this.to_date = "";
    }
    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id,
      physician_id: this.physician_id,
      from: this.from_date,
      to: this.to_date,
      test_type: ""
    };

    this.service.physician_specimen_details(this.sendData).subscribe(data => {
      this.specimen_list = data;

      if (this.specimen_list.status === "0") {
        this.physician_name = this.specimen_list.physician_name;
        this.physician_fax = this.specimen_list.physician_fax;
        this.physician_clinic = this.specimen_list.physician_clinic;
        this.physician_office_number = this.specimen_list.physician_office_number;
        this.total_specimen_count = this.specimen_list.total_specimen_count;
        this.from_date = this.from_date;
        this.to_date = this.to_date;
        this.notFount = true;
        this.countData = 0;
        this.spinner.hide();
      } else {
        this.data = this.specimen_list.specimens_data;
        this.physician_name = this.specimen_list.physician_name;
        this.physician_fax = this.specimen_list.physician_fax;
        this.physician_clinic = this.specimen_list.physician_clinic;
        this.physician_office_number = this.specimen_list.physician_office_number;
        this.total_specimen_count = this.specimen_list.total_specimen_count;
        this.histo_report_added_count = this.specimen_list.histo_report_added_count;
        this.histo_pending_report_count = this.specimen_list.histo_pending_report_count;
        this.total_histo_added = this.specimen_list.total_histo_added;
        this.pcr_pending_report = this.specimen_list.pcr_pending_report;
        this.pcr_reports_count = this.specimen_list.total_pcr_reports_count;
        this.total_added_pcr_reports_count = this.specimen_list.total_added_pcr_reports_count;
        this.from_date = this.from_date;
        this.to_date = this.to_date;
        this.countData = this.data.length;
        this.notFount = false;
        this.spinner.hide();
      }
    });
  }

  ngOnDestroy() {
    localStorage.removeItem("search_from_date");
    localStorage.removeItem("search_to_date");
  }

  // search data
  searchData() {
    this.spinner.show();
    localStorage.removeItem("search_from_date");
    localStorage.removeItem("search_to_date");

    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id,
      physician_id: this.physician_id,
      from: this.searchForm.value.from,
      to: this.searchForm.value.to,
      test_type: this.searchForm.value.test_type
    };

    this.service.physician_specimen_details(this.sendData).subscribe(data => {
      this.specimen_list = data;

      if (this.specimen_list.status === "0") {
        this.notFount = true;
        this.histo_report_added_count = 0;
        this.pcr_pending_report = 0;
        this.histo_pending_report_count = 0;
        this.pcr_reports_count = 0;
        this.total_specimen_count = 0;
        this.total_added_pcr_reports_count = 0;
        this.from_date = this.sendData.from;
        this.to_date = this.sendData.to;
        this.countData = 0;
        this.spinner.hide();
      } else {
        this.data = this.specimen_list.specimens_data;
        this.countData = this.data.length;
        this.physician_name = this.specimen_list.physician_name;
        this.physician_fax = this.specimen_list.physician_fax;
        this.physician_clinic = this.specimen_list.physician_clinic;
        this.physician_office_number = this.specimen_list.physician_office_number;
        this.total_specimen_count = this.specimen_list.total_specimen_count;
        this.histo_report_added_count = this.specimen_list.histo_report_added_count;
        this.histo_pending_report_count = this.specimen_list.histo_pending_report_count;
        this.total_histo_added = this.specimen_list.total_histo_added;
        this.pcr_pending_report = this.specimen_list.pcr_pending_report;
        this.pcr_reports_count = this.specimen_list.total_pcr_reports_count;
        this.total_added_pcr_reports_count = this.specimen_list.total_added_pcr_reports_count;
        this.from_date = this.sendData.from;
        this.to_date = this.sendData.to;
        this.notFount = false;
        this.spinner.hide();
      }
    });
  }
}
