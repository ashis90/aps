import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicianSpecimensComponent } from "./PhysicianSpecimensComponent";

describe('PhysicianSpecimensComponent', () => {
  let component: PhysicianSpecimensComponent;
  let fixture: ComponentFixture<PhysicianSpecimensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicianSpecimensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicianSpecimensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
