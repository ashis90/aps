import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { PhysicianService } from "src/app/dashboard/physician/physician.service";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder
} from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-physician-edit",
  templateUrl: "./physician-edit.component.html",
  styleUrls: ["./physician-edit.component.css"]
})
export class PhysicianEditComponent implements OnInit {
  id: any;
  sendData: any;
  regional_details: any = [];
  notFount: boolean;
  userDetails: any;
  FormData: FormGroup;

  // store data in this variable
  user_status: any;
  all_assign_clinic: any = "";
  all_sales_rep: any;
  all_partner_name: any;
  all_com_physicians_name: any;
  report_recive_way_fax: any;
  report_recive_way_email: any;
  report_recive_way_none: any;
  response: Object;
  userRole: any;
  user_role: string;
  account_id: any = "";
  constructor(
    private service: PhysicianService,
    private routes: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private formbuilder: FormBuilder,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {
    this.FormData = this.formbuilder.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      user_login: [""],
      user_email: [
        "",
        Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
      ],
      edit_user_pass: [
        "",
        [
          Validators.minLength(8),
          Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}/)
        ]
      ],
      //edit_user_pass: ["", Validators.minLength(6)],
      edit_con_pass: ["", [this.equalto("edit_user_pass")]],
      mobile: [""],
      user_url: [""],
      brn: [""],
      ban: [""],
      nbaccount: [""],
      commission_percentage: [""],
      state: [""],
      address: [""],

      clinic_add: [""],
      fax: [""],
      anthr_fax: [""],
      cell_phone: [""],
      manager_contact_name: [""],
      manager_cell_number: [""],
      npi_api: [""],
      assign_clinic: [""],
      combine_phy_acc: [""],
      sales_rep: [""],
      assign_partner: [""],
      clinic_name: [""],
      assign_specialty: [""],
      report_recive_way_fax: [""],
      report_recive_way_email: [""],
      report_recive_way_none: [""],
      user_status: [""],
      wound_positive_send: [""],
      postive_wound_send_date: [""],
      account_id: [""]
    });
    this.userRole = localStorage.userrole;
    this.user_role = localStorage.getItem("userrole");
  }

  equalto(repassword): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[repassword] == input;
      if (!isValid) {
        return { equalTo: { isValid } };
      } else {
        return null;
      }
    };
  }

  ngOnInit() {
    this.get_data();
  }

  get_data() {
    this.spinner.show();
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    this.sendData = { id: this.id };

    this.service.physician_details(this.sendData).subscribe(data => {
      this.regional_details = data;
      this.spinner.hide();
      if (this.regional_details.status === "1") {
        this.userDetails = this.regional_details.details;
        console.log(this.userDetails);
        this.all_assign_clinic = this.regional_details.assign_clinic;
        this.all_sales_rep = this.regional_details.all_sales_rep;
        this.all_partner_name = this.regional_details.all_partner_name;
        this.all_com_physicians_name = this.regional_details.all_com_physicians_name;

        this.FormData.patchValue({
          first_name: this.userDetails.fname,
          last_name: this.userDetails.lname,
          user_login: this.userDetails.user_login,
          user_email: this.userDetails.email,
          mobile: this.userDetails.mob,
          user_url: this.userDetails.url,
          address: this.userDetails.add,
          brn: this.userDetails.bank_routing_num,
          ban: this.userDetails.bank_account_num,
          nbaccount: this.userDetails.name_on_bank,
          commission_percentage: this.userDetails.commission_percentage,
          state: this.userDetails.state,
          clinic_add: this.userDetails.clinic_add,
          fax: this.userDetails.fax,
          anthr_fax: this.userDetails.anthr_fax,
          cell_phone: this.userDetails.cell,
          manager_contact_name: this.userDetails.manager,
          manager_cell_number: this.userDetails.manager_cell,
          npi_api: this.userDetails.npi_api,
          assign_clinic: this.userDetails.assign_clinic,
          combine_phy_acc: this.userDetails.combined_by,
          sales_rep: this.userDetails.sales_rep,
          assign_partner: this.userDetails.assign_partner,
          clinic_name: this.userDetails.clinic_name,
          assign_specialty: this.userDetails.assign_specialty,
          wound_positive_send: this.userDetails.wound_positive_send,
          postive_wound_send_date: this.userDetails.postive_wound_send_date,
          account_id: this.userDetails.account_id
        });

        if (this.userDetails.report_recive_way_fax) {
          this.report_recive_way_fax = this.userDetails.report_recive_way_fax;
        } else {
          this.report_recive_way_fax = "";
        }

        if (this.userDetails.report_recive_way_email) {
          this.report_recive_way_email = this.userDetails.report_recive_way_email;
        } else {
          this.report_recive_way_email = "";
        }
        if (this.userDetails.report_recive_way_none) {
          this.report_recive_way_none = this.userDetails.report_recive_way_none;
        } else {
          this.report_recive_way_none = "";
        }

        if (this.userDetails.user_status) {
          this.user_status = this.userDetails.user_status;
        } else {
          this.user_status = "";
        }

        this.notFount = false;
      } else {
        this.notFount = true;
      }
    });
  }

  update_data() {
    this.sendData = {
      user_id: this.id,
      first_name: this.FormData.value.first_name,
      last_name: this.FormData.value.last_name,
      user_login: this.FormData.value.user_login,
      user_email: this.FormData.value.user_email,
      edit_user_pass: this.FormData.value.edit_user_pass,
      edit_con_pass: this.FormData.value.edit_con_pass,
      mobile: this.FormData.value.mobile,
      user_url: this.FormData.value.user_url,
      brn: this.FormData.value.brn,
      ban: this.FormData.value.ban,
      nbaccount: this.FormData.value.nbaccount,
      commission_percentage: this.FormData.value.commission_percentage,
      address: this.FormData.value.address,
      sec_address: this.FormData.value.clinic_add,
      fax: this.FormData.value.fax,
      anthr_fax: this.FormData.value.anthr_fax,
      cell_phone: this.FormData.value.cell_phone,
      manager_contact_name: this.FormData.value.manager_contact_name,
      manager_cell_number: this.FormData.value.manager_cell_number,
      npi_api: this.FormData.value.npi_api,
      assign_clinic: this.FormData.value.assign_clinic,
      combine_phy_acc: this.FormData.value.combine_phy_acc,
      sales_rep: this.FormData.value.sales_rep,
      assign_partner: this.FormData.value.assign_partner,
      clinic_name: this.FormData.value.clinic_name,
      assign_specialty: this.FormData.value.assign_specialty,
      report_recive_way_fax: this.FormData.value.report_recive_way_fax,
      report_recive_way_email: this.FormData.value.report_recive_way_email,
      report_recive_way_none: this.FormData.value.report_recive_way_none,
      user_status: this.FormData.value.user_status,
      wound_positive_send: this.FormData.value.wound_positive_send,
      account_id: this.FormData.value.account_id
    };
    if (this.FormData.status === "VALID") {
      if (this.FormData.value.user_email !== "") {
        this.service.update_data(this.sendData).subscribe(data => {
          this.regional_details = data;
          if (this.regional_details.status === "1") {
            this.showSuccess(this.regional_details.message);
            this.get_data();
            this.notFount = true;
          } else {
            this.showSuccess(this.regional_details.message);
            this.get_data();
            this.notFount = false;
          }
        });
      } else {
        this.toastr.info("Please enter a email.", "Error", {
          timeOut: 3000
        });
      }
    } else {
      this.toastr.info("Please fill all required fields.", "Error", {
        timeOut: 3000
      });
    }
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
