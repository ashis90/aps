import { Component, OnInit } from "@angular/core";
import { PhysicianService } from "src/app/dashboard/physician/physician.service";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  FormBuilder
} from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { setTheme } from "ngx-bootstrap/utils";
import { ToastrService } from "ngx-toastr";
import { map, startWith } from "rxjs/operators";
import { ExcelService } from "../../../excel.service";

@Component({
  selector: "app-physician",
  templateUrl: "./physician.component.html",
  styleUrls: ["./physician.component.css"]
})
export class PhysicianComponent implements OnInit {
  // all variables
  // datePickerConfig: Partial<BsDatepickerConfig>;
  statusSelect = "";
  monthSelect = "";
  yearSelect = "";
  limit: number;
  loadmorehide: boolean;
  physician_list: any = [];
  public data: any;
  notFount: boolean;
  Year: Date;
  currentYear: any;
  allyear: any = [];
  searchForm: FormGroup;
  sendData: any;
  succMessage: any;
  total_physician: number;
  bsInlineValue = new Date();
  todayDate = "";
  maxDate: Date;
  statusMessage: string = "loading...";
  datePickerConfig: { containerClass: string; dateInputFormat: string };
  specimen_count_W: any;
  specimen_count_NF: any;
  exportData: any;
  newExeclData: any;

  // for comfirmation message
  public popoverTitle: string =
    '<i class="fa fa-trash-o"></i> Delete Physician';
  public popoverMessage: string =
    "Are you really <b>sure</b> you want to do this?";
  public cancelClicked: boolean = false;
  searchText: string;
  user_role: string;
  user_id: string;
  total_showing_users: any;
  response: any;
  filterData: any;
  // user_access: boolean = false;
  constructor(
    private service: PhysicianService,
    private routes: Router,
    private http: HttpClient,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private excelService: ExcelService
  ) {
    this.searchForm = this.fb.group({
      status: [""],
      state: [""],
      fname: [""],
      from_date: [""],
      to_date: [""],
      searchText: [""]
    });
    setTheme("bs3");
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate());
    this.datePickerConfig = Object.assign(
      {},
      {
        containerClass: "theme-blue",
        dateInputFormat: "MM/DD/YYYY"
      }
    );

    this.user_role = localStorage.getItem("userrole");
    this.user_id = localStorage.getItem("userid");
    // if (this.user_role === 'aps_sales_manager') {
    // } else {
    //   this.routes.navigate(['/page-not-found']);
    // }
  }

  // get data from service
  get_data() {
    this.spinner.show();
    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id
    };
    this.service.physicianList(this.sendData).subscribe(
      data => {
        this.physician_list = data;

        if (this.physician_list.status === "0") {
          this.loadmorehide = true;
          this.statusMessage = "No Data Found.";
          this.notFount = true;
          this.spinner.hide();
        } else {
          this.data = this.physician_list.physicians_details;
          this.specimen_count_W = this.physician_list.specimen_count_W;
          this.specimen_count_NF = this.physician_list.specimen_count_NF;
          this.total_showing_users = this.data.length;
          this.notFount = false;
          this.spinner.hide();
        }
      },
      error => {
        this.statusMessage =
          "Something want wrong. Please Try again after sometime OR Contact with administrator.";
        this.spinner.hide();
      }
    );
  }

  total_num_of_physicians() {
    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id
    };

    this.service.total_num_of_physicians(this.sendData).subscribe(data => {
      this.physician_list = data;
      if (this.physician_list.status === "0") {
        this.notFount = true;
      } else {
        this.total_physician = this.physician_list.physicians_count;
        this.notFount = false;
      }
    });
  }

  // show 10 data in view (first time load)
  ngOnInit() {
    if (localStorage.getItem("userrole") === "sales") {
      if (localStorage.getItem("view_popup") !== "Yes") {
        console.log(localStorage.getItem("view_popup"));
        this.routes.navigate(["/sales-training"]);
        return false;
      }
    }
    this.spinner.show();
    //this.loadmorehide = true;
    this.loadmorehide = false;

    this.limit = 0;
    this.Year = new Date();
    this.currentYear = this.Year.getFullYear();
    for (let i = 2017; i <= this.currentYear; i++) {
      this.allyear.push(i);
    }
    if (
      this.user_role === "aps_sales_manager" ||
      this.user_role === "data_entry_oparator" ||
      this.user_role === "regional_manager" ||
      this.user_role === "sales" ||
      this.user_role === "partner"
    ) {
      //this.loadmorehide = true;
      this.loadmorehide = false;
      this.total_num_of_physicians();
    }
    this.get_data();
  }
  resetForm() {
    this.searchForm.reset();
    this.get_data();
  }

  search_filter(term: string) {
    let allData: any;
    allData = this.physician_list.physicians_details;
    if (!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x =>
        x.full_name
          .trim()
          .toLowerCase()
          .includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }
  }

  // show load more data in view
  loadMore() {
    this.spinner.show();
    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id,
      limit: this.limit += 10
    };

    if (this.total_physician === this.total_showing_users) {
      this.showError("No more data found");
      this.loadmorehide = true;
      this.spinner.hide();
      return false;
    }

    this.service.physicianListAll(this.sendData).subscribe(
      data => {
        this.response = data;
        if (this.response.status === "0") {
          this.notFount = true;
          this.statusMessage = "No Data Found.";
          this.spinner.hide();
        } else {
          this.response.physicians_details.forEach(element => {
            this.data.push(element);
          });
          this.total_showing_users = this.data.length;
          this.specimen_count_NF = 0;
          this.specimen_count_W = 0;
          for (var i = 0; i < this.data.length; i++) {
            if (this.data[i].totalSpecimen_NF) {
              this.specimen_count_NF += parseInt(this.data[i].totalSpecimen_NF);
            }
            if (this.data[i].totalSpecimen_W) {
              this.specimen_count_W += parseInt(this.data[i].totalSpecimen_W);
            }
          }
          this.notFount = false;
          this.spinner.hide();
        }
      },
      error => {
        this.statusMessage =
          "Something want wrong. Please Try again after sometime OR Contact with administrator.";
        this.spinner.hide();
      }
    );
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim().toLowerCase();
  }

  // search data
  searchData() {
    this.loadmorehide = true;
    if (
      this.searchForm.value.status === "" &&
      this.searchForm.value.from_date === "" &&
      this.searchForm.value.to_date === "" &&
      this.searchForm.value.fname === "" &&
      this.searchForm.value.state === ""
    ) {
      this.showError("Please enter any value.");
      return;
    }
    this.spinner.show();
    this.sendData = {
      status: this.searchForm.value.status,
      fname: this.searchForm.value.fname,
      state: this.searchForm.value.state,
      from_date: this.convertDate(this.searchForm.value.from_date),
      to_date: this.convertDate(this.searchForm.value.to_date),
      user_role: this.user_role,
      user_id: this.user_id
    };
    localStorage.setItem(
      "search_from_date",
      this.convertDate(this.searchForm.value.from_date)
    );
    localStorage.setItem(
      "search_to_date",
      this.convertDate(this.searchForm.value.to_date)
    );

    this.service.search_physician_list(this.sendData).subscribe(
      data => {
        this.physician_list = data;
        if (this.physician_list.status === "0") {
          this.total_showing_users = 0;
          this.notFount = true;
          this.statusMessage = "No Data Found.";
          this.spinner.hide();
        } else {
          this.data = this.physician_list.physicians_details;
          this.total_showing_users = this.data.length;
          this.notFount = false;
          this.spinner.hide();
          this.specimen_count_NF = 0;
          this.specimen_count_W = 0;
          for (var i = 0; i < this.data.length; i++) {
            if (this.data[i].totalSpecimen_NF) {
              this.specimen_count_NF += parseInt(this.data[i].totalSpecimen_NF);
            }
            if (this.data[i].totalSpecimen_W) {
              this.specimen_count_W += parseInt(this.data[i].totalSpecimen_W);
            }
          }
        }
      },
      error => {
        this.statusMessage =
          "Something want wrong. Please Try again after sometime OR Contact with administrator";
        this.spinner.hide();
      }
    );
  }

  // change user status
  active_inactive(status, user_id) {
    if (confirm("Are you sure to change status ??")) {
      this.spinner.show();
      this.succMessage = "";
      this.sendData = {
        status: status,
        user_id: user_id
      };

      this.service.change_user_status(this.sendData).subscribe(data => {
        this.physician_list = data;
        if (this.physician_list.status === "0") {
          this.ngOnInit();
          this.showError(this.physician_list.message);
          this.notFount = true;
        } else {
          this.data = this.physician_list.physicians_details;
          this.ngOnInit();
          this.showSuccess(this.physician_list.message);
          this.notFount = false;
          this.spinner.hide();
        }
      });
    }
  }

  delete_phy(user_id: string) {
    if (user_id) {
      this.spinner.show();
      this.succMessage = "";
      this.sendData = {
        user_id: user_id
      };

      this.service.delete_physician(this.sendData).subscribe(data => {
        this.physician_list = data;
        if (this.physician_list.status === "0") {
          this.ngOnInit();
          this.showError(this.physician_list.message);
          this.notFount = true;
        } else {
          this.data = this.physician_list.physicians_details;
          this.ngOnInit();
          this.showSuccess(this.physician_list.message);
          this.notFount = false;
          this.spinner.hide();
        }
      });
    }
  }

  exportPhysicianDataForSales() {
    this.spinner.show();
    if (this.user_id) {
      this.succMessage = "";
      this.sendData = {
        user_id: this.user_id,
        from_date: this.convertDate(this.searchForm.value.from_date),
        to_date: this.convertDate(this.searchForm.value.to_date),
        status: this.searchForm.value.status,
        fname: this.searchForm.value.fname,
        state: this.searchForm.value.state
      };
      let exdata: any = [];
      this.service
        .export_physician_data_for_sales(this.sendData)
        .subscribe(data => {
          this.exportData = data;
          console.log(this.exportData);
          if (this.exportData.status === "0") {
            this.ngOnInit();
            this.showError(this.exportData.message);
            this.notFount = true;
          } else {
            //console.log(this.data);
            this.newExeclData = data;
            this.spinner.hide();
            if (this.newExeclData["status"] === "1") {
              let exportData = this.newExeclData.api_data;
              exportData.forEach((item, indx) => {
                console.log(item);
                exdata.push(item);
              });
              if (exdata.length > 0) {
                this.excelService.exportAsExcelFile(exdata, "physician");
              }
            } else if (this.newExeclData["status"] === "0") {
              this.toastr.info(this.newExeclData.err_msg, "Error", {
                timeOut: 3000
              });
            }
            //this.showSuccess(this.exportData.message);
            this.notFount = false;
            this.spinner.hide();
          }
        });
    }
  }

  trackByFn(index, item) {
    return item ? item.team_manager_name : undefined; // or item.id
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = "";
    } else {
      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
