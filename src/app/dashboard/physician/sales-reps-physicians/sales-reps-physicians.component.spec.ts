import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesRepsPhysiciansComponent } from './sales-reps-physicians.component';

describe('SalesRepsPhysiciansComponent', () => {
  let component: SalesRepsPhysiciansComponent;
  let fixture: ComponentFixture<SalesRepsPhysiciansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesRepsPhysiciansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesRepsPhysiciansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
