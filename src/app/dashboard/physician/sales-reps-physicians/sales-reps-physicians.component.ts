import { Component, OnInit } from "@angular/core";
import { PhysicianService } from "src/app/dashboard/physician/physician.service";
import { Router, ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { getLocale } from "ngx-bootstrap/chronos/public_api";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-sales-reps-physicians",
  templateUrl: "./sales-reps-physicians.component.html",
  styleUrls: ["./sales-reps-physicians.component.css"]
})
export class SalesRepsPhysiciansComponent implements OnInit {
  physician_list: any = [];
  public data: any;
  total_physician: any;
  total_charged_amt: any;
  total_paid_amt: any;
  notFount: boolean;
  Year: Date;
  currentYear: any;
  allyear: any = [];
  searchForm: FormGroup;
  sendData: any;
  succMessage: any;
  todayDate = "";
  user_role: any;
  user_id: any;
  sales_reps_id: any;
  statusSelect: string = "";
  monthSelect: string = "";
  yearSelect: string = "";
  sales_man_name: any;
  regional_man_name: any;
  specimen_count: any;
  search_from_date: any;
  constructor(
    private service: PhysicianService,
    private activatedRoute: ActivatedRoute,
    private routes: Router,
    private http: HttpClient,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    this.searchForm = new FormGroup({
      status: new FormControl(""),
      search_value: new FormControl(""),
      from_month: new FormControl(""),
      from_year: new FormControl("")
    });
    this.user_role = localStorage.getItem("userrole");
    this.user_id = localStorage.getItem("userid");
    this.sales_reps_id = this.activatedRoute.snapshot.paramMap.get("id");
  }

  ngOnInit() {
    this.spinner.show();
    this.Year = new Date();
    this.currentYear = this.Year.getFullYear();
    for (let i = 2017; i <= this.currentYear; i++) {
      this.allyear.push(i);
    }

    if (this.sales_reps_id) {
      this.get_physician_data_by_sales_reps_id();
    } else {
      this.get_sales_reps_physician();
    }
  }

  get_physician_data_by_sales_reps_id() {
    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id,
      sales_reps_id: this.sales_reps_id
    };

    this.service
      .physicians_list_by_sales_reps(this.sendData)
      .subscribe(data => {
        this.physician_list = data;
        if (this.physician_list.status === "0") {
          this.sales_man_name = this.physician_list.sales_man_name;
          this.regional_man_name = this.physician_list.regional_man_name;
          this.notFount = true;
          this.spinner.hide();
        } else {
          this.data = this.physician_list.physicians_data;
          this.total_physician = this.physician_list.total_physician;
          this.total_charged_amt = this.physician_list.total_charged_amt;
          this.specimen_count = this.physician_list.specimen_count;
          this.total_paid_amt = this.physician_list.total_paid_amt;
          this.sales_man_name = this.physician_list.sales_man_name;
          this.regional_man_name = this.physician_list.regional_man_name;
          this.notFount = false;
          this.spinner.hide();
        }
      });
  }

  get_sales_reps_physician() {
    this.sendData = {
      user_role: this.user_role,
      user_id: this.user_id
    };

    this.service
      .physicians_list_by_sales_reps(this.sendData)
      .subscribe(data => {
        this.physician_list = data;
        if (this.physician_list.status === "0") {
          this.notFount = true;
        } else {
          this.data = this.physician_list.sales_reps_data;
          this.notFount = false;
          this.spinner.hide();
        }
      });
  }
  // search data
  searchData() {
    this.spinner.show();
    this.sendData = {
      status: this.searchForm.value.status,
      from_month: this.searchForm.value.from_month,
      from_year: this.searchForm.value.from_year,
      search_value: this.searchForm.value.search_value,
      user_role: this.user_role,
      user_id: this.user_id,
      sales_reps_id: this.sales_reps_id
    };

    this.service
      .sales_reps_search_physician_list(this.sendData)
      .subscribe(data => {
        this.physician_list = data;
        if (this.physician_list.status === "0") {
          this.spinner.hide();
          this.notFount = true;
        } else {
          localStorage.setItem("search_from_date", this.total_physician = this.physician_list.from_date);
          localStorage.setItem("search_to_date", this.total_physician = this.physician_list.to_date);

          this.data = this.physician_list.physicians_data;
          this.total_physician = this.physician_list.total_physician;
          this.total_charged_amt = this.physician_list.total_charged_amt;
          this.total_paid_amt = this.physician_list.total_paid_amt;
          this.specimen_count = this.physician_list.specimen_count;
          this.notFount = false;
          this.spinner.hide();
        }
      });
  }

  // change user status
  active_inactive(status, user_id) {
    this.spinner.show();
    this.succMessage = "";
    this.sendData = {
      status: status,
      user_id: user_id
    };

    this.service.change_user_status(this.sendData).subscribe(data => {
      this.physician_list = data;
      if (this.physician_list.status === "0") {
        this.ngOnInit();
        this.succMessage = this.physician_list.message;
        this.notFount = true;
      } else {
        this.data = this.physician_list.physicians_data;
        this.total_physician = this.physician_list.total_physician;
        this.total_charged_amt = this.physician_list.total_charged_amt;
        this.total_paid_amt = this.physician_list.total_paid_amt;
        this.ngOnInit();
        this.succMessage = this.physician_list.message;
        this.notFount = false;
      }
    });
  }

  delete_user(user_id: string) {
    if (user_id) {
      this.spinner.show();
      this.succMessage = "";
      this.sendData = {
        user_id: user_id
      };

      this.service.delete_physician(this.sendData).subscribe(data => {
        this.physician_list = data;
        if (this.physician_list.status === "0") {
          this.ngOnInit();
          this.showError(this.physician_list.message);
          this.notFount = true;
        } else {
          this.data = this.physician_list.physicians_details;
          this.ngOnInit();
          this.showSuccess(this.physician_list.message);
          this.notFount = false;
          this.spinner.hide();
        }
      });
    }
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
