import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PhysicianService } from 'src/app/dashboard/physician/physician.service';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-physician-view',
  templateUrl: './physician-view.component.html',
  styleUrls: ['./physician-view.component.css']
})
export class PhysicianViewComponent implements OnInit {

  id: any;
  sendData: any;
  regional_details: any = [];
  notFount: boolean;
  userDetails: any;

constructor(private service: PhysicianService, private routes: Router, private http: HttpClient,
   private activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.sendData = { 'id': this.id, };

  this.service.physician_details(this.sendData).subscribe( data => {
    this.regional_details = data;
      if (this.regional_details.status === '1') {
        this.userDetails = this.regional_details.details;
        this.notFount = false;
        this.spinner.hide();
      } else {
        this.notFount = true;
        this.spinner.hide();
      }
 });
  }

}
