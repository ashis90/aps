import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { Headers, RequestOptions } from "@angular/http";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class PhysicianService {
  public baseUrl = environment.baseUrl + "Physician/";
  headers: Headers = new Headers();
  options: any;

  constructor(private http: HttpClient) {
    this.headers.append(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    this.headers.append("Access-Control-Allow-Origin", "*");
    this.headers.append("Accept", "application/json");
    this.options = new RequestOptions({ headers: this.headers });
  }
  physicianList(sendData) {
    return this.http.post(
      this.baseUrl + "top_new_physicians_list",
      JSON.stringify(sendData),
      this.options
    );
  }

  physicianListAll(sendData) {
    return this.http.post(
      //this.baseUrl + "all_physicians_list",
      this.baseUrl + "top_new_physicians_list",
      JSON.stringify(sendData),
      this.options
    );
  }

  total_num_of_physicians(sendData) {
    return this.http.post(
      this.baseUrl + "total_num_of_physicians",
      JSON.stringify(sendData),
      this.options
    );
  }

  search_physician_list(sendData) {
    return this.http.post(
      this.baseUrl + "search_physician_list",
      JSON.stringify(sendData),
      this.options
    );
  }

  physician_details(sendData) {
    return this.http.post(
      this.baseUrl + "physician_details",
      JSON.stringify(sendData),
      this.options
    );
  }

  delete_physician(sendData) {
    return this.http.post(
      this.baseUrl + "delete_physician",
      JSON.stringify(sendData),
      this.options
    );
  }

  update_data(sendData) {
    return this.http.post(
      this.baseUrl + "update_physician_details",
      JSON.stringify(sendData),
      this.options
    );
  }

  change_user_status(sendData) {
    return this.http.post(
      this.baseUrl + "change_user_status",
      JSON.stringify(sendData),
      this.options
    );
  }

  get_all_dropdown_data(sendData) {
    return this.http.post(
      this.baseUrl + "get_all_dropdown_data",
      JSON.stringify(sendData),
      this.options
    );
  }

  add_physician(sendData) {
    return this.http.post(
      this.baseUrl + "add_physician",
      JSON.stringify(sendData),
      this.options
    );
  }

  physicians_list_by_sales_reps(sendData) {
    return this.http.post(
      this.baseUrl + "physicians_list_by_sales_reps",
      JSON.stringify(sendData),
      this.options
    );
  }

  sales_reps_search_physician_list(sendData) {
    return this.http.post(
      this.baseUrl + "sales_reps_search_physician_list",
      JSON.stringify(sendData),
      this.options
    );
  }

  physician_specimen_details(sendData) {
    return this.http.post(
      this.baseUrl + "physician_specimen_details",
      JSON.stringify(sendData),
      this.options
    );
  }

  getSpecimenCountForPhysicians(sendData) {
    return this.http.post(
      this.baseUrl + "specimen_count_for_physicians",
      JSON.stringify(sendData),
      this.options
    );
  }

  partnerPhysicianList(sendData) {
    return this.http.post(
      this.baseUrl + "partner_physicians_list",
      JSON.stringify(sendData),
      this.options
    );
  }

  search_partner_physician_list(sendData) {
    return this.http.post(
      this.baseUrl + "search_partner_physician_list",
      JSON.stringify(sendData),
      this.options
    );
  }
  
  export_physician_data_for_sales(sendData) {
    return this.http.post(
      this.baseUrl + "exportPhysicianDataForSales",
      JSON.stringify(sendData),
      this.options
    );
  }
}
