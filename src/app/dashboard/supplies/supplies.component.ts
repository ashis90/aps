import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { SuppliesService } from 'src/app/dashboard/supplies/supplies.service';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl, AbstractControl, ValidatorFn, FormBuilder} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-supplies',
  templateUrl: './supplies.component.html',
  styleUrls: ['./supplies.component.css']
})
export class SuppliesComponent implements OnInit {
  user_role: string;
  FormData: FormGroup;
  user_id: string;
  sendData: any;
  response: any;
  details: any;
  user_email: string;
  phy_address: any;
  physi_address: string;
  addressValid: boolean;

  constructor(private service: SuppliesService, private routes: Router,
    private http: HttpClient, private activatedRoute: ActivatedRoute,
     private spinner: NgxSpinnerService, private formbuilder: FormBuilder, private toastr: ToastrService) {
    this.user_role = localStorage.getItem('userrole');
    this.user_id = localStorage.getItem('userid');
    this.user_email = localStorage.getItem('user_email');
    this.FormData = this.formbuilder.group({
      email: [''],
      supplies_address: [''],
      physician_address: [''],
      diff_address: [''],
    });
  }


  get_data () {
    this.sendData = { 'user_id': this.user_id, };
    this.service.supplies_details(this.sendData).subscribe( data => {
      this.response = data;
      if (this.response.status === '1') {
        this.phy_address = this.response.phy_address;
        this.FormData.patchValue({
          'email': this.user_email,
          'supplies_address': this.response.address,
        });
        this.spinner.hide();
      } else {
        this.spinner.hide();
      }
    });
  }

  ngOnInit() {
    if(localStorage.getItem('userrole') === 'sales'){
      if(localStorage.getItem('view_popup') !== 'Yes'){
        console.log(localStorage.getItem('view_popup'))
        this.routes.navigate(["/sales-training"]);
        return false;
      }
    }
    this.physi_address = '';
    this.addressValid = false;
    this.spinner.show();
    this.get_data();
  }



  send_supplies_mail() {
    this.sendData = {
      'user_id': this.user_id,
      'email': this.user_email,
      'supplies_address': this.FormData.value.supplies_address,
      'physician_address': this.FormData.value.physician_address,
      'diff_address': this.FormData.value.diff_address,
   };
   if (this.FormData.value.supplies_address === '' &&
     this.FormData.value.physician_address === '' && this.FormData.value.diff_address === '') {
     this.showError('Please enter your address.');
     this.addressValid = true;
     return;
   } else {
        this.service.send_supplies_mail(this.sendData).subscribe( data => {
          this.response = data;
              if (this.response.status === '1') {
                this.addressValid = false;
                this.showSuccess(this.response.message);
              } else {
                this.showError(this.response.message);
              }
        });
   }
  }




  showSuccess(message) {
    this.toastr.success(message, 'Success');
  }

  showError(message) {
    this.toastr.error(message, 'Oops!');
  }

}
