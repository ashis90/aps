import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SuppliesService {

  public baseUrl = environment.baseUrl + 'Supplies/';
  headers: Headers = new Headers;
  options: any;



              constructor(private http: HttpClient) {
                this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
                this.headers.append( 'Access-Control-Allow-Origin', '*');
                this.headers.append('Accept', 'application/json');
                this.options = new RequestOptions({ headers: this.headers });
              }


  supplies_details(sendData) {
    return this.http.post(this.baseUrl + 'supplies_details', JSON.stringify(sendData), this.options );
  }

  send_supplies_mail(sendData) {
    return this.http.post(this.baseUrl + 'send_supplies_mail', JSON.stringify(sendData), this.options );
  }

  sendSurveyDetails(sendData) {
    return this.http.post(this.baseUrl + 'sendSurveyDetails', JSON.stringify(sendData), this.options);
  }

}
