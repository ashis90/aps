import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class CommissionDataEntryService {
	public baseUrl = environment.baseUrl + 'CommissionDataEntry/';
	headers: Headers = new Headers();
	options: any;

	constructor(private http: HttpClient) {
		this.headers.append(
			'Access-Control-Allow-Headers',
			'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		);
		this.headers.append('Access-Control-Allow-Origin', '*');
		this.headers.append('Accept', 'application/json');
		this.options = new RequestOptions({ headers: this.headers });
	}

	commission_physician_details(sendData) {
		return this.http.post(this.baseUrl + 'physicians_list', JSON.stringify(sendData), this.options);
	}
	commission_data_entry_update(sendData) {
		return this.http.post(this.baseUrl + 'commission_data_entry_update', JSON.stringify(sendData), this.options);
	}

	last_date_update(sendData) {
		return this.http.post(this.baseUrl + 'last_date_update', JSON.stringify(sendData), this.options);
	}

	importCommissionData(sendData) {
		return this.http.post(this.baseUrl + 'import_commission_data', JSON.stringify(sendData), this.options);
	}
}
