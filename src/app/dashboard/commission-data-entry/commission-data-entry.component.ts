import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommissionDataEntryService } from 'src/app/dashboard/commission-data-entry/commission-data-entry.service';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import * as XLSX from 'xlsx';

@Component({
	selector: 'app-commission-data-entry',
	templateUrl: './commission-data-entry.component.html',
	styleUrls: ['./commission-data-entry.component.css']
})
export class CommissionDataEntryComponent implements OnInit {
	@ViewChild("xlFile") xlFile: ElementRef;
	@ViewChild("commissionYear") commissionYear: ElementRef;
	@ViewChild("commissionMonth") commissionMonth: ElementRef;
	sendData: any;
	user_id: any;
	physicians_count: string;
	response_data: any;
	meta_key: any;
	response: any = [];
	searchText: string;
	current_date: any;
	message: string;
	searchForm: FormGroup;
	user_role: string;
	public data: any;
	notFount: boolean;
	commission_data: boolean;
	paid_data: boolean;
	public show_paid_data: any;
	public show_commission_data: any;
	last_data_entry_date: any;
	filterData: any;
	Year: Date;
	currentYear: any;
	allyear: any = [];
	yearSelect: any;
	allMonth: any = [];
	dataEntryYear:any;
	file:any;
	arrayBuffer: any;
	error_import: any;
	successfull_import:any;
	msg:any;

	constructor(
		private service: CommissionDataEntryService,
		private routes: Router,
		private http: HttpClient,
		private activatedRoute: ActivatedRoute,
		private formBuilder: FormBuilder,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService
	) {
		this.searchForm = this.formBuilder.group({
			serach_physician_name: ['']
		});

		this.user_role = localStorage.getItem('userrole');
		this.user_id = localStorage.getItem('userid');
	}

	ngOnInit() {
		this.allyear = [];
		this.spinner.show();
		this.Year = new Date();
		this.currentYear = this.Year.getFullYear();
		for (let i = 2017; i <= this.currentYear; i++) {
			this.allyear.push(i);
		}
		this.yearSelect = this.currentYear;
		this.dataEntryYear = this.currentYear;
		this.getDataEntryData();

	}

	chooseNewYear(e){
		this.dataEntryYear = e.target.value;
	}

	displayMonth(apiMonth: any) {
		let monthList: any = [];
		if (apiMonth < this.yearSelect) {
			for (let j = 1; j <= 12; j++) {
				monthList.push(j);
			}
		} else if (apiMonth == this.yearSelect) {
			let currentMonth = this.Year.getMonth() + 1;
			for (let k = 1; k <= currentMonth; k++) {
				monthList.push(k);
			}
		}
		return monthList;

	}

	getDataEntryData() {
		this.sendData = {
			serach_physician_name: ''
		};
		this.spinner.show();
		this.service.commission_physician_details(this.sendData).subscribe((data) => {
			this.response_data = data;
			if (this.response_data.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.data = this.response_data.physicians_details;
				this.last_data_entry_date = this.response_data.last_data_entry_date;
				this.current_date = this.response_data.current_year;
				this.physicians_count = this.response_data.physicians_count;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}

	searchByName() {
		this.spinner.show();
		this.sendData = {
			serach_physician_name: this.searchForm.value.serach_physician_name
		};

		this.service.commission_physician_details(this.sendData).subscribe((data) => {
			this.response_data = data;
			if (this.response_data.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.data = this.response_data.physicians_details;
				this.current_date = this.response_data.current_year;
				this.physicians_count = this.response_data.physicians_count;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}

	search_filter(term: string) {
		let allData: any;
		allData = this.response_data.physicians_details;
		if (!term) {
			this.filterData = this.data;
			this.data = allData;
		} else {
			this.filterData = allData.filter((x) =>
				x.full_name.trim().toLowerCase().includes(term.trim().toLowerCase())
			);
			this.data = this.filterData;
		}
	}

	dataEntryUpdateDate(date) {
		if (date === '') {
			this.showError('Please Select Last Date.');
			return false;
		} else {
			this.sendData = {
				last_date: date,
				user_id: this.user_id
			};

			this.service.last_date_update(this.sendData).subscribe(
				(data) => {
					this.response = data;
					if (this.response.status === '1') {
						this.showSuccess('Last date is successfully updated.');
					} else {
						this.showError('Something went wrong. Please try again.');
					}
				},
				(error) => {
					this.showError(
						'Something went wrong. Please check your internet connection OR Contact with administrator.'
					);
				}
			);
		}
	}

	charged_toggle(user_id: string) {
		this.show_commission_data = user_id;
	}

	paid_toggle(user_id: string) {
		this.show_paid_data = user_id;
	}

	commission_data_update(meta_value: string, user_id: string, meta_key: string) {
		this.spinner.show();
		this.sendData = {
			user_id: user_id,
			meta_key: meta_key,
			meta_value: meta_value
		};

		this.service.commission_data_entry_update(this.sendData).subscribe((data) => {
			this.response = data;
			this.spinner.hide();
			if (this.response.status === '1') {
				this.show_commission_data = false;
				this.show_paid_data = false;
				this.showSuccess(this.response.message);
			} else {
				this.showError(this.response.message);
			}
		});
	}

	showSuccess(message) {
		this.toastr.success(message, 'Success');
	}

	showError(message) {
		this.toastr.error(message, 'Oops!');
	}

	incomingfile(event) {
		this.file = event.target.files[0];
	  }


	  importCommisionData() {

		let commissionYear = this.commissionYear.nativeElement.value;
		let commissionMonth = this.commissionMonth.nativeElement.value;
		if (!commissionYear) {
		  this.toastr.info("Please Select a year.", 'Error', {
			timeOut: 3000
		  });
		}else if (!commissionMonth) {
			this.toastr.info("Please Select a month.", 'Error', {
			  timeOut: 3000
			});
		} else if (this.file) {
		  if (confirm("Want to import excel?")) {
			//this.spinner.show();
			let fileReader = new FileReader();
			let fileData: any;
			fileReader.readAsArrayBuffer(this.file);
			fileReader.onload = (e) => {
			  this.arrayBuffer = fileReader.result;
			  var data = new Uint8Array(this.arrayBuffer);
			  var arr = new Array();
			  for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
			  var bstr = arr.join("");
			  var workbook = XLSX.read(bstr, { type: "binary" });
			  var first_sheet_name = workbook.SheetNames[0];
			  var worksheet = workbook.Sheets[first_sheet_name];
			  fileData = XLSX.utils.sheet_to_json(worksheet, { raw: false });
	
			  fileData.forEach(function (row) {
				Object.keys(row).map((elem, indx) => {
				  row[elem.replace(/\s+/g, '_').toLowerCase()] = row[elem];
				  delete row[elem];
				})
	
			  });
			  console.log(fileData);
			  /* display the result */
			  let params = { 'import_data': fileData, 'commision_year': commissionYear, 'commition_month': commissionMonth };
			  this.service.importCommissionData(params).subscribe(data => {
				this.spinner.hide();
				let postarr;
				postarr = data;
				
				if (postarr['status'] === '1') {
					this.xlFile.nativeElement.value = '';
					this.commissionYear.nativeElement.value = '';
					this.commissionMonth.nativeElement.value = '';
					this.msg = postarr['message'];
					this.error_import = postarr['error_import'];
					this.successfull_import = postarr['successfull_import'];
					this.toastr.info('Data Successfully imported', 'Success', {
						timeOut: 3000
					});
					console.log(this.error_import);
				} else if (postarr['status'] === '0') {
					this.error_import = '';
					this.successfull_import = '';
					this.msg = '';
					this.toastr.info('Data faild to import', 'Error', {
						timeOut: 3000
					});
				}
			  });
	
			}
		  }
		} else {
		  this.toastr.info('Please choose a file.', 'Error', {
			timeOut: 3000
		  });
		}
	
	  }
}
