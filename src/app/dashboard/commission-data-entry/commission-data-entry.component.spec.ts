import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionDataEntryComponent } from './commission-data-entry.component';

describe('CommissionDataEntryComponent', () => {
  let component: CommissionDataEntryComponent;
  let fixture: ComponentFixture<CommissionDataEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionDataEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionDataEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
