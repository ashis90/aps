import { TestBed } from '@angular/core/testing';

import { CommissionDataEntryService } from './commission-data-entry.service';

describe('CommissionDataEntryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommissionDataEntryService = TestBed.get(CommissionDataEntryService);
    expect(service).toBeTruthy();
  });
});
