import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnersCompanyManagerComponent } from './partners-company-manager.component';

describe('PartnersCompanyManagerComponent', () => {
  let component: PartnersCompanyManagerComponent;
  let fixture: ComponentFixture<PartnersCompanyManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnersCompanyManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnersCompanyManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
