import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnersCompanyComponent } from './partners-company.component';

describe('PartnersCompanyComponent', () => {
  let component: PartnersCompanyComponent;
  let fixture: ComponentFixture<PartnersCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnersCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnersCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
