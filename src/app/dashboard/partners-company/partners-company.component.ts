import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { FormGroup } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder
} from "@angular/forms";
import { PartnersCompanySubmittedReportsService } from "../../dashboard/partners-company-submitted-reports/partners-company-submitted-reports.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { environment } from "../../../environments/environment";

@Component({
  selector: "app-partners-company",
  templateUrl: "./partners-company.component.html",
  styleUrls: ["./partners-company.component.css"]
})
export class PartnersCompanyComponent implements OnInit {
  user_role: string;
  user_id: string;
  FormData: FormGroup;
  sendData: any;
  response: any;
  data: any;
  notFount: boolean;
  id: string;
  public url = environment.assetsUrl + "assets/uploads/";

  constructor(
    private service: PartnersCompanySubmittedReportsService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private formbuilder: FormBuilder,
    private toastr: ToastrService
  ) {
    this.user_role = localStorage.getItem("userrole");
    this.user_id = localStorage.getItem("userid");
    //this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.FormData = this.formbuilder.group({
      p_lastname: [""],
      assessioning_num: [""],
      external_id: [""]
      // test_type: ['']
    });
  }

  get_data() {
    if (this.id) {
      this.sendData = { user_id: this.id };
    } else {
      this.sendData = { user_id: this.user_id, user_role: this.user_role };
    }
    this.service.getPartners(this.sendData).subscribe(data => {
      this.response = data;
      if (this.response.status === "1") {
        this.data = this.response.partners_data;
        this.notFount = false;
        this.spinner.hide();
      } else {
        this.notFount = true;
        this.spinner.hide();
      }
    });
  }

  ngOnInit() {
    this.spinner.show();
    this.get_data();
  }
}
