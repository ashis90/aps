import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { RegionalService } from "src/app/dashboard/regional-manager/regional-list/regional.service";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder,
  NgForm
} from "@angular/forms";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-regional-add",
  templateUrl: "./regional-add.component.html",
  styleUrls: ["./regional-add.component.css"]
})
export class RegionalAddComponent implements OnInit {
  sendData: any;
  FormData: FormGroup;
  regional_details: any;
  public mask: string = "(999) 000-00-00-00";
  constructor(
    private service: RegionalService,
    private routes: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {
    this.FormData = this.formBuilder.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      user_login: ["", Validators.required],
      user_email: [
        "",
        [
          Validators.required,
          Validators.pattern("[a-zA-Z-0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$")
        ]
      ],
      user_pass: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}/)
        ]
      ],
      //user_pass: ['', [Validators.required, Validators.minLength(6)]],
      con_pass: ["", [Validators.required, this.equalto("user_pass")]],
      mobile: [""],
      user_url: [""],
      address: [""],
      brn: [""],
      ban: [""],
      nbaccount: [""],
      commission_percentage: [""]
    });
  }

  ngOnInit() {}

  add_data() {
    this.sendData = {
      first_name: this.FormData.value.first_name,
      last_name: this.FormData.value.last_name,
      user_login: this.FormData.value.user_login,
      user_email: this.FormData.value.user_email,
      user_pass: this.FormData.value.user_pass,
      con_pass: this.FormData.value.con_pass,
      mobile: this.FormData.value.mobile,
      user_url: this.FormData.value.user_url,
      address: this.FormData.value.address,
      brn: this.FormData.value.brn,
      ban: this.FormData.value.ban,
      nbaccount: this.FormData.value.nbaccount,
      commission_percentage: this.FormData.value.commission_percentage
    };

    this.service.add_data(this.sendData).subscribe(data => {
      this.regional_details = data;
      if (this.regional_details.status === "1") {
        this.routes.navigate(["/regional-manager"]);
        this.showSuccess(this.regional_details.message);
      } else {
        this.showError(this.regional_details.message);
      }
    });
    // this.FormData.reset();
  }

  equalto(repassword): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[repassword] === input;
      if (!isValid) {
        return { equalTo: { isValid } };
      } else {
        return null;
      }
    };
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
