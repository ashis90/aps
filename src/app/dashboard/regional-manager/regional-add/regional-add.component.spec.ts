import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionalAddComponent } from './regional-add.component';

describe('RegionalAddComponent', () => {
  let component: RegionalAddComponent;
  let fixture: ComponentFixture<RegionalAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionalAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionalAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
