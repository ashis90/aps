import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class RegionalService {
	public baseUrl = environment.baseUrl + 'RegionalManager/';
	headers: Headers = new Headers();
	options: any;

	constructor(private http: HttpClient) {
		this.headers.append(
			'Access-Control-Allow-Headers',
			'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		);
		this.headers.append('Access-Control-Allow-Origin', '*');
		this.headers.append('Accept', 'application/json');
		this.options = new RequestOptions({ headers: this.headers });
	}

	regionalManagersList(sendData): Observable<any> {
		return this.http.post(this.baseUrl + 'regionalManagersList', JSON.stringify(sendData), this.options);
	}

	regionalManagersListAll(sendData): Observable<any> {
		return this.http.post(this.baseUrl + 'regionalManagersListAll', JSON.stringify(sendData), this.options);
	}

	add_data(sendData) {
		return this.http.post(this.baseUrl + 'add_regional_man', JSON.stringify(sendData), this.options);
	}

	search_regional_list(sendData): Observable<any> {
		return this.http.post(this.baseUrl + 'search_regional_list', JSON.stringify(sendData), this.options);
	}

	regional_man_details(sendData) {
		return this.http.post(this.baseUrl + 'regional_man_details', JSON.stringify(sendData), this.options);
	}

	update_data(sendData) {
		return this.http.post(this.baseUrl + 'update_regional_man_details', JSON.stringify(sendData), this.options);
	}

	change_user_status(sendData) {
		return this.http.post(this.baseUrl + 'change_user_status', JSON.stringify(sendData), this.options);
	}

	totalRegionalManagersList(sendData) {
		return this.http.post(this.baseUrl + 'totalRegionalManagersList', JSON.stringify(sendData), this.options);
	}
}
