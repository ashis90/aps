import { Component, OnInit } from '@angular/core';
import { RegionalService } from 'src/app/dashboard/regional-manager/regional-list/regional.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-regional',
	templateUrl: './regional.component.html',
	styleUrls: [ './regional.component.css' ]
})
export class RegionalComponent implements OnInit {
	// all variables
	statusSelect = '';
	monthSelect = '';
	yearSelect = '';
	current_year = '';
	current_month = '';
	regional_list: any = [];
	public data: any;
	notFount: boolean;
	Year: Date;
	currentYear: any;
	allyear: any = [];
	searchForm: FormGroup;
	sendData: any;
	succMessage: any;
	public searchText: string;
	TotalRegionalManager: any;
	limit: number;
	total_showing_users: any;
	total_paid_amount: number;
	total_charged_amount: number;
	loadmorehide: boolean;
	user_role: any;
	user_id: any;
	countTotalRegionalManager: any;

	constructor(
		private service: RegionalService,
		private routes: Router,
		private http: HttpClient,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService,
		private fb: FormBuilder
	) {
		this.searchForm = this.fb.group({
			status: [ '' ],
			from_month: [ '' ],
			from_year: [ '' ],
			fname: [ '' ]
		});
		this.user_role = localStorage.getItem('userrole');
		this.user_id = localStorage.getItem('userid');
	}

	// get data from service
	get_data() {
		this.sendData = {
			user_role: this.user_role,
			user_id: this.user_id
		};

		this.service.regionalManagersList(this.sendData).subscribe((data) => {
			this.regional_list = data;
			if (this.regional_list.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.data = this.regional_list.regional_managers;
				this.total_showing_users = this.data.length;
				this.current_year = this.regional_list.curr_year;
				this.current_month = this.regional_list.curr_month;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}

	totalRegionalManagers() {
		this.sendData = {
			user_id: this.user_id
		};
		this.service.totalRegionalManagersList(this.sendData).subscribe((data) => {
			this.regional_list = data;
			if (this.regional_list.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.countTotalRegionalManager = this.regional_list.regional_managers_count;
				this.total_paid_amount = this.regional_list.total_paid_amt;
				this.total_charged_amount = this.regional_list.total_charged_amt;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}

	// show 10 data in view (first time load)
	ngOnInit() {
		this.spinner.show();
		this.loadmorehide = false;
		this.limit = 0;
		this.Year = new Date();
		this.currentYear = this.Year.getFullYear();
		for (let i = 2017; i <= this.currentYear; i++) {
			this.allyear.push(i);
		}

		// this.TotalRegionalManager();
		if (this.user_id) {
			this.get_data();
			this.totalRegionalManagers();
		}
	}

	// show load more data in view
	loadMore() {
		this.spinner.show();
		this.sendData = {
			user_role: this.user_role,
			user_id: this.user_id,
			limit: (this.limit += 10)
		};

		if (this.countTotalRegionalManager === this.total_showing_users) {
			this.showError('No more data found');
			this.loadmorehide = true;
			this.spinner.hide();
			return false;
		}

		this.service.regionalManagersListAll(this.sendData).subscribe((data) => {
			this.regional_list = data;
			if (this.regional_list.status === '0') {
				this.notFount = true;
				this.spinner.hide();
			} else {
				this.regional_list.regional_managers.forEach((element) => {
					this.data.push(element);
				});
				this.total_showing_users = this.data.length;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}

	// search data
	searchData() {
		if (
			this.searchForm.value.status === '' &&
			this.searchForm.value.from_month === '' &&
			this.searchForm.value.from_year === '' &&
			this.searchForm.value.fname === ''
		) {
			this.showError('Please enter any value.');
			return;
		}

		this.spinner.show();
		this.sendData = {
			status: this.searchForm.value.status,
			from_month: this.searchForm.value.from_month,
			from_year: this.searchForm.value.from_year,
			fname: this.searchForm.value.fname
		};

		this.service.search_regional_list(this.sendData).subscribe((data) => {
			this.regional_list = data;
			if (this.regional_list.status === '0') {
				this.notFount = true;
				this.loadmorehide = true;
				this.total_showing_users = 0;
				this.spinner.hide();
			} else {
				this.data = this.regional_list.regional_managers;
				this.total_showing_users = this.data.length;
				this.total_paid_amount = this.regional_list.total_paid_amt;
				this.total_charged_amount = this.regional_list.total_charged_amt;
				this.loadmorehide = true;
				this.notFount = false;
				this.spinner.hide();
			}
		});
	}

	// change user status
	active_inactive(status, user_id) {
		this.spinner.show();
		this.succMessage = '';
		this.sendData = {
			status: status,
			user_id: user_id
		};

		this.service.change_user_status(this.sendData).subscribe((data) => {
			this.regional_list = data;
			if (this.regional_list.status === '0') {
				this.get_data();
				this.succMessage = this.regional_list.message;
				this.notFount = true;
			} else {
				this.data = this.regional_list.regional_managers;
				this.get_data();
				this.succMessage = this.regional_list.message;
				this.notFount = false;
				setTimeout(() => {
					this.spinner.hide();
				}, 10);
			}
		});
	}

	trackByFn(index, item) {
		return item ? item.team_manager_name : undefined; // or item.id
	}

	showSuccess(message) {
		this.toastr.success(message, 'Success');
	}

	showError(message) {
		this.toastr.error(message, 'Oops!');
	}
}
