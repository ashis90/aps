import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RegionalService } from 'src/app/dashboard/regional-manager/regional-list/regional.service';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray, FormControl} from '@angular/forms';


@Component({
  selector: 'app-regional-view',
  templateUrl: './regional-view.component.html',
  styleUrls: ['./regional-view.component.css']
})
export class RegionalViewComponent implements OnInit {

  id: any;
  sendData: any;
  regional_details: any = [];
  notFount: boolean;
  userDetails: any;

constructor(private service: RegionalService, private routes: Router, private http: HttpClient, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.sendData = { 'id': this.id, };

  this.service.regional_man_details(this.sendData).subscribe( data => {
    this.regional_details = data;
      if (this.regional_details.status === '1') {
        this.userDetails = this.regional_details.details;
        this.notFount = false;
      } else {
        this.notFount = true;
      }
 });
  }


}
