import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { RegionalService } from "src/app/dashboard/regional-manager/regional-list/regional.service";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder
} from "@angular/forms";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-regional-edit",
  templateUrl: "./regional-edit.component.html",
  styleUrls: ["./regional-edit.component.css"]
})
export class RegionalEditComponent implements OnInit {
  id: any;
  sendData: any;
  regional_details: any = [];
  notFount: boolean;
  userDetails: any;
  FormData: FormGroup;

  // store data in this variable
  user_status: any;
  succMessage: any;
  errMessage: any;
  public mask: string = "(999) 000-00-00-00";
  constructor(
    private service: RegionalService,
    private routes: Router,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private formbuilder: FormBuilder
  ) {
    this.FormData = formbuilder.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      user_login: [""],
      user_email: [
        "",
        [
          Validators.required,
          Validators.pattern("[a-zA-Z-0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$")
        ]
      ],
      edit_user_pass: [
        "",
        [
          Validators.minLength(8),
          Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}/)
        ]
      ],
      //edit_user_pass: ['', Validators.minLength(6)],
      edit_con_pass: ["", [this.equalto("edit_user_pass")]],
      mobile: [""],
      user_url: [""],
      address: [""],
      brn: [""],
      ban: [""],
      nbaccount: [""],
      commission_percentage: [""],
      user_status: [""]
    });
  }

  equalto(repassword): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[repassword] == input;
      if (!isValid) {
        return { equalTo: { isValid } };
      } else {
        return null;
      }
    };
  }

  ngOnInit() {
    this.get_data();
  }

  get_data() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    this.sendData = { id: this.id };

    this.service.regional_man_details(this.sendData).subscribe(data => {
      this.regional_details = data;
      if (this.regional_details.status === "1") {
        this.userDetails = this.regional_details.details;

        this.FormData.patchValue({
          first_name: this.userDetails.fname,
          last_name: this.userDetails.lname,
          user_login: this.userDetails.user_login,
          user_email: this.userDetails.email,
          mobile: this.userDetails.mob,
          user_url: this.userDetails.url,
          address: this.userDetails.add,
          brn: this.userDetails.bank_routing_num,
          ban: this.userDetails.bank_account_num,
          nbaccount: this.userDetails.name_on_bank,
          commission_percentage: this.userDetails.commission_percentage
        });

        if (this.userDetails.user_status) {
          this.user_status = this.userDetails.user_status;
        } else {
          this.user_status = "";
        }

        this.notFount = false;
      } else {
        this.notFount = true;
      }
    });
  }

  update_data() {
    // if (this.FormData.value.edit_user_pass === this.FormData.value.edit_con_pass) {
    this.sendData = {
      user_id: this.id,
      first_name: this.FormData.value.first_name,
      last_name: this.FormData.value.last_name,
      user_login: this.FormData.value.user_login,
      user_email: this.FormData.value.user_email,
      edit_user_pass: this.FormData.value.edit_user_pass,
      edit_con_pass: this.FormData.value.edit_con_pass,
      mobile: this.FormData.value.mobile,
      user_url: this.FormData.value.user_url,
      brn: this.FormData.value.brn,
      ban: this.FormData.value.ban,
      nbaccount: this.FormData.value.nbaccount,
      commission_percentage: this.FormData.value.commission_percentage,
      address: this.FormData.value.address,
      user_status: this.FormData.value.user_status
    };

    this.service.update_data(this.sendData).subscribe(data => {
      this.regional_details = data;
      if (this.regional_details.status === "1") {
        this.showSuccess(this.regional_details.message);
        this.get_data();
        this.notFount = true;
      } else {
        this.showSuccess(this.regional_details.message);
        this.get_data();
        this.notFount = false;
      }
    });
  }

  showSuccess(message) {
    this.toastr.success(message, "Success");
  }

  showError(message) {
    this.toastr.error(message, "Oops!");
  }
}
