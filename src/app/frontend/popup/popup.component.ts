import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/frontend/login/login.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  modalreference: any;
  text_message: any;
  constructor(public modalRef: BsModalRef, private itemsService: LoginService,
     private routes: Router, private http: HttpClient, private toastr: ToastrService, private fb:FormBuilder) {
    this.modalreference = modalRef;
  }

  ngOnInit() {
  }

}
