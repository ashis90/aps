import { Component, OnInit } from "@angular/core";
import { ServicesService } from "./services.service";

@Component({
  selector: "app-services",
  templateUrl: "./services.component.html",
  styleUrls: ["./services.component.css"]
})
export class ServicesComponent implements OnInit {
  AllService: any = [];

  constructor(private services: ServicesService) {}

  ngOnInit() {
    let servicevalue;
    this.services.LoadData().subscribe(service => {
      servicevalue = service;
      this.AllService = servicevalue.service[0].site_page_desc;
    });
  }
}
