import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/frontend/login/login.service';
import { NotificationsService } from 'src/app/dashboard/notifications/notifications.service';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	postsArray: any = [];
	usertoken;
	token;
	user_role: string;
	user_id: string;
	sendData: any;
	reponse: any;
	notFount: boolean;
	incom_notification: any;
	com_notification: any;
	total_incom_notification: string;
	total_com_notification: string;
	status: boolean = false;
	assetsUrl = environment.assetsUrl;
	username: string;

	constructor(
		private service: LoginService,
		private servic2: NotificationsService,
		private routes: Router,
		private http: HttpClient,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService
	) {
		this.user_role = localStorage.getItem('userrole');
		this.user_id = localStorage.getItem('userid');
		this.username = localStorage.getItem('username');
	}

	get_data() {
		this.sendData = {
			user_role: this.user_role,
			user_id: this.user_id
		};
		this.servic2.get_unread_notifications(this.sendData).subscribe((data) => {
			this.reponse = data;
			if (this.reponse.status === '0') {
				this.total_incom_notification = this.reponse.count_incom;
				this.total_com_notification = this.reponse.count_com;
				this.notFount = true;
			} else {
				this.incom_notification = this.reponse.incom_notification;
				this.com_notification = this.reponse.com_notification;
				this.total_incom_notification = this.reponse.count_incom;
				this.total_com_notification = this.reponse.count_com;
				this.notFount = false;
			}
		});
	}

	ngOnInit() {
		this.usertoken = localStorage.getItem('usertoken');
		if (this.usertoken !== null || this.usertoken !== '') {
			if (
				this.user_role === 'aps_sales_manager' ||
				this.user_role === 'sales_regional_manager' ||
				this.user_role === 'data_entry_oparator' ||
				this.user_role === 'team_regional_Sales' ||
				this.user_role === 'team_sales_manager' ||
				this.user_role === 'sales'
			) {
				this.get_data();
			}
		}
	}

	logout() {
		this.service.islogout(this.usertoken).subscribe((data) => {
			this.postsArray = data;
			if (this.postsArray.status === '1') {
				localStorage.removeItem('usertoken');
				localStorage.clear();
				this.routes.navigate(['/login']);
			} else {
				this.routes.navigate(['/login']);
			}
		});
	}

	read_unread(id: string) {
		this.sendData = { note_id: id, user_id: this.user_id };
		this.servic2.read_unread(this.sendData).subscribe((data) => {
			this.reponse = data;
			if (this.reponse.status === '0') {
			} else {
				this.ngOnInit();
			}
		});
	}

	change_status(id: string) {
		this.sendData = { note_id: id, user_id: this.user_id };
		this.servic2.change_status(this.sendData).subscribe((data) => {
			this.reponse = data;
			if (this.reponse.status === '0') {
				this.showError('Something is worn. Please try again!');
			} else {
				this.ngOnInit();
				this.showSuccess('You are completed a notification.');
			}
		});
	}

	showSuccess(message) {
		this.toastr.success(message, 'Success');
	}

	showError(message) {
		this.toastr.error(message, 'Oops!');
	}

	clickEvent() {
		this.status = !this.status;
	}
}
