import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class LoginService {
	public baseUrl = environment.baseUrl + 'Login/';
	checklogin;
	headers: Headers = new Headers();
	options: any;
	constructor(private http: HttpClient) {
		this.headers.append(
			'Access-Control-Allow-Headers',
			'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		);
		this.headers.append('Access-Control-Allow-Origin', '*');
		this.headers.append('Accept', 'application/json');
		this.options = new RequestOptions({ headers: this.headers });
	}

	isLogin(sendData) {
		return this.http.post(this.baseUrl + 'login', JSON.stringify(sendData), this.options);
	}

	forgotPassword(sendData) {
		return this.http.post(this.baseUrl + 'forgotPassword', JSON.stringify(sendData), this.options);
	}

	islogout(token) {
		return this.http.post(this.baseUrl + 'logout_user', JSON.stringify(token), this.options);
	}

	// for Notification
	get_notifications(sendData) {
		return this.http.post(this.baseUrl + 'get_notifications', JSON.stringify(sendData), this.options);
	}

	read_unread(sendData) {
		return this.http.post(this.baseUrl + 'read_unread', JSON.stringify(sendData), this.options);
	}

	change_status(sendData) {
		return this.http.post(this.baseUrl + 'change_status', JSON.stringify(sendData), this.options);
	}

	send_mail(data) {
		return this.http.post(this.baseUrl + 'send_contact_mail', JSON.stringify(data), this.options);
	}
}
