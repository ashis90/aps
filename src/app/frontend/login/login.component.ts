import { Component, OnInit } from "@angular/core";
import { LoginService } from "src/app/frontend/login/login.service";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

import {
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormBuilder
} from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  message: string;
  sendData: any;
  postsArray: any = [];
  FormData: FormGroup;
  username: any;
  password: any;
  loginSuccess: boolean = false;
  captchaErrorMessage: any = true;
  captcha_test: boolean = false;
  errorMessage = true;
  //siteKey: any = "6LeQYFsUAAAAADgaAit29PYASxZ4mctzWYxB0v7b"; //https://www.antaresphysicianservices.com/
  siteKey: any = "6LfO3KsUAAAAAKlsSwHOK3JaKmuR5ru_RZX-TWOH"; //local host

  //siteKey: any = "6LcFzmwaAAAAACgmRpkU3mqhmZTeOO0n-f5yWttg"; //http://www.temp-antaresphysicianservices.com/sss

  //siteKey: any = "6LeQYFsUAAAAADgaAit29PYASxZ4mctzWYxB0v7b"; // Live
  // siteKey: any = "6LeihhcaAAAAAMDDVNgqFB-1DWF6jOElumNelLTj"; // Siswork
  //siteKey: any = '6LdL4KkUAAAAAGYqBQ0_aXJ-V3fU3PGG6TUWWKRe'; //For http://deve.sisworknew.com/ability_new/
  //siteKey: any = '6Lfz-QEaAAAAAMn9aGNI2Wt72HagOJa1bIRn6acU'; //For http://044ee92.netsolhost.com/

  constructor(
    private service: LoginService,
    private routes: Router,
    private http: HttpClient,
    private formbuilder: FormBuilder
  ) {
    this.FormData = this.formbuilder.group({
      userName: ["", Validators.required],
      password: ["", Validators.required],
      recaptcha: ["", Validators.required]
    });
  }

  handleSuccess(e) {
    if (e) {
      this.captcha_test = true;
    }
  }
  ngOnInit() {
    if (localStorage.usertoken) {
      this.routes.navigate(["/dashboard"]);
    }

    // if(localStorage.getItem('userrole') === 'sales' || localStorage.getItem('userrole') === 'sales_regional_manager'){
    //   if(localStorage.getItem('view_popup') !== 'Yes'){
    //     console.log(localStorage.getItem('view_popup'))
    //     this.routes.navigate(["/sales-training"]);
    //     return false;
    //   }else{
    //     this.routes.navigate(["/dashboard"]);
    //   }
    // }else{
    //   this.routes.navigate(["/dashboard"]);
    // }
  }

  check() {
    this.message = "";
    this.username = this.FormData.value.userName;
    this.password = this.FormData.value.password;
    this.sendData = { username: this.username, password: this.password };
    this.service.isLogin(this.sendData).subscribe(data => {
      this.postsArray = data;
      if (
        this.postsArray.status === "1" &&
        this.username &&
        this.password &&
        this.captcha_test
      ) {
        localStorage.setItem("usertoken", this.postsArray.details.token);
        localStorage.setItem("username", this.postsArray.details.display_name);
        localStorage.setItem("userrole", this.postsArray.details.user_role);
        localStorage.setItem("userid", this.postsArray.details.id);
        localStorage.setItem("user_login", this.postsArray.details.user_login);
        localStorage.setItem("user_email", this.postsArray.details.user_email);
        //if(this.postsArray.details.user_role ==='sales' || this.postsArray.details.user_role ==='sales_regional_manager'){
        localStorage.setItem("view_popup", this.postsArray.details.view_popup);
        localStorage.setItem(
          "training_timestamp",
          this.postsArray.details.training_timestamp
        );
        //}
        //this.routes.navigate(["/dashboard"]);
        if (
          localStorage.getItem("userrole") === "sales" ||
          localStorage.getItem("userrole") === "sales_regional_manager"
        ) {
          if (localStorage.getItem("view_popup") !== "Yes") {
            console.log(localStorage.getItem("view_popup"));
            this.routes.navigate(["/sales-training"]);
            return false;
          } else {
            this.routes.navigate(["/dashboard"]);
          }
        } else {
          this.routes.navigate(["/dashboard"]);
        }
      } else if (!this.username || !this.password) {
        this.message = "Username and Password connot be blank.";
        this.routes.navigate(["/login"]);
      } else if (!this.captcha_test) {
        this.message = "Captcha can not be blank.";
        this.routes.navigate(["/login"]);
      } else {
        this.message = "Please enter a valid username and password.";
        this.routes.navigate(["/login"]);
      }
    });
  }
}
