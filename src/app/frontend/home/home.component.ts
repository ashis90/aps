import { Component, OnInit } from "@angular/core";
import { HomeService } from "./home.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  AllHome: any = [];

  constructor(private home: HomeService) {}

  ngOnInit() {
    let homevalue;
    this.home.LoadData().subscribe(home => {
      homevalue = home;
      this.AllHome = homevalue.home[0].site_page_desc;
    });
  }
}
