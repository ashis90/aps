import { Component, OnInit } from "@angular/core";
import { AboutusService } from "./aboutus.service";

@Component({
  selector: "app-about",
  templateUrl: "./about.component.html",
  styleUrls: ["./about.component.css"]
})
export class AboutComponent implements OnInit {
  AllStatus: any = [];

  constructor(private aboutusService: AboutusService) {}

  ngOnInit() {
    let data;
    this.aboutusService.LoadData().subscribe(aboutUs => {
      data = aboutUs;
      this.AllStatus = data.aboutUs[0].site_page_desc;
    });
  }
}
