import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'src/app/dashboard/notifications/notifications.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  assetsUrl = environment.assetsUrl + 'assets/images/logo-footer.png';
  constructor(private service: NotificationsService) { }

  ngOnInit() {
    this.sales_notification();
  }

  sales_notification() {
    let params = {'user_id': localStorage.userid};
    this.service.sales_notification(params).subscribe( data => {
   });
  }
  year: number = new Date().getFullYear();
}
