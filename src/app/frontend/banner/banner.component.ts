import { Component, OnInit } from "@angular/core";
import { BannerService } from "./banner.service";

@Component({
  selector: "app-banner",
  templateUrl: "./banner.component.html",
  styleUrls: ["./banner.component.css"]
})
export class BannerComponent implements OnInit {
  AllBanner: any = [];

  constructor(private banner: BannerService) {}

  ngOnInit() {
    let bannerValue;
    this.banner.LoadData().subscribe(banner => {
      bannerValue = banner;
      this.AllBanner = bannerValue.banner[0].site_page_desc;
    });
  }
}
