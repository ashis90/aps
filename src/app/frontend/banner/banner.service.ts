import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Headers, RequestOptions } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class BannerService {
  headers: Headers = new Headers();
  options: any;

  constructor(private http: HttpClient) {
    this.headers.append(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  LoadData() {
    const url = environment.baseUrl + 'Frontapi/';
    // const url = "http://192.168.1.254/project/2018/part1/aps_new/codes/admin/api/Frontapi/get_aps_banner";
    return this.http.get(url + 'get_aps_banner', this.options);
  }
}
