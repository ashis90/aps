import { Component, OnInit } from '@angular/core';
import {SuppliesService } from '../../dashboard/supplies/supplies.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {

  surveyForm: FormGroup;
  constructor(
    private http: HttpClient,
    public itemsService: SuppliesService,
    public route: Router,
    private toastr: ToastrService
  ) {
    this.surveyForm = new FormGroup({
      'option1' : new FormControl('4'),
      'option2' : new FormControl('4'),
      'option3' : new FormControl('4'),
      'option4' : new FormControl('4'),
      'option5' : new FormControl('4'),
      'email' : new FormControl(''),
      'answer_survey' : new FormControl('', Validators.required),
      'submit' : new FormControl('Submit')
     });
  }

  ngOnInit() {
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.surveyForm.get(field).valid && (this.surveyForm.get(field).touched || this.surveyForm.get(field).dirty);
  }

  onSubmit() {
      let post_arr;
        if (this.surveyForm.status === 'VALID') {
          let fmData = Object.assign({}, this.surveyForm.value);
          this.itemsService.sendSurveyDetails(fmData).subscribe( data => {
            post_arr = data;
            if (post_arr['status'] === '1') {
              this.showSuccess(post_arr['message']);
            } else {
              this.showError(post_arr['message']);
            }
          });
        } else {
          this.validateAllFormFields(this.surveyForm);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {
      Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
        }
      });
    }

    showSuccess(message) {
      this.toastr.success(message, 'Success');
    }

    showError(message) {
      this.toastr.error(message, 'Oops!');
    }

}
