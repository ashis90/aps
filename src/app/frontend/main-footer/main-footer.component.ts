import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'src/app/dashboard/notifications/notifications.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-main-footer',
  templateUrl: './main-footer.component.html',
  styleUrls: ['./main-footer.component.css']
})
export class MainFooterComponent implements OnInit {
  assetsUrl = environment.assetsUrl + 'assets/images/logo-footer.png';
  constructor(private service: NotificationsService) { }

  ngOnInit() {
    this.sales_notification();
  }

  sales_notification() {
    let params = { 'user_id': localStorage.userid };
    this.service.sales_notification(params).subscribe(data => {
    });
  }
  year: number = new Date().getFullYear();
}
