import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/frontend/login/login.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-forgot-password',
	templateUrl: './forgot-password.component.html',
	styleUrls: [ './forgot-password.component.css' ]
})
export class ForgotPasswordComponent implements OnInit {
	message: string;
	sendData: any;
	response: any;
	FormData: FormGroup;
	userEmail: any;
	constructor(
		private service: LoginService,
		private routes: Router,
		private http: HttpClient,
		private formbuilder: FormBuilder,
		private toastr: ToastrService
	) {
		this.FormData = this.formbuilder.group({
			userEmail: [ '', [ Validators.required, Validators.email ] ]
		});
	}

	ngOnInit() {}

	sendPassword() {
		this.message = '';
		this.userEmail = this.FormData.value.userEmail;
		this.sendData = { userEmail: this.userEmail };
		this.service.forgotPassword(this.sendData).subscribe((data) => {
			this.response = data;
			if (this.response.status == '1') {
				this.showSuccess(this.response.message);
			} else {
				this.showWarning(this.response.message);
			}
		});
	}

	showSuccess(message) {
		this.toastr.success(message, 'Success');
	}

	showError(message) {
		this.toastr.error(message, 'Oops!');
	}

	showWarning(message) {
		this.toastr.warning(message, 'Oops!');
	}
}
