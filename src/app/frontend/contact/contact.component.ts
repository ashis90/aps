import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/frontend/login/login.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormArray,FormControl,ValidatorFn,AbstractControl,FormBuilder,ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  postsArray:any = [] ;
   contactForm: FormGroup;
   sendData:any;

  constructor(private itemsService: LoginService, private routes: Router, private http: HttpClient,private toastr: ToastrService,private fb:FormBuilder) 
  {
    this.contactForm = this.fb.group({
      name : ['',[Validators.required]],
      email : ['',[Validators.required]],
      message : ['',[Validators.required]],
    });
   }

  ngOnInit() {
  }

  onSubmit()
  {
      if(this.contactForm.status == "VALID")
      {
        this.sendData = {name:this.contactForm.value.name, email:this.contactForm.value.email, subject:this.contactForm.value.subject,message:this.contactForm.value.message};
        this.itemsService.send_mail(this.sendData).subscribe(data =>
          {
            this.postsArray = data;
            if(this.postsArray.status==1)
            {
              this.toastr.info('Thank you for connecting with us!', 'Success', {
                timeOut: 3000
              });
              this.contactForm.reset();
            }

          });
      }
      else
      {
          this.validateAllFormFields(this.contactForm);
      }
}

displayFieldCss(field: string) {
  return {
    'has-error': this.isFieldValid(field),
    'has-feedback': this.isFieldValid(field)
  };
}

isFieldValid(field: string) {
  return !this.contactForm.get(field).valid && (this.contactForm.get(field).touched || this.contactForm.get(field).dirty);
}

validateAllFormFields(formGroup: FormGroup) {         
Object.keys(formGroup.controls).forEach(field => {  
  const control = formGroup.get(field);             
  if (control instanceof FormControl) {             
    control.markAsTouched({ onlySelf: true });
  } else if (control instanceof FormGroup) {        
    this.validateAllFormFields(control);           
  }
});
}

}
