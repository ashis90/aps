import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './frontend/login/login.service';

const MINUTES_UNITL_AUTO_LOGOUT = 240 // in Minutes
const CHECK_INTERVALL = 1000 // in ms
const STORE_KEY = 'lastAction';
@Injectable({
  providedIn: 'root'
})
export class AutoLogoutService {
  deleteArray:any = [];
  message = false;
  loggedIn: boolean=true;
  usertoken;
  postsArray;
  constructor(
    private router: Router,
    private ngZone: NgZone,
    public service:LoginService,
  ) {
    this.check();
    this.initListener();
    this.initInterval();
  }

  get lastAction() {
    return parseInt(localStorage.getItem(STORE_KEY));
  }
  set lastAction(value) {
    localStorage.setItem(STORE_KEY,value.toString());
  }

  initListener() {
    this.ngZone.runOutsideAngular(() => {
      document.body.addEventListener('click', () => this.reset());
    });
  }

  initInterval() {
    this.ngZone.runOutsideAngular(() => {
      setInterval(() => {
        this.check();
      }, CHECK_INTERVALL);
    })
  }

  reset() {
    this.lastAction = Date.now();
  }

  check() {
    const now = Date.now();
    const timeleft = this.lastAction + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
    const diff = timeleft - now;
    const isTimeout = diff < 0;
    this.usertoken = localStorage.getItem('usertoken');
    this.ngZone.run(() => {
      if (isTimeout) {
        this.service.islogout(this.usertoken).subscribe((data) => {
          this.postsArray = data;
          if (this.postsArray.status === '1') {
            localStorage.removeItem('usertoken');
            localStorage.clear();
            this.router.navigate([ '/login' ]);
          } else {
            this.router.navigate([ '/login' ]);
          }
        });
      }
    });
  }
}