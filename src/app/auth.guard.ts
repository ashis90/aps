import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, from } from 'rxjs';
import { LoginService } from './all-service/login.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {
	constructor(private login: LoginService, private router: Router) {}
	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {
		let roles = next.routeConfig.data.roles;
		if (
			localStorage.getItem('usertoken') != null &&
			localStorage.getItem('userrole') != null &&
			localStorage.getItem('userid') != null
		) {
			
			if (roles.indexOf(localStorage.getItem('userrole')) !== -1) {
				return true;
			} else {
				this.router.navigate([ '/page-not-found' ]);
				return false;
			}
		} else {
			this.router.navigate([ '/login' ]);
			return false;
		}
	}
}
