import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./frontend/home/home.component";
import { HeaderComponent } from "./frontend/header/header.component";
import { FooterComponent } from "./frontend/footer/footer.component";
import { AboutComponent } from "./frontend/about/about.component";
import { ContactComponent } from "./frontend/contact/contact.component";
import { ServicesComponent } from "./frontend/services/services.component";
import { AppRoutingModule } from "./app-routing/app-routing.module";
import { LoginComponent } from "./frontend/login/login.component";
import { PageNotFoundComponent } from "./frontend/page-not-found/page-not-found.component";
import { AuthGuard } from "./auth.guard";
import { HttpClientModule } from "@angular/common/http";
import { DashHomeComponent } from "./dashboard/side-menu/dash-home/dash-home.component";
import { DashSidemenuComponent } from "./dashboard/side-menu/dash-sidemenu/dash-sidemenu.component";
import { MyaccountComponent } from "./dashboard/myaccount/myaccount.component";
import { RegionalComponent } from "./dashboard/regional-manager/regional-list/regional.component";
import { SalesComponent } from "./dashboard/sales-manager/sales-list/sales.component";
import { PhysicianComponent } from "./dashboard/physician/physician-list/physician.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RegionalEditComponent } from "./dashboard/regional-manager/regional-edit/regional-edit.component";
import { RegionalViewComponent } from "./dashboard/regional-manager/regional-view/regional-view.component";
import { DataTableModule } from "angular-6-datatable";
// import { SalesManagerComponent } from './dashboard/sales-manager/sales-list/sales.component';
import { SalesEditComponent } from "./dashboard/sales-manager/sales-edit/sales-edit.component";
import { SalesViewComponent } from "./dashboard/sales-manager/sales-view/sales-view.component";
import { PhysicianEditComponent } from "./dashboard/physician/physician-edit/physician-edit.component";
import { PhysicianViewComponent } from "./dashboard/physician/physician-view/physician-view.component";
import { NgxSpinnerModule } from "ngx-spinner";
import { ExcelService } from "./excel.service";

import { TooltipModule } from "ngx-bootstrap/tooltip";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { CommissionComponent } from "./dashboard/sales-manager/commission/commission.component";
import { CommissionHistoryComponent } from "./dashboard/sales-manager/commission-history/commission-history.component";
import { SalesAddComponent } from "./dashboard/sales-manager/sales-add/sales-add.component";
import { RegionalAddComponent } from "./dashboard/regional-manager/regional-add/regional-add.component";
import { PhysicianAddComponent } from "./dashboard/physician/physician-add/physician-add.component";
import { TrainingComponent } from "./dashboard/side-menu/sales-training/training.component";
import { SalesRepsPhysiciansComponent } from "./dashboard/physician/sales-reps-physicians/sales-reps-physicians.component";
import { PhysicianSpecimensComponent } from "./dashboard/physician/physician-specimens/physician-specimens.component";
import { AnnouncementsComponent } from "./dashboard/side-menu/announcements/announcement-list/announcements.component";
import { AnnouncementAddComponent } from "./dashboard/side-menu/announcements/announcement-add/announcement-add.component";
import { AnnouncementEditComponent } from "./dashboard/side-menu/announcements/announcement-edit/announcement-edit.component";
import { CKEditorModule } from "ngx-ckeditor";
import { PopupMessageComponent } from "./dashboard/side-menu/popup-message/popup-message.component";
import { NewsletterComponent } from "./dashboard/side-menu/newsletter/newsletter.component";
import { AnalyticsComponent } from "./dashboard/side-menu/analytics/analytics.component";
import { ModalModule } from "ngx-bootstrap/modal";

// Import angular-fusioncharts
import { FusionChartsModule } from "angular-fusioncharts";
// Import FusionCharts library
import * as FusionCharts from "fusioncharts";
// Load FusionCharts Individual Charts
import * as Charts from "fusioncharts/fusioncharts.charts";
import * as TimeSeries from "fusioncharts/fusioncharts.timeseries";
// Use fcRoot function to inject FusionCharts library, and the modules you want to use
FusionChartsModule.fcRoot(FusionCharts, Charts, TimeSeries);

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";
import { NotificationsComponent } from "./dashboard/notifications/notifications.component";
import { FieldErrorDisplayComponent } from "./field-error-display/field-error-display.component";
import { ConfirmationPopoverModule } from "angular-confirmation-popover";
import { SuppliesComponent } from "./dashboard/supplies/supplies.component";
import { Training2Component } from "./dashboard/side-menu/training/training2.component";
import { SubmittedReportsComponent } from "./dashboard/submitted-reports/submitted-reports.component";
import { WoundSubmittedReportsComponent } from "./dashboard/wound-submitted-reports/wound-submitted-reports.component";
import { SearchPipePipe } from "./pipe/search-pipe.pipe";
import { SurveyComponent } from "./frontend/survey/survey.component";
import { CommissionDataEntryComponent } from "./dashboard/commission-data-entry/commission-data-entry.component";
import { PopupComponent } from "./frontend/popup/popup.component";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { ForgotPasswordComponent } from "./frontend/forgot-password/forgot-password.component";
import { CollectionsComponent } from "./frontend/collections/collections.component";
import { NgxMaskModule } from "ngx-mask";
import { NgxCaptchaModule } from "ngx-captcha";
import { MainHeaderComponent } from "./frontend/main-header/main-header.component";
import { MainFooterComponent } from "./frontend/main-footer/main-footer.component";
import { MatchHeightDirective } from "./match-height.directive";
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatInputModule,
  MatRippleModule
} from "@angular/material";
import { AnnouncementsHomeComponent } from "./dashboard/side-menu/announcements-home/announcements-home.component";
import { AboutusService } from "./frontend/about/aboutus.service";
import { ServicesService } from "./frontend/services/services.service";
import { HomeService } from "./frontend/home/home.service";
import { BannerComponent } from "./frontend/banner/banner.component";
import { BannerService } from "./frontend/banner/banner.service";
import { PartnersCompanySubmittedReportsComponent } from "./dashboard/partners-company-submitted-reports/partners-company-submitted-reports.component";
import { PartnersCompanyComponent } from "./dashboard/partners-company/partners-company.component";
import { PartnersCompanyManagerComponent } from "./dashboard/partners-company-manager/partners-company-manager.component";
import { BillerComponent } from "./dashboard/biller/biller.component";
import { PhysicianAnalyticsComponent } from "./dashboard/side-menu/physician-analytics/physician-analytics.component";
import { AbilityDiagnosticsPartnersComponent } from "./dashboard/ability-diagnostics-partners/ability-diagnostics-partners.component";
import { PartnerPhysiciansComponent } from "./dashboard/ability-diagnostics-partners/partner-physicians/partner-physicians.component";
import { AbilityPartnerEditComponent } from "./dashboard/ability-diagnostics-partners/ability-partner-edit/ability-partner-edit.component";
import { SalesAnalyticsComponent } from './dashboard/side-menu/sales-analytics/sales-analytics.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    DataTableModule,
    NgxSpinnerModule,
    TooltipModule.forRoot(),
    Ng2SearchPipeModule,
    BsDatepickerModule.forRoot(),
    CKEditorModule,
    FusionChartsModule,
    ToastrModule.forRoot(),
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: "danger" // set defaults here
    }),
    NgxCaptchaModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    NgxMaskModule.forRoot()
  ],
  exports: [
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    MatAutocompleteModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    AboutComponent,
    ContactComponent,
    ServicesComponent,
    LoginComponent,
    PageNotFoundComponent,
    DashHomeComponent,
    DashSidemenuComponent,
    MyaccountComponent,
    RegionalComponent,
    SalesComponent,
    PhysicianComponent,
    RegionalEditComponent,
    RegionalViewComponent,
    SalesEditComponent,
    SalesViewComponent,
    PhysicianEditComponent,
    PhysicianViewComponent,
    CommissionComponent,
    CommissionHistoryComponent,
    SalesAddComponent,
    RegionalAddComponent,
    PhysicianAddComponent,
    TrainingComponent,
    SalesRepsPhysiciansComponent,
    PhysicianSpecimensComponent,
    AnnouncementsComponent,
    AnnouncementEditComponent,
    AnnouncementAddComponent,
    PopupMessageComponent,
    NewsletterComponent,
    AnalyticsComponent,
    NotificationsComponent,
    FieldErrorDisplayComponent,
    SuppliesComponent,
    Training2Component,
    SubmittedReportsComponent,
    SearchPipePipe,
    SurveyComponent,
    CommissionDataEntryComponent,
    PopupComponent,
    ForgotPasswordComponent,
    CollectionsComponent,
    MainHeaderComponent,
    MainFooterComponent,
    MatchHeightDirective,
    AnnouncementsHomeComponent,
    BannerComponent,
    PartnersCompanySubmittedReportsComponent,
    PartnersCompanyComponent,
    PartnersCompanyManagerComponent,
    BillerComponent,
    PhysicianAnalyticsComponent,
    AbilityDiagnosticsPartnersComponent,
    PartnerPhysiciansComponent,
    AbilityPartnerEditComponent,
    SalesAnalyticsComponent,
    WoundSubmittedReportsComponent
  ],

  entryComponents: [PopupComponent],
  providers: [
    AuthGuard,
    ExcelService,
    AboutusService,
    ServicesService,
    HomeService,
    BannerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
