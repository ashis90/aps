// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  //  baseUrl: "http://localhost/ability/aps_admin/api/",
  //  assetsUrl: "http://localhost/ability/aps_admin/"

  baseUrl: "https://www.antaresphysicianservices.com/beta-2.0/apsadmin/api/",
  assetsUrl: "https://www.antaresphysicianservices.com/beta-2.0/"

  // baseUrl: "http://apstest.live/apsadmin/api/",
  // assetsUrl: "http://apstest.live/"

  // baseUrl: 'http://deve.sisworknew.com/aps_new/codes/admin/api/',
  // assetsUrl: 'http://deve.sisworknew.com/aps_new/codes/'

  // baseUrl: 'https://antaresphysicianservices.com/beta-2.0/admin/api/',
  // assetsUrl: 'https://antaresphysicianservices.com/beta-2.0/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
