export const environment = {
  production: true,
  // baseUrl: "http://192.168.1.254/project/2018/part1/aps_new/codes/admin-3.0/api/",
  // assetsUrl: "http://192.168.1.254/project/2018/part1/ability_new/codes/abadmin/"

  // baseUrl: 'https://antaresphysicianservices.com/beta/admin/api/',
  // assetsUrl: 'https://antaresphysicianservices.com/beta/'

  // baseUrl: "https://www.antaresphysicianservices.com/apsadmin/api/",
  // assetsUrl: "https://www.antaresphysicianservices.com/apsadmin/"

  baseUrl: "https://www.antaresphysicianservices.com/beta-2.0/apsadmin/api/",
  assetsUrl: "https://www.antaresphysicianservices.com/beta-2.0/apsadmin/"

  // baseUrl: 'http://deve.sisworknew.com/aps_new/codes/admin/api/',
  // assetsUrl: 'http://deve.sisworknew.com/aps_new/codes/'

  //baseUrl: 'https://antaresphysicianservices.com/beta-2.0/admin/api/',
  // assetsUrl: 'https://antaresphysicianservices.com/beta-2.0/'
};
