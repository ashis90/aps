<?php
date_default_timezone_set('Asia/Kolkata');
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");

class MY_Model extends CI_Model {

    public $image_url = '';
 	public function __construct() {
        parent::__construct();
        $this->load->database();
		$this->image_url = base_url('assets/upload/');
	}
	public function customquery($query = '') {
        $this->db->query("SET SQL_BIG_SELECTS=1"); 
        if (!empty($query)) {
            $result = $this->db->query($query)
                    ->result_array();
            return $result;
        }
        return false;
    }
    
    function getTableData($table_name, $condition ="", $field ,$get_data = 1,$order = "", $order_by="" , $limit = "", $start=""){
		$this->db->select($field); 
		$this->db->from($table_name);
		$this->db->where($condition);
		$this->db->order_by($order, $order_by); 
		if($limit!="" &&  $start!=""){
			$this->db->limit($limit, $start); 
		}		
		$query = $this->db->get();
		//echo $this->db->last_query();
		if($query->num_rows())
		{
		    $result = $query->result_array(); 
		    
		    if($get_data == '1'){
			 $table_data = array();
		    foreach ($result as $key => $value) {
		        	 $table_data = $value;
		    }
		}
		else{
			$table_data = $result;
		}
			
		return $table_data;
		}
		
		
	}
    
    function editTableData($table_name, $data, $condition ){
    	if(!empty($table_name)){
		$this->db->where($condition);
		$this->db->update($table_name, $data);
		
		//echo $this->db->last_query();
		//exit;
		if ($this->db->affected_rows() > 0)
		   {
			return $success = 'yes';
		   }
		else
		   {
			return $success = 'no';
		   }
		}
		else{
			return false;
		}
		
	
	 }   
    
    function addTableData($table_name, $data){
    	$data_insert_id = $this->db->insert($table_name, $data);
		return $this->db->insert_id();
    }
    

	function delete_data_id($table_name, $data, $id){
    $this->db->where($data, $id);
    $this->db->delete($table_name);  
 	return $success = "Success";
	 }
	 
	//  function delete__id($table_name, $data, $id){
	// 	$this->db->where($data, $id);
	// 	$this->db->delete($table_name);  
	// 	 return $success = "Success";
	// 	 }
	
	
   function delete_data_condition($table_name, $data){
    $this->db->where($data, $id);
    
    echo $this->db->last_query();
	exit;
    
    $this->db->delete($table_name);  
 	return $success = "Success";
 	}
	
	
//join two table
function get_table_data_by_join($select_data ,$tbale1, $table2, $table1_primary_key, $tbale2_frg_key,$condition_data_field, $id, $extra_condition, $join, $get_data = 1)
 {
	   if($id){
		  $conditions =" ( `".$tbale1.'`.'."`".$condition_data_field."` = ".$id.")".$extra_condition;
		}
		$join_table = $tbale1.".".$table1_primary_key. '=' .$table2.".".$tbale2_frg_key;
	    $this->db->select($select_data);    
		$this->db->from($tbale1);
		$this->db->join($table2, $join_table, $join);
		if($id){
		   $this->db->where($conditions);
		}
		$query = $this->db->get();
		
		/*echo $this->db->last_query();*/
		
		if($query->num_rows())
		{
		    $result = $query->result_array(); 
		    if($get_data == '1'){
			 $table_data = array();
		    foreach ($result as $key => $value) {
		        	 $table_data   = $value;
		    }
		}
		else{
			$table_data = $result;
		}
		return $table_data;
		}
  }  
 
 		 //more than two table join 
  		function join_three_table($select_data ,$tbale1, $table2, $tbale3, $table1_join_key_table2 ,$table1_join_key_table3, $tbale2_join_key_table1, $tbale3_primary_key, $condition_data_field,$id,$join, $get_data = 1)
		{
	    
	    if($id){
		  $conditions =" ( `".$tbale1.'`.'."`".$condition_data_field."` = ".$id.")";
		}
			
			$this->db->select($select_data);
			$this->db->from($tbale1); 
			$this->db->join($table2 , $table2.'.'.$tbale2_join_key_table1 .'='. $tbale1.'.'.$table1_join_key_table2 , $join);
			$this->db->join($tbale3 , $tbale3.'.'.$tbale3_primary_key .'='. $tbale1.'.'.$table1_join_key_table3, $join);
			
			if($id){
			    $this->db->where($conditions);
			}
		
		      
		$query = $this->db->get();
		if($query->num_rows())
		{
		    $result = $query->result_array(); 
		    if($get_data == '1'){
			 $table_data = array();
		    foreach ($result as $key => $value) {
		        	 $table_data   = $value;
		    }
			}
		else{
			$table_data = $result;
		}
		return $table_data;
		}
	}
	
	function get_row($tablename,$condition)
	{
		$this->db->select('*');
		$this->db->from($tablename);		
		$this->db->where($condition);
		$query = $this->db->get();
		return $result = $query->num_rows();
	}

	function get_cancelled($search)
	{
		$specimen_det = "SELECT * FROM (SELECT `id`,`lab_id`,`assessioning_num`,`physician_id`,`p_lastname`,`p_firstname`,`collection_date`,`qc_check`,`patient_address` FROM `wp_abd_specimen` WHERE `physician_accepct` = '0' AND `status` = '0' AND `qc_check` = '0'".$search." )specimen 
						INNER JOIN 
						(SELECT * FROM `wp_abd_specimen_stage_details`)stages ON `specimen`.`id` = `stages`.`specimen_id` WHERE `stages`.`pass_fail` = '0' GROUP BY `stages`.`specimen_id`";
		
		$stage_specimen_results = $this->db->query($specimen_det);
		$spe= $stage_specimen_results->result();
		
		$pcr_stages_specimen_sql = "SELECT * FROM (SELECT `id`,`lab_id`,`assessioning_num`,`physician_id`,`p_lastname`,`p_firstname`,`collection_date`,`qc_check`,`patient_address` FROM `wp_abd_specimen` WHERE `physician_accepct` = '0' AND `status` = '0' AND `qc_check` = '0' ".$search." )specimen 
		INNER JOIN 
		(SELECT `specimen_id`, `stage_det_id`, `stage_id` as `pcr_stage`, `pass_fail` FROM `wp_abd_pcr_stage_details`)pcr_stages ON `specimen`.`id` = `pcr_stages`.`specimen_id` WHERE `pcr_stages`.`pass_fail` = '0' GROUP BY `pcr_stages`.`specimen_id`";
		
		$pcr_stages_specimen_results = $this->db->query($pcr_stages_specimen_sql);	

		$pcr_spe = $pcr_stages_specimen_results->result();
		
		$specimen_results = array_merge($spe, $pcr_spe);	
		return $specimen_results;
		
					
	}
	
	function getUserData($userid)
	{
		$userSql = "SELECT u1.ID,u1.user_login AS username,u1.user_email,u1.user_url,m1.meta_value AS firstname,m2.meta_value AS lastname,m3.meta_value AS role,m4.meta_value AS nickname,m5.meta_value AS mobile,
		m6.meta_value AS fax,m7.meta_value AS address,m8.meta_value AS way_recive  
		FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name') 
		JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name') 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'nickname') 
		JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = '_mobile') 
		JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = 'fax')
		JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = '_address')
		JOIN wp_abd_usermeta m8 ON (m8.user_id = u1.ID AND m8.meta_key = 'report_recive_way')
		WHERE u1.ID='".$userid."'";
		
		$excquery = $this->db->query($userSql);
		$userArr = $excquery->result();
		return $userArr;
	}

	function get_specimen_stages()
	{	
		$sql="";
		$sql= "SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`qc_check` FROM 
		(SELECT `id`,`assessioning_num`,`qc_check` FROM `wp_abd_specimen` WHERE `status` = '0' 
		AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' 
		AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` 
		WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` 
		WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id`";
		$sql.="INNER JOIN 
		(SELECT `specimen_id`, count(*) as `nod` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '1' group by `specimen_id` having count(*) <= '3' )stages_info 
		 ON `specimen`.`id` = `stages_info`.`specimen_id` ORDER BY `specimen`.`id` DESC"; 

		$results = $this->db->query($sql);	

		$specimen_details = $results->result();
		return $specimen_details;

	}


}
?>