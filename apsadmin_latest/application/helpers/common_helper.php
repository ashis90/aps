<?php
if(!defined('BASEPATH')) EXIT("No direct script access allowed");

  if(!function_exists('common_viewloader')){
	function common_viewloader($viewfilepath='',$param=array()){
		$CI = &get_instance();
		$CI->load->view('header');
		
		if($viewfilepath!=''){
			$CI->load->view($viewfilepath,$param);
		}
		$CI->load->view('footer');
	}
}

// display function 
  if(!function_exists('pr')){
	function pr($display_data=array()){
		if(!empty($display_data)){
			echo "<pre>";
			print_r($display_data);
			echo "</pre>";
		}
	}
}

  function short_description($string,$count_value){
		$string = strip_tags($string);
		if (strlen($string) > $count_value) {
		$stringCut = substr($string, 0, $count_value);
		$string = substr($stringCut, 0, strrpos($stringCut, ' ')).' ...'; 
		}
		return $string;
	}

  if(!function_exists('get_user_details')){
	function get_user_details($logged_in_id){
		$CI = & get_instance();	
		if(!empty($logged_in_id)){
		$conditions = " ( `sysadm_id` = '".$logged_in_id."' AND `sysadm_status` = 'Active')";
		$select_fields = '*';
		$is_multy_result = 1;
		$admindata = $CI->BlankModel->getTableData('sysadmin', $conditions, $select_fields, $is_multy_result);
		return $admindata;
		}
	}	
}
  
  if(!function_exists('image_uploads')){
	
	function image_uploads($folder_name, $thumb_Size_width, $thumb_Size_hight, $file_name){		
		    $CI = & get_instance();	
		    //file upload destination
            $config['upload_path'] = './assets/uploads/'.$folder_name;
            $config['allowed_types'] = 'jpg|JPG|png|PNG|JPEG|jpeg|mp4';
            $config['max_size']   = '1000000';
  		    $config['max_width']  = '1024000';
  		    $config['max_height'] = '768000';
            $config['max_filename'] = '255';
            $config['encrypt_name'] = TRUE;
            //thumbnail path
            $thumb_path = './assets/uploads/thumb/';
            //  $thumb_path = './assets/uploads/thumb_new/';
            //store file info once uploaded
            $CI->load->library('upload', $config);
            $CI->load->library('image_lib');
            
            $file_data = array();
            //check for errors
            $is_file_error = FALSE;
            //check if file was selected for upload
           if (!$_FILES) {
                $is_file_error = TRUE;
				$display_error  =  handle_error('Select at least one file.');
            }
          
            //if file was selected then proceed to upload
            if (!$is_file_error) {
                //load the preferences
                //$CI->load->library('upload', $config);
                //check file successfully uploaded. 'file_name' is the name of the input
               
                if (!$CI->upload->do_upload($file_name)) {
                    //if file upload failed then catch the errors
                    $display_error  = handle_error($CI->upload->display_errors());
                    $is_file_error = TRUE;
                 } else {
                	  
                    //store the file info
                    $file_data = $CI->upload->data();
                    if (!is_file($thumb_path . $file_data['file_name'])) {
                        $thumb_config = array(
                            'source_image' => $file_data['full_path'], //get original image
                            'new_image' => $thumb_path,
                            'maintain_ratio' => true,
                            'width' => $thumb_Size_width,
                            'height' => $thumb_Size_hight
                        );
                       // $CI->load->library('image_lib', $config); //load library
                       
                         $CI->image_lib->initialize($thumb_config);
                        $CI->image_lib->resize(); //do whatever specified in config
                    }
                }
            }
            // There were errors, we have to delete the uploaded files
            if ($is_file_error) {
                if ($file_data) {
                    $file = './assets/uploads/'.$folder_name.$file_data['file_name'];
                    if (file_exists($file)) {
                        unlink($file);
                    }
                    $thumb = $thumb_path . $file_data['file_name'];
                    if ($thumb) {
                        unlink($thumb);
                    }
                }
                $message =  $display_error;
            }
		   if (!$is_file_error) {
                $message = $file_data; 
               	} 
		return $message;
	}
	
}

  function handle_error($err) {
     	$CI = & get_instance();	
        $error = $err . "\r\n";
        return $error;
    }
    

 
  if(!function_exists('get_data_anothertable')){
	function get_data_anothertable($id,$tablename,$field_name){
		$CI = & get_instance();	
		if(!empty($id)){
		$conditions = " ( $field_name = '".$id."')";
		$select_fields = '*';
		$is_multy_result = 0;
		$admindata = $CI->BlankModel->getTableData($tablename, $conditions, $select_fields, $is_multy_result);
		return $admindata;
		}
	}
  }   
 
  if(!function_exists('get_nos_rows')){
	function get_nos_rows($tablename,$condition){
		$CI = & get_instance();	
		if(!empty($tablename)){		
		$admindata = $CI->BlankModel->get_row($tablename,$condition);
		return $admindata;
		}
	}
	}
	
  if(!function_exists('get_table_data')){
		function get_table_data($tablename,$conditions){		
			$CI = & get_instance();	
			if(!empty($tablename)){		
			$select_fields = '*';
			$is_multy_result = 1;
			$admindata = $CI->BlankModel->getTableData($tablename, $conditions, $select_fields, $is_multy_result);
			return $admindata;
			}
		}	
	}

  function get_user_role($user_id){
     $CI = & get_instance();	
     $conditions = "`user_id` = '".$user_id."' AND `meta_key` ='wp_abd_capabilities'";
     $sql_get_user_meta = $CI->BlankModel->getTableData('wp_abd_usermeta', $conditions, '*', 1);
     $serialized_data = $sql_get_user_meta['meta_value'];
     $role_array = unserialize($serialized_data);
	 $role='';
	 if( $role_array ){
     foreach($role_array as $key=>$value){
      $role.= $key;
	 }
	}
  return $role;
  }	
  
  function get_users_by_role($role, $select_data){
      $CI = & get_instance();	
     
      $get_users_det = $CI->BlankModel->customquery( "SELECT ".$select_data." FROM wp_abd_users INNER JOIN wp_abd_usermeta ON wp_abd_users.ID = wp_abd_usermeta.user_id WHERE wp_abd_usermeta.meta_key = 'wp_abd_capabilities' AND wp_abd_usermeta.meta_value LIKE '%".$role."%' ORDER BY wp_abd_users.user_nicename");
     
      return $get_users_det;
  }	  
      
  function get_users_by_role_n_status($role, $select_data, $status){
      $CI = & get_instance();	
      $get_users_det = $CI->BlankModel->customquery("SELECT ".$select_data." 
						FROM wp_abd_users AS u
						LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
						LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
						WHERE  um1.meta_key = '_status' AND um1.meta_value = '".$status."'
						AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%".$role."%' ORDER BY u.user_nicename ASC");
    
      return $get_users_det;
  }	  
   
  function get_user_meta($user_id, $key, $single = ""){
      $CI = & get_instance();     
      if(!empty($user_id)){
      $conditions = "`user_id` = ".$user_id." AND `meta_key` = '".$key."'";
      $select_fields = 'meta_value';
	  $is_multy_result = 1;
	  $get_users_det = $CI->BlankModel->getTableData('wp_abd_usermeta', $conditions, $select_fields, $is_multy_result);
      return $get_users_det;
  }else {
  	return false;
  }
  }   

  function get_assign_user($assignerID ='' )
  {
      $CI = & get_instance(); 
      $users_id = array();   
      $conditions = "`meta_value` = ".$assignerID." AND `meta_key` = 'assign_to'";
      $select_fields = 'user_id';
	  $is_multy_result = 0;
	  $get_users_det = $CI->BlankModel->getTableData('wp_abd_usermeta', $conditions, $select_fields, $is_multy_result);
	  if($get_users_det){
	  foreach ($get_users_det as  $value) {
      array_push($users_id,$value['user_id']);
	  }
	 }
      array_push($users_id, $assignerID);
	  $users_id = implode(",", $users_id);
 	  return $users_id;
  }
  
  function get_user_meta_value($user_id, $key, $single = ""){
      $CI = & get_instance();   
      if(!empty($user_id)){  
      $conditions = "`user_id` = ".$user_id." AND `meta_key` = '".$key."'";
      $select_fields = 'meta_value';
	  $is_multy_result = 1;
	  $get_users_det = $CI->BlankModel->getTableData('wp_abd_usermeta', $conditions, $select_fields, $is_multy_result);
      if(!empty($get_users_det)){   
        return $get_users_det['meta_value'];
      }
      else{
        return false;
      }
     }
     else {
  	  return false;
     }
  }

  function get_role($sdata){
     $role_array = unserialize($sdata);
     $role='';
     foreach($role_array as $key=>$value){
     	$role.= $key;
	 }
	 return $role;
   }

  //  function update_user_meta($user_id, $key, $value){
  //     $CI = & get_instance();	     
  //     $update_conditions = "( `user_id` = ".$user_id." AND `meta_key` = '".$key."' )";    
	 //  $update_data = array('meta_value' => $value);	
	 //  $update_user_meta = $CI->BlankModel->editTableData('wp_abd_usermeta', $update_data, $update_conditions);     
  //     return $update_user_meta;
  // }

  function update_user_meta($user_id='', $key='', $val=''){
      $CI = & get_instance();
      if (isset($val) && !empty($val)) {
      	$value = $val;
      } else { $value = ''; }
  
      $check_sql = "select * from `wp_abd_usermeta` where `meta_key` = '".$key."' AND `user_id`= '".$user_id."'";
       $check_result =  $CI->BlankModel->customquery($check_sql);

       if(isset($check_result) && !empty($check_result)){
      $update_conditions = "( `user_id` = ".$user_id." AND `meta_key` = '".$key."' )";
	  $update_data = array('meta_value' => $value);	
	  $update_user_meta = $CI->BlankModel->editTableData('wp_abd_usermeta', $update_data, $update_conditions);     
      return $update_user_meta;
       } else {
       	$insert_data = array(
		  'user_id' => $user_id,
		  'meta_key' => $key,
		  'meta_value' => $value);  
		$add_user_meta = $CI->BlankModel->addTableData('wp_abd_usermeta', $insert_data);     
		return $add_user_meta;
       }
  }

  function add_user_meta($user_id,$key,$value){

  	if(isset($user_id) && !empty($user_id)){
	  	if (isset($value) && !empty($value)) {
	      	$value = $value;
	      } else { $value = ''; }
      
		$CI = & get_instance(); 
		$insert_data = array(
		  'user_id' => $user_id,
		  'meta_key' => $key,
		  'meta_value' => $value);  
		$add_user_meta = $CI->BlankModel->addTableData('wp_abd_usermeta', $insert_data);     
		return $add_user_meta;
	} else {
		return false;
	}
	}
  
  function get_total_user(){
  	  $CI = & get_instance();	
      $get_users_det = $CI->BlankModel->customquery( "SELECT count(*) as totuser FROM wp_abd_users WHERE user_status=0");
	  return $get_users_det[0]['totuser'];
  }
  
  function get_latest_user(){
  	  $CI = & get_instance();	
      $get_users_det = $CI->BlankModel->customquery( "SELECT * FROM wp_abd_users WHERE user_status=0 ORDER BY user_registered desc LIMIT 5");
	  return $get_users_det;
  }  
 
  function get_latest_login(){
  	  $CI = & get_instance();	
      $get_users_det = $CI->BlankModel->customquery( "SELECT u1.ID,u1.user_email,m1.meta_value AS firstname,m2.meta_value AS lastlogincount,m3.meta_value AS lastlogin FROM wp_abd_users u1 JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name') JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'when_last_login_count') JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'when_last_login') ORDER BY m3.meta_value DESC LIMIT 5");
	  return $get_users_det;
  }
  
  function get_role_specialist($role,$speciality){
  	  $CI = & get_instance();
	  	
	$get_users_det = $CI->BlankModel->customquery("SELECT u1.ID,u1.user_email,m1.meta_value AS firstname,m2.meta_value AS speciality,m3.meta_value AS role FROM wp_abd_users u1
	JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name')
	JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'assign_specialty')
	JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') WHERE m2.meta_value='".$speciality."' AND m3.meta_value like '%".$role."%' GROUP BY u1.ID");
	
	return $get_users_det;
  } 
  
  
  function mailBody($bodypart){
		$data='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		</head>
		<body>
		<table width="600" border="0" cellspacing="1" cellpadding="3" align="center" style="border:1px solid #d6d6d6; font:normal 12px/16px Arial, Helvetica, sans-serif; color:#818181;">
		   <tr>
			<td align="center" valign="top" style="height:120px; border-bottom:3px solid #eeefef;"><img src="https://www.abilitydiagnostics.com/abadmin/assets/dist/img/logo.png" alt="Ability Diagnostics" /></td>
		  </tr>
		  <tr>
			<td align="left" valign="top" style="padding:10px 20px 20px 20px; color:#4b4b4b;">
				<table width="100%" border="0" cellspacing="2" cellpadding="0">
				  <tr>
					<td align="left" valign="top">'.$bodypart.'</td>
				  </tr>
				</table>
			</td>
		  </tr>
		  <tr>
			<td align="center" valign="middle" style="height:50px; background-color:#eaf6e2; color:#4b4b4b">Copyright &copy; '.date("Y").' Ability Diagnostics, All Rights Reserved.</td>
		  </tr>
		</table>
		</body>
		</html>';
		
		return $data;
	}

  function sendMail($to="", $subject="", $body="",$from="",$fromname="",$type="",$replyto="",$bcc="",$cc=""){
		if(empty($type))
		{
			$type="html";
		}
		if($type=="plain")
		{
			$body = strip_tags($body);
		}
		if($type=="html")
		{
			$body = "<font face='Verdana, Arial, Helvetica, sans-serif'>".$body."</font>";
		}
		/* To send HTML mail*/ 
		$headers = "MIME-Version: 1.0\r\n"; 
		$headers.= "Content-type: text/".$type."; charset=utf-8 \r\n";
		/* additional headers */ 
		//$headers .= "To: <".$to.">\r\n"; 
		if(!empty($from))
		{
			$headers .= "From: ".$fromname." <".$from.">\r\n";
		}
		if(!empty($replyto))
		{
			$headers .= "Reply-To: <".$replyto.">\r\n"; 
		}
		if(!empty($cc))
		{
			$headers .= "Cc: ".$cc."\r\n";
		}
		if(!empty($bcc))
		{
			$headers .= "Bcc: ".$bcc."\r\n";
		}
		if(@mail($to, $subject, $body, $headers))
		{
			return 1;
		}
		else
		{
			return $headers;
		}
	}
  
  
  /*----------------------Add function by Ashis-----------------------------*/
  
   /**
  * /reg_mgr_get_total_amt
  * @param undefined $reg_mgr_id
  * @param undefined $column_name
  * @param undefined $curr_month
  * @param undefined $curr_year
  */
  
  function reg_mgr_get_total_amt($reg_mgr_id, $column_name, $curr_month, $curr_year){
  	$total_amt = 0;
   	$total_amount = 0;
  	$CI = & get_instance();	
   	$reg_user_role = get_user_role($reg_mgr_id);
  
 	
    $sales_reps_sql = 'SELECT `ID` FROM wp_abd_users u1 
			JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "assign_to") 
			JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 		
	        WHERE (m3.meta_value LIKE "%sales_regional_manager%"  
	        OR m3.meta_value LIKE  "%team_sales_manager%"  
	        OR m3.meta_value LIKE  "%team_regional_Sales%"
	        OR m3.meta_value LIKE  "%sales%")
	        AND (m4.meta_value = "'.$reg_mgr_id.'")';
	
	$sales_reps_data = $CI->BlankModel->customquery($sales_reps_sql);
    $team_reg_id[0]['ID'] = $reg_mgr_id;
    
    if($reg_user_role == "team_regional_Sales" || $reg_user_role == "sales_regional_manager"){	  
	   $sales_reps_data = array_merge($sales_reps_data, $team_reg_id);  
   }
      
    foreach($sales_reps_data as $sales_reps){
		
    $physicians_sql = 'SELECT `ID` FROM wp_abd_users u1 
			JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "added_by") 
			JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 		
	        WHERE (m3.meta_value LIKE "%physician%")
	        AND (m4.meta_value = "'.$sales_reps['ID'].'")';
	        
	$physician_array = $CI->BlankModel->customquery($physicians_sql);
   
    foreach($physician_array as $physicians){
	     $physician_id = $physicians['ID'];      
 	
	 if($column_name == 'chargedamt'){       
         $charged_amount = get_user_meta_value($physician_id, 'month_'.$curr_month.'_'.$curr_year.'_chargedamt', TRUE);
      
	    if(!empty($charged_amount)){
	       $total_amt += $charged_amount;
	      }	   
       }
	 elseif($column_name == 'paidamt'){
		 $paid_amount = get_user_meta_value($physician_id, 'month_'.$curr_month.'_'.$curr_year.'_paidamt', TRUE);
	   
         if(!empty($paid_amount)){
	        $total_amt += $paid_amount;
          }
	   }			
	  }
	}
	return $total_amount = $total_amt;
}
  /**
  * get_total_amt
  * @param undefined $sales_user_id
  * @param undefined $column_name
  * @param undefined $curr_month
  * @param undefined $curr_year

  * @return
  */
  
  function get_total_amt($sales_user_id, $column_name, $curr_month, $curr_year){
  	$total_amt = 0;
  	$total_char_amt = 0;
  	$total_amount = 0;
    $CI = & get_instance();	
												
    $physicians_sql = 'SELECT `ID` FROM wp_abd_users u1 
			JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "added_by") 
			JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 		
	        WHERE (m3.meta_value LIKE "%physician%")
	        AND (m4.meta_value = "'.$sales_user_id.'")';
	        
	$physician_array = $CI->BlankModel->customquery($physicians_sql);
   
    foreach($physician_array as $physicians){	 
	   $physician_id = $physicians['ID'];   
 	
	 if($column_name == 'chargedamt'){       
        $charged_amount = get_user_meta_value($physician_id, 'month_'.$curr_month.'_'.$curr_year.'_chargedamt',TRUE);
   
	   if(!empty($charged_amount)){
	       $total_amt += $charged_amount;
	     }	   
     }
	 elseif($column_name == 'paidamt'){
		$paid_amount = get_user_meta_value($physician_id, 'month_'.$curr_month.'_'.$curr_year.'_paidamt',TRUE);
	
        if(!empty($paid_amount)){
	       $total_amt += $paid_amount;
          }
	   }			
	  }
	return $total_amount = $total_amt;
    
    }


    function get_total_amt_phy($phy_user_id, $column_name, $curr_month, $curr_year){
  	$total_amt = 0;
  	$total_char_amt = 0;
  	$total_amount = 0;
    $CI = & get_instance();	
   
 	
	 if($column_name == 'chargedamt'){
        $charged_amount = get_user_meta_value($phy_user_id, 'month_'.$curr_month.'_'.$curr_year.'_chargedamt',TRUE);
	       $total_amt = $charged_amount; 
     }
	 elseif($column_name == 'paidamt'){
		$paid_amount = get_user_meta_value($phy_user_id, 'month_'.$curr_month.'_'.$curr_year.'_paidamt',TRUE);
	
	       $total_amt = $paid_amount;
	   }
	return $total_amount = $total_amt;
    
    }
  
  
  /*--------------------------------END-------------------------------------*/
  function unique_array($my_array, $key) { 
    $result = array(); 
    $i = 0; 
    $key_array = array(); 
    
    foreach($my_array as $val) { 
        if (!in_array($val[$key], $key_array)) { 
            $key_array[$i] = $val[$key]; 
            $result[$i] = $val; 
        } 
        $i++; 
    } 
    return $result; 
}


/*
		|--------------------------------------------------------------------------
		| curl Add User Table
		|--------------------------------------------------------------------		
		*/


    function add_table_data_curl_users($table,$table_data,$data)
	{
		   $CI = &get_instance();
		   $api_url = $CI->config->item('api_url').'General/curl_add_table_users';
           $objarray=array("table"=>$table,
       						"data" =>$table_data,
       						"details" =>$data
       					);
           // echo json_encode($objarray);die;
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $api_url);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
           curl_setopt($ch, CURLOPT_HEADER, FALSE);
           curl_setopt($ch, CURLOPT_POST,TRUE);
           curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($objarray));
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
           $response = curl_exec($ch);
           curl_close($ch);
           $result=json_decode($response,true);

           return $result['status'];
	} 
		/*
		|--------------------------------------------------------------------------
		| curl Edit User Table
		|--------------------------------------------------------------------		
		*/

	    function edit_table_data_curl_users($table,$data)
	    {

	       $CI = &get_instance();
		   $api_url = $CI->config->item('api_url').'General/curl_edit_table_users';
           $objarray=array("table"=>$table,
       						"data" =>$data,
       					);
           // echo $api_url;
           // echo json_encode($objarray);die;
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $api_url);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
           curl_setopt($ch, CURLOPT_HEADER, FALSE);
           curl_setopt($ch, CURLOPT_POST,TRUE);
           curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($objarray));
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
           $response = curl_exec($ch);
           curl_close($ch);
           $result=json_decode($response,true);

           //print_r($result);die;
           return $result['status'];
	} 

			/*
		|--------------------------------------------------------------------------
		| curl common function
		|--------------------------------------------------------------------		
		*/

	    function common_curl_data($api_url,$data)
	    {

	       $CI = &get_instance();
           $objarray=$data;
           // echo json_encode($objarray);die;
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $api_url);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
           curl_setopt($ch, CURLOPT_HEADER, FALSE);
           curl_setopt($ch, CURLOPT_POST,TRUE);
           curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($objarray));
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
           $response = curl_exec($ch);
           curl_close($ch);
           $result=json_decode($response,true);
           // print_r($result);die;

           return $result;
	} 
		                 /*
    |--------------------------------------------------------------------------
    | common Add Data
    |--------------------------------------------------------------------   
    */

      function common_add_data_curl($table,$data)
     {

       
       $CI = &get_instance();
       $api_url = $CI->config->item('api_url').'ApsCurl/abilityAddTableData';
       $objarray=array(
          "table_name"=>$table,
          "data"=>$data
         );

       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $api_url);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
       curl_setopt($ch, CURLOPT_HEADER, FALSE);
       curl_setopt($ch, CURLOPT_POST,TRUE);
       curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($objarray));
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       $response = curl_exec($ch);
       curl_close($ch);
       
       $result=json_decode($response,true);
   }

?>