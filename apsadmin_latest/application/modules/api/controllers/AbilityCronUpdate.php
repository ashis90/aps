<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class AbilityCronUpdate extends MY_Controller {

    function __construct() {
        parent::__construct();
       date_default_timezone_set('MST7MDT');
    }
    function index()
    {
      echo "string";

    }
     

        /*
      |--------------------------------------------------------------------------
      | APS table update
      |--------------------------------------------------------------------   
      */

       function curl_aps_update(){
        $data = json_decode(file_get_contents('php://input'), true);

         //echo json_encode(array('status'=>$data));die;


        if (count($data['wp_abd_partners_company']) > 0) {
          $query_one= "DELETE FROM wp_abd_partners_company";
          $this->db->query($query_one);
          $this->db->insert_batch('wp_abd_partners_company', $data['wp_abd_partners_company']);
        }
        // if (count($data['wp_abd_partners_specimen']) > 0) {
        //   $query_two= "DELETE FROM wp_abd_partners_specimen";
        //   $this->db->query($query_two);
        //   //$this->db->delete('wp_abd_partners_specimen');
        //   $this->db->insert_batch('wp_abd_partners_specimen', $data['wp_abd_partners_specimen']);
        // }

        if (count($data['wp_abd_partners_specimen']) > 0) {
          for($i=0;$i<count($data['wp_abd_partners_specimen']);$i++){
             $sql_partners_specimen = 'SELECT `id` FROM wp_abd_partners_specimen WHERE `id`= '. $data['wp_abd_partners_specimen'][$i]['id'] .'';
             $partners_specimen_id = $this->BlankModel->customquery($sql_partners_specimen);

             if (count($partners_specimen_id) > 0) {

              $this->db->where('id',$data['wp_abd_partners_specimen'][$i]['id']);
               $this->db->update('wp_abd_partners_specimen',$data['wp_abd_partners_specimen'][$i]);
             }else{
              $this->db->insert('wp_abd_partners_specimen', $data['wp_abd_partners_specimen'][$i]);

             }
            
          }
        }
        // if (count($data['wp_abd_generated_pcr_reports']) > 0) {
        //   $this->db->insert_batch('wp_abd_generated_pcr_reports', $data['wp_abd_generated_pcr_reports']);
        // }
        // if (count($data['wp_abd_generated_pcr_reports_archive']) > 0) {
        //   $this->db->insert_batch('wp_abd_generated_pcr_reports_archive', $data['wp_abd_generated_pcr_reports_archive']);
        // }
          if (count($data['wp_abd_generated_pcr_reports']) > 0) {
          for($i=0;$i<count($data['wp_abd_generated_pcr_reports']);$i++){
             $sql_generated_pcr_reports = 'SELECT `id` FROM wp_abd_generated_pcr_reports WHERE `id`= '. $data['wp_abd_generated_pcr_reports'][$i]['id'] .'';
             $generated_pcr_reports_id = $this->BlankModel->customquery($sql_generated_pcr_reports);

             if (count($generated_pcr_reports_id) > 0) {

              $this->db->where('id',$data['wp_abd_generated_pcr_reports'][$i]['id']);
              $this->db->update('wp_abd_generated_pcr_reports',$data['wp_abd_generated_pcr_reports'][$i]);
             }else{
              $this->db->insert('wp_abd_generated_pcr_reports', $data['wp_abd_generated_pcr_reports'][$i]);

             }
            
          }
        }
//        if (count($data['wp_abd_generated_pcr_reports_archive']) > 0) {
//          for($i=0;$i<count($data['wp_abd_generated_pcr_reports_archive']);$i++){
//             $sql_generated_pcr_reports_archive = 'SELECT `id` FROM wp_abd_generated_pcr_reports_archive WHERE `id`= '. $data['wp_abd_generated_pcr_reports_archive'][$i]['id'] .'';
//             $generated_pcr_reports_archive_id = $this->BlankModel->customquery($sql_generated_pcr_reports_archive);
//
//             if (count($generated_pcr_reports_archive_id) > 0) {
//
//              //$this->db->where('id',$data['wp_abd_generated_pcr_reports_archive'][$i]['id']);
//              //$this->db->update('wp_abd_generated_pcr_reports_archive',$data['wp_abd_generated_pcr_reports_archive'][$i]);
//             }else{
//              $this->db->insert('wp_abd_generated_pcr_reports_archive', $data['wp_abd_generated_pcr_reports_archive'][$i]);
//
//             }
//            
//          }
//        }
        if (count($data['wp_abd_nail_pathology_report']) > 0) {
          for($i=0;$i<count($data['wp_abd_nail_pathology_report']);$i++){
             $sql_nail_pathology_report = 'SELECT `nail_funagl_id` FROM wp_abd_nail_pathology_report WHERE `nail_funagl_id`= '. $data['wp_abd_nail_pathology_report'][$i]['nail_funagl_id'] .'';
             $nail_pathology_report_id = $this->BlankModel->customquery($sql_nail_pathology_report);

             if (count($nail_pathology_report_id) > 0) {

              $this->db->where('nail_funagl_id',$data['wp_abd_nail_pathology_report'][$i]['nail_funagl_id']);
               $this->db->update('wp_abd_nail_pathology_report',$data['wp_abd_nail_pathology_report'][$i]);
             }else{
              $this->db->insert('wp_abd_nail_pathology_report', $data['wp_abd_nail_pathology_report'][$i]);

             }
            
          }
        }
//        if (count($data['wp_abd_nail_pathology_report_archive']) > 0) {
//          for($i=0;$i<count($data['wp_abd_nail_pathology_report_archive']);$i++){
//             $sql_nail_pathology_archive = 'SELECT `nail_funagl_id` FROM wp_abd_nail_pathology_report_archive WHERE `nail_funagl_id`= '. $data['wp_abd_nail_pathology_report_archive'][$i]['nail_funagl_id'] .'';
//             $nail_pathology_report_archive_id = $this->BlankModel->customquery($sql_nail_pathology_archive);
//
//             if (count($nail_pathology_report_archive_id) > 0) {
//
//              $this->db->where('nail_funagl_id',$data['wp_abd_nail_pathology_report_archive'][$i]['nail_funagl_id']);
//               $this->db->update('wp_abd_nail_pathology_report_archive',$data['wp_abd_nail_pathology_report_archive'][$i]);
//             }else{
//              $this->db->insert('wp_abd_nail_pathology_report_archive', $data['wp_abd_nail_pathology_report_archive'][$i]);
//
//             }
//            
//          }
//        }
        if (count($data['wp_abd_specimen']) > 0) {
          for($i=0;$i<count($data['wp_abd_specimen']);$i++){
             $sql_specimen = 'SELECT `id` FROM wp_abd_specimen WHERE `id`= '. $data['wp_abd_specimen'][$i]['id'] .'';
             $specimen_id = $this->BlankModel->customquery($sql_specimen);

             if (count($specimen_id) > 0) {

              $this->db->where('id',$data['wp_abd_specimen'][$i]['id']);
               $this->db->update('wp_abd_specimen',$data['wp_abd_specimen'][$i]);
             }else{
              $this->db->insert('wp_abd_specimen', $data['wp_abd_specimen'][$i]);

             }
            
          }
        }
//        if (count($data['wp_abd_specimen_archive']) > 0) {
//          for($i=0;$i<count($data['wp_abd_specimen_archive']);$i++){
//             $sql_specimen_archive = 'SELECT `id` FROM wp_abd_specimen_archive WHERE `id`= '. $data['wp_abd_specimen_archive'][$i]['id'] .'';
//             $specimen_archive_id = $this->BlankModel->customquery($sql_specimen_archive);
//
//             if (count($specimen_archive_id) > 0) {
//
//              $this->db->where('id',$data['wp_abd_specimen_archive'][$i]['id']);
//               $this->db->update('wp_abd_specimen_archive',$data['wp_abd_specimen_archive'][$i]);
//             }else{
//              $this->db->insert('wp_abd_specimen_archive', $data['wp_abd_specimen_archive'][$i]);
//
//             }
//            
//          }
//        }
        if (count($data['wp_abd_clinical_info']) > 0) {
          for($i=0;$i<count($data['wp_abd_clinical_info']);$i++){
             $sql_clinical_info = 'SELECT `add_spe_id` FROM wp_abd_clinical_info WHERE `add_spe_id`= '. $data['wp_abd_clinical_info'][$i]['add_spe_id'] .'';
             $clinical_info_id = $this->BlankModel->customquery($sql_clinical_info);

             if (count($clinical_info_id) > 0) {

              $this->db->where('add_spe_id',$data['wp_abd_clinical_info'][$i]['add_spe_id']);
               $this->db->update('wp_abd_clinical_info',$data['wp_abd_clinical_info'][$i]);
             }else{
              $this->db->insert('wp_abd_clinical_info', $data['wp_abd_clinical_info'][$i]);

             }
            
          }
        }
//        if (count($data['wp_abd_clinical_info_archive']) > 0) {
//          for($i=0;$i<count($data['wp_abd_clinical_info_archive']);$i++){
//             $sql_clinical_info_archive = 'SELECT `add_spe_id` FROM wp_abd_clinical_info_archive WHERE `add_spe_id`= '. $data['wp_abd_clinical_info_archive'][$i]['add_spe_id'] .'';
//             $clinical_info_archive_id = $this->BlankModel->customquery($sql_clinical_info_archive);
//
//             if (count($clinical_info_archive_id) > 0) {
//
//              $this->db->where('add_spe_id',$data['wp_abd_clinical_info_archive'][$i]['add_spe_id']);
//               $this->db->update('wp_abd_clinical_info_archive',$data['wp_abd_clinical_info_archive'][$i]);
//             }else{
//              $this->db->insert('wp_abd_clinical_info_archive', $data['wp_abd_clinical_info_archive'][$i]);
//
//             }
//            
//          }
//        }
        if (count($data['wp_abd_archive_notifiation']) > 0) {
          for($i=0;$i<count($data['wp_abd_archive_notifiation']);$i++){
             $sql_archive_notifiation = 'SELECT `note_id` FROM wp_abd_archive_notifiation WHERE `note_id`= '. $data['wp_abd_archive_notifiation'][$i]['note_id'] .'';
             $archive_notifiation_id = $this->BlankModel->customquery($sql_archive_notifiation);

             if (count($archive_notifiation_id) > 0) {

              $this->db->where('note_id',$data['wp_abd_archive_notifiation'][$i]['note_id']);
               $this->db->update('wp_abd_archive_notifiation',$data['wp_abd_archive_notifiation'][$i]);
             }else{
              $this->db->insert('wp_abd_archive_notifiation', $data['wp_abd_archive_notifiation'][$i]);

             }
            
          }
        }

          if (count($data['wp_abd_auto_mail']) > 0) {
          for($i=0;$i<count($data['wp_abd_auto_mail']);$i++){
             $sql_auto_mail = 'SELECT `id` FROM wp_abd_auto_mail WHERE `id`= '. $data['wp_abd_auto_mail'][$i]['id'] .'';
             $auto_mail_id = $this->BlankModel->customquery($sql_auto_mail);

             if (count($auto_mail_id) > 0) {

              $this->db->where('id',$data['wp_abd_auto_mail'][$i]['id']);
              $this->db->update('wp_abd_auto_mail',$data['wp_abd_auto_mail'][$i]);
             }else{
              $this->db->insert('wp_abd_auto_mail', $data['wp_abd_auto_mail'][$i]);

             }
            
          }
        }

        if (count($data['wp_abd_holidays']) > 0) {
          for($i=0;$i<count($data['wp_abd_holidays']);$i++){
             $sql_holidays = 'SELECT `id` FROM wp_abd_holidays WHERE `id`= '. $data['wp_abd_holidays'][$i]['id'] .'';
             $holidays_id = $this->BlankModel->customquery($sql_holidays);

             if (count($holidays_id) > 0) {

              $this->db->where('id',$data['wp_abd_holidays'][$i]['id']);
               $this->db->update('wp_abd_holidays',$data['wp_abd_holidays'][$i]);
             }else{
              $this->db->insert('wp_abd_holidays', $data['wp_abd_holidays'][$i]);

             }
            
          }
        }
        if (count($data['wp_abd_notification']) > 0) {
          for($i=0;$i<count($data['wp_abd_notification']);$i++){
             $sql_notification = 'SELECT `note_id` FROM wp_abd_notification WHERE `note_id`= '. $data['wp_abd_notification'][$i]['note_id'] .'';
             $notification_id = $this->BlankModel->customquery($sql_notification);

             if (count($notification_id)>0) {

              $this->db->where('note_id',$data['wp_abd_notification'][$i]['note_id']);
               $this->db->update('wp_abd_notification',$data['wp_abd_notification'][$i]);
             }else{
              $this->db->insert('wp_abd_notification', $data['wp_abd_notification'][$i]);

             }
            
          }
        }

        if (count($data['wp_abd_data_entry_notification']) > 0) {
          for($i=0;$i<count($data['wp_abd_data_entry_notification']);$i++){
             $sql_data_entry_notification = 'SELECT `id` FROM wp_abd_data_entry_notification WHERE `id`= '. $data['wp_abd_data_entry_notification'][$i]['id'] .'';
             $data_entry_notification_id = $this->BlankModel->customquery($sql_data_entry_notification);

          if (count($data_entry_notification_id)>0) {

              $this->db->where('id',$data['wp_abd_data_entry_notification'][$i]['id']);
               $this->db->update('wp_abd_data_entry_notification',$data['wp_abd_data_entry_notification'][$i]);
             }else{
              $this->db->insert('wp_abd_data_entry_notification', $data['wp_abd_data_entry_notification'][$i]);

             }
            
          }
        }
        

        

        echo json_encode(array('status'=>"success",'error'=>$this->db->error() ));die;
   }

                


          function curl_testing(){

              
                  $from_date   = date('2020-06-08');
                  $to_date     = date('2020-12-15');

                  $query3=$this->db->get('wp_abd_partners_specimen');
              $wp_abd_partners_company_data=$query3->result_array();


                   $objarray=array(
            "wp_abd_partners_specimen" =>$wp_abd_partners_company_data,

 

            );

            $data = json_decode(file_get_contents('php://input'), true);

            echo json_encode(array('status'=> $objarray));die;
            //echo json_encode(array('status'=>"success"));die;
             }

    

}
?>