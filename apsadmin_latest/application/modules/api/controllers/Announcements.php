<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Announcements extends MY_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('MST7MDT');       
    }
    function index()
    {
    	echo "test Announcements service";
    }

///////////////////////////////////////// get all announcement data  /////////////////////////// 
	public function all_announcement()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$announcements = array();
		$conditions = "is_active = 'Active'";
		$select_fields = '*';
		$is_multy_result = 0;
		$memDetails  = $this->BlankModel->getTableData('announcements', $conditions, $select_fields, $is_multy_result, 'modify_date', 'DESC');

		if ($memDetails) {
			foreach ($memDetails as $value) {
				$descriptions = stripslashes(short_description($value['descriptions'], 200));
				$fname = get_user_meta_value($value['user_id'],'first_name',TRUE);
				$lname = get_user_meta_value($value['user_id'],'last_name',TRUE);
				$added_by = $fname.' '.$lname;
				$announcement_data = array(
					'id'=> $value['id'],
					'announcement_name'=>$value['announcement_name'],
					'descriptions'=> substr($descriptions,0,200),
					'user_id'=> $added_by,
					'announcements_for'=>$value['announcements_for'],
					'create_date' => $value['create_date'],
					'modify_date' => $value['modify_date'],
					'is_active' => $value['is_active']
				);

				array_push($announcements, $announcement_data);
			}
			echo json_encode(array("status" => "1", "announcements" => $announcements));
		} else
		  {
			echo json_encode(array("status" => "0"));
		  }
	}


/////////////////////////////// get edit announcement data /////////////////////////// 

	public function announcement_edit()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$announcements = array();
		$id =$data['id'];
		$conditions = " id = '".$id."' AND is_active = 'Active'";
		$select_fields = '*';
		$is_multy_result = 1;
		$memDetails  = $this->BlankModel->getTableData('announcements', $conditions, $select_fields, $is_multy_result);

		if ($memDetails) {
				$descriptions = strip_tags($memDetails['descriptions']);
				$fname = get_user_meta_value($id, 'first_name',TRUE);
				$lname = get_user_meta_value($id, 'last_name',TRUE);
				$added_by = $fname.' '.$lname;
				$announcement_data = array (
					'id'=> $memDetails['id'],
					'announcement_name'=>$memDetails['announcement_name'],
					'descriptions'=>strip_tags($descriptions),
					'added_by'=> $added_by,
					'announcements_for'=>$memDetails['announcements_for'],
					'create_date' => $memDetails['create_date'],
					'modify_date' => $memDetails['modify_date'],
					'is_active' => $memDetails['is_active']
				);

			array_push($announcements, $announcement_data);
			echo json_encode(array("status" => "1", "announcements" => $announcements));

		 } else {
			echo json_encode(array("status" => "0"));
		  }
	}



	public function update_announcement()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$announcements = array();
		$id = $data['id'];
		$announcement_name = $data['announcement_name'];
		$descriptions = $data['descriptions'];
		$announcements_for = $data['announcements_for'];
		$is_active = $data['is_active'];

		$conditions = " id = '".$id."'";
		$data = array('announcement_name'=>$announcement_name,'descriptions'=>$descriptions,'announcements_for'=>$announcements_for,'is_active'=>$is_active,);
		$update_sql = $this->BlankModel->editTableData('announcements', $data, $conditions);
		if ($update_sql = 'yes') {
			echo json_encode(array("status" => "1", "message" => 'Updated successfully.'));
		} else {
			echo json_encode(array("status" => "0", "message" => 'Failed to update!. Please try again.'));
		}
	}



	public function add_announcement()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$announcements = array();
		$id = $data['user_id'];
		$announcement_name = $data['announcement_name'];
		$descriptions = $data['descriptions'];
		$announcements_for = $data['announcements_for'];
		$is_active = $data['is_active'];
		$create_date = date("Y-m-d h:i:s");

		$tabledata = array('announcement_name'=>$announcement_name,'descriptions'=>$descriptions,'announcements_for'=>$announcements_for,'is_active'=>$is_active, 'user_id'=>$id,'	create_date'=>$create_date);
		$add_sql = $this->BlankModel->addTableData('announcements', $tabledata);
		if ($add_sql) {
			echo json_encode(array("status" => "1", "message" => 'Announcement added successfully.'));
		} else {
			echo json_encode(array("status" => "0", "message" => 'Something is worng!. Please try again.'));
		}
	}


	public function delete_announcement()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$id = $data['id'];
		$data = "id";

		$del_sql = $this->BlankModel->delete_data_id('announcements', $data, $id);
		if ($del_sql) {
			echo json_encode(array("status" => "1", "message" => 'Announcement deleted successfully.'));
		} else {
			echo json_encode(array("status" => "0", "message" => 'Something is worng!. Please try again.'));
		}
	}



//////////////////////// popup message/////////////////////////////////
public function popup_message()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		
		$conditions = " id = '1'";
		$select_fields = '*';
		$is_multy_result = 1;
		$details  = $this->BlankModel->getTableData('wp_abd_popup', $conditions, $select_fields, $is_multy_result);
		$role = explode(",",$details['role']); 

		$physician = '';$sales = '';$sales_regional_manager = '';$team_sales_manager = '';$team_regional_Sales = '';$clinic = '';$partner = '';
		
		if (in_array("physician",$role) == 1) {
			$physician = 'physician';
		}

		if (in_array("sales",$role) == 1) {
			$sales ='sales';
		}

		if (in_array("sales_regional_manager",$role) == 1) {
			$sales_regional_manager = 'sales_regional_manager';
		}

		if (in_array("team_sales_manager",$role) == 1) {
			$team_sales_manager = 'team_sales_manager';
		}

		if (in_array("team_regional_Sales",$role) == 1) {
			$team_regional_Sales = 'team_regional_Sales';
		}

		if (in_array("clinic",$role) == 1) {
			$clinic = 'clinic';
		}

		if (in_array("partner",$role) == 1) {
			$partner = 'partner';
		}

		$all_role = array(
			'physician' => $physician,
			'sales' => $sales,
			'sales_regional_manager' => $sales_regional_manager,
			'team_sales_manager' => $team_sales_manager,
			'team_regional_Sales' => $team_regional_Sales,
			'clinic' => $clinic,
			'partner' => $partner,
		);
		

		if ($details) {
		echo json_encode(array("status" => "1", "details" => $details, "all_role" => $all_role));
		} else {
		echo json_encode(array("status" => "0", "message" => 'Something is worng!.'));
		}
	}



	public function update_popup_message()
	{
		$data = json_decode(file_get_contents('php://input'), true);

		$physician = '';$sales = '';$sales_regional_manager = '';$team_sales_manager = '';$team_regional_Sales = '';$clinic = '';$partner = '';

		$role = array();
		$popup_text = $data['descriptions'];

		if($data['physician'] == '1' || !empty($data['physician'])) {
			$physician = 'physician';
			array_push($role,$physician);
		}
		if($data['sales'] == '1' || !empty($data['sales'])) {
			$sales = 'sales';
			array_push($role,$sales);
		}
		if($data['sales_regional_manager'] == '1' || !empty($data['sales_regional_manager'])) {
			$sales_regional_manager = 'sales_regional_manager';
			array_push($role,$sales_regional_manager);
		}
		if($data['team_sales_manager'] == '1' || !empty($data['team_sales_manager'])) {
			$team_sales_manager = 'team_sales_manager';
			array_push($role,$team_sales_manager);
		}
		if($data['team_regional_Sales'] == '1' || !empty($data['team_regional_Sales'])) {
			$team_regional_Sales = 'team_regional_Sales';
			array_push($role,$team_regional_Sales);
		}
		if($data['clinic'] == '1' || !empty($data['clinic'])) {
			$clinic = 'clinic';
			array_push($role,$clinic);
		}
		if($data['partner'] == '1' || !empty($data['partner'])) {
			$partner = 'partner';
			array_push($role,$partner);
		}
		$all_role = implode(',',$role);

		$conditions = " id = '1'";
		$data = array('popup_text'=> $popup_text,'role'=>$all_role);
		$update_sql = $this->BlankModel->editTableData('wp_abd_popup', $data, $conditions);

		if ($update_sql=='yes') {
		echo json_encode(array("status" => "1", "message" => 'Updated successfully.'));
		} else {
		echo json_encode(array("status" => "0", "message" => 'Something is worng!.'));
		}
	}



	function update_chk()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$sql = $this->db->query("SELECT * from `wp_abd_usermeta` where `user_id`='".$data."' AND `meta_key`='view_popup'");
		$result = $sql->row_array();
		if(!empty($result))
		{
			update_user_meta ($data, 'view_popup' , 'Yes');
			if(get_user_meta_value($data, 'training_timestamp')){
				update_user_meta ($data, 'training_timestamp' , date('Y-m-d H:i:s'));
			}else{
				$ins_data = array('user_id'=> $data,'meta_key'=> 'training_timestamp','meta_value'=>date('Y-m-d H:i:s'));
				$insert = $this->db->insert('wp_abd_usermeta',$ins_data);
			}

			echo json_encode(array("status" => "1", "message" => 'Updated successfully.'));
		}
		else
		{
			$ins_data = array(
				array('user_id'=> $data,
				'meta_key'=> 'view_popup',
				'meta_value'=>'Yes'
			),
				array('user_id'=> $data,
				'meta_key'=> 'training_timestamp',
				'meta_value'=>date('Y-m-d H:i:s')
				)
			);
			$insert = $this->db->insert_batch('wp_abd_usermeta',$ins_data);
			echo json_encode(array("status" => "1", "message" => 'Added successfully.'));
		}
		
	}


		function get_announcement()
		{
			$data = json_decode(file_get_contents('php://input'), true);
			$user_id = $data['userid'];
			$fullname = get_user_meta_value($user_id, 'first_name', TRUE).' '.get_user_meta_value($user_id, 'last_name', TRUE);

			$fetchPost = $this->db->query("SELECT * FROM `wp_abd_popup` WHERE `id` = '1'");
			$result_popup = $fetchPost->row_array();
			$roles = explode(",",$result_popup['role']);
			if($data['userrole']=='sales')
			{
				$announcement_sql = $this->db->query("SELECT * FROM `wp_abd_announcements` WHERE `is_active` = 'Active' AND  `announcements_for` = '2' ORDER BY `id` DESC");
				$result = $announcement_sql->result_array();
				echo json_encode(array("status" => "1", "role" => $data['userrole'],'result'=> $result,'popup_data'=>$result_popup,"pop_role"=>$roles, 'fullname'=>$fullname));
			}
			else if($data['userrole']=='physician' || $data['userrole']=='clinic' )
			{
				$announcement_sql = $this->db->query("SELECT * FROM `wp_abd_announcements` WHERE `is_active` = 'Active' AND  `announcements_for` = '1' ORDER BY `id` DESC");
				$result = $announcement_sql->result_array();
				echo json_encode(array("status" => "1", "role" => $data['userrole'], 'result'=> $result,'popup_data'=>$result_popup,"pop_role"=>$roles, 'fullname'=>$fullname));
			}
			else
			{
				echo json_encode(array("status" => "1", "role" => $data['userrole'],'popup_data'=>$result_popup,"pop_role"=>$roles, 'fullname'=>$fullname));
			}
			
		}

		function get_training_status()
		{
			$data = json_decode(file_get_contents('php://input'), true);
			$user_id = $data['user_id'];
			$view_popup =  get_user_meta_value($user_id, 'view_popup',TRUE);

			die(json_encode(array("status" => "1", 'view_popup'=>$view_popup)));
		}

   }