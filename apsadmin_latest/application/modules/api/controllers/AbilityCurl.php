<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class AbilityCurl extends MY_Controller {

    function __construct() {
        parent::__construct();
       date_default_timezone_set('MST7MDT');
    }
    function index()
    {

    }
     
  /**
    * /
    *  @ get_physician_active_list
    *
    */

        /*
      |--------------------------------------------------------------------------
      | ability Add User Table
      |--------------------------------------------------------------------   
      */

       function curl_add_table_users(){
        $data = json_decode(file_get_contents('php://input'), true);
        

        $this->db->insert($data['table'], $data['data']);
        $data_insert_id = $this->db->insert_id();
        if($data_insert_id) {
            
          $role_type_array = array($data['details']['user_role'] => '1');         
          $role = serialize( $role_type_array);        
          add_user_meta($data_insert_id, 'first_name', $data['details']['user_firstname']);
          add_user_meta($data_insert_id, 'last_name', $data['details']['user_lastname']);
          add_user_meta($data_insert_id, 'wp_abd_capabilities', $role);

          add_user_meta($data_insert_id, 'partners_company', $data['details']['partner_company']);

          add_user_meta($data_insert_id, 'nickname', strtolower($data['details']['user_firstname']));
          add_user_meta($data_insert_id, '_status', 'Active');

        }

        echo json_encode(array('status'=>$data_insert_id));die;
   }

           /*
      |--------------------------------------------------------------------------
      | APS Update User Table
      |--------------------------------------------------------------------   
      */
      
      function curl_edit_table_users(){

        $data = json_decode(file_get_contents('php://input'), true);
        
       /*--------------------------Update user table APS------------------------*/
        $conditions_update = " ( `ID` = '".$data['data']['user_id']."')";
         $user_data = array('user_email' => $data['data']['user_email'], 'user_login' =>$data['data']['user_username'], 'user_url' => $data['data']['user_url'],'user_nicename' => $data['data']['user_nickname']);

           $new_password = $data['data']['new_password'];

        $confirm_password = $data['data']['confirm_password'];
        if (isset($confirm_password) && !empty($confirm_password)) {
          $user_data['user_pass'] = md5(SECURITY_SALT.$new_password);
        }

      $programsedit = $this->BlankModel->editTableData($data['table'], $user_data, $conditions_update);

        if ($programsedit) {
            /*----------------------------Update User meta-----------------------------*/
        update_user_meta($data['data']['user_id'],'first_name',$data['data']['user_firstname']);
        update_user_meta($data['data']['user_id'],'last_name',$data['data']['user_lastname']);
          $role_type_array = array($data['data']['user_role'] => '1');
          $role = serialize( $role_type_array);        
        update_user_meta($data['data']['user_id'],'wp_abd_capabilities',$role);



        if(get_user_meta($data['data']['user_id'],'partners_company',true)){
          update_user_meta($data['data']['user_id'],'partners_company',$data['data']['partner_company']);
        }
        else{
            add_user_meta($data['data']['user_id'], 'partners_company', $data['data']['partner_company']);
        }




        if(get_user_meta($data['data']['user_id'],'_mobile',true)){
          update_user_meta($data['data']['user_id'],'_mobile',$data['data']['user_mobile']);
        }
        else{
            add_user_meta($data['data']['user_id'], '_mobile', $data['data']['user_mobile']);
        }
        if (get_user_meta($data['data']['user_id'],'_address', TRUE)) {
          update_user_meta($data['data']['user_id'],'_address',$data['data']['user_address']);
        }
        else{
            add_user_meta($data['data']['user_id'], '_address', $data['data']['user_address']);
        }
        if (get_user_meta($data['data']['user_id'],'nickname', TRUE)) {
          update_user_meta($data['data']['user_id'],'nickname',$data['data']['user_nickname']);
        } 
        else{
          add_user_meta($data['data']['user_id'], 'nickname', $data['data']['user_nickname']);
        }
        // update_user_meta($data['data']['user_id'],'_assign_partner',$data['assign_partner']);
        update_user_meta($data['data']['user_id'],'_status',$data['data']['user_status']);
        if(isset($data['data']['user_fax']))
        {
          if (get_user_meta($data['data']['user_id'],'fax', TRUE)) {
            update_user_meta($data['data']['user_id'],'fax',$data['data']['user_fax']);
          } else{
            add_user_meta($data['data']['user_id'], 'fax', $data['data']['user_fax']);
          }
        }

        if(isset($data['data']['anthr_fax_number']))
        {
          if (get_user_meta($data['data']['user_id'],'anthr_fax_number', TRUE)) {
            update_user_meta($data['data']['user_id'],'anthr_fax_number',$data['data']['anthr_fax_number']);
          } else{
            add_user_meta($data['data']['user_id'], 'anthr_fax_number', $data['data']['anthr_fax_number']);
          }
        }

        if(isset($data['data']['cell_phone']))
        {
          if (get_user_meta($data['data']['user_id'],'cell_phone', TRUE)) {
            update_user_meta($data['data']['user_id'],'cell_phone',$data['data']['cell_phone']);
          } else{
            add_user_meta($data['data']['user_id'], 'cell_phone', $data['data']['cell_phone']);
          }
        }



        if(isset($data['data']['manager_contact_name']))
        {
          if (get_user_meta($data['data']['user_id'],'manager_contact_name', TRUE)) {
            update_user_meta($data['data']['user_id'],'manager_contact_name',$data['data']['manager_contact_name']);
          } else{
            add_user_meta($data['data']['user_id'], 'manager_contact_name', $data['data']['manager_contact_name']);
          }
        }



        if(isset($data['data']['manager_cell_number']))
        {
          if (get_user_meta($data['data']['user_id'],'manager_cell_number', TRUE)) {
            update_user_meta($data['data']['user_id'],'manager_cell_number',$data['data']['manager_cell_number']);
          } else{
            add_user_meta($data['data']['user_id'], 'manager_cell_number', $data['data']['manager_cell_number']);
          }
        }



        if(isset($data['data']['combined_by']))
        {
          if (get_user_meta($data['data']['user_id'],'combined_by', TRUE)) {
            update_user_meta($data['data']['user_id'],'combined_by',$data['data']['combined_by']);
          } else{
            add_user_meta($data['data']['user_id'], 'combined_by', $data['data']['combined_by']);
          }
        }


        if(isset($data['data']['assign_clinic']))
        {
          if (get_user_meta($data['data']['user_id'],'_assign_clinic', TRUE)) {
            update_user_meta($data['data']['user_id'],'_assign_clinic',$data['data']['assign_clinic']);
          } else{
            add_user_meta($data['data']['user_id'], '_assign_clinic', $data['data']['assign_clinic']);
          }
        }


        if(isset($data['data']['added_by']))
        {
          if (get_user_meta($data['data']['user_id'],'added_by', TRUE)) {
            update_user_meta($data['data']['user_id'],'added_by',$data['data']['added_by']);
          } else{
            add_user_meta($data['data']['user_id'], 'added_by', $data['data']['added_by']);
          }
        }


        if(isset($data['data']['clinic_name']))
        {
          if (get_user_meta($data['data']['user_id'],'_clinic_name', TRUE)) {
            update_user_meta($data['data']['user_id'],'_clinic_name',$data['data']['clinic_name']);
          } else{
            add_user_meta($data['data']['user_id'], '_clinic_name', $data['data']['clinic_name']);
          }
        }


        if(isset($data['data']['assign_specialty']))
        {
          if (get_user_meta($data['data']['user_id'],'assign_specialty', TRUE)) {
            update_user_meta($data['data']['user_id'],'assign_specialty',$data['data']['assign_specialty']);
          } else{
            add_user_meta($data['data']['user_id'], 'assign_specialty', $data['data']['assign_specialty']);
          }
        }


        if(isset($data['data']['assign_partner']))
        {
          if (get_user_meta($data['data']['user_id'],'_assign_partner', TRUE)) {
            update_user_meta($data['data']['user_id'],'_assign_partner',$data['data']['assign_partner']);
          } else{
            add_user_meta($data['data']['user_id'], '_assign_partner', $data['data']['assign_partner']);
          }
        }



        if(isset($data['data']['clinic_addrs']))
        {
          if (get_user_meta($data['data']['user_id'],'clinic_addrs', TRUE)) {
            update_user_meta($data['data']['user_id'],'clinic_addrs',$data['data']['clinic_addrs']);
          } else{
            add_user_meta($data['data']['user_id'], 'clinic_addrs', $data['data']['clinic_addrs']);
          }
        }


        if(isset($data['data']['npi']))
        {
          if (get_user_meta($data['data']['user_id'],'npi', TRUE)) {
            update_user_meta($data['data']['user_id'],'npi',$data['data']['npi']);
          } else{
            add_user_meta($data['data']['user_id'], 'npi', $data['data']['npi']);
          }
        }

        ///////////////////////////////////////// Sales////////////////////////////////////////


        if(isset($data['data']['brn']))
        {
          if (get_user_meta($data['data']['user_id'],'_brn', TRUE)) {
            update_user_meta($data['data']['user_id'],'_brn',$data['data']['brn']);
          } else{
            add_user_meta($data['data']['user_id'], '_brn', $data['data']['brn']);
          }
        }


        if(isset($data['data']['ban']))
        {
          if (get_user_meta($data['data']['user_id'],'_ban', TRUE)) {
            update_user_meta($data['data']['user_id'],'_ban',$data['data']['ban']);
          } else{
            add_user_meta($data['data']['user_id'], '_ban', $data['data']['ban']);
          }
        }


        if(isset($data['data']['nbaccount']))
        {
          if (get_user_meta($data['data']['user_id'],'_nbaccount', TRUE)) {
            update_user_meta($data['data']['user_id'],'_nbaccount',$data['data']['nbaccount']);
          } else{
            add_user_meta($data['data']['user_id'], '_nbaccount', $data['data']['nbaccount']);
          }
        }


        if(isset($data['data']['commission_percentage']))
        {
          if (get_user_meta($data['data']['user_id'],'_commission_percentage', TRUE)) {
            update_user_meta($data['data']['user_id'],'_commission_percentage',$data['data']['commission_percentage']);
          } else{
            add_user_meta($data['data']['user_id'], '_commission_percentage', $data['data']['commission_percentage']);
          }
        }



        if(isset($data['data']['assign_to']))
        {
          if (get_user_meta($data['data']['user_id'],'assign_to', TRUE)) {
            update_user_meta($data['data']['user_id'],'assign_to',$data['data']['assign_to']);
          } else{
            add_user_meta($data['data']['user_id'], 'assign_to', $data['data']['assign_to']);
          }
        }


        if (isset($data['data']['llc']) == 'llc')
        {
          $ein_no = $data['ein_no'];
          if (get_user_meta($data['data']['user_id'],'_llc', TRUE)) {
            update_user_meta($data['data']['user_id'],'_llc',$data['data']['llc']);
            update_user_meta($data['data']['user_id'],'_ein_no',$data['data']['ein_no']);
          } else{
            add_user_meta($data['data']['user_id'], '_llc', $data['data']['llc']);
            add_user_meta($data['data']['user_id'], '_ein_no', $data['data']['ein_no']);
          }
          
        } elseif (isset($data['data']['llc']) == 'corporation') {
          if (get_user_meta($data['data']['user_id'],'_llc', TRUE)) {
            update_user_meta($data['data']['user_id'],'_llc',$data['data']['llc']);
            update_user_meta($data['data']['user_id'],'_ein_no','');
          } else{
            add_user_meta($data['data']['user_id'],'_llc',$data['data']['llc']);
          }
        }

           /////////////////////////////////////////End Sales////////////////////////////////////////


         $receivereport='';
         
         if(isset($data['data']['way_recive_fax']))
         {
          $receivereport .=$data['data']['way_recive_fax'].",";
         }
         if(isset($data['data']['way_recive_email']))
         {
          $receivereport .=$data['data']['way_recive_email'].",";
         }
         if(isset($data['data']['way_recive_none']))
         {
          $receivereport .=$data['data']['way_recive_none'].",";
         }
         
         update_user_meta($data['data']['user_id'],'report_recive_way',$receivereport);

      
         if(isset($data['data']['acc_cnt']))
         {
           $sql_data = "SELECT * from `wp_abd_usermeta` where `user_id`='".$data['data']['user_id']."' AND `meta_key`='specimen_stages'";
           $meta_results = $this->BlankModel->customquery($sql_data);
           if(empty( $meta_results))
           {
            $specimen_satges =implode(",",$data['data']['acc_cnt']);
            add_user_meta($data['data']['user_id'], 'specimen_stages', $specimen_satges);
           }
           else
           {
             $specimen_satges =implode(",",$data['data']['acc_cnt']);
             update_user_meta($data['data']['user_id'],'specimen_stages',$specimen_satges);
           }
         }


        }

        echo json_encode(array('status'=>$programsedit));die;
        

   }
              /*
      |--------------------------------------------------------------------------
      | APS Update User Table
      |--------------------------------------------------------------------   
      */
      
  function curl_edit_table_users_api_ability()
  {

        $data = json_decode(file_get_contents('php://input'), true);
        $id = intval($data['userid']);
        $user_id = $id;

        update_user_meta($user_id, 'first_name', $data['first_name']);
        update_user_meta($user_id, 'last_name', $data['last_name']);
        update_user_meta($user_id, '_mobile', $data['mobile']);
        update_user_meta($user_id, '_address',$data['address']);

        if(($data['user_pass'] === $data['con_pass']) && !empty($data['user_pass']) && !empty($data['con_pass']))
        {
          
        $pass = md5(SECURITY_SALT.$data['user_pass']);
        $con_pass = md5(SECURITY_SALT.$data['con_pass']);

        $up_data = array('user_pass'=>$pass,'user_email'=>$data['email'],'user_url'=>$data['user_url']);
        $conditions1 = " ( `ID` = '".$id."')";  
        $usersedit = $this->BlankModel->editTableData('users', $up_data, $conditions1);
        }
        
        $upd_data = array('user_email'=>$data['email'],'user_url'=>$data['user_url']);
        $conditions2 = " ( `ID` = '".$id."')";  
        $usersedit = $this->BlankModel->editTableData('users', $upd_data, $conditions2);
        

      
       }
             /*
    |--------------------------------------------------------------------------
    | curl Add User Table dataentry API Ability
    |--------------------------------------------------------------------   
    */

    function add_dataentry_user_curl_api(){

      $data=json_decode(file_get_contents('php://input'), true);

            $userdata = array(
            'user_login'  =>  $data['userName'],
            'user_email'  =>  $data['email'],
            'user_url'    =>  $data['website'],
            'display_name'=>  $data['firstName'].' '.$data['lastName'],
            'user_registered'=>date('Y-m-d H:i:s'),
            'user_nicename'=>strtolower($data['userName']),
            'user_pass'   =>  md5(SECURITY_SALT.$data['password'])           
            );
       $user_sql_insert = $this->BlankModel->addTableData('wp_abd_users',$userdata);
      if(!empty($user_sql_insert)){
        $role_type_array = array('data_entry_oparator' => '1');
        $role = serialize( $role_type_array);
     
        add_user_meta($user_sql_insert,'added_by', $data['user_id'] );
        add_user_meta($user_sql_insert,'first_name', $data['firstName'] );
        add_user_meta($user_sql_insert,'last_name', $data['lastName'] );
        add_user_meta($user_sql_insert,'_mobile', $data['phone'] );
        add_user_meta($user_sql_insert,'wp_abd_capabilities', $role );
        add_user_meta($user_sql_insert,'_address', $data['address'] );
        if($data['specimenControlAccess']){
          add_user_meta($user_sql_insert,'specimen_control_access',$data['specimenControlAccess']);
        }
        if($data['specimenControlAccessQc']){
          add_user_meta($user_sql_insert,'specimen_control_access_qc',$data['specimenControlAccessQc']);
        }
        add_user_meta($user_sql_insert, 'access_given_by', $data['user_id']);

            die(json_encode(array('status'=>'1')));
         }else{
            die(json_encode(array('status'=>'0')));
         } 

      }

                   /*
    |--------------------------------------------------------------------------
    | curl Add User Table dataentry API Aps
    |--------------------------------------------------------------------   
    */

      function edit_dataentry_user_curl_api(){

      $data=json_decode(file_get_contents('php://input'), true);
      $userdata = array();
      $up_status= array();
      if(!empty($data)){
        $request_user_id = $data["request_id"];

        $userdata['user_url'] =  $data['website'];
        $userdata['display_name'] =  $data['firstName'].' '.$data['lastName'];
        $userdata['user_nicename']=strtolower($data['userName']);

        if(!empty($data['password'])){
          $userdata['user_pass']   =  md5(SECURITY_SALT.$data['password']);      
        }
             
        $this->db->where('ID',$request_user_id);
        $this->db->update('wp_abd_users',$userdata);

        update_user_meta($request_user_id, 'first_name',$data['firstName'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        update_user_meta($request_user_id, 'last_name' ,$data['lastName'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        update_user_meta($request_user_id, 'user_url'  ,$data['website'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');

        $mob_sql = "SELECT * from `wp_abd_usermeta` where `meta_key`='_mobile' AND `user_id`= '".$request_user_id."'";
        $mob_sql_res = $this->BlankModel->customquery($mob_sql);
        if(empty($mob_sql_res))
        { 
          add_user_meta($request_user_id,'_mobile', $data['phone'] );
        }
        else
        {
          update_user_meta($request_user_id, '_mobile'   ,$data['phone'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        }

        $add_sql = "SELECT * from `wp_abd_usermeta` where `meta_key`='_address' AND `user_id`= '".$request_user_id."'";
        $add_sql_res = $this->BlankModel->customquery($add_sql);
        if(empty($add_sql_res))
        {
          add_user_meta($request_user_id,'_address', $data['address'] );
        }
        else
        {
          update_user_meta($request_user_id, '_address'  ,$data['address'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        }

        $acc1_sql = "SELECT * from `wp_abd_usermeta` where `meta_key`='specimen_control_access' AND `user_id`= '".$request_user_id."'";
        $acc1_sql_res = $this->BlankModel->customquery($acc1_sql);
        if(empty($acc1_sql_res))
        {
          add_user_meta($request_user_id,'specimen_control_access', 'add');
        }
        else
        {
          update_user_meta($request_user_id, 'specimen_control_access', $data['specimenControlAccess'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        }

        $acc2_sql = "SELECT * from `wp_abd_usermeta` where `meta_key`='specimen_control_access_qc' AND `user_id`= '".$request_user_id."'";
        $acc2_sql_res = $this->BlankModel->customquery($acc2_sql);
        if(empty($acc2_sql_res))
        {
          add_user_meta($request_user_id,'specimen_control_access_qc', 'check');
        }
        else
        {
          update_user_meta($request_user_id, 'specimen_control_access_qc', $data['specimenControlAccessQc'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
        }
      
        update_user_meta($request_user_id, 'access_given_by', $data['user_id'])==='yes'?array_push($up_status, 'yes'):array_push($up_status, 'no');
          die(json_encode(array('status'=>'1')));
              
    }
  }




           /*
      |--------------------------------------------------------------------------
      | APS delete User Table
      |--------------------------------------------------------------------   
      */

  function deleteUserApsAbility()
   {
       $data = json_decode(file_get_contents('php://input'), true);
       
       $uid = $data['uid'];
       
       $this->db->where("ID",$uid);
       $this->db->delete("wp_abd_users");
       
       $this->db->where("user_id",$uid);
       $this->db->delete("wp_abd_user_log");
       
       $this->db->where("user_id",$uid);
       $this->db->delete("wp_abd_usermeta");
       
       echo(json_encode(array("database"=>$this->db->database,'result'=>"success")));die;
   }
       function customqueryForAps()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        
        $query=base64_decode($data['query']);
        if($query)
        {
            $result = $this->db->query($query)->result_array();
        }
        else
        {
            $result=array();
        }
        echo(json_encode(array("database"=>$this->db->database,'result'=>$result)));die;
    }  
    
    function editApsTableData()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        
        $table_name=$data['table_name'];
        $dataArray=$data['data'];
        $condition=$data['condition'];
        
        $this->db->where($condition);
        $this->db->update($table_name, $dataArray);
        
        if ($this->db->affected_rows() > 0)
        {
             $success = 'yes';
        }
        else
        {
             $success = 'no';
        }
        echo(json_encode(array("database"=>$this->db->database,'result'=>$success)));die;
    } 
        function abilityCustomQuery()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        
        $query=base64_decode($data['query']);
        if($query)
        {
            $result = $this->db->query($query)->result_array();
        }
        else
        {
            $result=array();
        }
        echo(json_encode(array("database"=>$this->db->database,'result'=>$result)));die;
    }  
    
    function abilityEditTableData()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        
        $table_name=$data['table_name'];
        $dataArray=$data['data'];
        $condition=$data['condition'];
        
        $this->db->where($condition);
        $this->db->update($table_name, $dataArray);
        
        if ($this->db->affected_rows() > 0)
        {
             $success = 'yes';
        }
        else
        {
             $success = 'no';
        }
        echo(json_encode(array("database"=>$this->db->database,'result'=>$success)));die;
    } 
    
    function abilityAddTableData()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        
        $table_name=$data['table_name'];
        $dataArray=$data['data'];
        
        $this->db->insert($table_name, $dataArray);
    $id=$this->db->insert_id();
        
        echo(json_encode(array("database"=>$this->db->database,'result'=>$id)));die;
    }    

}
?>