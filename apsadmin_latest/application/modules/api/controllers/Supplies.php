<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Supplies extends MY_Controller {

    function __construct() {
        parent::__construct();
       
    }
    function index()
    {
    	echo "test Supplies";
    }

    function supplies_details()
    {
      $data = json_decode(file_get_contents('php://input'), true);
      $user_id = $data['user_id'];
      if($user_id){
        $sql = 'SELECT `ID` FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 
		JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "_assign_partner") 
	
		WHERE (m3.meta_value LIKE "%physician%")
        AND m4.meta_value = "'.$user_id.'"';
       
        $physicians_list = $this->BlankModel->customquery($sql); 
         if($physicians_list){
            $phy_add = array();
            $addr = array();
        foreach($physicians_list as $physician){
        $physician_id = $physician['ID'];
        $addr['phy_address'] = get_user_meta_value($physician_id, '_address',TRUE);
        if(!empty($addr['phy_address'])) {
        array_push($phy_add,$addr);
        }
        }
       } else {
        $phy_add = '';
       }
        $address = get_user_meta_value($user_id, '_address', TRUE);
        echo json_encode(array('status'=>'1', "address" => $address , "phy_address"=>$phy_add));
      } else {
        echo json_encode(array('status'=>'0'));
      }

    }

    function send_supplies_mail()
    {
      $data = json_decode(file_get_contents('php://input'), true);
      if (!empty($data['email']) && isset($data['email'])) {
        $diff_address = $data['diff_address'];
        $email = $data['email'];
        // $email = 'ashis@sleekinfosolutions.com';
        $user_id =  $data['user_id'];
        $name = get_user_meta_value ($user_id, 'first_name', TRUE). ' '. get_user_meta_value ($user_id, 'last_name', TRUE);
        $role = get_user_role($user_id );
         $role = ucwords(str_replace("_"," ","$role"));
       //////// mail body for admin//////////////
        $adminBody = "";
	    $adminBody.= 'Dear admin,<br><br> We have received an order request. Details are listed below:<br/><br/>
    				<div class="cont_wrapperInner">
                    <table width="100%" border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td align="left" valign="top">Requester Name:</td>
                        <td align="left" valign="top">'.$name.'</td>
                      </tr> 
					  <tr>
                        <td align="left" valign="top">Requester Degination:</td>
                        <td align="left" valign="top">'.$role.'</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top">Requester Email:</td>
                        <td align="left" valign="top">'.$email.'</td>
                      </tr>';

            if(isset($data['supplies_address']) && ($data['supplies_address'] !="" )){
            $adminBody.= '<tr>
                                <td align="left" valign="top">Supplies address:</td>
                                <td align="left" valign="top">'.$data['supplies_address'].'</td>
                                </tr>';
            }	
            if(isset($data['physician_address']) && ($data['physician_address'] !="")){                 
            $adminBody.='<tr>
                                <td align="left" valign="top">Supplies address:</td>
                                <td align="left" valign="top">'.$data['physician_address'].'</td>
                                </tr>';
            }                
            $adminBody.='<tr>
                                <td align="left" valign="top">Different address:</td>
                                <td align="left" valign="top">'.$diff_address.'</td>
                                </tr> 
                                </table>
                            </div>';

            $adminBody.= "<br>Thank You<br>Antares Physicians Services Teams<br>";
            // $to       = "support@abilitydiagnostics.com, trajanking@gmail.com";
            $to = $email;
            $from     = "support@abilitydiagnostics.com";
            $adminSubject  = "Supply order";



            $config['mailtype'] = 'html';
            $config['charset'] = 'UTF-8';
            $config['newline'] = "\r\n";
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;

        $this->load->library('email',$config);

        $this->email->from($from, 'Antares Physician Services');
        $this->email->to($to);
        $this->email->subject($adminSubject);
        $this->email->message($adminBody);
        $admin = $this->email->send();

        //////// mail body for user//////////////
    $userBody = '';
    $userBody .= "Dear ".$name.",<br><br>Thank you for your order with us.<br/> we will contact you soon.";
	$userBody .= "<br><br>Thank You<br>";
	$userBody .= "Antares Physician Services Teams<br>";
	$toa   = $email;
    $userSubject  = "Thank you for your Order";

    $this->email->from($from, 'Antares Physician Services');
    $this->email->to($toa);
    $this->email->subject($userSubject);
    $this->email->message($userBody);
    $admin = $this->email->send();
    }

        if ($admin) {
        echo json_encode(array("status" => "1", "message" => 'Thank you for your Order request.'));
        } else {
        echo json_encode(array("status" => "0", "message" => 'Something is worng!. Please try again.'));
        }

    }


    public function sendSurveyDetails()
			{
				$data = json_decode(file_get_contents('php://input'), true);
       if(!empty($data)){
           $MailFormat = '<div class="cont_wrapperInner">
            <table class="table table-striped listing_table doodle display" cellspacing="0" width="100%">
            <thead>
            <tr>
            <th>Survey Question</th>
            <th>Rating </th>
            </tr>
            </thead>

            <tbody>
            <tr>
            <td align="left" valign="top"><strong> How satisfied are you with the support your received:</strong></td>
            <td align="center" valign="top">'.$data["option1"].'</td>
            </tr>
            <tr>
            <td align="left" valign="top"><strong>How responsive were we to your inquiries?</strong></td>
            <td align="center" valign="top">'.$data["option2"].'</td>
            </tr>

            <tr>
            <td align="left" valign="top"><strong>How satisfied are you with the diagnosis reports (for physicians)?</strong></td>
            <td align="center" valign="top">'.$data["option3"].'</td>
            </tr>
            <tr>
            <td align="left" valign="top"><strong>How satisfied are you with the turnaround time?</strong></td>
            <td align="center" valign="top">'.$data["option4"].'</td>
            </tr>

            <tr>
            <td align="left" valign="top"><strong> Overall, how was your experience?</strong></td>
            <td align="center" valign="top">'.$data["option5"].'</td>
            </tr>
            </tbody>
            </table>

            <table>
                <tr>
                <td align="left" valign="top"><strong>Sender Email:</strong></td>
                <td align="center" valign="top">'.$data["email"].'</td>
                </tr>
                <tr>
                <td align="left" valign="top"><strong>Response:</strong></td>
                <td align="center" valign="top">'.$data["answer_survey"].'</td>
                </tr>
            </table>
             </div>';
            $message  = "Dear admin,<br>Someone filled out a survey. Results below:<br/><br/>";
            $message .= $MailFormat;
            $message .= "Thank You<br><br> ";
            $message .= "Antares Physicians Services Teams<br>";
            // $body     = mailBody($message);
            // //$to       = "support@abilitydiagnostics.com, trajanking@gmail.com";
            // $to       = "saptarshi@sleekinfosolutions.com";
            // $from     = "support@abilitydiagnostics.com";
            // $subject  = "Ability Diagnostics Survey Report-";
            
            // sendMail($to, $subject,$body,$from,"Ability Diagnostics");

                $config['mailtype'] = 'html';
                $config['charset'] = 'UTF-8';
                $config['newline'] = "\r\n";
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
            $this->load->library('email',$config);

            $this->email->from('support@abilitydiagnostics.com', 'Antares Physicians Services');
            $this->email->to($data["email"]);
            //$this->email->cc('another@another-example.com');
            //$this->email->bcc('them@their-example.com');

            $this->email->subject('Antares Physicians Services Survey Report-');
            $this->email->message($message);
            $send_mail = $this->email->send();
            if ($send_mail) {
             die( json_encode(array('status'=>'1', 'message' => 'Thank you for your feedback')));
            } else {
             die( json_encode(array('status'=>'0', 'message' => 'Something is wrong. Please try again!')));
            }
       }
			}
    

 }
?>