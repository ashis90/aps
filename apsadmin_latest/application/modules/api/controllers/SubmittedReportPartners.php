<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class SubmittedReportPartners extends MY_Controller {

    function __construct() {
        parent::__construct();
       
    }
    function index()
    {
    	echo "test SubmittedReportPartners";
    }

    function get_submitted_reports()
    {
      $data = json_decode(file_get_contents('php://input'), true);
      $user_id    = '';
      $partner_id ='';
      if(isset($data['user_id'])){
        $user_id    = $data['user_id'];
      }else if(isset($data['partner_id'])){
        $partner_id = $data['partner_id'];
      }
      
      // $physician_name = get_user_meta_value($user_id, 'first_name', TRUE).' '.get_user_meta_value($user_id, 'last_name', TRUE);
      $array_data     = array();
      $result         = array();
      $result2        = array();     
      $user_role      = get_user_role($user_id);   
      $extra_sql      = "";   
     
	// if($user_role == "combine_physicians_accounts"){
    
    if(!empty($user_id)){
       $sql = "SELECT  t1.meta_value as partners_company  FROM wp_abd_users 
          INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id  
          WHERE  t1.meta_key = 'partners_company'  
        AND t1.user_id = $user_id";
       

        $partners_company_list = $this->BlankModel->customquery($sql);

	    $list_of_phy = "";
	      if($partners_company_list) {       
	       foreach($partners_company_list as $partners_company) {    
	  		  // $list_of_phy.= $partners_company['partners_company'].',';	
	  		  $list_of_phy = $partners_company['partners_company'];	
		    }
	       $user_id = trim($list_of_phy, ',');
	      }	   
        $p_id =  $user_id;
      }else if(!empty($partner_id)){
        $p_id =  $partner_id;
      }

      $sql2 ="";
      $sql2 .= "SELECT `wp_abd_specimen`.`assessioning_num`,
       `wp_abd_specimen`.`p_lastname`,
        `wp_abd_specimen`.`p_firstname`,
        `wp_abd_specimen`.`create_date` AS `specimen_create_date`,
        `wp_abd_specimen`.`collection_date`,
        `wp_abd_specimen`.`date_received`,
		`wp_abd_specimen`.`partners_company`,
		`wp_abd_partners_specimen`.`physician_name`,
		`wp_abd_partners_specimen`.`external_id`,
		`wp_abd_nail_pathology_report`.`nail_pdf`,
		`wp_abd_nail_pathology_report`.`is_downloaded`,
		`wp_abd_specimen`.`id`,
		`wp_abd_nail_pathology_report`.`create_date`,
		`wp_abd_generated_pcr_reports`.`report_pdf_name` as `pcr_pdf`,
		`wp_abd_generated_pcr_reports`.`is_downloaded`

       FROM `wp_abd_specimen` 
       
       LEFT JOIN `wp_abd_nail_pathology_report`  
       ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
       
       INNER JOIN `wp_abd_partners_specimen`
       ON `wp_abd_specimen`.`id` = `wp_abd_partners_specimen`.`specimen_id`  
        
       LEFT JOIN `wp_abd_generated_pcr_reports`  
       ON `wp_abd_specimen`.`assessioning_num` = `wp_abd_generated_pcr_reports`.`accessioning_num`  
   
       WHERE `wp_abd_specimen`.`partners_company` = $p_id
       AND (`wp_abd_nail_pathology_report`.`nail_pdf` != '' OR `wp_abd_generated_pcr_reports`.`report_pdf_name` != '')

       AND `wp_abd_specimen`.`status` = '0' 
       AND `wp_abd_specimen`.`physician_accepct` = '0' 
       AND `wp_abd_specimen`.`qc_check` = '0'         
       
       ORDER BY `wp_abd_specimen`.`assessioning_num` DESC
       ";
     
      $array_data = $this->BlankModel->customquery($sql2);    
     
      $submitted_rep = array();
      $sub_rep = array();
      foreach ($array_data as $key => $value) {
        $sub_rep['assessioning_num'] = $value['assessioning_num'];
        $sub_rep['physician_name']   = $value['physician_name'];
        $sub_rep['external_id']   = $value['external_id'];
        $sub_rep['patient_name']     = $value['p_firstname'].' '.$value['p_lastname'];
        $sub_rep['collection_date']  = date('m-d-Y',strtotime($value['collection_date']));
        $sub_rep['date_received']    = date('m-d-Y',strtotime($value['date_received']));
        $sub_rep['is_downloaded']    = $value['is_downloaded'];
        $sub_rep['specimen_id']    = $value['id'];

        if(!empty($value['nail_pdf'])) {
        $sub_rep['nail_pdf'] = REPORT_PDF_URL.'histo_report_pdf/'.$value['nail_pdf'];
        } else {
          $sub_rep['nail_pdf'] ='';
        }
        
        if(!empty($value['assessioning_num'])) {
          $pcr_sql = "SELECT * FROM `wp_abd_generated_pcr_reports`  WHERE `accessioning_num` ='".$value['assessioning_num']."'";
          $pcr_result = $this->BlankModel->customquery($pcr_sql);           
        }
     
        if(!empty($pcr_result)) {
          $sub_rep['pcr_pdf'] = REPORT_PDF_URL.'pcr_report_pdf/'.$pcr_result[0]['report_pdf_name'];
          $sub_rep['pcr_pdf_is_downloaded'] =  $pcr_result[0]['is_downloaded'];
        }
        else {
          $sub_rep['pcr_pdf'] = '';  
        }
     
        array_push( $submitted_rep, $sub_rep);
      }


      if ($submitted_rep) {
      echo json_encode(array("status"=>"1", "submitted_rep"=> $submitted_rep));
      } 
      else {
      echo json_encode(array("status"=>"0", "submitted_rep"=> 'No data found.'));
      }
    }

    //////////////////////////////////////

    public function change_submitted_reports_color() {
      $data = json_decode(file_get_contents('php://input'), true);
      $specimen_id = $data['specimen_id'];
      $up_data = array('is_downloaded'=>'1');
      $conditions1 = " ( `specimen_id` = '".$specimen_id."')";
      $result = $this->BlankModel->editTableData('nail_pathology_report', $up_data, $conditions1);

      if($result){
           echo json_encode(array("status"=>"1"));
       } 
      else {
           echo json_encode(array("status"=>"0"));
       }
    }

    /////////////////////////////////

    public function change_color_for_pcr() {
      $data = json_decode(file_get_contents('php://input'), true);
      $accessioning_num = $data['accessioning_num'];

      $up_data = array('is_downloaded'=>'1');
      $conditions1 = " ( `accessioning_num` = '".$accessioning_num."')";
      $result = $this->BlankModel->editTableData('generated_pcr_reports', $up_data, $conditions1);

        if($result){
           echo json_encode(array("status"=>"1"));
        } else {
           echo json_encode(array("status"=>"0"));
        }
    }

////////////////////// search data  //////////////////

    function search_submitted_reports(){
      
      $data = json_decode(file_get_contents('php://input'), true);
      $user_id    = '';
      $partner_id = '';
      if(isset($data['user_id'])){
        $user_id  = $data['user_id'];
      }
      else if(isset($data['partner_id'])){
        $partner_id = $data['partner_id'];
      }
      
      $paitent_name      = $data['p_lastname'];
      $assessioning_num  = $data['assessioning_num'];
      $external_id       = $data['external_id'];
      $test_type         = $data['test_type'];
     
      //$physician_name = get_user_meta_value($user_id, 'first_name', TRUE).' '.get_user_meta_value($user_id, 'last_name', TRUE);
   
      $array_data = array();
      $result = array();
      $result2 = array();
   
      if(!empty($user_id)){
       $sql = "SELECT  t1.meta_value as partners_company  FROM wp_abd_users 
           INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id  
           WHERE  t1.meta_key = 'partners_company'  
         AND t1.user_id = $user_id";   
       $partners_company_list = $this->BlankModel->customquery($sql);
 
       $list_of_phy = "";
         if($partners_company_list) {       
          foreach($partners_company_list as $partners_company) {    
           // $list_of_phy.= $partners_company['partners_company'].',';	
           $list_of_phy = $partners_company['partners_company'];	
         }
          $user_id = trim($list_of_phy, ',');
         }	   
         $p_id =  $user_id;
       }else if(!empty($partner_id)){
         $p_id =  $partner_id;
       }

      $sql ="";
      $sql .= "SELECT `wp_abd_specimen`.`assessioning_num`,
       `wp_abd_specimen`.`p_lastname`,
        `wp_abd_specimen`.`p_firstname`,
        `wp_abd_specimen`.`create_date` AS `specimen_create_date`,
        `wp_abd_specimen`.`collection_date`,
        `wp_abd_specimen`.`date_received`,
		`wp_abd_specimen`.`partners_company`,
		`wp_abd_partners_specimen`.`physician_name`,
		`wp_abd_partners_specimen`.`external_id`,
		`wp_abd_specimen`.`test_type`,
		`wp_abd_nail_pathology_report`.`nail_pdf`,
		`wp_abd_nail_pathology_report`.`is_downloaded`,
		`wp_abd_specimen`.`id`,
		`wp_abd_nail_pathology_report`.`create_date`,
		`wp_abd_generated_pcr_reports`.`report_pdf_name` as `pcr_pdf`,
		`wp_abd_generated_pcr_reports`.`is_downloaded`

       FROM `wp_abd_specimen` 
       
       LEFT JOIN `wp_abd_nail_pathology_report`  
       ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
       
       INNER JOIN `wp_abd_partners_specimen`
       ON `wp_abd_specimen`.`id` = `wp_abd_partners_specimen`.`specimen_id`  
        
       LEFT JOIN `wp_abd_generated_pcr_reports`  
       ON `wp_abd_specimen`.`assessioning_num` = `wp_abd_generated_pcr_reports`.`accessioning_num`  
      
       WHERE `wp_abd_specimen`.`partners_company` = $p_id
       AND (`wp_abd_nail_pathology_report`.`nail_pdf` != '' OR `wp_abd_generated_pcr_reports`.`report_pdf_name` != '')

       AND `wp_abd_specimen`.`status` = '0' 
       AND `wp_abd_specimen`.`physician_accepct` = '0' 
       AND `wp_abd_specimen`.`qc_check` = '0'";     
   
      if( !empty($paitent_name)){
        $sql.=" AND   CONCAT(`wp_abd_specimen`.`p_firstname`, ' ', `wp_abd_specimen`.`p_lastname`) LIKE  '%".$paitent_name."%' OR `wp_abd_specimen`.`p_lastname` LIKE '%".trim($paitent_name)."%' OR `wp_abd_specimen`.`p_firstname` LIKE '%".trim($paitent_name)."%'";
       }
    
      if( !empty($assessioning_num)){
        $sql.=" AND `wp_abd_specimen`.`assessioning_num` LIKE '%".trim($assessioning_num)."%'";
      }

      if ( !empty($external_id)) {
      	$sql.=" AND `wp_abd_partners_specimen`.`external_id` LIKE '%".trim($external_id)."%'";
      } 
      
      if( !empty($test_type) && $test_type!='All'){
        $sql.=" AND `wp_abd_specimen`.`test_type` LIKE '%".$test_type."%'";
      }

        $sql.= "ORDER BY `wp_abd_specimen`.`assessioning_num` DESC ";
       
      $array_data = $this->BlankModel->customquery($sql); 

   
      $submitted_rep = array();
      $sub_rep = array();
      foreach ($array_data as $key => $value) {
        $sub_rep['assessioning_num'] = $value['assessioning_num'];
        $sub_rep['physician_name'] = $value['physician_name'];
        $sub_rep['external_id']   = $value['external_id'];
        $sub_rep['patient_name'] = $value['p_firstname'].' '.$value['p_lastname'];
        $sub_rep['collection_date']  = date('m-d-Y',strtotime($value['collection_date']));
        $sub_rep['date_received']    = date('m-d-Y',strtotime($value['date_received']));
        $sub_rep['is_downloaded']   = $value['is_downloaded'];
        $sub_rep['specimen_id']     = $value['id'];
        // $sub_rep['nail_pdf'] = $value['nail_pdf'];
 
        if(!empty($value['nail_pdf'])) {
        $sub_rep['nail_pdf'] = REPORT_PDF_URL.'histo_report_pdf/'.$value['nail_pdf'];
        } else {
          $sub_rep['nail_pdf'] ='';
        }
        
        if(!empty($value['assessioning_num']))
        {
          $pcr_sql = "SELECT * FROM `wp_abd_generated_pcr_reports`  WHERE `accessioning_num` ='".$value['assessioning_num']."'";
          $pcr_result = $this->BlankModel->customquery($pcr_sql);
          
        }
     
        if(!empty($pcr_result))
        {
          $sub_rep['pcr_pdf'] = REPORT_PDF_URL.'pcr_report_pdf/'.$pcr_result[0]['report_pdf_name'];  
          $sub_rep['pcr_pdf_is_downloaded'] =  $pcr_result[0]['is_downloaded'];
        }
        else
        {
          $sub_rep['pcr_pdf'] = '';  
        }
        
        array_push( $submitted_rep, $sub_rep);
      }

      if ($submitted_rep) {
      echo json_encode(array("status"=>"1", "submitted_rep"=> $submitted_rep));
      } else {
      echo json_encode(array("status"=>"0", "submitted_rep"=> 'No data found.'));
      }
    }

    function get_partners(){
      $data           = json_decode(file_get_contents('php://input'), true);

      $user_id        = $data['user_id'];
      // $physician_name = get_user_meta_value($user_id, 'first_name', TRUE).' '.get_user_meta_value($user_id, 'last_name', TRUE);
      $array_data     = array();
      $result         = array();
      $result2        = array();     
      $user_role      = get_user_role($user_id);   
      $extra_sql      = "";   
     
  // if($user_role == "combine_physicians_accounts"){

      if($user_role == 'aps_sales_manager'){
  
       $partners_mgr_count_sql = "SELECT COUNT(r1.user_id) as user_count,r2.partner_id as partner_id, r2.	partner_name as partners_company from (SELECT `ID` as user_id, t1.meta_value as partners_company FROM wp_abd_users INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id WHERE t2.meta_key = 'wp_abd_capabilities' AND t2.meta_value LIKE '%partners_company_manager%' AND t1.meta_key = 'partners_company') r1 RIGHT JOIN `wp_abd_partners_company` r2 ON r2.partner_id = r1.partners_company WHERE r2.status='1' GROUP BY r2.partner_id";
       

        $partners_mgr_count_data = $this->BlankModel->customquery($partners_mgr_count_sql);


        $partners_specimen_count_sql = "SELECT COUNT(r1.spe_id) as specimen_count,r2.partner_id as partners_company FROM 
(SELECT `wp_abd_specimen`.`id` as spe_id,`wp_abd_specimen`.`partners_company` as partners_company
       
       FROM `wp_abd_specimen` 
       
       LEFT JOIN `wp_abd_nail_pathology_report`  
       ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
       
       INNER JOIN `wp_abd_partners_specimen`
       ON `wp_abd_specimen`.`id` = `wp_abd_partners_specimen`.`specimen_id`  
       
       LEFT JOIN `wp_abd_generated_pcr_reports`  
       ON `wp_abd_specimen`.`assessioning_num` = `wp_abd_generated_pcr_reports`.`accessioning_num`  
       WHERE `wp_abd_specimen`.`partners_company` != '' 
       AND (`wp_abd_nail_pathology_report`.`nail_pdf` != '' OR `wp_abd_generated_pcr_reports`.`report_pdf_name` != '')

       AND `wp_abd_specimen`.`status` = '0' 
       AND `wp_abd_specimen`.`physician_accepct` = '0' 
       AND `wp_abd_specimen`.`qc_check` = '0'         
       
       ORDER BY `wp_abd_specimen`.`assessioning_num` DESC) r1 RIGHT JOIN `wp_abd_partners_company` r2 ON r1.partners_company = r2.partner_id WHERE r2.status='1' GROUP BY r2.partner_id";
       

        $partners_specimen_count_data = $this->BlankModel->customquery($partners_specimen_count_sql);
        
          $pertnar_spe = array();
        if($partners_mgr_count_data) {       
         for($i=0;$i<count($partners_mgr_count_data);$i++){
          $pertnar_spe[$i]['partner_id'] = $partners_mgr_count_data[$i]['partner_id'];
          $pertnar_spe[$i]['user_count'] = $partners_mgr_count_data[$i]['user_count'];
          $pertnar_spe[$i]['partners_company'] = $partners_mgr_count_data[$i]['partners_company'];

          if($partners_mgr_count_data[$i]['partner_id'] == $partners_specimen_count_data[$i]['partners_company']){
           $pertnar_spe[$i]['specimen_count'] = $partners_specimen_count_data[$i]['specimen_count'];
          }
         }
        }    

        if ($pertnar_spe) {
        echo json_encode(array("status"=>"1", "partners_data"=> $pertnar_spe));
        } else {
        echo json_encode(array("status"=>"0", "partners_data"=> 'No data found.'));
        }
    }else{
      echo json_encode(array("status"=>"0", "partners_data"=> 'No data found.'));
    } 
  }


  function get_partners_company_managers(){
    $data           = json_decode(file_get_contents('php://input'), true);

    if(!empty($data)){
      $partner_id        = $data['partner_id'];
      // $physician_name = get_user_meta_value($user_id, 'first_name', TRUE).' '.get_user_meta_value($user_id, 'last_name', TRUE);
      $user_role      = $data['user_role'];
      $extra_sql      = "";   
    
  // if($user_role == "combine_physicians_accounts"){

      if($user_role == 'aps_sales_manager'){

      $partners_mgr_sql = "SELECT r2.partner_name as partners_company, r1.mgr_name from (SELECT `ID` as user_id, t1.meta_value as partners_company, concat(t3.meta_value, ' ', t4.meta_value) as mgr_name FROM wp_abd_users INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id INNER JOIN wp_abd_usermeta as t3 ON wp_abd_users.ID = t3.user_id INNER JOIN wp_abd_usermeta as t4 ON wp_abd_users.ID = t4.user_id WHERE t2.meta_key = 'wp_abd_capabilities' AND t2.meta_value LIKE '%partners_company_manager%' AND t1.meta_key = 'partners_company' AND t3.meta_key = 'first_name' AND t4.meta_key = 'last_name' AND t1.meta_value=$partner_id) r1 LEFT JOIN `wp_abd_partners_company` r2 ON r2.partner_id = r1.partners_company WHERE r2.status='1'";
      

        $partners_mgr_data = $this->BlankModel->customquery($partners_mgr_sql);    

        if ($partners_mgr_data) {
        echo json_encode(array("status"=>"1", "partners_mgr_data"=> $partners_mgr_data));
        } else {
        echo json_encode(array("status"=>"0", "partners_mgr_data"=> 'No data found.'));
        }
    }else{
      echo json_encode(array("status"=>"0", "partners_mgr_data"=> 'No data found.'));
    } 
  }

  }

 }
?>