<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class SubmittedReportBiller extends MY_Controller {

    function __construct() {
        parent::__construct();
       
    }
    function index()
    {
    	echo "test SubmittedReportBiller";
    }

    function get_submitted_reports()
    {
      $data       = json_decode(file_get_contents('php://input'), true);
      $user_role  = '';
      $partner_id = '';
      if(isset($data['user_role'])){
        $user_role   = $data['user_role'];
      }
   
      if($user_role == "biller" ){
      $array_data    = array();
      
      $sql2  = "";
      $sql2 .= "SELECT 
        `wp_abd_specimen`.`id`,
        `wp_abd_specimen`.`assessioning_num`,
        `wp_abd_specimen`.`physician_id`,
        `wp_abd_specimen`.`p_lastname`,
        `wp_abd_specimen`.`p_firstname`,
        `wp_abd_specimen`.`create_date` AS `specimen_create_date`,
        `wp_abd_specimen`.`collection_date`,
        `wp_abd_specimen`.`date_received`,
		`wp_abd_nail_pathology_report`.`amend_note`,
		`wp_abd_nail_pathology_report`.`nail_pdf`,
		`wp_abd_nail_pathology_report`.`is_downloaded`,	
		`wp_abd_nail_pathology_report`.`create_date`,
		`wp_abd_generated_pcr_reports`.`report_pdf_name` as `pcr_pdf`,
		`wp_abd_generated_pcr_reports`.`is_downloaded`

       FROM `wp_abd_specimen`        
       LEFT JOIN `wp_abd_nail_pathology_report`  
       ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
       
       LEFT JOIN `wp_abd_generated_pcr_reports` 
       ON `wp_abd_specimen`.`assessioning_num` = `wp_abd_generated_pcr_reports`.`accessioning_num`  
   
       WHERE  `wp_abd_specimen`.`create_date` > '2019-12-01 23:59:59'
       AND (`wp_abd_nail_pathology_report`.`nail_pdf` != '' OR `wp_abd_generated_pcr_reports`.`report_pdf_name` != '')
        
       AND `wp_abd_specimen`.`physician_accepct` = '0' 
       AND `wp_abd_specimen`.`qc_check` = '0'         
       AND `wp_abd_specimen`.`status` = '0' 
       ORDER BY `wp_abd_specimen`.`assessioning_num` DESC ";
     
      $array_data = $this->BlankModel->customquery($sql2);    
     
      $submitted_rep = array();
      $sub_rep       = array();      
      $data_count    = count($array_data);       
      $limit = 100;     
      $start = 1;     
    
      foreach ($array_data as $key => $value) {      	
      	if($start <= $limit) {	    	
        $sub_rep['accessioning_num'] = $value['assessioning_num'];
        $sub_rep['patient_name']     = $value['p_firstname'].' '.$value['p_lastname'];
        $sub_rep['collection_date']  = date('m-d-Y',strtotime($value['collection_date']));
        $sub_rep['date_received']    = date('m-d-Y',strtotime($value['date_received']));
        $sub_rep['is_downloaded']    = $value['is_downloaded'];
        $sub_rep['specimen_id']      = $value['id'];
        
        if($value['amend_note'] != "" ) {
		$sub_rep['amend_note']       = $value['amend_note'];
        }
        else {
		$sub_rep['amend_note']       = "";			
		}       
        
        $physician_first_name = get_user_meta_value( $value['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta_value( $value['physician_id'], 'last_name');
		$sub_rep['physician_name'] = $physician_first_name.' '.$physician_last_name;    
      
        if(!empty($value['nail_pdf'])) {
          $sub_rep['nail_pdf'] = REPORT_PDF_URL.'histo_report_pdf/'.$value['nail_pdf'];
        } else {
          $sub_rep['nail_pdf'] ='';
        }
        
        if(!empty($value['assessioning_num'])) {
          $pcr_sql = "SELECT * FROM `wp_abd_generated_pcr_reports`  WHERE `accessioning_num` ='".$value['assessioning_num']."'";
          $pcr_result = $this->BlankModel->customquery($pcr_sql);           
        }
     
        if(!empty($pcr_result)) {
          $sub_rep['pcr_pdf'] = REPORT_PDF_URL.'pcr_report_pdf/'.$pcr_result[0]['report_pdf_name'];
          $sub_rep['pcr_pdf_is_downloaded'] =  $pcr_result[0]['is_downloaded'];
        }
        else {
          $sub_rep['pcr_pdf'] = '';  
        }
       
        array_push( $submitted_rep, $sub_rep);
        $start ++;       
       }	
	  }

      if ($submitted_rep) {
      echo json_encode(array("status"=>"1", "submitted_rep"=> $submitted_rep, 'count' => $data_count));
      } 
      else {
      echo json_encode(array("status"=>"0", "submitted_rep"=> 'No data found.', 'count' => "0"));
      }
	}  
	else{
	  echo json_encode(array("status"=>"0"));	
	}      
      
  }


////////////////////// search data  //////////////////

    function search_submitted_reports()
    {      
      $data = json_decode(file_get_contents('php://input'), true);
      $paitent_name     = $data['p_name'];
      $accessioning_num = $data['accessioning_num'];
      $physician_name   = $data['physician_name'];
      $test_type        = $data['test_type'];
      $user_role        = '';
      if(isset($data['user_role'])){
      $user_role        = $data['user_role'];
      }
   
      if($user_role == "biller" ){
      $array_data = array();
        
      $sql  ="";
      $sql .= "
      SELECT 
        `wp_abd_specimen`.`id`,
        `wp_abd_specimen`.`assessioning_num`,
        `wp_abd_specimen`.`physician_id`,
        `wp_abd_specimen`.`p_lastname`,
        `wp_abd_specimen`.`p_firstname`,
        `wp_abd_specimen`.`create_date` AS `specimen_create_date`,
        `wp_abd_specimen`.`collection_date`,
        `wp_abd_specimen`.`date_received`,
		`wp_abd_nail_pathology_report`.`amend_note`,
		`wp_abd_nail_pathology_report`.`nail_pdf`,
		`wp_abd_nail_pathology_report`.`is_downloaded`,	
		`wp_abd_nail_pathology_report`.`create_date`,
		`wp_abd_generated_pcr_reports`.`report_pdf_name` as `pcr_pdf`,
		`wp_abd_generated_pcr_reports`.`is_downloaded`

       FROM `wp_abd_specimen` 
       
       LEFT JOIN `wp_abd_nail_pathology_report`  
       ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
       
       LEFT JOIN `wp_abd_generated_pcr_reports` 
       ON `wp_abd_specimen`.`assessioning_num` = `wp_abd_generated_pcr_reports`.`accessioning_num`  
   
       WHERE  `wp_abd_specimen`.`create_date` > '2019-12-01 23:59:59'";      
      
      if( !empty($paitent_name)){
      $sql.=" AND   CONCAT(`wp_abd_specimen`.`p_firstname`, ' ', `wp_abd_specimen`.`p_lastname`) LIKE  '%".$paitent_name."%' OR `wp_abd_specimen`.`p_lastname` LIKE '%".trim($paitent_name)."%' OR `wp_abd_specimen`.`p_firstname` LIKE '%".trim($paitent_name)."%'";
       }
      
      if( !empty($accessioning_num)){
      $sql.=" AND `wp_abd_specimen`.`assessioning_num` LIKE '%".$accessioning_num."%'";
      }

      if( !empty($test_type) && $test_type != 'All' ){
      $sql.=" AND `wp_abd_specimen`.`test_type` LIKE '%".$test_type."%'";
      }       
      
      $sql.=" AND (`wp_abd_nail_pathology_report`.`nail_pdf` != '' OR `wp_abd_generated_pcr_reports`.`report_pdf_name` != '')
       AND `wp_abd_specimen`.`physician_accepct` = '0' 
       AND `wp_abd_specimen`.`qc_check` = '0'         
       AND `wp_abd_specimen`.`status` = '0' 
       ORDER BY `wp_abd_specimen`.`assessioning_num` DESC";
     
      $array_data = $this->BlankModel->customquery($sql);    
    
      $submitted_rep = array();
      $sub_rep       = array();
      foreach ($array_data as $key => $value) {
        $sub_rep['accessioning_num'] = $value['assessioning_num'];
        $sub_rep['patient_name']     = $value['p_firstname'].' '.$value['p_lastname'];
        $sub_rep['collection_date']  = date('m-d-Y',strtotime($value['collection_date']));
        $sub_rep['date_received']    = date('m-d-Y',strtotime($value['date_received']));
        $sub_rep['is_downloaded']    = $value['is_downloaded'];
        $sub_rep['specimen_id']      = $value['id'];
        
        if($value['amend_note'] != "" ) {
		$sub_rep['amend_note']       = $value['amend_note'];
        }
        else {
		$sub_rep['amend_note']       = "";			
		}       
        
        $physician_first_name = get_user_meta_value( $value['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta_value( $value['physician_id'], 'last_name');		
		$sub_rep['physician_name'] = $physician_first_name.' '.$physician_last_name;
          
        if(!empty($value['nail_pdf'])) {
        $sub_rep['nail_pdf'] = REPORT_PDF_URL.'histo_report_pdf/'.$value['nail_pdf'];
        } 
        else {
          $sub_rep['nail_pdf'] ='';
        }
        
        if(!empty($value['assessioning_num'])) {
          $pcr_sql = "SELECT * FROM `wp_abd_generated_pcr_reports`  WHERE `accessioning_num` ='".$value['assessioning_num']."'";
          $pcr_result = $this->BlankModel->customquery($pcr_sql);           
        }
     
        if(!empty($pcr_result)) {
          $sub_rep['pcr_pdf'] = REPORT_PDF_URL.'pcr_report_pdf/'.$pcr_result[0]['report_pdf_name'];
          $sub_rep['pcr_pdf_is_downloaded'] =  $pcr_result[0]['is_downloaded'];
        }
        else {
          $sub_rep['pcr_pdf'] = '';  
        }
          array_push( $submitted_rep, $sub_rep);
      }

      if ($submitted_rep) {
      echo json_encode(array("status"=>"1", "submitted_rep"=> $submitted_rep));
      } 
      else {
      echo json_encode(array("status"=>"0", "submitted_rep"=> 'No data found.'));
      }
	}  
	else{
	  echo json_encode(array("status"=>"0"));	
	}
  
  }
  

 }
?>