<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class CommissionDataEntry extends MY_Controller {

    function __construct() {
        parent::__construct();
       
    }
    function index()
    {
    	echo "test Supplies";
    }

   function physicians_list()
   {
    $physicians = array();
    $status = 'Active';	
	$data = json_decode(file_get_contents('php://input'), true);

	if(isset($data['serach_physician_name'])){		 	
		       
	$sql = "SELECT ID
			FROM wp_abd_users a
			INNER JOIN wp_abd_usermeta um1 ON um1.user_id = a.ID 
			INNER JOIN wp_abd_usermeta b1 ON b1.user_id = a.ID AND b1.meta_key = 'first_name' 
			INNER JOIN wp_abd_usermeta b2 ON b2.user_id = a.ID AND b2.meta_key = 'last_name'			
    		WHERE (um1.meta_key = 'wp_abd_capabilities' AND um1.meta_value LIKE '%physician%')	     
				        
			AND (b1.meta_value LIKE '%".trim($data['serach_physician_name'])."%' 
			OR b2.meta_value LIKE '%".trim($data['serach_physician_name'])."%' 		
			OR a.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '', $data['serach_physician_name']))."%'
			OR a.display_name LIKE '%".trim($data['serach_physician_name'])."%')";
      
	}
	else{			
	$sql = ' 
		SELECT  ID FROM wp_abd_users 
		INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
		INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID  = t2.user_id
		WHERE  t1.meta_key = \'wp_abd_capabilities\' AND ';
	$sql .= 't1.meta_value LIKE \'%"physician"%\' ';
    $sql .= ' 
		   AND t2.meta_key = \'_status\' 		   
		   AND t2.meta_value =  \''.$status.'\'';	 
	$sql .= ' ORDER BY wp_abd_users.user_registered DESC';	
	}  
	
	
    $physicians_list = $this->BlankModel->customquery($sql); 
 	
 	if($physicians_list){
	 
	   $physician_count = 0;  
	   foreach($physicians_list as $physician){
	   	    
	   	    $physician_id= $physician['ID'];
	   	    $entry_month = get_user_meta_value($physician_id, '_month', TRUE);
		    $entry_year  = get_user_meta_value($physician_id, '_year',  TRUE);
		   
		   	if(empty($entry_month)){
				 $month_data = date('n');
			}else{
				 $month_data = $entry_month;
			}
			if(empty($entry_year)){
				 $year_data = date('Y');   
			}else{
				 $year_data = $entry_year;
			}	   	    
	   	  			
		    $charged_amount = get_user_meta_value($physician_id, 'month_'.$month_data.'_'.$year_data.'_chargedamt', TRUE);
		    $paid_amount    = get_user_meta_value($physician_id, 'month_'.$month_data.'_'.$year_data.'_paidamt', TRUE);
		    
			$physician_data = array(		
									'physician_id'     => $physician_id,
							    	'full_name'        => get_user_meta_value($physician_id, 'first_name', TRUE).' '.get_user_meta_value($physician_id, 'last_name', TRUE),
							    	'commission_month' => ($entry_month !="" ? $entry_month : $month_data),
							    	'commission_year'  => ($entry_year!="" ? $entry_year : $year_data),
							    	'charged_amount'   => ($charged_amount != "" ? number_format($charged_amount, 2, '.', ',') : ""),
							    	'paid_amount'      => ($paid_amount != "" ? number_format($paid_amount, 2, '.', ',') : ""),	    	
		  						);
			array_push($physicians, $physician_data);
			$physician_count ++;
	    }
	     $last_data_entry_date = get_user_meta_value('66','last_commissions_entry_date',TRUE);
	    echo json_encode(array('status'=>'1', "physicians_details" => $physicians, 'physicians_count'=> $physician_count, 'current_year' => date('Y'), 'last_data_entry_date'=>$last_data_entry_date));
	}
	else{
        echo json_encode(array('status'=>'0', "message" => 'no data found!'));
     }
  }
  
  function commission_data_entry_update(){
 	
   $data = json_decode(file_get_contents('php://input'), true);
   $user_id    = $data['user_id'];
   $meta_key   = $data['meta_key'];
   $meta_value = $data['meta_value'];
      
   ///////////// Ability Curl Added////////////////////////
    $api_url=$this->config->item('api_url').'General/commission_data_entry_update';  
    common_curl_data($api_url,$data);
        
   if(isset($user_id) && is_numeric($meta_value)){	
	
   if($meta_key == '_month'){		
		
		$month_exits = get_user_meta_value($user_id, $meta_key, TRUE);
	if($month_exits){
		update_user_meta($user_id, $meta_key, $meta_value);
	}
	else{
		add_user_meta($user_id, $meta_key, $meta_value );  
	}	
	$update_month_data = get_user_meta_value($user_id, $meta_key,TRUE); 
	echo json_encode(array('status'=>'1' , 'message' => 'Month Data Updated.' ));
	
	}	
	
   elseif($meta_key == '_year'){	
		
	$year_exits = get_user_meta_value($user_id, $meta_key, TRUE);
	if($year_exits){
		update_user_meta($user_id, $meta_key, $meta_value);
	}
	else{
		add_user_meta($user_id, $meta_key, $meta_value);
	}
	$update_year_data = get_user_meta_value($user_id, $meta_key, TRUE); 
	
	echo json_encode(array('status'=>'1', 'message' => 'Year Data Updated.' ));
	
	}	
	
   else{
		
		$entry_month = get_user_meta_value($user_id, '_month', TRUE);
		$entry_year  = get_user_meta_value($user_id, '_year', TRUE);
        
    	if(empty($entry_month)){
			 $month_data = date('n');
		}else{
			 $month_data = $entry_month;
		}
		if(empty($entry_year)){
			 $year_data = date('Y');   
		}else{
			 $year_data = $entry_year;
		}	  
		
		$datas_meta_key = 'month_'.$month_data.'_'.$year_data.'_'.$meta_key;
		$data_exits = get_user_meta_value($user_id, $datas_meta_key, TRUE);
		if(!empty($meta_value)){
		    $meta_value = str_replace(',', '', $meta_value);			
		 if($data_exits){
			update_user_meta($user_id, $datas_meta_key, $meta_value);
		 }
		 else{
			add_user_meta($user_id, $datas_meta_key, $meta_value);
		   }
		   echo json_encode(array('status'=>'1', 'message' => 'Data Updated.'));   
		}
		else{
	  	   echo json_encode(array('status'=>'0', 'message' => 'Enter Amount.'));
	      }    
        }				
     
   }
   else{
	  echo json_encode(array('status'=>'0', 'message' => 'Error'));
   }     
  } 

////////////////////////////////////
     public function last_date_update()
     {
     	$data = json_decode(file_get_contents('php://input'), true);
     	$last_date = $data['last_date'];
     	$user_id = $data['user_id'];
         
        ///////////// Ability Curl Added////////////////////////
        $api_url=$this->config->item('api_url').'General/last_date_update';  
        common_curl_data($api_url,$data);
         
         
     	if (!empty($last_date) && !empty($user_id)) {
     	$result = update_user_meta ($user_id, 'last_commissions_entry_date' , $last_date);
				if ($result) {
				echo json_encode(array('status'=>'1'));
				}else {
				echo json_encode(array('status'=>'0'));
				}
     	}else{
     		echo json_encode(array('status'=>'0'));
     	}

	 }
	 
	  /**
	 * Import Commission Data Entry Section
	 * 
	 * @return
	 */
	public function import_commission_data(){	 	
		$data = json_decode(file_get_contents('php://input'), true);	 	
		if(!empty($data)){	
	   $import_data         = $data['import_data'];
	   $commision_year      = $data['commision_year'];
	   $commision_month     = $data['commition_month'];
	   $successfully_data = array();
	   $error_data = array();
	   if($commision_year != '' &&  $commision_month != '' && $import_data != '' ){
		   foreach ($import_data as $apiData) {
		  
			   if(isset($apiData['referring_provider']) && trim($apiData['referring_provider']) != '' ){
	     	
				   $name_split_arr = explode(',',$apiData['referring_provider']);   
				
                   $physician_sql = 'SELECT `ID` FROM wp_abd_users u1 
				   JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 
				   JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = "_status")
				   JOIN wp_abd_usermeta m6 ON (m6.user_id = u1.ID AND m6.meta_key = "first_name")	
				   JOIN wp_abd_usermeta m7 ON (m7.user_id = u1.ID AND m7.meta_key = "last_name")	
				   WHERE (m3.meta_value LIKE "%physician%")        
				   AND(m6.meta_value LIKE "%'.trim($name_split_arr[1]).'%"
				   AND m7.meta_value LIKE "%'.trim($name_split_arr[0]).'%"  
				   AND m5.meta_value LIKE "%Active%")';
				   $physician_data = $this->db->query($physician_sql)->row_array();
				  
				   $physician_name = $apiData['referring_provider'];	
				
				   if( isset($physician_data)){
				
				  /**
				   * 
				   * @var Charged Amount
				   * 
				   */
				   if($apiData['total_charges'] != '' && !empty($apiData['total_charges'])){
					  $datas_meta_key = 'month_'.$commision_month.'_'.$commision_year.'_chargedamt';
					  $data_exits = get_user_meta_value($physician_data['ID'], $datas_meta_key, TRUE);
					  $meta_value = $apiData['total_charges'];
					  $meta_value = (double)str_replace(array(',','$'), '', $meta_value);		
					  if($data_exits){
						 update_user_meta($physician_data['ID'], $datas_meta_key, $meta_value);
					   }
					  else{
						 add_user_meta($physician_data['ID'], $datas_meta_key, $meta_value);
					   }	
					}
			   
				  /**
				   * 
				   * @var Paid Amount
				   * 
				   */
				   if($apiData['total_payment'] != '' && !empty($apiData['total_payment'])){
					  $datas_meta_key = 'month_'.$commision_month.'_'.$commision_year.'_paidamt';
					  $data_exits = get_user_meta_value($physician_data['ID'], $datas_meta_key, TRUE);
					  $meta_value = $apiData['total_payment'];
					  $meta_value = (double)str_replace(array(',','$'), '', $meta_value);		
					  if($data_exits){
						 update_user_meta($physician_data['ID'], $datas_meta_key, $meta_value);
					   }
					  else{
						 add_user_meta($physician_data['ID'], $datas_meta_key, $meta_value);
					   }	
					 }
					$successfully_imported = array('physician_name' => trim($physician_name), 'total_charges' => $apiData['total_charges'], 'total_payment' => $apiData['total_payment'] ); 
					
					array_push($successfully_data, $successfully_imported);
					
				   } // End Physician exist condition
				  //Missing Physician
				  else{
					
				  $eror_imported = array('physician_name' => trim($physician_name), 'total_charges' => $apiData['total_charges'], 'total_payment' => $apiData['total_payment'] );
				  
				  array_push($error_data, $eror_imported);
				  
				  }
				  
				  } // Physician Date Condition  

				 } // End For loop
					   
			 } // Commission Data Condition
	   
		  die(json_encode(array('status' => '1', 'message'=> 'Data Updated.', 'error_import' => $error_data, 'successfull_import' => $successfully_data)));
			
		}
	
	}

  }
?>