<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class AbilityPartners extends MY_Controller {

    function __construct() {
        parent::__construct();
       
    }
    function index()
    {
    	echo "test login";
    	echo $filePath = dirname(__FILE__);

    }
	/**
	* @ Ability Partners
	*/

    function abilityDiagnosticsPartnersList() {
    $data = json_decode(file_get_contents('php://input'), true);    
    $current_user_id = $data['user_id'];
    $user_role       = $data['user_role'];
    $partners_data   = array();	
  
	if( $user_role == "aps_sales_manager" || $user_role == "data_entry_oparator"){
		$partner_sql = 'SELECT u1.ID, `b1`.`meta_value` AS `first_name`, `b2`.`meta_value` AS `last_name`, `b3`.`meta_value` AS `status`
		FROM wp_abd_users u1 
		INNER JOIN wp_abd_usermeta as m3 ON u1.ID = m3.user_id AND m3.meta_key = "wp_abd_capabilities" 
		INNER JOIN wp_abd_usermeta as b1 ON u1.ID = b1.user_id AND b1.meta_key = "first_name" 
		INNER JOIN wp_abd_usermeta as b2 ON u1.ID = b2.user_id AND b2.meta_key = "last_name" 
		INNER JOIN wp_abd_usermeta as b3 ON u1.ID = b3.user_id AND b3.meta_key = "_status" 		
        AND (m3.meta_value LIKE "%partner%" ) GROUP BY u1.ID';
 		
    $ability_partners = $this->BlankModel->customquery($partner_sql);      
    $ability_partner_count = 0;  
   
    if($ability_partners){
    foreach($ability_partners as $partner){	     
	    $partner_id     = $partner['ID'];
        $partner_name   = $partner['first_name'].' '.$partner['last_name'];      	
        $partner_status =  $partner['status'];

        $physician_sql = "SELECT COUNT(*) AS total_physician FROM wp_abd_users AS u 
					        JOIN wp_abd_usermeta m1 ON (m1.user_id = u.ID AND m1.meta_key = '_status')
					        LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id 
					        LEFT JOIN wp_abd_usermeta AS um3 ON u.ID = um3.user_id 
					        WHERE um2.meta_key = 'wp_abd_capabilities' 
					        AND um2.meta_value LIKE '%physician%' 
					        AND um3.meta_key = '_assign_partner' 
					        AND um3.meta_value IN($partner_id)
					        AND (m1.meta_value = 'Active')";
					             
		$total_physician = $this->BlankModel->customquery($physician_sql);
		
	    $partner_array_data = array(
								'user_id'           => $partner_id,								
								'tot_phy'           => $total_physician[0]['total_physician'],
								'short_tot_phy'     => (int)$total_physician[0]['total_physician'],
								'partner_name'      => $partner_name,					
								'u_status'          => $partner_status );    
     $ability_partner_count ++;
     array_push($partners_data, $partner_array_data);
     }

     if($total_physician){
	  echo json_encode(array('status'=>'1', 'total_partners' => $ability_partner_count, 'partners_data' => $partners_data));
	 }
    }
	}
    else {
	  echo json_encode(array('status'=>'0'));
    }        
   }

	/**
	* 
	* Partner Details
	*/
	
    function partnerDetails(){
        $data = json_decode(file_get_contents('php://input'), true);
        if($data['id']!=""){
        $user_id = $data['id'];
        $conditions = " ( `ID` = '".$user_id."')";		
        
        $select_fields = '*';
        $is_multy_result = 1;
        $memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
   
        $fname  = get_user_meta_value($user_id, 'first_name',TRUE);
        $lname  = get_user_meta_value($user_id, 'last_name',TRUE);
        $mob    = get_user_meta_value($user_id, '_mobile',TRUE);
        $add    = get_user_meta_value($user_id, '_address',TRUE);
        $fax    = get_user_meta_value($user_id, 'fax',TRUE);
        $user_status = get_user_meta_value($user_id, '_status',TRUE);
        $assign_to   = get_user_meta_value($user_id, 'assign_to',TRUE);
        $company_logo= get_user_meta_value($user_id, 'company_logo',TRUE);
      
        $specimen_information = array('fname' => $fname,
							          'lname' => $lname,							      
							          'email' => $memDetails['user_email'],
							          'user_login' => $memDetails['user_login'],
							          'url' => $memDetails['user_url'],
							          'mob' => $mob,
							          'add' => $add,						
							          'fax' => $fax,
							          'user_status' => $user_status, 
							          'assign_to'   => $assign_to,
							          'company_logo'=> $company_logo,
							          );
        if($specimen_information)
        {
          echo json_encode(array("status" => "1","details" =>$specimen_information));
        }
        else
        {
          echo json_encode(array("status" => "0"));
        }
  
        }
    }

	/**
	* 
	* Update Partner Details
	*/
	
   function updatePartnerDetails() {
	
	 $status = array();
	 if($this->input->post()){
		$data =  $this->input->post();
		$otherdata = 'no';
		$user_id = $this->input->post('user_id');

	     if($_FILES && $_FILES['file_data']['name']){		 		
			$config['upload_path'] = 'D:/server/http/project/2018/part1/ability_new/codes/abadmin/assets/uploads/partner_images';
			$config['allowed_types'] = 'jpeg|jpg|png';
			$config['max_size'] = 5000000;
			$new_name = time().'_'.$_FILES["file_data"]['name'];
			$config['file_name'] = $new_name;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('file_data')){
				die( json_encode(array( "status" => '0', 'message'=>$this->upload->display_errors())));
			}
			else{
				$uploadData = $this->upload->data();
				$fileName = $uploadData['file_name'];
			}
	      }

	$userdata =array();
	if(!empty($data['user_url'])){
		$userdata['user_url'] = $data['user_url'];
	}
	else{
		$userdata['user_url'] = '';
	}

	if(!empty($data['user_email'])){
	$user_email = $data['user_email'];
	$this->db->where('ID !=', $user_id);
	$this->db->where('user_email', $user_email);
	$resData = $this->db->get('wp_abd_users');

	if($resData->num_rows() > 0){
		die(json_encode(array( "status" => "1", "message" => "Email exits. Please try with a new one." )));
	}
	else{
		$userdata['user_email'] = $data['user_email'];
    }
	}
	if(!empty($data['edit_con_pass']) && $data['edit_con_pass'] !== null) {
	 $password = md5(SECURITY_SALT.$data['edit_con_pass']);
	 $userdata['user_pass'] = $password;
	} 

	$conditions = "( `ID` = ".$user_id.")";
	$update = $this->BlankModel->editTableData('users', $userdata, $conditions);

    if($update)
    {
		$otherdata = update_user_meta($user_id, 'first_name', $data['first_name']);
		array_push($status,$otherdata);

		$otherdata = update_user_meta($user_id, 'last_name', $data['last_name']);

		$otherdata = update_user_meta($user_id, '_mobile',  $data['mobile']);
		array_push($status,$otherdata);

		$otherdata = update_user_meta($user_id, '_address', $data['address']);
		array_push($status,$otherdata);	
		
		$otherdata = update_user_meta($user_id, 'fax', $data['fax']);
		array_push($status,$otherdata);

		$otherdata = update_user_meta($user_id, '_status', $data['user_status']);
		array_push($status,$otherdata);
		
		if(isset($fileName)){
		$otherdata = update_user_meta($user_id, 'company_logo', $fileName);
		array_push($status,$otherdata);	
		}
		

    }
        ///////////// Ability Curl Added////////////////////////
        $api_url=$this->config->item('api_url').'General/updatePartnerDetails';  
        common_curl_data($api_url,$data); 
         
	 if($update == 'no' || $update == 'yes' || in_array('yes', $status)){
	  echo json_encode(array("status" => "1","message" => 'User details has been successfully updated.'));
	 }
	  else{
	  echo json_encode(array("status" => "0","message" => 'Something is worng!. Please try again.'));
	  }
	}
	else{
	  echo json_encode(array("status" => "0","message" => 'Something is worng!. Please try again.'));
	}
	}
   
  }


?>