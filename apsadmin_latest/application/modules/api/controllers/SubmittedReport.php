<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class SubmittedReport extends MY_Controller {

    function __construct() {
        parent::__construct();
       
    }
    function index()
    {
    	echo "test SubmittedReport";
    }

    function get_submitted_reports()
    {
      $data           = json_decode(file_get_contents('php://input'), true);

      $user_id        = $data['user_id'];
      $physician_name = get_user_meta_value($user_id, 'first_name', TRUE).' '.get_user_meta_value($user_id, 'last_name', TRUE);
      $array_data     = array();
      $result         = array();
      $result2        = array();     
      $user_role      = get_user_role($user_id);   
      $extra_sql      = "";   
      $totalHistoReportCount=0;
      $totalPcrReportCount=0;
     
	if($user_role == "combine_physicians_accounts"){
	
      $sql = 'SELECT ID FROM wp_abd_users 
		      INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID = t1.user_id 
		      INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID = t2.user_id 
		      WHERE  t1.meta_key = \'wp_abd_capabilities\' AND t2.meta_key = \'combined_by\' AND ( ';
      $sql .= 't1.meta_value LIKE \'%"physician"%\' ';
      $sql .= 'AND t2.meta_value = "'.$user_id.'"';
      $sql .= ' ) ORDER BY wp_abd_users.user_registered DESC';
    
      $physicians_list = $this->BlankModel->customquery($sql);
      $list_of_phy = "";
      if($physicians_list) {       
       foreach($physicians_list as $physician) {    
  		  $list_of_phy.= $physician['ID'].',';	
	    }
       $user_id = trim($list_of_phy, ',');
      }	   
	   $where = " WHERE ( `wp_abd_specimen`.`physician_id` IN(".$user_id."))";
	 } 
	 
	else{	 	
	   $where = " WHERE ( `wp_abd_specimen`.`physician_id` = '".$user_id."')";      
	 } 
	 
	
    
      /**
	  * 
	  * @var Histo Join
	  * 
	  */
     
       $sql = "SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`p_lastname`, `wp_abd_specimen`.`p_firstname`, `wp_abd_specimen`.`create_date` AS `specimen_create_date`, `wp_abd_specimen`.`collection_date`, `wp_abd_specimen`.`date_received`, `wp_abd_nail_pathology_report`.`nail_pdf`,  `wp_abd_nail_pathology_report`.`is_downloaded`,`wp_abd_specimen`.`id`,
`wp_abd_nail_pathology_report`.`create_date` 
       FROM `wp_abd_specimen`      
       INNER JOIN `wp_abd_nail_pathology_report`       
       ON `wp_abd_specimen`.`id` = `wp_abd_nail_pathology_report`.`specimen_id`
       " .$where. " 
       AND `wp_abd_specimen`.`status` = '0' 
       AND `wp_abd_specimen`.`physician_accepct` = '0' 
       AND `wp_abd_specimen`.`qc_check` = '0' 
       AND `wp_abd_nail_pathology_report`.`status` = 'Active'  
       ORDER BY `wp_abd_specimen`.`assessioning_num` DESC
       ";
      $result = $this->BlankModel->customquery($sql); 
	 
	   /**
	   * 
	   * @var PCR Join
	   * 
	   */

      $sql2 ="";
      $sql2 .= "SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`p_lastname`, `wp_abd_specimen`.`p_firstname`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`collection_date`,`wp_abd_specimen`.`date_received`, `wp_abd_generated_pcr_reports`.`report_pdf_name` as `pcr_pdf`,`wp_abd_generated_pcr_reports`.`is_downloaded`,`wp_abd_specimen`.`id` FROM `wp_abd_specimen`
              
       JOIN  `wp_abd_generated_pcr_reports` ON `wp_abd_generated_pcr_reports`.`accessioning_num` = `wp_abd_specimen`.`assessioning_num`
             
        " .$where. "    
      
       AND `wp_abd_specimen`.`status` = '0' 
       AND `wp_abd_specimen`.`physician_accepct` = '0' 
       AND `wp_abd_specimen`.`qc_check` = '0'       
       ORDER BY `wp_abd_generated_pcr_reports`.`accessioning_num` DESC
       ";
     
      $result2 = $this->BlankModel->customquery($sql2);    
      $array_data22 = array_merge($result, $result2);
      $array_data = unique_array($array_data22, "assessioning_num");

     
      $submitted_rep = array();
      $sub_rep = array();
      foreach ($array_data as $key => $value) {
        $sub_rep['assessioning_num'] = $value['assessioning_num'];
        $sub_rep['physician_name']   = $physician_name;
        $sub_rep['patient_name']     = $value['p_firstname'].' '.$value['p_lastname'];
        $sub_rep['collection_date']  = date('m-d-Y',strtotime($value['collection_date']));
        $sub_rep['date_received']    = date('m-d-Y',strtotime($value['date_received']));
        $sub_rep['is_downloaded']    = $value['is_downloaded'];
        $sub_rep['specimen_id']    = $value['id'];
     
        if(!empty($value['nail_pdf'])) {
        $sub_rep['nail_pdf'] = REPORT_PDF_URL.'histo_report_pdf/'.$value['nail_pdf'];
        $totalHistoReportCount++;
        } else {
          $sub_rep['nail_pdf'] ='';
        }
        
        if(!empty($value['assessioning_num']))
        {
          $pcr_sql = "SELECT * FROM `wp_abd_generated_pcr_reports`  WHERE `accessioning_num` ='".$value['assessioning_num']."'";
          $pcr_result = $this->BlankModel->customquery($pcr_sql);
           
        }
     
        if(!empty($pcr_result))
        {
          $sub_rep['pcr_pdf'] = REPORT_PDF_URL.'pcr_report_pdf/'.$pcr_result[0]['report_pdf_name'];
          $sub_rep['pcr_pdf_is_downloaded'] =  $pcr_result[0]['is_downloaded'];
          $totalPcrReportCount++;
        }
        else
        {
          $sub_rep['pcr_pdf'] = '';  
        }
        

        array_push( $submitted_rep, $sub_rep);
      }


      if ($submitted_rep) {
      echo json_encode(array("status"=>"1", "submitted_rep"=> $submitted_rep,'total_pcr_report_count'=>$totalPcrReportCount,'total_histo_report_count'=>$totalHistoReportCount));
      } else {
      echo json_encode(array("status"=>"0", "submitted_rep"=> 'No data found.'));
      }
    }

    //////////////////////////////////////

    public function change_submitted_reports_color()
    {
      $data = json_decode(file_get_contents('php://input'), true);
      $specimen_id = $data['specimen_id'];

        $up_data = array('is_downloaded'=>'1');
        $conditions1 = " ( `specimen_id` = '".$specimen_id."')";
        $result = $this->BlankModel->editTableData('nail_pathology_report', $up_data, $conditions1);

        if($result){
           echo json_encode(array("status"=>"1"));
        } else {
           echo json_encode(array("status"=>"0"));
        }
    }


    ///////////////////////////////////

    public function change_color_for_pcr()
    {
      $data = json_decode(file_get_contents('php://input'), true);
      $accessioning_num = $data['accessioning_num'];

        $up_data = array('is_downloaded'=>'1');
        $conditions1 = " ( `accessioning_num` = '".$accessioning_num."')";
        $result = $this->BlankModel->editTableData('generated_pcr_reports', $up_data, $conditions1);

        if($result){
           echo json_encode(array("status"=>"1"));
        } else {
           echo json_encode(array("status"=>"0"));
        }
    }

////////////////////// search data  //////////////////

    function search_submitted_reports()
    {
      
      $data = json_decode(file_get_contents('php://input'), true);
      $tbl1=SPECIMEN_MAIN;
      $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
      $tbl3=GENERATE_PCR_REPORT_MAIN;
      if($data['dataType'] == 'general'){
          $tbl1=SPECIMEN_MAIN;
          $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;
          $tbl3=GENERATE_PCR_REPORT_MAIN;

      }else if($data['dataType'] == 'archive'){
          $tbl1=SPECIMEN_ARCHIVE;
          $tbl2=NAIL_PATHOLOGY_REPORT_ARCHIVE;
          $tbl3=GENERATE_PCR_REPORT_ARCHIVE;
      }
      $user_id = $data['user_id'];
      $paitent_name = $data['p_lastname'];
      $assessioning_num = $data['assessioning_num'];
      $test_type = $data['test_type'];
      $totalHistoReportCount=0;
      $totalPcrReportCount=0;
      $physician_name = get_user_meta_value($user_id, 'first_name', TRUE).' '.get_user_meta_value($user_id, 'last_name', TRUE);
   
      $array_data = array();
      $result = array();
      $result2 = array();
        
      /**
	  * 
	  * @var Histo Join
	  * 
	  */
      $sql ="";
      $sql .= "SELECT s1.`assessioning_num`, s1.`p_lastname`, s1.`p_firstname`, s1.`create_date`, s1.`collection_date`, s1.`date_received`, np.`nail_pdf`,np.`is_downloaded`,s1.`id` FROM $tbl1 s1
      
       JOIN $tbl2 np ON s1.`id`=np.`specimen_id`
      
       WHERE  s1.`physician_id` = '".$user_id."' 
       AND s1.`status` = '0' 
       AND s1.`physician_accepct` = '0' 
       AND s1.`qc_check` = '0'
       AND np.`status` = 'Active'  ";
    
      if( !empty($paitent_name)){
      $sql.=" AND   CONCAT(s1.`p_firstname`, ' ', s1.`p_lastname`) LIKE  '%".$paitent_name."%' OR s1.`p_lastname` LIKE '%".trim($paitent_name)."%' OR s1.`p_firstname` LIKE '%".trim($paitent_name)."%'";
       }
    
      if( !empty($assessioning_num)){
      $sql.=" AND s1.`assessioning_num` LIKE '%".$assessioning_num."%'";
      }

      if( !empty($test_type) && $test_type!='All'){
      $sql.=" AND s1.`test_type` LIKE '%".$test_type."%'";
      }
       
      $result = $this->BlankModel->customquery($sql); 

      /**
	  * 
	  * @var PCR Join
	  * 
	  */ 

      $sql2 ="";
      $sql2 .= "SELECT s1.`assessioning_num`, s1.`p_lastname`, s1.`p_firstname`, s1.`create_date`, s1.`collection_date`,s1.`date_received`, gp.`report_pdf_name` as `pcr_pdf`,gp.`is_downloaded`,s1.`id` FROM $tbl1 s1
              
       JOIN  `wp_abd_generated_pcr_reports` gp ON gp.`accessioning_num` = s1.`assessioning_num`
             
       WHERE  s1.`physician_id` = '".$user_id."' AND s1.`status` = '0' AND s1.`physician_accepct` = '0' AND s1.`qc_check` = '0'";
    
     
      if( !empty($paitent_name)){
      $sql2.=" AND   CONCAT(s1.`p_firstname`, ' ', s1.`p_lastname`) LIKE  '%".$paitent_name."%' OR s1.`p_lastname` LIKE '%".trim($paitent_name)."%' OR s1.`p_firstname` LIKE '%".trim($paitent_name)."%'";
       }
    
      if( !empty($assessioning_num)){
      $sql2.=" AND s1.`assessioning_num` LIKE '%".$assessioning_num."%'";
      }

      if( !empty($test_type) && $test_type!='All'){
      $sql2.=" AND s1.`test_type` LIKE '%".$test_type."%'";
      }

      $result2 = $this->BlankModel->customquery($sql2); 
   
      $array_data22 = array_merge($result, $result2);
//die(json_encode(array("status"=>$result2)));
      $array_data = unique_array($array_data22, "assessioning_num");
  
    
      $submitted_rep = array();
      $sub_rep = array();
      foreach ($array_data as $key => $value) {
        $sub_rep['assessioning_num'] = $value['assessioning_num'];
        $sub_rep['physician_name'] = $physician_name;
        $sub_rep['patient_name'] = $value['p_firstname'].' '.$value['p_lastname'];
        $sub_rep['collection_date']  = date('m-d-Y',strtotime($value['collection_date']));
        $sub_rep['date_received']    = date('m-d-Y',strtotime($value['date_received']));
        $sub_rep['is_downloaded']   = $value['is_downloaded'];
        $sub_rep['specimen_id']     = $value['id'];
        // $sub_rep['nail_pdf'] = $value['nail_pdf'];
 
        if(!empty($value['nail_pdf'])) {
        $sub_rep['nail_pdf'] = REPORT_PDF_URL.'histo_report_pdf/'.$value['nail_pdf'];
        $totalHistoReportCount++;
        } else {
          $sub_rep['nail_pdf'] ='';
        }
        
        if(!empty($value['assessioning_num']))
        {
          $pcr_sql = "SELECT * FROM $tbl3  WHERE `accessioning_num` ='".$value['assessioning_num']."'";
          $pcr_result = $this->BlankModel->customquery($pcr_sql);
          //print_r($pcr_result); exit;
          
        }
     
        if(!empty($pcr_result))
        {
          $sub_rep['pcr_pdf'] = REPORT_PDF_URL.'pcr_report_pdf/'.$pcr_result[0]['report_pdf_name'];  
          $sub_rep['pcr_pdf_is_downloaded'] =  $pcr_result[0]['is_downloaded'];
          $totalPcrReportCount++;
        }
        else
        {
          $sub_rep['pcr_pdf'] = '';  
        }
        
        array_push( $submitted_rep, $sub_rep);
      }
// print_r( $submitted_rep); exit;

      if ($submitted_rep) {
      echo json_encode(array("status"=>"1", "submitted_rep"=> $submitted_rep,'total_pcr_report_count'=>$totalPcrReportCount,'total_histo_report_count'=>$totalHistoReportCount));
      } else {
      echo json_encode(array("status"=>"0", "submitted_rep"=> 'No data found.'));
      }
    }
  


 
  
 /**
 * 
 * Wounds Positive 
 */
 
  public function get_submitted_reports_for_wounds()
  {
      $data           = json_decode(file_get_contents('php://input'), true);
      $user_id        = $data['user_id'];
     
      $array_data     = array();
      $user_role      = get_user_role($user_id);   
      $totalPcrReportCount = 0;

	   /**
	   * 
	   * @var PCR Join
	   * 
	   */

      $pcr_sql ="";
      $pcr_sql .= "SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`physician_id`, `wp_abd_specimen`.`p_lastname`, `wp_abd_specimen`.`p_firstname`, `wp_abd_specimen`.`test_type`, `wp_abd_specimen`.`collection_date`,`wp_abd_specimen`.`date_received`, `wp_abd_generated_pcr_reports`.`report_pdf_name` as `pcr_pdf`,`wp_abd_generated_pcr_reports`.`is_downloaded`,`wp_abd_specimen`.`id`, `wp_abd_generated_pcr_reports`.`wound_report_status` FROM `wp_abd_specimen`
              
       JOIN  `wp_abd_generated_pcr_reports` ON `wp_abd_generated_pcr_reports`.`accessioning_num` = `wp_abd_specimen`.`assessioning_num`
             
       WHERE ( `wp_abd_generated_pcr_reports`.`wound_report_status` = 'Positive')
       
       AND `wp_abd_specimen`.`test_type` = 'W'  
       AND `wp_abd_specimen`.`status` = '0' 
       AND `wp_abd_specimen`.`physician_accepct` = '0' 
       AND `wp_abd_specimen`.`qc_check` = '0'       
       ORDER BY `wp_abd_generated_pcr_reports`.`accessioning_num` DESC
       ";
     
      $pcr_result = $this->BlankModel->customquery($pcr_sql);  
     
      $submitted_rep = array();
      $sub_rep = array();
      
      if($pcr_result){
	  	
      foreach ($pcr_result as $key => $value) {
       
       if($value['physician_id'] != 0){
       	
       	  $wound_positive_send = get_user_meta_value($value['physician_id'], 'wound_positive_send', TRUE);
       if($wound_positive_send == "Yes"){
			
		
	   	$physician_name = get_user_meta_value($value['physician_id'], 'first_name', TRUE).' '.get_user_meta_value($value['physician_id'], 'last_name', TRUE);
	    
        
        $sub_rep['assessioning_num'] = $value['assessioning_num'];
        $sub_rep['physician_name']   = $physician_name;
        $sub_rep['patient_name']     = $value['p_firstname'].' '.$value['p_lastname'];
        $sub_rep['collection_date']  = date('m-d-Y',strtotime($value['collection_date']));
        $sub_rep['date_received']    = date('m-d-Y',strtotime($value['date_received']));
        $sub_rep['specimen_id']      = $value['id'];
  
        $sub_rep['pcr_pdf'] = REPORT_PDF_URL.'pcr_report_pdf/'.$value['pcr_pdf'];
        $sub_rep['pcr_pdf_is_downloaded'] =  $value['is_downloaded'];
       
        $totalPcrReportCount++;
      
        array_push( $submitted_rep, $sub_rep);
        
         }
	    }
	
	  } 
	  
      if ($submitted_rep) {
      echo json_encode(array("status"=>"1", "submitted_rep"=> $submitted_rep, 'total_pcr_report_count'=>$totalPcrReportCount));
      }
      else {
      echo json_encode(array("status"=>"0", "submitted_rep"=> 'No data found.'));
      }
    
	 }
	 else {
      echo json_encode(array("status"=>"0", "submitted_rep"=> 'No data found.'));
     }
  
    }
 
 
 
	 /**
	 * Search Wound Positive
	 */

 
   function search_submitted_reports_for_wounds()
    {
      
      $data = json_decode(file_get_contents('php://input'), true);          
      $user_id = $data['user_id'];      
      $paitent_name = $data['p_lastname'];
      $accessioning_num = $data['assessioning_num'];
      $totalPcrReportCount=0;
         
      $array_data = array();
      $result = array();
      $result2 = array();
        
     /**
	  * 
	  * @var PCR Join
	  * 
	  */ 

      $pcr_sql ="";
      
      $pcr_sql .= "SELECT s1.`assessioning_num`, s1.`physician_id`, s1.`p_lastname`, s1.`p_firstname`, s1.`create_date`, s1.`collection_date`,s1.`date_received`, s1.`test_type`, gp.`report_pdf_name` as `pcr_pdf`,gp.`is_downloaded`, gp.`wound_report_status`, s1.`id` FROM `wp_abd_specimen` s1
              
       JOIN `wp_abd_generated_pcr_reports` gp ON gp.`accessioning_num` = s1.`assessioning_num`
             
       WHERE gp.`wound_report_status` = 'Positive' 
       AND s1.`status` = '0' 
       AND s1.`physician_accepct` = '0' 
       AND s1.`qc_check` = '0' 
       AND s1.`test_type`= 'W'";
     
      if( !empty($paitent_name)){
      $pcr_sql.=" AND   CONCAT(s1.`p_firstname`, ' ', s1.`p_lastname`) LIKE  '%".$paitent_name."%' OR s1.`p_lastname` LIKE '%".trim($paitent_name)."%' OR s1.`p_firstname` LIKE '%".trim($paitent_name)."%'";
       }
    
      if( !empty($accessioning_num)){
      $pcr_sql.=" AND s1.`assessioning_num` LIKE '%".$accessioning_num."%'";
      }
  
      $pcr_result = $this->BlankModel->customquery($pcr_sql); 
    
      $submitted_rep = array();
      $sub_rep = array();
      
      if($pcr_result){
	  	
      foreach ($pcr_result as $key => $value) {
      if($value['physician_id'] != 0){
       	
       	 $wound_positive_send = get_user_meta_value($value['physician_id'], 'wound_positive_send', TRUE);
      if($wound_positive_send == "Yes"){
			
      	$physician_name = get_user_meta_value($value['physician_id'], 'first_name', TRUE).' '.get_user_meta_value($value['physician_id'], 'last_name', TRUE);
      	
        $sub_rep['assessioning_num'] = $value['assessioning_num'];
        $sub_rep['physician_name']   = $physician_name;
        $sub_rep['patient_name']     = $value['p_firstname'].' '.$value['p_lastname'];
        $sub_rep['collection_date']  = date('m-d-Y',strtotime($value['collection_date']));
        $sub_rep['date_received']    = date('m-d-Y',strtotime($value['date_received']));
        $sub_rep['is_downloaded']    = $value['is_downloaded'];
        $sub_rep['specimen_id']      = $value['id'];
     
        $sub_rep['pcr_pdf'] = REPORT_PDF_URL.'pcr_report_pdf/'.$value['pcr_pdf'];
        $sub_rep['pcr_pdf_is_downloaded'] =  $value['is_downloaded'];
       
        $totalPcrReportCount++;
      
        array_push( $submitted_rep, $sub_rep);
        
         }
	    }
	
	  } 
	
      if ($submitted_rep) {
      echo json_encode(array("status"=>"1", "submitted_rep"=> $submitted_rep, 'total_pcr_report_count'=>$totalPcrReportCount));
      }
      else {
      echo json_encode(array("status"=>"0", "submitted_rep"=> 'No data found.'));
      }
    
	 }
	 else {
      echo json_encode(array("status"=>"0", "submitted_rep"=> 'No data found.'));
     }
     
    }
   
 } 
 
 
 
?>