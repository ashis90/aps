<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Login extends MY_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('MST7MDT');
       
    }
    function index()
    {
    	echo "test login";
	}
	

    function login()
    {
		$details = array();
		$data = json_decode(file_get_contents('php://input'), true);
	    $email = $data['username'];
	    $pass = md5(SECURITY_SALT.$data['password']);
		$token = md5(openssl_random_pseudo_bytes(32));
		$new_token = hash('sha256', $token);

    	$conditions = " (user_login ='".$email."' AND `user_pass` = '".$pass."' AND `user_status` = '0' )";
		$select_fields = '*';
		$is_multy_result = 1;
		$memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
		
		if($memDetails['user_pass'] == $pass)
		{
			if(!empty($memDetails['ID']))
		    {
		
			$time = date('Y-m-d H:i:s');
			$current_login_time = strtotime($time);
			$usermetaedit = update_user_meta ($memDetails['ID'], 'when_last_login' , $current_login_time);
		   }

			$up_data = array('user_activation_key'=>$new_token);
		    $conditions1 = " ( `user_login` = '".$email."')";
		    $usersedit = $this->BlankModel->editTableData('users', $up_data, $conditions1);

		    $conditions = " (user_login ='".$email."' AND `user_pass` = '".$pass."' AND `user_activation_key` = '".$new_token."' AND `user_status` = '0' )";
		    $select_fields = '*';
		    $is_multy_result = 1;
		    $memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);

			$det_row['token'] =  $memDetails['user_activation_key'];
			$det_row['id'] = $memDetails['ID'];
            $det_row['user_login'] = $memDetails['user_login'];
            $det_row['user_pass'] = $memDetails['user_pass'];
            $det_row['user_nicename'] = $memDetails['user_nicename'];
            $det_row['user_email'] = $memDetails['user_email'];
            $det_row['user_url'] = $memDetails['user_url']; 
            $det_row['user_registered'] = $memDetails['user_registered']; 
            $det_row['user_status'] = $memDetails['user_status']; 
			$det_row['display_name'] = $memDetails['display_name'];
			$det_row['user_role'] = get_user_role($memDetails['ID']); 
			$det_row['view_popup'] = get_user_meta_value($memDetails['ID'],'view_popup'); 
			$det_row['training_timestamp'] = get_user_meta_value($memDetails['ID'],'training_timestamp');
			$details = $det_row ;
			$uid = $memDetails['ID'];
			$ulogin = $memDetails['user_login'];
			$uemail = $memDetails['user_email'];
			$this->db->insert('wp_abd_user_log',array('user_id'=>$uid,'user_login'=>$ulogin,'user_email'=>$uemail,'site_view'=>'aps', 'time_stamp' => date("Y-m-d h:i:s")));

			$user_log_curl = array('user_id'=>$uid,'user_login'=>$ulogin,'user_email'=>$uemail,'site_view'=>'aps', 'time_stamp' => date("Y-m-d h:i:s"));

			common_add_data_curl('wp_abd_user_log',$user_log_curl);

			die( json_encode(array("status" => "1","details" => $details)));			
		} else
		  {
			die( json_encode(array("status" => "0")));
		  }
	}

	function forgotPassword()
	{
		$data = json_decode(file_get_contents('php://input'), true);
	    $email = $data['userEmail'];
	    $defaultPass= 'ADsaltlake';
	    $password = md5(SECURITY_SALT.$defaultPass);

	    $conditions = " (`user_email` ='".$email."' AND `user_status` = '0')";
		$select_fields = '*';
		$is_multy_result = 1;
		$userDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);

		if (isset($userDetails) && !empty($userDetails)) {

		 $conditions = " ( `ID` = '".$userDetails['ID']."')";
  		 $user_data = array('user_pass' => $password);
  		 $sql_result = $this->BlankModel->editTableData('users', $user_data, $conditions);

  		 $user_message = "";
		$MailFormat= '<div>
		<table width="100%" border="0" cellpadding="2" cellspacing="2">
		
		<tr>
		<td align="left" valign="top">Password:</td>
		<td align="left" valign="top">'.$defaultPass.'</td>
		</tr>
		</table>
		</div>';
		
		$user_message .= "Dear user,<br><br> Your password is given below. <br/><br/>";
		$user_message .= $MailFormat;
		$user_message .= "Thank You<br>";
		$user_message .= "Antares Physician Services Teams<br>";		
		$user_body     = mailBody($user_message);
		
		$from          = "info@abilitydiagnostics.com";
		$subject       = "Antares Physician Services Login Details";
		$result        = sendMail($email, $subject, $user_body, $from, "Antares Physician Services");
		
		if($result){
			die( json_encode(array("status" => "1","message" => "Your password is sent to your email. Please check.")));
		  } else {
			die( json_encode(array("status" => "0","message" => "Something want worng. Please try again.")));
		  }

		} else{
            die( json_encode(array("status" => "0","message" => "Email does not exist. Please enter registered email.")));
		}

	}



	function user_exists()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$conditions = " ( `user_activation_key` = '".$data."' AND `user_status` = '0' AND `user_activation_key`!= '')";
		$select_fields = '*';
		$is_multy_result = 1;
		$memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
		if($memDetails)
		{
			echo json_encode(array("status" => "1"));
		}
		else
		{
			echo json_encode(array("status" => "0"));
		}
	}


	function get_user_details()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$conditions = " ( `user_activation_key` = '".$data."' AND `user_status` = '0' AND `user_activation_key`!= '')";
		$select_fields = '*';
		$is_multy_result = 1;
		$memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
		if($memDetails)
		{
			echo json_encode($memDetails);
		}
		else
		{
			echo json_encode(array("status" => "0"));
		}
	}



	function logout_user()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$up_data = array('user_activation_key'=>'');
		$conditions1 = " ( `user_activation_key` = '".$data."')";
		$usersedit = $this->BlankModel->editTableData('users', $up_data, $conditions1);
		if($usersedit)
		{
			echo json_encode(array("status" => "1"));
		}
		else
		{
			echo json_encode(array("status" => "0"));
		}
	}


	function user_details()
	{	
		$data = json_decode(file_get_contents('php://input'), true);
		$id = intval($data['userid']);
		$user_id = "'".$id."'";

		$conditions = " ( `ID` = '".$id."')";
		$select_fields = '*';
		$is_multy_result = 1;
		$memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
		

		$fname = get_user_meta_value( $user_id, 'first_name', TRUE);
		$lname = get_user_meta_value( $user_id, 'last_name', TRUE);
		$mob  = get_user_meta_value( $user_id, '_mobile', TRUE);
		$add =  get_user_meta_value( $user_id, '_address', TRUE);
		$clinic = get_user_meta_value( $user_id, 'clinic_addrs', TRUE);
		$fax =  get_user_meta_value( $user_id, 'fax', TRUE);
		$cell = get_user_meta_value( $user_id, 'cell_phone', TRUE);
		$manager = get_user_meta_value( $user_id, 'manager_contact_name', TRUE);
		$manager_cell = get_user_meta_value( $user_id, 'manager_cell_number', TRUE);
		$offce_num = get_user_meta_value( $user_id, 'office_number', TRUE);
		$npi_api = get_user_meta_value( $user_id, 'npi', TRUE);
		$bank_routing_num = get_user_meta_value( $user_id, 'bank_routing_num', TRUE);
		$bank_account_num = get_user_meta_value( $user_id, 'bank_account_num', TRUE);
		$name_on_bank = get_user_meta_value( $user_id, '_nbaccount', TRUE);
		$wound_positive_send = get_user_meta_value( $user_id, 'wound_positive_send', TRUE);
        $pharmd_notification_type = get_user_meta_value( $user_id, 'pharmd_notification_type', TRUE);
		$role =get_user_role($memDetails['ID']);

		$specimen_information = array('fname' => $fname,'lname' => $lname,'email' => $memDetails['user_email'],'user_login'=>$memDetails['user_login'],'url' => $memDetails['user_url'], 'mob' => $mob,'add' => $add,'clinic' => $clinic,'fax' => $fax,'cell' => $cell,'manager' => $manager, 'manager_cell' => $manager_cell, 'offce_num' => $offce_num,'npi_api' => $npi_api,'role' => $role,'bank_routing_num' => $bank_routing_num,'bank_account_num' => $bank_account_num,'nbaccount' => $name_on_bank,'wound_positive_send'=>$wound_positive_send,"pharmd_notification_type"=>$pharmd_notification_type);
	
		if($specimen_information)
		{
			echo json_encode(array("status" => "1","details" =>$specimen_information));
		}
		else
		{
			echo json_encode(array("status" => "0"));
		}

	}



	function update_user_details()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$status = array();
		$user_id = $data['user_id'];
		
	$otherdata = update_user_meta ($user_id, 'first_name' , $data['fname']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, 'last_name' , $data['lname']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, '_mobile' , $data['mobile']);
	array_push($status,$otherdata);
	
	$otherdata = update_user_meta ($user_id, '_address' , $data['add']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, 'clinic_addrs' , $data['clinic']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, 'fax' , $data['fax']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, 'cell_phone' , $data['cell']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, 'manager_contact_name' , $data['manager']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, 'manager_cell_number' , $data['manager_cell']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, 'npi' , $data['npi_api']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, '_brn' , $data['brn']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, '_ban' , $data['ban']);
    array_push($status,$otherdata);

	$otherdata = update_user_meta ($user_id, '_nbaccount' , $data['nbaccount']);
	array_push($status,$otherdata);
    
    $otherdata_pharmd_notification_type = update_user_meta($user_id, 'pharmd_notification_type', $data['pharmd_notification_type']);
    array_push($status, $otherdata_pharmd_notification_type);
	
	$otherdata_wound_positive_send = update_user_meta($user_id, 'wound_positive_send', $data['wound_positive_send']);
    array_push($status, $otherdata_wound_positive_send);
    $otherdata_wound_positive_send = update_user_meta($user_id, 'wound_positive_send_timestamp', date('Y-m-d H:i:s'));
    array_push($status, $otherdata_wound_positive_send);

	$up_data = array('user_url'=>$data['user_url']);
	$conditions1 = " ( `ID` = '".$user_id."')";
	$update = $this->BlankModel->editTableData('users', $up_data, $conditions1);

		
 if(($data['user_pass'] === $data['con_pass']) && !empty($data['user_pass']) && !empty($data['con_pass']))
		{
		$con_pass = md5(SECURITY_SALT.$data['con_pass']);

		$up_data = array('user_pass'=>$con_pass);
		$conditions1 = " ( `ID` = '".$user_id."')";
		$update = $this->BlankModel->editTableData('users', $up_data, $conditions1);
		}
	
    ///////////// Ability Curl Added////////////////////////
    $api_url=$this->config->item('api_url').'General/update_user_details';  
    common_curl_data($api_url,$data);
        
	if($update == 'no' ||$update == 'yes' || in_array('yes',$status)) {
   echo json_encode(array("status" => "1","message" => 'User details has been successfully updated.'));
   } else {
   echo json_encode(array("status" => "0","message" => 'Something is worng!. Please try again.'));
   }
	}

	
function send_contact_mail()
			{
				$data=json_decode(file_get_contents('php://input'), true);

				$MailFormat = "<div class='cont_wrapperInner' style='border:4px solid #ff0000;width:670px;padding:10px;text-align:justify;'>
			<p style='text-align: center'><img src='".base_url()."assets/uploads/thumb/logo_aps.png' alt='logo' align='middle'></p>
			<h1 style='color:#FF0000' align='center'>Contact Us</h1>
				Dear Admin,<br> We have received a message from ".$data['name']."<br/>Details are listed below:<br/>
				<div class='cont_wrapperInner'>
				<table width='100%' border='0' cellpadding='2' cellspacing='2'>
				<tr>
					<td align='left' valign='top'>Name:</td>
					<td align='left' valign='top'>".$data['name']."</td>
				</tr> 
				<tr>
					<td align='left' valign='top'>Email:</td>
					<td align='left' valign='top'>".$data['email']."</td>
				</tr>
				<tr>
					<td align='left' valign='top'>Message:</td>
					<td align='left' valign='top'>".$data['message']."</td>
				</tr> 
				</table>
				<br>
				</div>Thank You<br>Antares Physician Services Team<br> </div>";
				$message = $MailFormat;
				
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From:Antares Physician Services <support@abilitydiagnostics.com>\r\n";
				$headers .= 'Bcc: ashis@sleekinfosolutions.com' . "\r\n";   
				$to_email = "support@abilitydiagnostics.com";
				 
				$subject  = $data['name']." Sent A Contact Request - ";
			 
				$this->load->library('email');

		        $this->email->from('support@abilitydiagnostics.com', 'Antares Physician Services');
		        //$this->email->to('anurag@sleekinfosolutions.com');
		        //$this->email->to($stage_det['user_email']);
		        $this->email->to($to_email);
		        $this->email->cc('steele@abilitydiagnostics.com');
		        $this->email->subject($subject);
		        $this->email->message($message);
		        $this->email->set_mailtype('html');
		        $this->email->send();
	
				echo json_encode(array('status'=>'1'));
			}		

	

}

?>