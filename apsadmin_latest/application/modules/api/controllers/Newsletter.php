<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Newsletter extends MY_Controller {

    function __construct() {
        parent::__construct();       
    }
    function index()
    {
    	echo "test Physician service";
    }


/////////////// get all physician and sales representative////////////////////////

	public function get_phy_and_sales()
	{
		$data = json_decode(file_get_contents('php://input'), true);
    $phy_list = array();
  
    $roles =  'physician';
	$sql = ' 
	SELECT  ID 
	FROM    wp_abd_users INNER JOIN wp_abd_usermeta
	ON      wp_abd_users.ID          = wp_abd_usermeta.user_id 
	WHERE   wp_abd_usermeta.meta_key = \'wp_abd_capabilities\' 
	AND     ( 
	';  
    $sql .= 'wp_abd_usermeta.meta_value LIKE \'%"'.$roles.'"%\' ';
    $sql .= ' ) ORDER BY display_name ASC';  
   
    $physicians_list = $this->BlankModel->customquery($sql);  	
 	if($physicians_list){
    $physician_count = 0;  
	foreach($physicians_list as $physician){
	$physician_id = $physician['ID'];
    $fname = get_user_meta_value($physician_id, 'first_name',TRUE);
	$lname = get_user_meta_value($physician_id, 'last_name',TRUE);
	$fullname = $fname.' '.$lname;
	$phy = array(
		'id'=>$physician_id,
		'phy_name' => $fullname,
	);
	array_push($phy_list,$phy);
	}
	  }


	  
// sales representative
    $sal_list = array();
	  $sales_reps_sql = 'SELECT `ID` FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 		
        WHERE (m3.meta_value LIKE "%sales_regional_manager%"  
        OR m3.meta_value LIKE  "%team_sales_manager%"  
        OR m3.meta_value LIKE  "%sales%"  
		OR m3.meta_value LIKE  "%team_regional_Sales%")';

		 $sales_list = $this->BlankModel->customquery($sales_reps_sql);  	
		 if($sales_list){
		$physician_count = 0;  
		foreach($sales_list as $physician){
		$physician_id = $physician['ID'];
		$fullname = get_user_meta_value($physician_id, 'first_name',TRUE).' '.get_user_meta_value($physician_id, 'last_name',TRUE);
		$sal = array(
			'id'=>$physician_id,
			'sal_name' => $fullname,
		);
		array_push($sal_list,$sal);
		}
		  }

		if ($phy_list) {
		echo json_encode(array("status" => "1", "physician" => $phy_list, "sales" => $sal_list));
		} else {
		echo json_encode(array("status" => "0", "message" => 'Something is worng!.'));
		}
	}




	public function send_newsletter()
	{
		$config['mailtype'] = 'html';
        $config['charset'] = 'UTF-8';
        $config['newline'] = "\r\n";
		$data = json_decode(file_get_contents('php://input'), true);
		// print_r($data);
		$news_content = $data['news_content'];
		$physician_id = $data['physician_id'];
		$sales_id = $data['sales_id'];
		$seleted_id = array_merge($physician_id,$sales_id);

		$phy_id = array();
		$id = array();
		foreach ($seleted_id as $value) {
			if(isset($value['checked']) && $value['checked']=='1'){
				$id = $value['id'];
				 $id['phy_id'] = $value['id'].'/';
				 array_push($phy_id,$id);
		
			$condition ="id = $id";
			$field = 'user_email';
			$get_data = 1;
			$get_email = $this->BlankModel->getTableData('wp_abd_users', $condition, $field, $get_data);
			$email = $get_email['user_email'];
			$name = get_user_meta_value($id, 'first_name',TRUE).' '.get_user_meta_value($id, 'last_name',TRUE);
           
			$to = $email;
			$from = "support@abilitydiagnostics.com";
			$mail_format    = "";
			$mail_format .= "<div class='cont_wrapperInner' style='border:4px solid #34b6e6;width:670px;padding:10px;text-align:justify;'>
			<p style='text-align: center'><img src='".base_url()."assets/images/logo.png' alt='logo' align='middle'></p>
			<h1 style='color:#2e4b01' align='center'>Ability Diagonostics</h1>
			<table width='100%' border='0' cellpadding='2' cellspacing='2' style='background:none; border:none;margin-top:25px;'>
			<tr>
			<td colspan='2' style='font:normal 13px/18px Arial, Helvetica, sans-serif; font-weight:bold; border:none;'>Hello, " .$name." 	<br>
			</td>
			</tr>	
			<tr>
			<td>".$news_content."</td>
			</tr>
			</table>
			<br>
			<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'> Thanks,</span> <br>
			<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'> Antares Physicians Services</span><br>			
			</div>";
			
			// $bcc .= "ashis@sleekinfosolutions.com,demoforclient@gmail.com,trajanking@gmail.com";
			// $mail_send = $obj->sendMail($to, 'Nail Fungal Pathology Reports Available', $mail_format, $from, 'Ability Diagnostics', '', '', $bcc);

			$this->load->library('email',$config);
			$this->email->from($from, 'Ability Diagnostics');
			$this->email->to($to);
			$this->email->subject('News letter');
			$this->email->message($mail_format);
			$admin = $this->email->send();
			}
		}
		$total_id = count($phy_id);
		if($total_id == '0'){
			echo json_encode(array("status" => "3", "message" => 'Please select at least one physicians or sales representative!.'));
		} elseif ($admin) {
		echo json_encode(array("status" => "1", "message" => 'News letter sent successfully!'));
		} else {
		echo json_encode(array("status" => "0", "message" => 'Something is worng!.'));
		}
	}

	



   }