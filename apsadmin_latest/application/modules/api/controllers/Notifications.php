<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Notifications extends MY_Controller {

    function __construct() {
        parent::__construct(); 
        date_default_timezone_set('MST7MDT');      
    }
    function index()
    {
    	echo "test Notifications service";
    }


////// get all notifications/////////////////////

public function get_notifications()
{
	$data = json_decode(file_get_contents('php://input'), true);
	$user_id = $data['user_id'];
	$user_role = $data['user_role'];

	if($user_role == 'sales'|| $user_role == 'data_entry_oparator' || $user_role == 'sales_regional_manager' ){
		$condition = " `users_id` = '".$user_id."' AND `status` = '0' AND `note_for` ='Sales' AND DATE(`note_publish_date`) <= CURDATE()";
		} 
	else{ 
	    $condition =" `status` = '0' AND `note_for` = 'Sales' AND DATE(`note_publish_date`) <= CURDATE() GROUP BY `notification_details` "; 
	}
	
	$field = '*';
	$get_data = 0;
	$get_incom_notification = $this->BlankModel->getTableData('notification', $condition, $field, $get_data,'note_id','DESC');
	
	if(empty($get_incom_notification)){
		$get_incom_notification = 'There have no data';
		$count_incom = 0;
	}
	else{
		$count_incom = count( $get_incom_notification);
	}

	if($user_role == 'sales'|| $user_role == 'data_entry_oparator' || $user_role == 'sales_regional_manager' ){
		$condition = "`users_id` ='".$user_id."' AND `status` = '1' GROUP BY `notification_details`";
		}
	else{
		$condition ="`status` = '1' GROUP BY `notification_details`"; 
		}
		
		$com_notification = $this->BlankModel->getTableData('notification', $condition, $field, $get_data,'modify_date','DESC');

		$com_notifi = array();
		$notification = array();
		if ($com_notification ) {
		foreach ($com_notification as $value) {
			$reader_id = $value['reader_id'];
			$notification['reader_name'] = get_user_meta_value( $reader_id, 'first_name', TRUE).' '.get_user_meta_value( $reader_id, 'last_name', TRUE);
			$notification['note_id'] = $value['note_id'];
			$notification['reader_id'] = $value['reader_id'];
			$notification['notification_details'] = $value['notification_details'];
			$notification['note_read'] = $value['note_read'];
			$notification['modify_date'] = $value['modify_date'];
			array_push($com_notifi, $notification);
		    }
	    }

		if(empty($com_notifi)){
			$com_notifi = 'There have no data';
			$count_com = 0;
		} else {
			$count_com = count( $com_notifi);
		}

	if ($count_com== 0 &&	$count_incom == 0) {
		echo json_encode(array("status" => "0", "incom_notification" => 'Something is worng!.',"count_com"=>$count_com, "count_incom"=>$count_incom));
	} 
	else {
		echo json_encode(array("status" => "1", "incom_notification" => $get_incom_notification, "com_notification"=> $com_notifi, "count_com"=>$count_com, "count_incom"=>$count_incom));
	}
	
}

// get unread notifications for header section
public function get_unread_notifications () {
	$data = json_decode(file_get_contents('php://input'), true);
	$user_id = $data['user_id'];
	$user_role = $data['user_role'];

	if($user_role == 'sales'|| $user_role == 'data_entry_oparator' || $user_role == 'sales_regional_manager'|| $user_role == 'team_sales_manager' || $user_role =='team_regional_Sales'){
		$condition = " `users_id` = '".$user_id."' AND `status` = '0' AND `note_read`= 'Unread' AND `note_for` ='Sales' AND DATE(`note_publish_date`) <= CURDATE() GROUP BY `notification_details`";
		} 
	else{ 
	    $condition =" `status` = '0' AND `note_for` = 'Sales' AND `note_read`= 'Unread' AND DATE(`note_publish_date`) <= CURDATE() GROUP BY `notification_details` "; 
	}
	
	$field = '*';
	$get_data = 0;
	$get_incom_notification = $this->BlankModel->getTableData('notification', $condition, $field, $get_data,'note_id','DESC');
	
	if(empty($get_incom_notification)){
		$get_incom_notification = 'There have no data';
		$count_incom = 0;
	}
	else{
		$count_incom = count( $get_incom_notification);
	}

	if($user_role == 'sales'|| $user_role == 'data_entry_oparator' || $user_role == 'sales_regional_manager' ){
		$condition = " `users_id` ='".$user_id."' AND `status` = '1' GROUP BY `notification_details`";
		}
	else{
		$condition =" `status` = '1' GROUP BY `notification_details`"; 
		}
		
		$com_notification = $this->BlankModel->getTableData('notification', $condition, $field, $get_data,'modify_date','DESC');

		$com_notifi = array();
		$notification = array();
		if ($com_notification ) {
		foreach ($com_notification as $value) {
			$reader_id = $value['reader_id'];
			$notification['reader_name'] = get_user_meta_value( $reader_id, 'first_name', TRUE).' '.get_user_meta_value( $reader_id, 'last_name', TRUE);
			$notification['note_id'] = $value['note_id'];
			$notification['reader_id'] = $value['reader_id'];
			$notification['notification_details'] = $value['notification_details'];
			$notification['note_read'] = $value['note_read'];
			$notification['modify_date'] = $value['modify_date'];
			array_push($com_notifi, $notification);
		}
	}


		if(empty($com_notifi)){
			$com_notifi = 'There have no data';
			$count_com = 0;
		} 
		else {
			$count_com = count( $com_notifi);
		}


	if ($count_com== 0 &&	$count_incom == 0) {
		echo json_encode(array("status" => "0", "incom_notification" => 'Something is worng!.',"count_com"=>$count_com, "count_incom"=>$count_incom));
	} 
	else {
		echo json_encode(array("status" => "1", "incom_notification" => $get_incom_notification, "com_notification"=> $com_notifi, "count_com"=>$count_com, "count_incom"=>$count_incom));
	}
 
 }



public function read_unread()
{
	$data = json_decode(file_get_contents('php://input'), true);
	$note_id = $data['note_id'];
	$user_id = $data['user_id'];
	
	$curr_date = date("Y-m-d h:i:sa");
	$up_data = array('note_read'=> 'Read', 'modify_date'=>$curr_date, 'reader_id'=>$user_id);
	$conditions1 = " ( `note_id` = '".$note_id."')";
	$update = $this->BlankModel->editTableData('notification', $up_data, $conditions1);
	if($update =='yes') {
		echo json_encode(array("status" => "1", "message" => 'Readed successfully!.'));
	} else {
		echo json_encode(array("status" => "0", "message" => 'Something is worng. Please try again!.'));
	}
}

public function change_status()
{
	$data = json_decode(file_get_contents('php://input'), true);
	$note_id = $data['note_id'];
	$user_id = $data['user_id'];
	$curr_date = date("Y-m-d h:i:sa");
	$up_data = array('status'=> '1', 'modify_date'=>$curr_date, 'reader_id'=>$user_id);
	$conditions1 = " ( `note_id` = '".$note_id."')";
	$update = $this->BlankModel->editTableData('notification', $up_data, $conditions1);
	if($update =='yes') {
		echo json_encode(array("status" => "1", "message" => 'Completed notification.'));
	} else {
		echo json_encode(array("status" => "0", "message" => 'Something is worng. Please try again!.'));
	}
}


//Notifications Add

     function sales_notification(){
	      $nail_report_specimen_det = "SELECT `physician_id`, count(specimen.physician_id) as report_count,`specimen_id` as `specimen` FROM 
									   (SELECT `specimen_id`,`nail_funagl_id` FROM wp_abd_nail_pathology_report) report
										 INNER JOIN 
									   (SELECT `id`, `physician_id` FROM wp_abd_specimen) specimen
										 ON
									    report.specimen_id = specimen.id GROUP BY specimen.physician_id";
								 
		  $get_specimen_report = $this->db->query($nail_report_specimen_det)->result_array();
		  foreach($get_specimen_report as $spe_rep){
		  	   $sales_id = get_user_meta_value( $spe_rep['physician_id'], 'added_by', true );
			
			if($sales_id == "" || $sales_id == 0 || empty($sales_id)){
			   $sales_id = "66";
			} 
			
			if($spe_rep['report_count'] == '1'){				 
			   $note_count = $this->db->query("SELECT `note_id` FROM wp_abd_notification WHERE `users_id` = '".$sales_id."' AND `specimen_id` = '".$spe_rep['specimen']."' AND `physician_id` = '".$spe_rep['physician_id']."' AND `note_for` = 'Sales'")->row_array();  
			   if(!empty($note_count)){
			   }
			   else{
				$fname = get_user_meta_value($spe_rep['physician_id'], 'first_name',true);
				$lname = get_user_meta_value($spe_rep['physician_id'],'last_name',true );
				$physician_name = $fname.' '.$lname;
				  if( $physician_name == ""){
					  $physician_sql  = $this->db->query("SELECT `user_login` FROM wp_abd_users WHERE `ID` = '".$spe_rep['physician_id']."'")->row_array();  
					  $physician_name = $physician_sql['user_login'].'’s';
					  }
				$sales_notification_details ="Dr. " .$physician_name." just got his first report.";  	
				
				$notification_insert_array = array(
													'users_id'     => $sales_id,
													'specimen_id'  => $spe_rep['specimen'],
													'physician_id' => $spe_rep['physician_id'],
													'notification_details' =>  nl2br($sales_notification_details),
													'note_read'		       => 'Unread',
													'note_publish_date'    => date('Y-m-d'),
													'notification_details_aft_date' => '0',
													'status'               => '0',
													'note_for'		       => 'Sales',
													'create_date'          => date('Y-m-d H:i:s')
													);
				$insert_notification = $this->db->insert('wp_abd_notification', $notification_insert_array);		
				}
			  }
		   } 		  
		}
		
   }