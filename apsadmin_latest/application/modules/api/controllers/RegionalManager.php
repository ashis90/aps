<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class RegionalManager extends MY_Controller {

    function __construct() {
        parent::__construct();
       
    }
    function index()
    {
    	echo "test login";
    }
	/**
	* 
	 Regional Managers List
	*/
    function regionalManagersList()
    {
    $data = json_decode(file_get_contents('php://input'), true);
 
    $total_char_amt = 0;
   	$charged_amt_for_own =0;
    $curr_month = date('n');
    $curr_year  = date('Y');	 
    $regional_manager_array = array();	
    $total_pa_amt = 0;	
    $paid_amt_for_own = 0;	
    $total_charged_amt = 0;	
    $total_paid_amt = 0;	
    
    $sql ="SELECT `ID` FROM wp_abd_users u1 
           JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 		
           WHERE (m3.meta_value LIKE '%sales_regional_manager%'  
           OR m3.meta_value LIKE  '%regional_manager%'  
           OR m3.meta_value LIKE  '%team_regional_Sales%' ) ORDER BY user_registered DESC  LIMIT 0,10";
        
    $regional_managers = $this->BlankModel->customquery($sql); 
    $regional_managers_count = 0;  
	
	foreach($regional_managers as $regional){		
	$user_id = $regional['ID'];
	$user_role = get_user_role($user_id);
	$regional_manager_status = get_user_meta_value($user_id, '_status', TRUE);
	$regional_manager_name   = get_user_meta_value($user_id, 'first_name', TRUE).' '.get_user_meta_value($user_id, 'last_name', TRUE);
	
	$sales_reps_sql =  'SELECT count(ID) as count_id FROM wp_abd_users u1 
	                 	JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = "_status")
						JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "assign_to") 
						JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 		
				        WHERE (m3.meta_value LIKE "%sales_regional_manager%"  
				        OR m3.meta_value LIKE  "%sales%"   
				        OR m3.meta_value LIKE  "%team_regional_Sales%")
				        AND (m4.meta_value = "'.$user_id.'" )
				        AND m1.meta_value = "Active"';
	
	$sales_reps_count = $this->BlankModel->customquery($sales_reps_sql);	
	$total_sales_ref = $sales_reps_count[0]['count_id'];  
  
   if( $user_role == "team_regional_Sales" || $user_role == "sales_regional_manager" )
    {
       $total_sales = 1+ $total_sales_ref;      
    }
    else
    {
       $total_sales = $total_sales_ref;     
    }
    /**
	* 
	* @var Charged Amount
	* 
	*/
    $charged_amount = reg_mgr_get_total_amt($user_id, 'chargedamt', $curr_month, $curr_year);     
  
   if($charged_amount !="" )
   {
      $charged_amt = "$".number_format($charged_amount, 2, '.', ',');
      $short_charged_amt = $charged_amount;
   }
   else{
      $charged_amt ="$00.00";
      $short_charged_amt = 0;
   }  
   
   if (is_numeric($charged_amount)) {
      $total_char_amt += $charged_amount;
    } 
    
   if($charged_amt_for_own != 0 ){
      $total_charged_amt = $charged_amt_for_own + $total_char_amt;
   }
   else
   {
      $total_charged_amt = $total_char_amt;
   }
   /**
   * 
   * @var Paid Amount
   * 
   */   
   
   $paid_amount = reg_mgr_get_total_amt($user_id , 'paidamt', $curr_month, $curr_year);
   if($paid_amount !="" )
   {
    	$paid_amt       = "$".number_format($paid_amount, 2, '.', ',');
    	$short_paid_amt = $paid_amount;
   }
   else{
        $paid_amt       = "$00.00";
        $short_paid_amt = 0;
   }
   if(is_numeric($paid_amount)){
   	 $total_pa_amt += $paid_amount; 
   }
   if($paid_amt_for_own !=0 ){
	 $total_paid_amt = $paid_amt_for_own + $total_pa_amt;
   }
   else{
	 $total_paid_amt = $total_pa_amt; 
    }
    $total_paid_amount    = "$".number_format($total_paid_amt, 2, '.', ',');
    $total_charged_amount = "$".number_format($total_charged_amt, 2, '.', ',');
  	$regional_manager_array_data = array(
		'regional_manager_name' => $regional_manager_name,
		'user_id'               => $user_id,
		'charged_amt'           => $charged_amount,
		'total_sales'           => (int)$total_sales,
		'charged_amt'           => $charged_amt,
        'paid_amt'              => $paid_amt,
        'short_charged_amt'     => (int)$short_charged_amt,
        'short_paid_amt'        => (int)$short_paid_amt,
		'status'                => $regional_manager_status,
	);	
	array_push($regional_manager_array, $regional_manager_array_data);
	$regional_managers_count ++;
    }
	if($regional_manager_array){
	 echo json_encode(array('status'=>'1', "regional_managers" => $regional_manager_array, 'regional_managers_count'=> $regional_managers_count, 'total_paid_amt'=> $total_paid_amount, 'total_charged_amt'=> $total_charged_amount, 'curr_year' => $curr_year, 'curr_month' => $curr_month));
     }
    else{
     echo json_encode(array('status'=>'0', "message" => 'no data found!'));
    }
  }

    /**
	* /
	* Get all regionalManagersList start
    */  

  function regionalManagersListAll(){
    $data           = json_decode(file_get_contents('php://input'), true);
    $limit          = $data['limit'];
   	$total_char_amt = 0;
   	$charged_amt_for_own = 0;
    $curr_month = date('n');
    $curr_year  = date('Y');	 
    $regional_manager_array = array();	
    $total_pa_amt      = 0;	
    $paid_amt_for_own  = 0;	
    $total_charged_amt = 0;	
    $total_paid_amt    = 0;	
     	
	$roles =  array('sales_regional_manager','regional_manager','team_regional_Sales');	
	$sql = ' 
			SELECT  ID 
			FROM    wp_abd_users INNER JOIN wp_abd_usermeta
			ON      wp_abd_users.ID          =  wp_abd_usermeta.user_id 
			WHERE   wp_abd_usermeta.meta_key =  \'wp_abd_capabilities\' 
			AND     ( 
			';  
    $i = 1;  
    foreach ( $roles as $role ) {  
    $sql .= 'wp_abd_usermeta.meta_value LIKE \'%"'.$role.'"%\' ';  
     if ( $i < count( $roles ) ) $sql .= ' OR ';  
        $i++;  
     }  
    $sql .= ' )';  
    $sql .= ' ORDER BY user_registered DESC';
    $sql.= " LIMIT $limit,10";
  
    $regional_managers = $this->BlankModel->customquery($sql); 
    $regional_managers_count = 0; 
    $regional_manager_name = '';
	
	foreach($regional_managers as $regional){		
	$user_id = $regional['ID'];
	$user_role = get_user_role($user_id);
	$regional_manager_status = get_user_meta_value($user_id, '_status', TRUE);
	$regional_manager_name   = get_user_meta_value($user_id, 'first_name', TRUE)." ".get_user_meta_value($user_id, 'last_name', TRUE);
	
	$sales_reps_sql = ' SELECT count(ID) as count_id FROM wp_abd_users u1 
		                JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = "_status")
						JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "assign_to") 
						JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities") 		
				        WHERE (m3.meta_value LIKE "%sales_regional_manager%" 				     
				        OR m3.meta_value LIKE  "%sales%"   
				        OR m3.meta_value LIKE  "%team_regional_Sales%")
	       				AND (m4.meta_value = "'.$user_id.'")
	       				AND (m1.meta_value = "Active")';
	
	$sales_reps_count = $this->BlankModel->customquery($sales_reps_sql);	
	$total_sales_ref = $sales_reps_count[0]['count_id'];  
  
   if($user_role == "team_regional_Sales" || $user_role == "sales_regional_manager")
    {
       $total_sales =  1 + $total_sales_ref;
      
    }
    else
    {
       $total_sales =  $total_sales_ref;
    
    }    
  
    $charged_amount = reg_mgr_get_total_amt($user_id, 'chargedamt', $curr_month, $curr_year); 
   if($charged_amount != "" )
    {
  	$charged_amt = "$".number_format($charged_amount, 2, '.', ',');
  	$short_charged_amt = $charged_amount;
    }
   else{
      $charged_amt = "$00.00";
      $short_charged_amt = 0;
   }   
   if(is_numeric($charged_amount) && is_numeric($charged_amount)) {
      $total_char_amt += $charged_amount;
    } 
   if($charged_amt_for_own != 0 )
   {
      $total_charged_amt = $charged_amt_for_own+$total_char_amt;
   }
   else
   {
      $total_charged_amt = $total_char_amt;
   }    	
   $paid_amount = reg_mgr_get_total_amt($user_id , 'paidamt', $curr_month, $curr_year);	
   if($paid_amount !="" )
   {
   	$paid_amt = "$".number_format($paid_amount, 2, '.', ',');
   	$short_paid_amt = $paid_amount;
   }
   else{
   $paid_amt ="$00.00";
   $short_paid_amt = 0;
   }
   if(is_numeric($paid_amount) && is_numeric($paid_amount)){
     $total_pa_amt += $paid_amount; 
 	}
	if($paid_amt_for_own != 0 )
	{
	$total_paid_amt = ($paid_amt_for_own + $total_pa_amt);
	}
	else
	{
	$total_paid_amt = $total_pa_amt; 
	}
    $total_paid_amount    = "$".number_format($total_paid_amt, 2, '.', ',');
    $total_charged_amount = "$".number_format($total_charged_amt, 2, '.', ',');
  	$regional_manager_array_data = array(
		'regional_manager_name' => $regional_manager_name,
		'user_id'               => $user_id,
		'charged_amt'           => $charged_amount,
		'total_sales'           => (int)$total_sales,
		'charged_amt'           => $charged_amt,
        'paid_amt'              => $paid_amt,
        'short_charged_amt'     => (int)$short_charged_amt,
        'short_paid_amt'        => (int)$short_paid_amt,
		'status'                => $regional_manager_status
	
	);	
	array_push($regional_manager_array, $regional_manager_array_data);
	$regional_managers_count ++;
    }
	if($regional_manager_array){	 
	    echo json_encode(array('status'=>'1', "regional_managers" => $regional_manager_array, 'regional_managers_count'=> $regional_managers_count, 'total_paid_amt'=> $total_paid_amount, 'total_charged_amt'=> $total_charged_amount));
     }
    else{
      echo json_encode(array('status'=>'0', "message" => 'no data found!'));
     }
  }


  function totalRegionalManagersList(){
    $data = json_decode(file_get_contents('php://input'), true);
    $total_char_amt = 0;
   	$charged_amt_for_own = 0;
    $curr_month = date('n');
    $curr_year  = date('Y');	 
    $total_pa_amt = 0;	
    $paid_amt_for_own = 0;	
    $total_charged_amt = 0;	
    $total_paid_amt = 0;	
    $assign_to = $data['user_id'];
    $user_role = get_user_role($assign_to);

   $roles =  array('sales_regional_manager','regional_manager','team_regional_Sales');
    // if($user_role == 'aps_sales_manager') {
	$sql = 'SELECT  ID 
			FROM    wp_abd_users INNER JOIN wp_abd_usermeta
			ON      wp_abd_users.ID          =  wp_abd_usermeta.user_id 
			WHERE   wp_abd_usermeta.meta_key =  \'wp_abd_capabilities\' 
			AND     ( 
			';
    $i = 1;  
    foreach ( $roles as $role ) {  
    $sql .= 'wp_abd_usermeta.meta_value LIKE \'%"'.$role.'"%\' ';  
     if ( $i < count( $roles ) ) $sql .= ' OR ';  
        $i++;  
     }  
    $sql .= ' )';  
    $sql .= ' ORDER BY display_name ASC';

// } else {
//     $sql = 'SELECT  ID FROM  wp_abd_users 
//     INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID =  t1.user_id 
//     INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID =  t2.user_id 
//       WHERE t1.meta_key =  \'wp_abd_capabilities\' 
//       AND t2.meta_key =  \'assign_to\' AND t2.meta_value = '.$assign_to.' AND ( 
//       ';
//     $i = 1;  
//     foreach ( $roles as $role ) {  
//     $sql .= 't1.meta_value LIKE \'%"'.$role.'"%\' ';  
//      if ( $i < count( $roles ) ) $sql .= ' OR ';  
//         $i++;  
//      }  
//     $sql .= ' )';  
//     $sql .= ' ORDER BY display_name';
// }
  
    $regional_managers = $this->BlankModel->customquery($sql);

    $reg_man_details = array();
    foreach ($regional_managers as $value) {
    	
    	$reg_details['id'] = $value['ID'];
    	$reg_details['name'] = get_user_meta_value($value['ID'], 'first_name',TRUE ).' '.get_user_meta_value($value['ID'], 'last_name',TRUE );

    
array_push($reg_man_details,$reg_details);

      $charged_amount = reg_mgr_get_total_amt($value['ID'], 'chargedamt', $curr_month, $curr_year);     
      
      
   if($charged_amount !="" )
   {
      $charged_amt = "$".number_format($charged_amount, 2, '.', ',');
   }
   else{
      $charged_amt ="$00.00";
   }   
   if (is_numeric($charged_amount) && is_numeric($charged_amount)) {
      $total_char_amt += $charged_amount;
    } 
   if($charged_amt_for_own != 0 )
   {
      $total_charged_amt = $charged_amt_for_own+$total_char_amt;
   }
   else
   {
      $total_charged_amt = $total_char_amt;
   }
   
   
   $paid_amount = reg_mgr_get_total_amt($value['ID'] , 'paidamt', $curr_month, $curr_year);
   if($paid_amount !="" )
   {
   	$paid_amt = "$".number_format($paid_amount, 2, '.', ',');
   }
   else{
    $paid_amt ="$00.00";
   }
   if(is_numeric($paid_amount) && is_numeric($paid_amount)) {
   	$total_pa_amt += $paid_amount; 
   }
   if($paid_amt_for_own !=0 ){
	$total_paid_amt = $paid_amt_for_own+$total_pa_amt;
   }
   else{
	$total_paid_amt = $total_pa_amt; 
   }
   
    $total_paid_amount    = "$".number_format($total_paid_amt, 2, '.', ',');
    $total_charged_amount = "$".number_format($total_charged_amt, 2, '.', ',');
   
    } 

    if(!empty($regional_managers) && isset($regional_managers)){
	    echo json_encode(array('status'=>'1', 'regional_managers_count'=> count($regional_managers), 'reg_man_details'=>$reg_man_details , 'total_paid_amt'=> $total_paid_amount, 'total_charged_amt'=> $total_charged_amount,));
     }
    else{
      echo json_encode(array('status'=>'0', "regional_managers_count" => '0'));
     }

  }


    /**
	* /
	* search_regional_list start
	*/
	
  function search_regional_list()
  {
   
    $data = json_decode(file_get_contents('php://input'), true);
    $curr_month = '0';
    $curr_year  = '0'; 
    $status = '';
    $fname = '';
    if($data['status']!=""){
       $status = $data['status'];
     }
     if($data['fname']!=""){
        $fname = $data['fname'];
     }
     if($data['from_month']!=""){
      $curr_month = $data['from_month'];  
     }
     if($data['from_year']!=""){
       $curr_year = $data['from_year']; 
     }

    $total_char_amt = 0;
    $charged_amt_for_own =0;
    $regional_manager_array = array();  
    $total_pa_amt = 0;  
    $paid_amt_for_own = 0;  
    $total_charged_amt = 0; 
    $total_paid_amt = 0;  
    $sql = "";

    $sql .= "SELECT ID
		FROM wp_abd_users 
		JOIN wp_abd_usermeta m1 ON (m1.user_id = wp_abd_users.ID AND m1.meta_key = 'wp_abd_capabilities') ";
      
    if(!empty($status) ) {
	  $sql .= " JOIN wp_abd_usermeta m2 ON (m2.user_id = wp_abd_users.ID AND m2.meta_key = '_status')";
      }   
      
    if (!empty($fname) ) {
      $sql .= "  JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = 'first_name' 
				 JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = 'last_name'" ;
      }
      
      $sql .= " WHERE ( m1.meta_value LIKE '%sales_regional_manager%'  
                     OR m1.meta_value LIKE '%regional_manager%'  
                     ) ";
     
    if ($status != '' && !empty($status)) {
      $sql .="	AND m2.meta_value = '$status' ";
     } 
    if ($fname != '' && !empty($fname)) {
      $sql .= "    
        	AND ( b1.meta_value LIKE '%" .strtok( $fname, " "). "%' 
			OR b2.meta_value LIKE '%".trim($fname)."%' 
			OR wp_abd_users.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '', $fname))."%'
			OR wp_abd_users.display_name LIKE '%".trim($fname)."%')";   
     }
        
	  $sql .=" ORDER BY wp_abd_users.user_registered DESC";

    $regional_managers = $this->BlankModel->customquery($sql); 
    $regional_managers_count = 0;  
  
  if($regional_managers) {
 
  foreach($regional_managers as $regional){   
	$user_id = $regional['ID'];
	$user_role = get_user_role($user_id);
	$regional_manager_first_name = get_user_meta_value($user_id, 'first_name');
	$regional_manager_last_name  = get_user_meta_value($user_id, 'last_name');
	$regional_manager_name       = $regional_manager_first_name." ".$regional_manager_last_name;
	$regional_manager_status     = get_user_meta_value($user_id, '_status');

    $sales_reps_sql =  'SELECT count(ID) as count_id FROM wp_abd_users u1 
				        JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "assign_to") 
				        JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = "_status")
				        JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")     
				        WHERE (m3.meta_value LIKE "%sales_regional_manager%"  
				        OR m3.meta_value LIKE  "%sales%"
				        OR m3.meta_value LIKE  "%team_regional_Sales%")
				        AND (m4.meta_value = "'.$user_id.'")
				        AND (m1.meta_value = "Active")';

	$sales_reps_count = $this->BlankModel->customquery($sales_reps_sql);  
	$total_sales_ref = $sales_reps_count[0]['count_id'];  
  
   if($user_role == "team_regional_Sales"|| $user_role == "sales_regional_manager"){
       $total_sales = 1+$total_sales_ref;    
    }
    else
    {
       $total_sales =  $total_sales_ref;    
    }       
  
    $charged_amount = reg_mgr_get_total_amt($user_id, 'chargedamt', $curr_month, $curr_year);
 
 if($charged_amount !="" ){
       $charged_amt = "$".number_format($charged_amount, 2, '.', ',');
       $short_charged_amt = $charged_amount;
   }
   else{
       $charged_amt = "$00.00";
       $short_charged_amt = 0;
   }
   
   if(is_numeric($charged_amount) && is_numeric($charged_amount)) {
   	  $total_char_amt += $charged_amount;
    } 
   if($charged_amt_for_own != 0 ){
  	  $total_charged_amt = $charged_amt_for_own+$total_char_amt;
   }
   else{
   	 $total_charged_amt = $total_char_amt;
   }
      
  	$paid_amount = reg_mgr_get_total_amt($user_id , 'paidamt', $curr_month, $curr_year);
  
	if($paid_amount !="" )
	{
	    $paid_amt = "$".number_format($paid_amount, 2, '.', ',');
	    $short_paid_amt = $paid_amount;
	}
	else{
	 	$paid_amt ="$00.00";
	  	$short_paid_amt = 0;
	}
	if (is_numeric($paid_amount) && is_numeric($paid_amount)) {

		$total_pa_amt += $paid_amount; 
	}
	if($paid_amt_for_own !=0 )
	{
		$total_paid_amt = $paid_amt_for_own+$total_pa_amt;
	}
	else
	{
		$total_paid_amt = $total_pa_amt; 
    }
    $total_paid_amount    = "$".number_format($total_paid_amt, 2, '.', ',');
    $total_charged_amount = "$".number_format($total_charged_amt, 2, '.', ',');
    $regional_manager_array_data = array(
               'regional_manager_name' =>  $regional_manager_name,
               'user_id'               =>  $user_id,
               'charged_amt'           =>  $charged_amount,
               'total_sales'           =>  (int)$total_sales,
               'charged_amt'           =>  $charged_amt,
               'paid_amt'              =>  $paid_amt,
               'short_charged_amt'     =>  (int)$short_charged_amt,
               'short_paid_amt'        =>  (int)$short_paid_amt,
               'status'                =>  $regional_manager_status
            );  
     array_push($regional_manager_array, $regional_manager_array_data);
     $regional_managers_count ++;
     }
     if($regional_manager_array){
      echo json_encode(array('status'=>'1', "regional_managers" => $regional_manager_array, 'regional_managers_count'=> $regional_managers_count, 'total_paid_amt'=> $total_paid_amount, 'total_charged_amt'=> $total_charged_amount));
     }
     else {
      echo json_encode(array('status'=>'0', "regional_managers" => '', 'regional_managers_count'=> '0', 'total_paid_amt'=> '0', 'total_charged_amt'=> '0'));
     }
    } 
    else {
      echo json_encode(array('status'=>'0', "regional_managers" => '', 'regional_managers_count'=> '0', 'total_paid_amt'=> '0', 'total_charged_amt'=> '0'));
     }
  }
  
   /**
   * search_regional_list End
   */

    function regional_man_details(){
    $data = json_decode(file_get_contents('php://input'), true);
    if($data['id']!=""){
    $user_id = $data['id'];
    
    $conditions = " ( `ID` = '".$user_id."')";		
    
    $select_fields = '*';
    $is_multy_result = 1;
    $memDetails  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
    
    $fname                 = get_user_meta_value( $user_id, 'first_name');
    $lname                 = get_user_meta_value( $user_id, 'last_name');
    $mob                   = get_user_meta_value( $user_id, '_mobile');
    $add                   = get_user_meta_value( $user_id, '_address');
    $clinic                = get_user_meta_value( $user_id, 'clinic_addrs');
    $fax                   = get_user_meta_value( $user_id, 'fax');
    $cell                  = get_user_meta_value($user_id, 'cell_phone');
    $manager               = get_user_meta_value($user_id, 'manager_contact_name');
    $manager_cell          = get_user_meta_value( $user_id, 'manager_cell_number');
    $offce_num             = get_user_meta_value( $user_id, 'office_number');
    $npi_api               = get_user_meta_value( $user_id, 'npi');
    $acc_cntrl             = get_user_meta_value($user_id, 'specimen_control_access');
    $acc_cntrl_qc          = get_user_meta_value($user_id, 'specimen_control_access_qc',true);
    $role                  = get_user_role($data['id']);
    $bank_routing_num      = get_user_meta_value($user_id, '_brn');
    $bank_account_num      = get_user_meta_value($user_id, '_ban');
    $name_on_bank          = get_user_meta_value($user_id, '_nbaccount');
    $user_status           = get_user_meta_value($user_id, '_status');
    $commission_percentage = get_user_meta_value($user_id, '_commission_percentage');

    $specimen_information = array('fname' => $fname,'lname' => $lname,'acc_cntrl' => $acc_cntrl,'acc_cntrl_qc' => $acc_cntrl_qc,'email' => $memDetails['user_email'],'user_login'=>$memDetails['user_login'],'url' => $memDetails['user_url'], 'mob' => $mob,'add' => $add,'clinic' => $clinic,'fax' => $fax,'cell' => $cell,'manager' => $manager, 'manager_cell' => $manager_cell, 'offce_num' => $offce_num,'npi_api' => $npi_api,'role' => $role,'bank_routing_num' => $bank_routing_num,'bank_account_num' => $bank_account_num,'user_status' => $user_status, 'user_registered_date' => $memDetails['user_registered'], 'commission_percentage' => $commission_percentage, 'name_on_bank' => $name_on_bank);
      
        if($specimen_information)
        {
          echo json_encode(array("status" => "1","details" =>$specimen_information));
        }
        else
        {
          echo json_encode(array("status" => "0"));
        }
  
        }
    }

// update regional manager details
    function update_regional_man_details()
    {
    $data = json_decode(file_get_contents('php://input'), true);
    $user_id = $data['user_id'];
    $status = array();
    $otherdata = 'no';
 
    $otherdata = update_user_meta ($user_id, 'first_name' , $data['first_name']);
    array_push($status,$otherdata);

    $otherdata = update_user_meta ($user_id, 'last_name' , $data['last_name']);
   
    $otherdata = update_user_meta ($user_id, '_mobile' , $data['mobile']);
    array_push($status,$otherdata);

    $otherdata = update_user_meta ($user_id, '_brn' , $data['brn']);
    array_push($status,$otherdata);
   
    $otherdata = update_user_meta ($user_id, '_ban' , $data['ban']);
    array_push($status,$otherdata);
  
    $otherdata = update_user_meta ($user_id, '_nbaccount' , $data['nbaccount']);
    array_push($status,$otherdata);
   
    $otherdata = update_user_meta ($user_id, '_commission_percentage' , $data['commission_percentage']);
    array_push($status,$otherdata);
   
    $otherdata = update_user_meta ($user_id, '_address' , $data['address']);
    array_push($status,$otherdata);
   
    $otherdata = update_user_meta ($user_id, '_status' , $data['user_status']);
    array_push($status,$otherdata);
       
    $userdata =array();
    if(!empty($data['user_url'])){
    $url = $data['user_url'];
    }else {
    $url ='';
    }
   
   if (!empty($data['edit_con_pass']) && $data['edit_con_pass'] !== null) {
     $password = md5(SECURITY_SALT.$data['edit_con_pass']);
     $userdata =array('user_pass' => "$password",'user_url' => "$url");
   } else {
      $userdata =array('user_url' => "$url");
   }

   $conditions = "( `ID` = ".$user_id.")";
   $update = $this->BlankModel->editTableData('users',$userdata, $conditions);
        
    ///////////// Ability Curl Added////////////////////////
    $api_url=$this->config->item('api_url').'General/update_regional_man_details';  
    common_curl_data($api_url,$data);
        
        
  if($update == 'no' || $update == 'yes' || in_array('yes',$status)){
      echo json_encode(array("status" => "1","message" => 'User details has been successfully updated.'));
    }
  else{
      echo json_encode(array("status" => "0","message" => 'Something is worng!. Please try again.'));
    }
   
   }	

    function change_user_status() {
      $data = json_decode(file_get_contents('php://input'), true);
      $user_id = $data['user_id'];
      $status = $data['status'];

      if ($status =='Inactive') {
        $update = update_user_meta ($user_id, '_status','Active');
      } else {
        $update = update_user_meta ($user_id, '_status','Inactive');
      }
        
        ///////////// Ability Curl Added////////////////////////
        $api_url=$this->config->item('api_url').'General/change_user_status';  
        common_curl_data($api_url,$data);

      if($update = 'yes'){
        echo json_encode(array("status" => "1","message" =>' User details has been successfully updated.'));
      }
      else{
        echo json_encode(array("status" => "0","message" => 'Something is worng!. Please try again.'));
      }
    }
  /**
  * 
  * Add Regional Manager
  */
    function add_regional_man()
    {
      $data = json_decode(file_get_contents('php://input'), true);
      if(!empty($data) && isset($data)){
        $password =   md5(SECURITY_SALT.$data['con_pass']);
        
        $check_email    = email_validation('users', 'user_email', $data['user_email']);
        $check_username = username_validation('users', 'user_login', $data['user_login']);
        
        if(!$check_email && !$check_username){
			
        $created_date = date("Y-m-d h:i:sa");
        $table_data = array(
					        'user_login'      => $data['user_login'],
					        'user_pass'       => $password,
					        'user_email'      => $data['user_email'],
					        'user_url'        => $data['user_url'],					    
					        'user_registered' => $created_date
        					);
        $user_id = $this->BlankModel->addTableData('users', $table_data);
     if($user_id){
        $role_type_array = array('regional_manager' => '1');
        $role = serialize( $role_type_array);

        add_user_meta($user_id,'first_name', $data['first_name'] );
        add_user_meta($user_id,'last_name', $data['last_name'] );
        add_user_meta($user_id,'_mobile', $data['mobile'] );
        add_user_meta($user_id,'wp_abd_capabilities', $role );
        add_user_meta($user_id,'_address', $data['address'] );
        add_user_meta($user_id,'_status', 'Active' );        
        add_user_meta($user_id, '_ban' ,$data['ban']);  
		add_user_meta($user_id, '_brn' ,$data['brn']);  
		add_user_meta($user_id, '_nbaccount' ,$data['nbaccount']);  
		add_user_meta($user_id, '_commission_percentage' ,$data['commission_percentage']);  
        
        ///////////// Ability Curl Added////////////////////////
        $api_url=$this->config->item('api_url').'General/add_regional_man';  
        common_curl_data($api_url,$data);
         
         echo json_encode(array("status" => "1","message" => 'User Added successfully.'));
        }         
        else {
          echo json_encode(array("status" => "0","message" => 'Something is worng!. Please try again'));
        }
       }
       else {
          echo json_encode(array("status" => "0","message" => $check_email.' '.$check_username ));
        }
      }      
    }
 }
?>