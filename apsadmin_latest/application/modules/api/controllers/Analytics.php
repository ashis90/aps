<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Analytics extends MY_Controller {

    function __construct() {
        parent::__construct();       
    }
    function index()
    {
    	echo "test Physician service";
    }


/////////////// get data ////////////////////////

	public function get_data()
	{
	$data = json_decode(file_get_contents('php://input'), true);
    $phy_list = array();
  
    $roles =  'physician';
    $args = array('DPM'  => 'DPM',
		          'PCP'=> 'PCP',
		          'Ortho'  => 'Ortho',
		          'Nurse_Practitioner'=> 'Nurse Practitioner',
		          'DO'  => 'DO',
		          'Dermatologist'=> 'Dermatologist',
		          'Geriatric'  => 'Geriatric',
		          'Bariatric'=> 'Bariatric',
		          'PA'=> 'PA',
		          'OBGYN'  => 'OBGYN');
	
	$name = array();
	$coun = array();
	$name2 = array();
	$coun2 = array();
	 foreach($args as $key => $specialties){

	$sql = 'SELECT  u1.ID FROM wp_abd_users u1
	JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = "assign_specialty")
	JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = "wp_abd_capabilities")	
    WHERE (m3.meta_value LIKE "%physician%")
    AND (m4.meta_value = "'.$specialties.'")';
	    $physician_count = $this->BlankModel->customquery($sql);
		$total_physician_count = count($physician_count);
		array_push($coun,$total_physician_count);
		array_push($name,$key);
		$result = array_combine($name,$coun);
		$count2 = 0;
		foreach ($physician_count as $value) {
			$que = "SELECT `id` FROM `wp_abd_specimen` WHERE `physician_id` = '".$value['ID']."' AND `create_date` > '2017-03-27 23:59:59'";
			$specimen_count = $this->BlankModel->customquery($que);
			$total_specimen_count = count($specimen_count);
			$count2 = $count2 + $total_specimen_count;
		}
		
		array_push($coun2,$count2);
		array_push($name2,$key);
		$result2 = array_combine($name2,$coun2);
	}

		if ($result) {
			echo json_encode(array("status" => "1", "result" => $result, "result2" => $result2));
		} else {
			echo json_encode(array("status" => "0", "message" => 'Something is worng!.'));
		}
	}


	 /**
   * Top 10 Physician list
   */
  function get_physician_analytics_data()
  {
    $physicians = array();
	$data       = json_decode(file_get_contents('php://input'), true);

	if(!empty($data)){
    $limit      = ' LIMIT 0, 10';
    $curr_month = date('n');
    $curr_year  = date('Y');
    $user_role  = $data['user_role'];
    $user_id    = $data['user_id'];
    $activationSum=0;
    $usrCount=0;
      
      if($user_role == 'sales_regional_manager' || $user_role == 'regional_manager') {
         $user_id = get_assign_user($data['user_id']);
      }
      
      $sql = 'SELECT ID, `b1`.`meta_value` AS `first_name`, `b2`.`meta_value` AS `last_name`, `b3`.`meta_value` AS `status`,wp_abd_users.`user_registered` as "user_registered" FROM wp_abd_users 
		      INNER JOIN wp_abd_usermeta ON wp_abd_users.ID = wp_abd_usermeta.user_id 
		      INNER JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = "first_name" 
			  INNER JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = "last_name" 
			  INNER JOIN wp_abd_usermeta as b3 ON wp_abd_users.ID = b3.user_id AND b3.meta_key = "_status" 
		      AND   wp_abd_usermeta.meta_key = \'wp_abd_capabilities\' 
		      AND ( ';
      $sql .= 'wp_abd_usermeta.meta_value LIKE \'%"physician"%\' ';
      $sql .= ' ) WHERE wp_abd_users.`user_registered` < DATE_SUB(NOW(), INTERVAL 2 MONTH) ORDER BY user_registered DESC';
    
	$physicians_list = $this->BlankModel->customquery($sql);
    $extra = "";
    if ($physicians_list) {
      $physician_count = 0;
      foreach ($physicians_list as $physician) {
        $dateDiff =0;
        $physician_id = $physician['ID'];
        $sales_id     = get_user_meta_value($physician_id, 'added_by', TRUE);
        if ($sales_id) {
          $sales_reps_name = get_user_meta_value($sales_id, 'first_name', TRUE) . " " . get_user_meta_value($sales_id, 'last_name', TRUE);
          
        } else {
          $sales_reps_name = '';
        }

         
        if($user_id == "76389" && $physician_id == "137"){
		   $extra = " AND `create_date` > '2018-06-12 23:59:59' ";
		}
        else{
		   $extra = " AND `create_date` > '2017-03-27 23:59:59' ";
		}
        
        $specimen_sql = "SELECT MAX(u1.`create_date`) as 'create_date', COUNT(u1.`id`) AS 'total_orders',u1.`test_type` FROM (SELECT * FROM `wp_abd_specimen` UNION ALL SELECT * From `wp_abd_specimen_archive`) u1 WHERE u1.`physician_id` = $physician_id AND u1.`status` = '0' AND u1.`physician_accepct` = '0' AND u1.`qc_check` = '0' GROUP BY u1.`test_type`";
        
        $specimen_submitted = $this->BlankModel->customquery($specimen_sql);
    
        $specimen_submitted_date = '';
        $totalSpecimen    = 0;
        $totalSpecimen_NF = 0;
        $totalSpecimen_W  = 0;
        $specimen_submitted_date_NF = '';
        $specimen_submitted_date_W  = '';
        if(array_key_exists("0", $specimen_submitted)){
          if($specimen_submitted[0]['test_type'] == 'NF'){
            $specimen_submitted_date_NF = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
            $totalSpecimen_NF  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
          }else if($specimen_submitted[0]['test_type'] == 'W'){
            $specimen_submitted_date_W = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
            $totalSpecimen_W  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
          }

          if(array_key_exists("1", $specimen_submitted)){
            if($specimen_submitted[1]['test_type'] == 'NF'){
              $specimen_submitted_date_NF = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
              $totalSpecimen_NF  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
            }else if($specimen_submitted[1]['test_type'] == 'W'){
              $specimen_submitted_date_W = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
              $totalSpecimen_W  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
            }
          }
          $specimen_submitted_date = (strtotime($specimen_submitted_date_NF) > strtotime($specimen_submitted_date_W)? $specimen_submitted_date_NF : $specimen_submitted_date_W);
          $totalSpecimen  = ((int)$totalSpecimen_NF + (int)$totalSpecimen_W);
        }else{
          $totalSpecimen  = 0;
          $specimen_submitted_date = '';
        }
      if(!empty($specimen_submitted_date)){
        $diff = abs(strtotime($specimen_submitted_date) - strtotime($physician['user_registered']));
        $dateDiff = floor($diff / (60*60*24));  
        if($dateDiff > 0){
          $activationSum += $dateDiff;
          ++$usrCount;

        }
        
      }else{
        $dateDiff =0;
      }
        $physician_data = array(
          'totalSpecimen_NF' => $totalSpecimen_NF,
          'totalSpecimen_W'  => $totalSpecimen_W,
          'short_totalSpecimen' => (int) $totalSpecimen,
          'specimen_submitted_date' => $specimen_submitted_date,
          'dateDiff'=>$dateDiff,
          'sales_id'    => $sales_id,
          'physician_id'   => $physician_id,
          'full_name'      => $physician['first_name']. ' '.$physician['last_name'],
		  'user_status'    => $physician['status'],
		  'user_registered' => $physician['user_registered']
        );
        array_push($physicians, $physician_data);
        $physician_count++;
      }
       die(json_encode(array(
        "status" => '1',
        "physicians_details" => $physicians,
        'avgDateDiff'=>$activationSum?floor(($activationSum / $usrCount)):0,
        'usrCount'=>$usrCount?$usrCount:0,
        "physicians_count" => $physician_count
      )));
    } else {
      echo json_encode(array(
        "status" => '0',
        "message" => 'no data found!'
      ));
	}
  }else {
      echo json_encode(array(
        "status" => '0',
        "message" => 'no data found!'
      ));
	}
}


function searcht_physician_analytics_data() {    
  $physicians = array();
  $data       = json_decode(file_get_contents('php://input'), true);   
  $status     = $data['status'];  
  $search_data ='';  
  if(isset($data['fname'])) {
    $search_data = $data['fname'];
  }
  $from_date = $data['from_date'];
  $to_date   = $data['to_date'];
  $user_role = $data['user_role'];
  $user_id   = $data['user_id'];
  $roles = 'physician';
  $sql   = '';
  $activationSum=0;
  $usrCount=0;
  
  // if user role is Aps Sales Manager and Data Entry Oparator
  
  $sql .= 'SELECT `ID`, `b1`.`meta_value` AS `first_name`, `b2`.`meta_value` AS `last_name`,wp_abd_users.`user_registered` as "user_registered" FROM  `wp_abd_users` 
              INNER JOIN wp_abd_usermeta as t1 ON wp_abd_users.ID  = t1.user_id 
              INNER JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = "first_name" 
      INNER JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = "last_name"';

  if($status != '') {
  $sql .= ' INNER JOIN wp_abd_usermeta as t2 ON wp_abd_users.ID  = t2.user_id AND t2.meta_key = "_status"';
  }   
  $sql .= ' AND t1.meta_key = \'wp_abd_capabilities\' AND t1.meta_value LIKE \'%"physician"%\'';
  if($status != '') {
  $sql .= ' AND t2.meta_value =  \'' . $status . '\'';
  }      
  if($search_data != '') {
  $sql .= "    
        AND (b1.meta_value LIKE '%".strtok( $search_data, " ")."%' 
    OR b2.meta_value LIKE '%".trim($search_data)."%' 
    OR wp_abd_users.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '', $search_data))."%'
    OR wp_abd_users.display_name LIKE '%".trim($search_data)."%')";   
  }    
   //Sales Reps
if(!empty($from_date) && !empty($to_date)) {
  $sql .= " WHERE wp_abd_users.user_registered BETWEEN '".$from_date." 00:00:00' AND '".$to_date." 23:59:59'";   
}
  
 $sql .= ' GROUP BY wp_abd_users.ID ORDER BY user_registered DESC';   
  $physicians_list = $this->BlankModel->customquery($sql);

  if($physicians_list) {
     $physician_count = 0;
    foreach ($physicians_list as $physician) {
      $physician_id = $physician['ID'];
      $sales_id     = get_user_meta_value($physician_id, 'added_by', TRUE);
      if($sales_id) {
      $sales_reps_name = get_user_meta_value($sales_id, 'first_name', TRUE) . " " . get_user_meta_value($sales_id, 'last_name', TRUE);
      }
      
      if($user_id == "76389" && $physician_id == "137"){
      $extra = " AND `create_date` > '2018-06-12 23:59:59' ";
      }

       
  $specimen_sql = "SELECT MAX(u1.`create_date`) as 'create_date', COUNT(u1.`id`) AS 'total_orders',u1.`test_type` FROM (SELECT * FROM `wp_abd_specimen` UNION ALL SELECT * From `wp_abd_specimen_archive`) u1 WHERE u1.`physician_id` = $physician_id AND u1.`status` = '0' AND u1.`physician_accepct` = '0' AND u1.`qc_check` = '0' GROUP BY u1.`test_type`";  

        
      $specimen_submitted = $this->BlankModel->customquery($specimen_sql);

      $specimen_submitted_date = '';
      $totalSpecimen     = 0;
      $totalSpecimen_NF  = 0;
      $totalSpecimen_W   = 0;
      $specimen_submitted_date_NF = '';
      $specimen_submitted_date_W  = '';
      if(array_key_exists("0", $specimen_submitted)){
       if($specimen_submitted[0]['test_type'] == 'NF'){
          $specimen_submitted_date_NF = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
          $totalSpecimen_NF  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
        }
        else if($specimen_submitted[0]['test_type'] == 'W'){
          $specimen_submitted_date_W = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
          $totalSpecimen_W  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
        }

        if(array_key_exists("1", $specimen_submitted)){
          if($specimen_submitted[1]['test_type'] == 'NF'){
            $specimen_submitted_date_NF = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
            $totalSpecimen_NF  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
          }
          else if($specimen_submitted[1]['test_type'] == 'W'){
            $specimen_submitted_date_W = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
            $totalSpecimen_W  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
          }
        }
        $specimen_submitted_date = (strtotime($specimen_submitted_date_NF) > strtotime($specimen_submitted_date_W)? $specimen_submitted_date_NF : $specimen_submitted_date_W);
        $totalSpecimen  = ((int)$totalSpecimen_NF + (int)$totalSpecimen_W);
      }
      else{
        $totalSpecimen  =0;
        $specimen_submitted_date = '';
      }

      if(!empty($specimen_submitted_date)){
        $diff = abs(strtotime($specimen_submitted_date) - strtotime($physician['user_registered']));
        $dateDiff = floor($diff / (60*60*24));  
        if($dateDiff > 0){
          $activationSum += $dateDiff;
          ++$usrCount;

        }
        
      }else{
        $dateDiff =0;
      }
  
      $physician_data = array(
        'totalSpecimen_NF'        => $totalSpecimen_NF,
        'totalSpecimen_W'         => $totalSpecimen_W,
        'short_totalSpecimen'     => (int) $totalSpecimen,
        'specimen_submitted_date' => $specimen_submitted_date,
        'dateDiff'=>$dateDiff,
        'sales_id'                => $sales_id,
        'physician_id'            => $physician_id,
        'full_name'               => $physician['first_name'].' '.$physician['last_name'],
        'user_status'             => get_user_meta_value($physician_id, '_status', TRUE),
    'sales_reps_name'         => $sales_reps_name,
    'user_registered' => $physician['user_registered']
      );
      array_push($physicians, $physician_data);
      $physician_count++;
      
    }
    
    echo json_encode(array(
      'status' => '1',
      "physicians_details" => $physicians,
      'avgDateDiff'=>$activationSum?floor(($activationSum / $usrCount)):0,
      'usrCount'=>$usrCount?$usrCount:0,
      'physicians_count' => $physician_count
    ));
  }
  
  else {
    echo json_encode(array(
      'status' => '0',
      "message" => 'no data found!'
    ));
  }
}



function get_sales_analytics_data()
{
  $physicians = array();
$data       = json_decode(file_get_contents('php://input'), true);

if(!empty($data)){
  $limit      = ' LIMIT 0, 10';
  $user_role = $data['user_role'];
  $user_id   = $data['user_id'];
  $activationSum=0;
  $usrCount=0;
  $salesRepsArr = array();
    
    if($user_role == 'sales_regional_manager' || $user_role == 'regional_manager') {
       $user_id = get_assign_user($data['user_id']);
    }

    $sales_reps_sql = "SELECT `ID`,`user_registered`, m4.meta_value as 'first_name', m5.meta_value as 'last_name' FROM wp_abd_users u1 
		JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 
    JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'first_name') 
    JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = 'last_name')				
        WHERE (m3.meta_value LIKE '%sales_regional_manager%'        
        OR m3.meta_value LIKE  '%sales%'  
        )";
    $sales_reps = $this->BlankModel->customquery($sales_reps_sql);     
    
    foreach($sales_reps as $sales_representative)
    {
      $last_submitted_date='';
      $salesID = $sales_representative['ID'];
      
      $sql = 'SELECT ID, `b1`.`meta_value` AS `first_name`, `b2`.`meta_value` AS `last_name`, `b3`.`meta_value` AS `status`,wp_abd_users.`user_registered` as "user_registered" FROM wp_abd_users 
          INNER JOIN wp_abd_usermeta ON wp_abd_users.ID = wp_abd_usermeta.user_id 
          INNER JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = "first_name" 
        INNER JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = "last_name" 
        INNER JOIN wp_abd_usermeta as b3 ON wp_abd_users.ID = b3.user_id AND b3.meta_key = "_status" 
        INNER JOIN wp_abd_usermeta as b4 ON wp_abd_users.ID = b4.user_id AND b4.meta_key = "added_by" AND b4.meta_value IN("'.$salesID.'")  
          AND   wp_abd_usermeta.meta_key = \'wp_abd_capabilities\' 
          AND ( ';
      $sql .= 'wp_abd_usermeta.meta_value LIKE \'%"physician"%\'';
      $sql .= ' ) WHERE wp_abd_users.`user_registered` < DATE_SUB(NOW(), INTERVAL 2 MONTH) ORDER BY user_registered DESC';

      $physicians_list = $this->BlankModel->customquery($sql);
      $extra = "";
  if ($physicians_list) {
    $physician_count = 0;
    foreach ($physicians_list as $physician) {
      $dateDiff =0;
      $physician_id = $physician['ID'];
       
      if($user_id == "76389" && $physician_id == "137"){
     $extra = " AND `create_date` > '2018-06-12 23:59:59' ";
  }
      else{
     $extra = " AND `create_date` > '2017-03-27 23:59:59' ";
  }
      
      $specimen_sql = "SELECT MAX(u1.`create_date`) as 'create_date', COUNT(u1.`id`) AS 'total_orders',u1.`test_type` FROM (SELECT * FROM `wp_abd_specimen` UNION ALL SELECT * From `wp_abd_specimen_archive`) u1 WHERE u1.`physician_id` = $physician_id AND u1.`status` = '0' AND u1.`physician_accepct` = '0' AND u1.`qc_check` = '0' GROUP BY u1.`test_type`";
      
      $specimen_submitted = $this->BlankModel->customquery($specimen_sql);
      $specimen_submitted_date_NF = '';
      $specimen_submitted_date_W  = '';
      $specimen_submitted_date = '';
      $totalSpecimen    = 0;
      $totalSpecimen_NF = 0;
      $totalSpecimen_W  = 0;

      if(array_key_exists("0", $specimen_submitted)){
        if($specimen_submitted[0]['test_type'] == 'NF'){
          $specimen_submitted_date_NF = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
          $totalSpecimen_NF  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
        }else if($specimen_submitted[0]['test_type'] == 'W'){
          $specimen_submitted_date_W = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
          $totalSpecimen_W  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
        }

        if(array_key_exists("1", $specimen_submitted)){
          if($specimen_submitted[1]['test_type'] == 'NF'){
            $specimen_submitted_date_NF = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
            $totalSpecimen_NF  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
          }else if($specimen_submitted[1]['test_type'] == 'W'){
            $specimen_submitted_date_W = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
            $totalSpecimen_W  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
          }
        }
        $specimen_submitted_date = (strtotime($specimen_submitted_date_NF) > strtotime($specimen_submitted_date_W)? $specimen_submitted_date_NF : $specimen_submitted_date_W);
        $last_submitted_date = (strtotime($last_submitted_date) < strtotime($specimen_submitted_date))?$specimen_submitted_date:$last_submitted_date;
        $totalSpecimen  = ((int)$totalSpecimen_NF + (int)$totalSpecimen_W);
      }else{
        $totalSpecimen  = 0;
        $specimen_submitted_date = '';
      }
      }
    } 
    if(!empty($last_submitted_date)){
      $diff = abs(strtotime($last_submitted_date) - strtotime($physician['user_registered']));
      $dateDiff = floor($diff / (60*60*24));  
      if($dateDiff > 0){
        $activationSum += $dateDiff;
        ++$usrCount;

      }
      
    }else{
      $dateDiff =0;
    }
    $sales_arr = array(
        'physician_count'=> $physicians_list?count($physicians_list):0,
        'specimen_submitted_date' => $last_submitted_date,
        'dateDiff'=>$dateDiff,
        'full_name'      => $sales_representative['first_name']. ' '.$sales_representative['last_name'],
        'user_registered' => $sales_representative['user_registered']
        );
      array_push($salesRepsArr, $sales_arr);
    }
      echo json_encode(array(
        "status" => '1',
        "salesDetails" => $salesRepsArr,
        'avgDateDiff'=>$activationSum?floor(($activationSum / $usrCount)):0,
        'usrCount'=>$usrCount?$usrCount:0
      ));
      }else {
        echo json_encode(array(
          "status" => '0',
          "message" => 'no data found!'
        ));
      }
    }


    function searcht_sales_analytics_data()
{
  $physicians = array();
$data       = json_decode(file_get_contents('php://input'), true);

if(!empty($data)){
  $limit      = ' LIMIT 0, 10';
  $curr_month = date('n');
  $curr_year  = date('Y');
  $status     = $data['status'];  
  $search_data = '';  
  if(isset($data['fname'])) {
    $search_data = $data['fname'];
  }
  $from_date = $data['from_date'];
  $to_date   = $data['to_date'];
  $user_role = $data['user_role'];
  $user_id   = $data['user_id'];
  $activationSum=0;
  $usrCount=0;
  $salesRepsArr = array();
    
    if($user_role == 'sales_regional_manager' || $user_role == 'regional_manager') {
       $user_id = get_assign_user($data['user_id']);
    }

    $sales_reps_sql = "SELECT `ID`,`user_registered`, m4.meta_value as 'first_name', m5.meta_value as 'last_name' FROM wp_abd_users u1 
		INNER JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities' AND (m3.meta_value LIKE '%sales_regional_manager%' OR m3.meta_value LIKE  '%sales%')) 
    INNER JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'first_name') 
    INNER JOIN wp_abd_usermeta m5 ON (m5.user_id = u1.ID AND m5.meta_key = 'last_name')";				
      
      if($status != '') {
        $sales_reps_sql .= ' INNER JOIN wp_abd_usermeta as t2 ON u1.ID  = t2.user_id AND t2.meta_key = "_status" AND t2.meta_value =  \'' . $status . '\'';
        }   
        // if($status != '') {
        // $sales_reps_sql .= ' AND t2.meta_value =  \'' . $status . '\'';
        // }      
        if($search_data != '') {
        $sales_reps_sql .= "    
              AND (m4.meta_value LIKE '%".strtok( $search_data, " ")."%' 
          OR m5.meta_value LIKE '%".trim($search_data)."%' 
          OR u1.user_nicename LIKE '%".strtolower(preg_replace('/\s*/', '', $search_data))."%'
          OR u1.display_name LIKE '%".trim($search_data)."%')";   
        }    
         //Sales Reps
      if(!empty($from_date) && !empty($to_date)) {
        $sales_reps_sql .= " WHERE u1.user_registered BETWEEN '".$from_date." 00:00:00' AND '".$to_date." 23:59:59'";   
      }
      $sales_reps_sql .= ' ORDER BY u1.user_registered DESC'; 
    
      $sales_reps = $this->BlankModel->customquery($sales_reps_sql);     
    foreach($sales_reps as $sales_representative)
    {
      $last_submitted_date='';
      $salesID = $sales_representative['ID'];
      
      $sql = 'SELECT ID, `b1`.`meta_value` AS `first_name`, `b2`.`meta_value` AS `last_name`, `b3`.`meta_value` AS `status`,wp_abd_users.`user_registered` as "user_registered" FROM wp_abd_users 
          INNER JOIN wp_abd_usermeta ON wp_abd_users.ID = wp_abd_usermeta.user_id 
          INNER JOIN wp_abd_usermeta as b1 ON wp_abd_users.ID = b1.user_id AND b1.meta_key = "first_name" 
          INNER JOIN wp_abd_usermeta as b2 ON wp_abd_users.ID = b2.user_id AND b2.meta_key = "last_name" 
          INNER JOIN wp_abd_usermeta as b3 ON wp_abd_users.ID = b3.user_id AND b3.meta_key = "_status" 
          INNER JOIN wp_abd_usermeta as b4 ON wp_abd_users.ID = b4.user_id AND b4.meta_key = "added_by" AND b4.meta_value IN("'.$salesID.'")  
          AND   wp_abd_usermeta.meta_key = \'wp_abd_capabilities\' 
          AND ( ';
      $sql .= 'wp_abd_usermeta.meta_value LIKE \'%"physician"%\'';
      $sql .= ' ) WHERE wp_abd_users.`user_registered` < DATE_SUB(NOW(), INTERVAL 2 MONTH) ORDER BY user_registered DESC';

      $physicians_list = $this->BlankModel->customquery($sql);
      $extra = "";
  if ($physicians_list) {
    $physician_count = 0;
    foreach ($physicians_list as $physician) {
      $dateDiff =0;
      $physician_id = $physician['ID'];
       
      if($user_id == "76389" && $physician_id == "137"){
     $extra = " AND `create_date` > '2018-06-12 23:59:59' ";
  }
      else{
     $extra = " AND `create_date` > '2017-03-27 23:59:59' ";
  }
      
      $specimen_sql = "SELECT MAX(u1.`create_date`) as 'create_date', COUNT(u1.`id`) AS 'total_orders',u1.`test_type` FROM (SELECT * FROM `wp_abd_specimen` UNION ALL SELECT * From `wp_abd_specimen_archive`) u1 WHERE u1.`physician_id` = $physician_id AND u1.`status` = '0' AND u1.`physician_accepct` = '0' AND u1.`qc_check` = '0' GROUP BY u1.`test_type`";
      
      $specimen_submitted = $this->BlankModel->customquery($specimen_sql);
      $specimen_submitted_date_NF = '';
      $specimen_submitted_date_W  = '';
      $specimen_submitted_date = '';
      $totalSpecimen    = 0;
      $totalSpecimen_NF = 0;
      $totalSpecimen_W  = 0;

      if(array_key_exists("0", $specimen_submitted)){
        if($specimen_submitted[0]['test_type'] == 'NF'){
          $specimen_submitted_date_NF = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
          $totalSpecimen_NF  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
        }else if($specimen_submitted[0]['test_type'] == 'W'){
          $specimen_submitted_date_W = ($specimen_submitted[0]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[0]['create_date'])) : "");
          $totalSpecimen_W  = ($specimen_submitted[0]['total_orders'] != "" ? $specimen_submitted[0]['total_orders'] : 0);
        }

        if(array_key_exists("1", $specimen_submitted)){
          if($specimen_submitted[1]['test_type'] == 'NF'){
            $specimen_submitted_date_NF = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
            $totalSpecimen_NF  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
          }else if($specimen_submitted[1]['test_type'] == 'W'){
            $specimen_submitted_date_W = ($specimen_submitted[1]['create_date'] != "" ? date('F d, Y', strtotime($specimen_submitted[1]['create_date'])) : "");
            $totalSpecimen_W  = ($specimen_submitted[1]['total_orders'] != "" ? $specimen_submitted[1]['total_orders'] : 0);
          }
        }
        $specimen_submitted_date = (strtotime($specimen_submitted_date_NF) > strtotime($specimen_submitted_date_W)? $specimen_submitted_date_NF : $specimen_submitted_date_W);
        $last_submitted_date = (strtotime($last_submitted_date) < strtotime($specimen_submitted_date))?$specimen_submitted_date:$last_submitted_date;
        $totalSpecimen  = ((int)$totalSpecimen_NF + (int)$totalSpecimen_W);
      }else{
        $totalSpecimen  = 0;
        $specimen_submitted_date = '';
      }
      }
    } 
    if(!empty($last_submitted_date)){
      $diff = abs(strtotime($last_submitted_date) - strtotime($physician['user_registered']));
      $dateDiff = floor($diff / (60*60*24));  
      if($dateDiff > 0){
        $activationSum += $dateDiff;
        ++$usrCount;

      }
      
    }else{
      $dateDiff =0;
    }
    $sales_arr = array(
        'physician_count'=> $physicians_list?count($physicians_list):0,
        'specimen_submitted_date' => $last_submitted_date,
        'dateDiff'=>$dateDiff,
        'full_name'      => $sales_representative['first_name']. ' '.$sales_representative['last_name'],
        'user_registered' => $sales_representative['user_registered']
        );
      array_push($salesRepsArr, $sales_arr);
    }
      echo json_encode(array(
        "status" => '1',
        "salesDetails" => $salesRepsArr,
        'avgDateDiff'=>$activationSum?floor(($activationSum / $usrCount)):0,
        'usrCount'=>$usrCount?$usrCount:0
      ));
      }else {
        echo json_encode(array(
          "status" => '0',
          "message" => 'no data found!'
        ));
      }
    }

   }