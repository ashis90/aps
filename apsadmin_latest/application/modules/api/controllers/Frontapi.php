<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Frontapi extends MY_Controller {

    function __construct() {
        parent::__construct();
       
    }
    function index()
    {
       echo "About Us";
	}

	function get_aps_about_list(){
      	$data = json_decode(file_get_contents('php://input'), true);
		$sqlAboutus = "SELECT * FROM `wp_abd_sitepages` WHERE `site_page_name`= 'ABOUT US' AND `site_page_type`='APS'";
        $aboutus_result = $this->BlankModel->customquery($sqlAboutus);
        $aboutus_result_count = count($aboutus_result);
        if ($aboutus_result_count == 0) {
            echo json_encode(array("status" => "0", "message" => 'Fail'));
        } else{
         	echo json_encode(array("status" => "1", "message" => 'Success', 'aboutUs' => $aboutus_result));
        }

    }
    

    function get_aps_services(){
        $data = json_decode(file_get_contents('php://input'), true);
        $sql_services = "SELECT * FROM `wp_abd_sitepages` WHERE `site_page_name` = 'OUR SERVICES' AND `site_page_type`='APS'";
        $service_result = $this->BlankModel->customquery($sql_services);
        $service_result_count = count($service_result);
        if ($service_result_count == 0) {
            echo json_encode(array("status" => "0", "message" => 'Fail'));
        } else{
            echo json_encode(array("status" => "1", "message" => 'Success', 'service' => $service_result));
        }
    }

    function get_aps_home(){
        $data = json_decode(file_get_contents('php://input'), true);
        $sql_home = "SELECT * FROM `wp_abd_sitepages` WHERE `site_page_name` = 'Home' AND `site_page_type`='APS'";
        $home_result = $this->BlankModel->customquery($sql_home);
        $home_result_count = count($home_result);
        if ( $home_result_count == 0 ){
            echo json_encode(array("status" => "0", "message" => 'Fail'));
        } else{
            echo json_encode(array("status" => "1", "message" => 'Success', 'home' => $home_result));
        }
    }

    function get_aps_banner(){
        $data = json_decode(file_get_contents('php://input'), true);
        $sql_banner = "SELECT * FROM `wp_abd_sitepages` WHERE `site_page_name` = 'Banner' AND `site_page_type` = 'APS'";
        $banner_result = $this->BlankModel->customquery($sql_banner);
        $banner_result_count = count($banner_result);
        if ( $banner_result_count == 0 ) {
            echo json_encode(array("status" => "0", "message" => 'Fail'));
        } else{
            echo json_encode(array("status" => "1", "message" => 'Success', 'banner' => $banner_result));
        }
    }
		

}

?>