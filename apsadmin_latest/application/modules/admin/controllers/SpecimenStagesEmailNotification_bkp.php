<?php if (!defined('BASEPATH')) EXIT("No direct script access allowed");
class SpecimenStagesEmailNotification extends MY_Controller 
{
     function __construct() {
        parent::__construct();
        date_default_timezone_set('MST7MDT');
    }
    function index()
    {}
 
   /**
   * 
   * Get HISTO Avg time
   */
  
    function specimenStagesTatEmailNotification()
    {  
      
    $details = array();
    $specimen_results = $this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
    (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2020-01-01 23:59:59' AND `id` NOT IN 
    (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN 
    (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id` ORDER BY `id` DESC");
   
    if($specimen_results)
    {

	$mail_format    = "";
	$mail_format   .= "<div class='cont_wrapperInner' style='border:4px solid #34b6e6;width:820px;padding:10px;'>
	<p style='text-align: center'><img src='https://www.abilitydiagnostics.com/abadmin/assets/frontend/main_images/logo.png' alt='Ability Diagonostics' align='middle'></p>
	<h1 style='color:#2e4b01' align='center'>Ability Diagonostics</h1>

    <p style='font:normal 13px/18px Arial, Helvetica, sans-serif; font-weight:bold; border:none;'>Hi, Admin <br>
	<p style='font:normal 13px/18px Arial, Helvetica, sans-serif; margin-bottom:0px; color:#000000;'> Below is a list of the Specimens where Specimen go beyond their given Turn Around Time(TAT).</p>
	</p>
	
	<table width='100%' border='0' cellpadding='2' cellspacing='2' style='background:none; border:none;margin-top:25px;'>
	
	<tr class='users_list'>
	<th width='15%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Accessioning Number</strong></th>
	<th width='15%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Delivery to Lab</strong></th>
	<th width='10%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Gross</strong></th>
	<th width='10%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Microtomy</strong></th>
	<th width='10%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>QC Check </strong></th>
	<th width='20%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Pending Specimen List</strong></th>
	<th width='20%' style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><strong>Pending Reports</strong></th>
	</tr>
	<tr>";
	
	/**
	* 
	* Ability HISTO Process
	* 
	*/
	   
    $count  = count($specimen_results);
	$current_datetime     = date('Y-m-d H:i:s');                   
	$current_timestamp    = strtotime($current_datetime);  
    foreach ($specimen_results as $specimen_data)
    {	
    	
      $specimen_timestamps   = strtotime($specimen_data['create_date']);               
      $seconds_calculation   = floor(abs($current_timestamp - $specimen_timestamps));
     
      $current_time_stage    = date('Y-m-d h:i:sa', $current_timestamp);
      $specimen_time_stage   = date('Y-m-d h:i:sa', $specimen_timestamps); 	
    	/**
		* 
		* @var Delivary to Lab 24 hrs.
		* 
		*/
       
        $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
      
         $stage1 = '';      
        //Check Stage Completed or Not.
        if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
        {              
           $color1 = 'Green';                    
        }
        else
        {              
          if($seconds_calculation > 86400) // 24 hrs.
          {
            $color1 = 'Red';
            $stage1 = 'Delivary to Lab';        
          }
          else
          {
           $color1 = 'Green';                
          } 
        }
        
        /**
		* 
		* @var Gross Description 8hrs.
		* 
		*/
        
        $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
         
          $stage2 = ''; 
           //Check Stage Completed or Not.
          if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
          {
            $color2 = 'Green';    
          }
          else
          {                  
           if($seconds_calculation > 28800)  // 8 hrs.
           {
            $color2 = 'Red'; 
            $stage2 = 'Gross Description';                
           }
           else
           {
            $color2 = 'Green';            
           }
          }

			/**
			* 
			* @var Microtomy  24 hrs.>86400) // 24 hrs.
			* 
			*/
          $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
         
         if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
          {
              $color3 = 'Green';    
          }  
          else
          {                 
            if($seconds_calculation > 86400) // 24 hrs.
            {
               $color3 = 'Red';
            }
            else 
            {
               $color3 = 'Green';
            } 
              
          }

          /**
		  * 
		  * @var QC Check  6 hrs.
		  * 
		  */
		  
          $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
          if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
          {
                $color4 = 'Green';    
          }
          else
          {
            if($seconds_calculation > 21600) // 6 hrs.
            {
                $color4 = 'Red';
            }
            else 
            {
                $color4 = 'Green';
            } 
          }

           /**
		   * 
		   * @var Pending Specimen List 48 hrs.
		   * 
		   */

            $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
            if(!empty($report_sql[0]['create_date']))
            {
                 $color5 = 'Green';  
                 $pending_specimen_status =  'Report Generated';                  
            }
            else
            {
             
              if($seconds_calculation > 172800) // 48 hrs.
              {
                 $color5 = 'Red';
                 $pending_specimen_status =  'Report Not Generated';  
              }
              else
              {
                 $color5 = 'Green';
                 $pending_specimen_status =  'Not Pass TAT';  
              }
            }

			/**
			* 
			* @var Pending Report 48 hrs.
			* 
			*/

             $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."' AND `status` = 'Active' ");
            if(!empty($report_sql[0]['create_date']))
            {
               $color6 = 'Green';
               $pending_report_status =  'Report Submitted';    
            }
            else
            {                     
              if($seconds_calculation > 172800) // 48 hrs.
              {
                 $color6 = 'Red';
                 $pending_report_status =  'Report Not Submitted';    
              }
              else
              {
                 $color6 = 'Green';
                 $pending_report_status =  'Not Pass TAT';    
              }
            }

        $specimen_information =  array( 'accessioning_num' => $specimen_data['assessioning_num'], 
        								'specimen_id' => $specimen_data['id'],
						                'color1' => $color1, 
						                'color2' => $color2,
						                'color3' => $color3, 
						                'color4' => $color4,						                 
						                'color5' => $color5,
						                'pending_specimen_status' => $pending_specimen_status, 
						                'color6' => $color6,
						                'pending_report_status' => $pending_report_status
						               
						                
						                 );
        array_push($details, $specimen_information); 
     }
       
     foreach($details as $specimnen){
	 	if (in_array("Red", $specimnen))
        {
        
       $mail_format.= "<tr>
		<td valign='center'>
			<div style='text-align: center;'>".$specimnen['accessioning_num']."</div>
		</td>
	    
	    <td valign='center'>
			<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/specimen-next-page/".$specimnen['specimen_id']."/1' target='_blank' title='Delivery To Lab' style='color: ".$specimnen['color1'].";'>".$specimnen['color1']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/specimen-next-page/".$specimnen['specimen_id']."/2' target='_blank' title='Gross' style='color: ".$specimnen['color2'].";'>".$specimnen['color2']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/specimen-next-page/".$specimnen['specimen_id']."/3' target='_blank' title='Microtomy' style='color: ".$specimnen['color3'].";'>".$specimnen['color3']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/specimen-next-page/".$specimnen['specimen_id']."/4' target='_blank' title='QC Check' style='color: ".$specimnen['color4'].";'>".$specimnen['color4']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;'><a href='https://www.abilitydiagnostics.com/add-specimen-report/".$specimnen['specimen_id']."' target='_blank' title='Pending Specimen' style='color: ".$specimnen['color5'].";'>".$specimnen['pending_specimen_status']."</a></div>
		</td>
		
		<td valign='center'>
		<div style='text-align: center;' ><a href='javascript:void(0);' title='Pending Reports' style='color: ".$specimnen['color6'].";'> ".$specimnen['pending_report_status']."</a></div>
		</td>
		
		</tr>"; 
           
        }
	 	
	 	
	 } 
       
       
       
   $mail_format.= "</tr>
	</table>
	<br>
	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><span style='color: Red'>Red:</span> Accessioning Number go Beyond Their TAT.</span> <br>
	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'><span style='color: Green'>Green:</span> Accessioning Number Process may be completed or it has time to process.</span> <br><br>
	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'> Thanks,</span> <br>
	<span style='font:bold 13px/18px Arial, Helvetica, sans-serif;color:#000000;'> Support Team </span><br>
	<a href='mailto:support@abilitydiagnostics.com' target='_blank' style='font:normal 13px/18px Arial, Helvetica, sans-serif;color:#000000;'>support@abilitydiagnostics.com</a> 
	
	</div>";
	   
	
	echo $mail_format;
	
	    $from = "support@abilitydiagnostics.com";
		//$cc   = "trajanking@gmail.com";
		$bcc  = "ashis@sleekinfosolutions.com";
       
		$config['mailtype'] = 'html';
		$config['charset']  = 'UTF-8';
		$config['newline']  = "\r\n";
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset']  = 'iso-8859-1';
		$config['wordwrap'] = TRUE;

        $this->load->library('email',$config);
        $this->email->from($from, 'Ability Diagnostics');
        $this->email->to('ashis@sleekinfosolutions.com');
       // $this->email->cc($cc);
        $this->email->bcc($bcc);
        $this->email->subject('Specimens TAT Notification');
        $this->email->message($mail_format);
        $admin = $this->email->send();  
       
       
        }
   
   
    }
    


	/**
	* 
	* PCR Avg time
	*/
	
  function get_pcr_avg_time()
    {   
        $tot_minutes=0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
   
        $details = array();
        $specimen_results =$this->BlankModel->customquery("SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`qc_check`
        FROM `wp_abd_specimen` INNER JOIN `wp_abd_clinical_info` ON wp_abd_specimen.id = wp_abd_clinical_info.specimen_id  WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
        AND wp_abd_specimen.status = '0' AND  wp_abd_specimen.qc_check = '0' AND wp_abd_specimen.physician_accepct = '0'
        AND wp_abd_specimen.create_date > '2020-03-31 23:59:59' ORDER BY `wp_abd_specimen`.`id` DESC");
        if($specimen_results)
        {
            $count = count($specimen_results);
            $total_specimen_count = 0;
            $stage2_total_specimen_count = 0;
            $stage3_total_specimen_count = 0;
            $report_total_specimen_count = 0;
            foreach ($specimen_results as $specimen_data)
            {
                /**
				* 
				* @var Delivery to Extraction
				* 
				*/
				
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_pcr_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
            
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $specimen_count = 1;
				$total_specimen_count+= $specimen_count;	
				
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$total_specimen_count);
               /* $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);
*/
                $n = $avg_minutes*60; 
				$day = floor($n / (24 * 3600)); 

			 	$n = ($n % (24 * 3600)); 
				$hour = round(($n / 3600),1); 


                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400) // 24 hrs
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                /**
				* 
				* @var Assign Well Stages
				* 
				*/
								
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_assign_stage` WHERE `specimen_id` ='".$specimen_data['id']."'"); 
              
                 if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $stage2_specimen_count = 1;           
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2 = date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2  = date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                
				  $stage2_total_specimen_count+= $stage2_specimen_count;	
                  $minutes_stage2 = floor(abs($timestamp_stage2 -(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$stage2_total_specimen_count);
                
                  $n = $avg_minutes_stage2*60; 
				  $day_stage2 = floor($n / (24 * 3600)); 

			 	  $n = ($n % (24 * 3600)); 
				  $hour_stage2 = round(($n / 3600),1); 




                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>86400) // 24 hrs
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }
               
                 /**
				  * 
				  * @var Data Analysis 72 hrs.
				  * 
				  */
				  
                 $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `create_date` as `specimen_timestamp` FROM `wp_abd_import_data` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."' LIMIT 0,1"); 
                
                
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $stage3_specimen_count	   = 1;                 	
                  $stage3_total_specimen_count+= $stage3_specimen_count;	
                 	
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$stage3_total_specimen_count);
        
				  $n = $avg_minutes_stage3*60; 
				  $day_stage3 = floor($n / (24 * 3600)); 

			 	  $n = ($n % (24 * 3600)); 
				  $hour_stage3 = round(($n / 3600),1); 

			


                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>259600) // 72 hrs.
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

				/**
				* 
				* @var Report Generated
				* 
				*/

                $pcr_report_sql = $this->BlankModel->customquery("SELECT `created_date` FROM `wp_abd_generated_pcr_reports` WHERE `accessioning_num` ='".$specimen_data['assessioning_num']."'");
                  
                    if(!empty($pcr_report_sql[0]['created_date']))
                    {
                    	
					$report_specimen_count	   = 1;                 	
					$report_total_specimen_count+= $report_specimen_count;	
                    	
                      $timestamp_report = strtotime($pcr_report_sql[0]['created_date']);
                      $formatted_time_report = date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time_report  = date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_report  = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_report += $minutes_report;
                      $avg_minutes_report  = floor($tot_minutes_report/$report_total_specimen_count);
                    
                    
					  $n = $avg_minutes_report*60; 
					  $day_stage_report = floor($n / (24 * 3600)); 
     				  $n = ($n % (24 * 3600)); 
					  $hour_stage_report = round(($n / 3600),1); 

                    
                    
                      /*$hour_stage_report   = round(($avg_minutes_report/60),1);
                      $day_stage_report    = floor($hour_stage_report/24); */

                      $chk_hour_pcr_report = floor(abs(strtotime($pcr_report_sql[0]['created_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_pcr_report > 144000)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_report, $original_time_report); 
                      }
                      else
                      {
                         $color5 = '';
                         $time5 =  dateDiff($formatted_time_report, $original_time_report); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }



                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2, 'stage3'=>$time3, 'report'=>$time5,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,'color4'=>$color5);
                
                array_push($details, $specimen_information); 
            
            }

			if(!empty($day))
			{
			$day1 = $day." Days";
			}
			else{
			$day1= "" ;
			}
			if(!empty($hour))
			{
			$hour1 = $hour." Hours";
			}
			else{
			$hour1= "" ;
			}

			if(!empty($day_stage2))
			{
			$day2 = $day_stage2." Days";
			}
			else{
			$day2= "" ;
			}
			if(!empty($hour_stage2))
			{
			$hour2 = $hour_stage2." Hours";
			}
			else{
			$hour2= "" ;
			}

			if(!empty($day_stage3))
			{
			$day3 = $day_stage3." Days";
			}
			else{
			$day3= "" ;
			}
			if(!empty($hour_stage3))
			{
			$hour3 = $hour_stage3." Hours";
			}
			else{
			$hour3= "" ;
			}
			
		
			
		    if(!empty($day_stage_report))
			{
			$day4 = $day_stage_report." Days";
			}
			else{
			$day4= "" ;
			}
			
			if(!empty($hour_stage_report))
			{
			$hour4 = $hour_stage_report." Hours";
			}
			else{
			$hour4= "" ;
			}

         
      
        }
   
    }
     
    
   
  
       

}       

?>