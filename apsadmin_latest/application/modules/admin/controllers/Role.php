<?php
	// class Role extends MY_Controller{
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
	class Role extends MY_Controller {
		function __construct(){
			parent::__construct();
 			$this->session_checked($is_active_session = 1);
		}


		function index(){
			$sql_role = "SELECT * FROM wp_abd_role_management";
			$query = $this->db->query($sql_role);
			$resList = $query->result_array();

			common_viewloader('Role/index', array('roleList'=> $resList));
		}

		function add_role(){
        	$this->session_checked($is_active_session = 1);
			common_viewloader('Role/addNewRole'); 	
		}

		function createNewRole(){

			if ($this->input->post()) {
				
				$this->form_validation->set_rules('role_name','Role Name', 'required');
				$this->form_validation->set_rules('role_shortname','Role Name', 'required');
				if ($this->form_validation->run()==FALSE) {
					common_viewloader('Role/addNewRole');
				} else{
					$data = $this->input->post();
					$insert_role = $this->BlankModel->addTableData('wp_abd_role_management',$data);
					if ($insert_role) {
						$this->session->set_flashdata('success','Successfully added Role');
		  	  			header('location:'.base_url().'admin/role/');	
					}
				}
			} else{
				$this->session->set_flashdata('Err','Submission Failed');
				header('location:'.base_url().'admin/role');
			}
		}

		function roleEdit($eid=''){
			$conditions = " ( `role_id` = '".$eid."')";		
			$select_fields = '*';
           	$is_multy_result = 1;
           	$role_data  = $this->BlankModel->getTableData('wp_abd_role_management', $conditions, $select_fields, $is_multy_result);
           	common_viewloader('Role/role_edit', array('role_data'=>$role_data));
		}

		function roleEditing(){
			if ($this->input->post()) {
				$data = $this->input->post();
				$role_id = $data['role_id'];
				$this->form_validation->set_rules('role_name','Role Name', 'required');
				$this->form_validation->set_rules('role_shortname','Role Name', 'required');
				if ($this->form_validation->run()==FALSE) {
					$this->roleEdit($role_id);
				} else{
					$data = $this->input->post();
					$role_id = $data['role_id'];
					$conditions = "(`role_id` = '".$role_id."')";
					$update_role = $this->BlankModel->editTableData('wp_abd_role_management', $data, $conditions);
					if ($update_role) {
						$this->session->set_flashdata('update','Successfully Updated');
						header('location:'.base_url().'admin/role/');
					}
				}
			} else{
				$this->session->set_flashdata('Err','Submission Failed');
				header('location:'.base_url().'admin/role/');
			}
		}

		function roleDelete($did=''){
			// echo $did;
			// exit();
			$delete_role = $this->BlankModel->delete_data_id('wp_abd_role_management','role_id',$did);
			// print_r($delete_role); exit();
			if ($delete_role) {
				$this->session->set_flashdata('delete','Successfully Deleted');
				header('location:'.base_url().'admin/role');
			}
		}
	}

?>