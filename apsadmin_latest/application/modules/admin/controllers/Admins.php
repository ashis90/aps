<?php
if(!defined('BASEPATH'))
EXIT("No direct script access allowed");
class Admins extends MY_Controller{
	function __construct(){
		parent::__construct();
     
	}
	public function index(){		
		$this->session_checked($is_active_session = 1);				
		$latestuser = get_latest_user();

		$sqlUser = "SELECT u1.ID,u1.user_login AS nickname,u1.user_email,u1.user_status,u1.user_registered,m1.meta_value AS firstname,m2.meta_value AS lastname,m3.meta_value AS role
			FROM wp_abd_users u1
			JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name')
			JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name')
			JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') WHERE u1.user_status=0 ORDER BY u1.user_registered desc LIMIT 5";
		$queryUser = $this->db->query($sqlUser);
		$resultUser = $queryUser->result_array();

    	$latestlogin = get_latest_login();
	
		$histoData = array();

		if ($this->input->post('submit')) {
			$histoData = array();

			$data = $this->input->post();
			$start_date = $data['start_date'];
			$newStartDate = date("Y-m-d", strtotime($start_date));
			$end_date = $data['end_date'];
			$newEndDate = date("Y-m-d", strtotime($end_date));

			////////////////////////For Histro Line Chart///////////////////////////
	
			$sqlHistro = "SELECT ABS(COUNT(`nail_funagl_id`)) AS nail_funagl_id,date_format( str_to_date( `create_date`,'%Y-%m-%d') ,'%Y-%m-%d') AS create_date FROM `wp_abd_nail_pathology_report` WHERE  date_format( str_to_date( `create_date`,'%Y-%m-%d') ,'%Y-%m-%d')  BETWEEN '".$newStartDate."' AND '".$newEndDate."' GROUP BY date_format( str_to_date( `create_date`,'%Y-%m-%d') ,'%Y-%m-%d') ";
			
			$queryHistro = $this->db->query($sqlHistro);
			$resultHistro = $queryHistro->result_array();




			$sqlPCR = "SELECT ABS(COUNT(`id`)) AS pcrID ,date_format( str_to_date( `created_date`,'%Y-%m-%d') ,'%Y-%m-%d') AS create_date FROM `wp_abd_generated_pcr_reports` WHERE  date_format( str_to_date( `created_date`,'%Y-%m-%d') ,'%Y-%m-%d')  BETWEEN '".$newStartDate."' AND '".$newEndDate."' GROUP BY date_format( str_to_date( `created_date`,'%Y-%m-%d') ,'%Y-%m-%d') ";
			
			$queryPCR = $this->db->query($sqlPCR);
			$resultPCR = $queryPCR->result_array();

			$nail_funagl_id = array_column($resultHistro,'nail_funagl_id');
			$histo_create_date = array_column($resultHistro, 'create_date');


			$pcrID = array_column($resultPCR,'pcrID');
			$pcr_create_date = array_column($resultPCR, 'create_date');


			$pcrCount=0;
			$histoCount=0;
			$crt_data = '';
    		while (($newStartDate) <= ($newEndDate)) {
    			$crt_data =$newStartDate;
				if(in_array($newStartDate,$histo_create_date)){
					$histoCount= (Int)$nail_funagl_id[array_search($newStartDate,$histo_create_date)];
			    }
		        else{
		          $blank_data = 0;
		          $histoCount=$blank_data;		     
		        }
		        if(in_array($newStartDate,$pcr_create_date)){
					$pcrCount= (Int)$pcrID[array_search($newStartDate,$pcr_create_date)];
			    }
		        else{
		          $blank_data = 0;
		          $pcrCount=$blank_data;		          
		        }

		      $new_collection_arr = array('create_date'=> $crt_data, 'histo_count'=>$histoCount,'pcr_count'=>$pcrCount);
		      array_push($histoData,$new_collection_arr);
		      
		      $newStartDate = date ("Y-m-d", strtotime("+1 day", strtotime($newStartDate)));
			}

		}

		/////////////////////////select physician/////////////////////////////////

		$sqlPhysician = "SELECT u1.ID,m1.meta_value AS firstname,m2.meta_value AS lastname,m3.meta_value AS role,m4.meta_value AS _status FROM wp_abd_users u1 
			JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name') 
			JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name') 
			JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities')
            JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = '_status') 
			WHERE m3.meta_value like '%physician%' AND m4.meta_value = 'Active' 
			ORDER by `user_login` ASC";

		$queryPhysician = $this->db->query($sqlPhysician);
		$resultPhysician = $queryPhysician->result_array();
		

		common_viewloader('Admins/index', array('latestuserList' => $latestuser,
										        'latestLogin'    => $latestlogin,
										        'userList'       => $resultUser,	
										    	'physicianList'  => $resultPhysician,
										    	));

	}
	
	/**
	* Admin Login
	* 
	* @return
	*/
	public function login(){	
		$this->session_checked($is_active_session = 0);		
		$this->load->library(array('form_validation'));
		if ($this->input->post()) {
			
		$rules = array(
			array(
				'field' => 'username',
				'label' => 'User Name',
				'rules' => 'required',
				'errors' => array()
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required',
				'errors' => array()
			)
		);
		$this->form_validation->set_rules($rules);
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if($this->form_validation->run() == true){			
				$conditions = $this->input->post();   
				$password   = md5(SECURITY_SALT.$conditions['password']);
	        	$user_name  = $this->db->escape($conditions['username']);
	        	$conditions = " ( `sysadm_login` = ".$user_name." AND `sysadm_password` = '".$password."' AND `sysadm_status` = 'Active')";
				$select_fields   = '*';
				$is_multy_result = 1;
				$admin  = $this->BlankModel->getTableData('sysadmin', $conditions, $select_fields, $is_multy_result);	

				

	       ///////////////////////////////////////////Captcha////////////////////////////////////////////////
			      $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
			       
			      $userIp=$this->input->ip_address();
			         
			       //$secret='6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe';  /////////////////localhost/////
//			      $secret='6Lc7164UAAAAAItBxyKHWUFKQjypHApCCxfMs3On'; 
                     $secret='6LeQYFsUAAAAALaMGqjZO7Vj0PExQ4czR2hLvYom';  // /////////////////Live/////
			       
			      $credential = array(
			            'secret' => $secret,
			            'response' => $this->input->post('g-recaptcha-response')
			        );
			 
			      $verify = curl_init();
			      curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
			      curl_setopt($verify, CURLOPT_POST, true);
			      curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($credential));
			      curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
			      curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
			      $response = curl_exec($verify);
			 
			      $status= json_decode($response, true);
			       
			  //     if($status['success']){ 
			    
			  //      $this->session->set_flashdata('success1', 'Google Recaptcha Successful');
					// common_viewloader('Admins/login');

			  //     }else{
			  //      $this->session->set_flashdata('error1', 'Sorry Google Recaptcha Unsuccessful!!');
					// common_viewloader('Admins/login');

			  //     }
	       ///////////////////////////////////////////Captcha////////////////////////////////////////////////
			     

				if((!empty($admin)) && ($status['success'])){		
					$this->set_admin_session($admin);
			        $this->session->set_flashdata('success1', 'Google Recaptcha Successful');
					// common_viewloader('Admins/login');
					redirect('admin/dashboard');
				} elseif(!$status['success']){
			       $this->session->set_flashdata('errorCaptcha', 'Sorry Google Recaptcha Required!!');
					common_viewloader('Admins/login');

			    }
			  //   elseif ((empty($admin)) && (!$status['success'])) {
			  //   	$this->session->set_flashdata('errorLogin', 'Sorry Username, Password & Google Recaptcha Required!!');
					// common_viewloader('Admins/login');
			  //   }
				else{
					$this->session->set_flashdata('error', 'Username or Password does not match.');
					common_viewloader('Admins/login');
				}
			}
			else{ 		
				common_viewloader('Admins/login');
			}
		} else{
			common_viewloader('Admins/login');
		}
	}

	public function logout(){
		$this->destroy_admin_session();
		$this->session->set_flashdata('success', 'You logout successfully');
		redirect('admin/admins/login');
	}

	public function profile(){
		$this->session_checked($is_active_session = 1);		
		$sql_admin = "SELECT * FROM `wp_abd_sysadmin`";
		$query = $this->db->query($sql_admin);
		$result_admin = $query->result_array();
		common_viewloader('Admins/profile', array('result_admin' => $result_admin));
	}
	
	public function updateProfile(){
		if ($this->input->post()) {
			$data = $this->input->post();

			$this->form_validation->set_rules('sysadm_fname','First Name','required');
			$this->form_validation->set_rules('sysadm_lname','Last Name','required');
			$this->form_validation->set_rules('sysadm_login','User Name','required');
			$this->form_validation->set_rules('sysadm_email','Email Id','required');
			$this->form_validation->set_rules('new_password','New Password','min_length[8]');
			$this->form_validation->set_rules('confirm_password','Confirm Password','matches[new_password]');

			if ($this->form_validation->run()==FALSE) {

				$this->profile();
			} else{
				$conditions = " ( `sysadm_id` = '".$data['sysadm_id']."')";

				$admin_data = array('sysadm_fname'=>$data['sysadm_fname'],
									'sysadm_lname'=>$data['sysadm_lname'],
									'sysadm_email'=>$data['sysadm_email'],
									'sysadm_login'=>$data['sysadm_login']);

				$new_password = $data['new_password'];
				$confirm_password = $data['confirm_password'];

				if (isset($confirm_password) && !empty($confirm_password)) {
					$admin_data['sysadm_password'] = md5(SECURITY_SALT.$new_password);
				}

				$admin_update =  $this->BlankModel->editTableData('wp_abd_sysadmin',$admin_data,$conditions);
				if ($admin_data) {
					$this->session->set_flashdata('profile','Successfully Updated Profile');
			     	header('location:'.base_url().'admin/admins/profile');	
				} else{
					$this->session->set_flashdata('ntprofile','Updated Failed');
					header('location:/'.base_url().'admin/admins/profile');
				}

			}
		}
	}

	
}
?>