<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
class User extends MY_Controller {

    function __construct() {
        parent::__construct();
          $this->session_checked($is_active_session = 1);
    }


 public function index($roletype='') 
 {
 	$this->session_checked($is_active_session = 1);
	$wherefld = '';
 	if(isset($roletype) && $roletype!='all')
	{
		$wherefld=" WHERE m3.meta_value like '%".$roletype."%'";
	}
	
	
	/*---------------------------------Query to show listing-------------------------------*/
	$sqlUserlist = "SELECT u1.ID,u1.user_login AS nickname,u1.user_email,m1.meta_value AS firstname,m2.meta_value AS lastname,m3.meta_value AS role
	-- , m4.meta_value as partners_company
	FROM wp_abd_users u1
	JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name')
	JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name')
	-- LEFT JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = 'partners_company')
	JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') ".$wherefld." GROUP BY u1.ID";

	$exequerylist = $this->db->query($sqlUserlist);
	$resArrList = $exequerylist->result_array(); 

	// echo "<pre>";
	// foreach ($resArrList as $value) {
		// $val = array($value['role'] => '1');
		// $vall = unserialize($val);
	// print_r($value);
	// }
	// exit();

	$sqlRole = "SELECT * FROM `wp_abd_role_management`";
	$executeRole = $this->db->query($sqlRole);
	$resArr = $executeRole->result_array();


	$sqlPartnerCompany = "SELECT * FROM `wp_abd_partners_company`";
	$executePartner = $this->db->query($sqlPartnerCompany);
	$resArrPartner = $executePartner->result_array();
	// echo "<pre>";
	// print_r($resArrPartner);
	// exit();
	
	/*---------------------------END-------------------------------*/
	
	// common_viewloader('User/index', array('userList' => $resArrList,'roleList' => $roleArr)); 
	common_viewloader('User/index', array('userList' => $resArrList,
										'roleList' => $resArr, 
										'partnerList' => $resArrPartner)); 
	// common_viewloader('User/index',$result); 
 }
 
  function userEdit()
  {


		if(isset($roleArr['user_id']))
		{
			//echo "string";die;
			$sqlRoles = "SELECT * FROM `wp_abd_role_management`";
		  	$executeRoles = $this->db->query($sqlRoles);
		  	$roleArr = $executeRoles->result_array();
		  	
			/*---------------------------END-------------------------------*/
			
			/*------------------------Find partner list Array---------------*/
			
			$sqlpartner = "SELECT u1.ID,m1.meta_value AS firstname,m2.meta_value AS lastname,m3.meta_value AS role FROM wp_abd_users u1 
			JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name') 
			JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name') 
			JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 
			WHERE m3.meta_value like '%partner%'";
			
			$exequerypartner = $this->db->query($sqlpartner);
			$partnerArr = $exequerypartner->result_array();
			
			/*----------------------------END------------------------------*/


			/*------------------------Find Clinic list Array---------------*/
			
			$sqlclinic = "SELECT u1.ID,m1.meta_value AS firstname,m2.meta_value AS lastname,m3.meta_value AS role,m4.meta_value AS _status FROM wp_abd_users u1 
			JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name') 
			JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name') 
			JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities')
            JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = '_status') 
			WHERE m3.meta_value like '%clinic%' AND m4.meta_value = 'Active' 
			ORDER by `user_login` ASC";
			
			$exequeryclinic = $this->db->query($sqlclinic);
			$clinicArr = $exequeryclinic->result_array();
			
			/*----------------------------END------------------------------*/


			/*------------------------Find combine physicians accounts list Array---------------*/
			
			$sqlcombinephysicians = "SELECT u1.ID,m1.meta_value AS firstname,m2.meta_value AS lastname,m3.meta_value AS role,m4.meta_value AS _status FROM wp_abd_users u1 
			JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name') 
			JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name') 
			JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities')
            JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = '_status') 
			WHERE m3.meta_value like '%combine_physicians_accounts%' AND m4.meta_value = 'Active' 
			ORDER by `user_login` ASC";
			
			$exequerycombine = $this->db->query($sqlcombinephysicians);
			$combineArr = $exequerycombine->result_array();
			
			/*----------------------------END------------------------------*/

			/*------------------------Find Sales Representative list Array---------------*/
			
			$sqlsalesrepresentative = "SELECT u1.ID,m1.meta_value AS firstname,m2.meta_value AS lastname,m3.meta_value AS role,m4.meta_value AS _status FROM wp_abd_users u1 
			JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name') 
			JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name') 
			JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities')
            JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = '_status') 
			WHERE m3.meta_value like '%sales%' AND m4.meta_value = 'Active' 
			ORDER by `user_login` ASC";
			
			$exequerysales = $this->db->query($sqlsalesrepresentative);
			$salesArr = $exequerysales->result_array();
			
			/*----------------------------END------------------------------*/


			/*---------------------Find Sales Regional/Regional Manager list Array---------------*/
			
			$sqlsalesregional= "SELECT u1.ID,m1.meta_value AS firstname,m2.meta_value AS lastname,m3.meta_value AS role,m4.meta_value AS _status FROM wp_abd_users u1 
			JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name') 
			JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name') 
			JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities')
            JOIN wp_abd_usermeta m4 ON (m4.user_id = u1.ID AND m4.meta_key = '_status') 
			WHERE (m3.meta_value like '%sales_regional_manager%' AND m4.meta_value = 'Active') OR (m3.meta_value like '%regional_manager%' AND m4.meta_value = 'Active')
			ORDER by `user_login` ASC";
			
			$exequerysalesregional = $this->db->query($sqlsalesregional);
			$salesregionalArr = $exequerysalesregional->result_array();
			
			/*----------------------------END------------------------------*/

			/*------------------------Find Assign Speciality Physician list Array---------------*/
			
			$sqlassignspeciality = "SELECT DISTINCT `meta_value`  FROM `wp_abd_usermeta` WHERE `meta_key` LIKE 'assign_specialty'";
			
			$exequeryassign = $this->db->query($sqlassignspeciality);
			$assignSpecialityArr = $exequeryassign->result_array();
			
			/*----------------------------END------------------------------*/

			$sqlPartnerCompany = "SELECT * FROM `wp_abd_partners_company`";
			$executePartner = $this->db->query($sqlPartnerCompany);
			$resArrPartner = $executePartner->result_array();
            $user_id = $_REQUEST['user_id'];
		    $conditions = " ( `ID` = '".$user_id."')";	        
            $select_fields = '*';
            $is_multy_result = 1;
            $user_data  = $this->BlankModel->getTableData('users', $conditions, $select_fields, $is_multy_result);
            $user_data['status'] = get_user_meta_value($user_id,'_status', TRUE);

	
			common_viewloader('User/user_mgredit', array('user_data' => $user_data,
				'roleList'       => $roleArr,
				'partnerList'    => $partnerArr,
				'clinicList'     => $clinicArr,
				'combinePhysicianList' => $combineArr, 
				'assignSpecialityList' => $assignSpecialityArr,
				'salesList'            => $salesArr,
				'salesregionalList'    => $salesregionalArr,
				'partnerCompanyList'   => $resArrPartner
			)); 	
		}
  }
  
  function userEditing()
  {
   
  	if ($this->input->post()) {
  		$data = $this->input->post();
  		$user_id = $data['user_id'];	

  		$this->form_validation->set_rules('user_username', 'User Name', 'trim|required|max_length[50]');
  		$this->form_validation->set_rules('user_email', 'User Email', 'trim|required|valid_email|trim|xss_clean' );   
  		$this->form_validation->set_rules('user_firstname', 'User First Name', 'trim|required|max_length[50]');
  		$this->form_validation->set_rules('user_role', 'User Role', 'required');
  		$this->form_validation->set_rules('new_password', 'Password', 'min_length[8]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'matches[new_password]');

  		if ($this->form_validation->run()== FALSE) {
  			$this->userEdit($user_id);
  	    } 
  		else{

  			$user_login = $data['user_username'];
  			$user_email = $data['user_email'];
			$sqlRole = "SELECT * FROM `wp_abd_users` WHERE (`user_login` = '$user_login' AND `ID`!='$user_id') OR (`user_email` = '$user_email' AND `ID`!='$user_id')";
			$executeRole = $this->db->query($sqlRole);
			$val = $executeRole->result_array();
	
			if ($val) {				
				$this->session->set_flashdata('email', 'User Name Or Email already exists!!');
  				$this->userEdit($user_id);		  	  
			} 
			else{
		  		$data = array();
			    $data = $this->input->post();
			    
				if (!empty($_FILES['company_logo']['name']) )
			 	{
					$filename = $_FILES['company_logo']['name'];
					$path = './assets/uploads/partner_images/';
					$fieldname = 'company_logo';
					$file_name = file_upload($path,$fieldname);
					$full_path = base_url('assets/uploads/partner_images/').$file_name;
					if (get_user_meta($data['user_id'],'company_logo', true)) {
						update_user_meta($data['user_id'],'company_logo', $full_path);
					} 
					else{
			  	 		add_user_meta($data['user_id'], 'company_logo', $full_path);
			  	 	}
			 	}
					
			   	/*----------------------------Update User meta-----------------------------*/
				update_user_meta($data['user_id'],'first_name',$data['user_firstname']);
				update_user_meta($data['user_id'],'last_name',$data['user_lastname']);
			    $role_type_array = array($data['user_role'] => '1');
			    $role = serialize( $role_type_array);				 
				update_user_meta($data['user_id'],'wp_abd_capabilities',$role);



				if(get_user_meta($data['user_id'],'partners_company',true)){
				 	update_user_meta($data['user_id'],'partners_company',$data['partner_company']);
				}
				else{
			  	 	add_user_meta($data['user_id'], 'partners_company', $data['partner_company']);
				}




				if(get_user_meta($data['user_id'],'_mobile',true)){
				 	update_user_meta($data['user_id'],'_mobile',$data['user_mobile']);
				}
				else{
			  	 	add_user_meta($data['user_id'], '_mobile', $data['user_mobile']);
				}
				if (get_user_meta($data['user_id'],'_address', TRUE)) {
				 	update_user_meta($data['user_id'],'_address',$data['user_address']);
				}
				else{
			  	 	add_user_meta($data['user_id'], '_address', $data['user_address']);
				}
				if (get_user_meta($data['user_id'],'nickname', TRUE)) {
				 	update_user_meta($data['user_id'],'nickname',$data['user_nickname']);
				} 
				else{
				 	add_user_meta($data['user_id'], 'nickname', $data['user_nickname']);
				}
				// update_user_meta($data['user_id'],'_assign_partner',$data['assign_partner']);
				update_user_meta($data['user_id'],'_status',$data['user_status']);
				if(isset($data['user_fax']))
				{
				 	if (get_user_meta($data['user_id'],'fax', TRUE)) {
					 	update_user_meta($data['user_id'],'fax',$data['user_fax']);
				 	} else{
				 		add_user_meta($data['user_id'], 'fax', $data['user_fax']);
				 	}
				}

				if(isset($data['anthr_fax_number']))
				{
				 	if (get_user_meta($data['user_id'],'anthr_fax_number', TRUE)) {
					 	update_user_meta($data['user_id'],'anthr_fax_number',$data['anthr_fax_number']);
				 	} else{
				 		add_user_meta($data['user_id'], 'anthr_fax_number', $data['anthr_fax_number']);
				 	}
				}

				if(isset($data['cell_phone']))
				{
				 	if (get_user_meta($data['user_id'],'cell_phone', TRUE)) {
					 	update_user_meta($data['user_id'],'cell_phone',$data['cell_phone']);
				 	} else{
				 		add_user_meta($data['user_id'], 'cell_phone', $data['cell_phone']);
				 	}
				}



				if(isset($data['manager_contact_name']))
				{
				 	if (get_user_meta($data['user_id'],'manager_contact_name', TRUE)) {
					 	update_user_meta($data['user_id'],'manager_contact_name',$data['manager_contact_name']);
				 	} else{
				 		add_user_meta($data['user_id'], 'manager_contact_name', $data['manager_contact_name']);
				 	}
				}



				if(isset($data['manager_cell_number']))
				{
				 	if (get_user_meta($data['user_id'],'manager_cell_number', TRUE)) {
					 	update_user_meta($data['user_id'],'manager_cell_number',$data['manager_cell_number']);
				 	} else{
				 		add_user_meta($data['user_id'], 'manager_cell_number', $data['manager_cell_number']);
				 	}
				}



				if(isset($data['combined_by']))
				{
				 	if (get_user_meta($data['user_id'],'combined_by', TRUE)) {
					 	update_user_meta($data['user_id'],'combined_by',$data['combined_by']);
				 	} else{
				 		add_user_meta($data['user_id'], 'combined_by', $data['combined_by']);
				 	}
				}


				if(isset($data['assign_clinic']))
				{
				 	if (get_user_meta($data['user_id'],'_assign_clinic', TRUE)) {
					 	update_user_meta($data['user_id'],'_assign_clinic',$data['assign_clinic']);
				 	} else{
				 		add_user_meta($data['user_id'], '_assign_clinic', $data['assign_clinic']);
				 	}
				}


				if(isset($data['added_by']))
				{
				 	if (get_user_meta($data['user_id'],'added_by', TRUE)) {
					 	update_user_meta($data['user_id'],'added_by',$data['added_by']);
				 	} else{
				 		add_user_meta($data['user_id'], 'added_by', $data['added_by']);
				 	}
				}


				if(isset($data['clinic_name']))
				{
				 	if (get_user_meta($data['user_id'],'_clinic_name', TRUE)) {
					 	update_user_meta($data['user_id'],'_clinic_name',$data['clinic_name']);
				 	} else{
				 		add_user_meta($data['user_id'], '_clinic_name', $data['clinic_name']);
				 	}
				}


				if(isset($data['assign_specialty']))
				{
				 	if (get_user_meta($data['user_id'],'assign_specialty', TRUE)) {
					 	update_user_meta($data['user_id'],'assign_specialty',$data['assign_specialty']);
				 	} else{
				 		add_user_meta($data['user_id'], 'assign_specialty', $data['assign_specialty']);
				 	}
				}


				if(isset($data['assign_partner']))
				{
				 	if (get_user_meta($data['user_id'],'_assign_partner', TRUE)) {
					 	update_user_meta($data['user_id'],'_assign_partner',$data['assign_partner']);
				 	} else{
				 		add_user_meta($data['user_id'], '_assign_partner', $data['assign_partner']);
				 	}
				}



				if(isset($data['clinic_addrs']))
				{
				 	if (get_user_meta($data['user_id'],'clinic_addrs', TRUE)) {
					 	update_user_meta($data['user_id'],'clinic_addrs',$data['clinic_addrs']);
				 	} else{
				 		add_user_meta($data['user_id'], 'clinic_addrs', $data['clinic_addrs']);
				 	}
				}


				if(isset($data['npi']))
				{
				 	if (get_user_meta($data['user_id'],'npi', TRUE)) {
					 	update_user_meta($data['user_id'],'npi',$data['npi']);
				 	} else{
				 		add_user_meta($data['user_id'], 'npi', $data['npi']);
				 	}
				}

///////////////////////////////////////// Sales////////////////////////////////////////


				if(isset($data['brn']))
				{
				 	if (get_user_meta($data['user_id'],'_brn', TRUE)) {
					 	update_user_meta($data['user_id'],'_brn',$data['brn']);
				 	} else{
				 		add_user_meta($data['user_id'], '_brn', $data['brn']);
				 	}
				}


				if(isset($data['ban']))
				{
				 	if (get_user_meta($data['user_id'],'_ban', TRUE)) {
					 	update_user_meta($data['user_id'],'_ban',$data['ban']);
				 	} else{
				 		add_user_meta($data['user_id'], '_ban', $data['ban']);
				 	}
				}


				if(isset($data['nbaccount']))
				{
				 	if (get_user_meta($data['user_id'],'_nbaccount', TRUE)) {
					 	update_user_meta($data['user_id'],'_nbaccount',$data['nbaccount']);
				 	} else{
				 		add_user_meta($data['user_id'], '_nbaccount', $data['nbaccount']);
				 	}
				}


				if(isset($data['commission_percentage']))
				{
				 	if (get_user_meta($data['user_id'],'_commission_percentage', TRUE)) {
					 	update_user_meta($data['user_id'],'_commission_percentage',$data['commission_percentage']);
				 	} else{
				 		add_user_meta($data['user_id'], '_commission_percentage', $data['commission_percentage']);
				 	}
				}


				if(isset($data['commission_percentage']))
				{
				 	if (get_user_meta($data['user_id'],'_commission_percentage', TRUE)) {
					 	update_user_meta($data['user_id'],'_commission_percentage',$data['commission_percentage']);
				 	} else{
				 		add_user_meta($data['user_id'], '_commission_percentage', $data['commission_percentage']);
				 	}
				}


				if(isset($data['assign_to']))
				{
				 	if (get_user_meta($data['user_id'],'assign_to', TRUE)) {
					 	update_user_meta($data['user_id'],'assign_to',$data['assign_to']);
				 	} else{
				 		add_user_meta($data['user_id'], 'assign_to', $data['assign_to']);
				 	}
				}


				if (isset($data['llc']) == 'llc')
				{
					$ein_no = $data['ein_no'];
					if (get_user_meta($data['user_id'],'_llc', TRUE)) {
					 	update_user_meta($data['user_id'],'_llc',$data['llc']);
					 	update_user_meta($data['user_id'],'_ein_no',$data['ein_no']);
				 	} else{
				 		add_user_meta($data['user_id'], '_llc', $data['llc']);
				 		add_user_meta($data['user_id'], '_ein_no', $data['ein_no']);
				 	}
					
				} elseif (isset($data['llc']) == 'corporation') {
					if (get_user_meta($data['user_id'],'_llc', TRUE)) {
					 	update_user_meta($data['user_id'],'_llc',$data['llc']);
					 	update_user_meta($data['user_id'],'_ein_no','');
				 	} else{
					 	add_user_meta($data['user_id'],'_llc',$data['llc']);
				 	}
				}

/////////////////////////////////////////End Sales////////////////////////////////////////


				 $receivereport='';
				 
				 if(isset($data['way_recive_fax']))
				 {
				 	$receivereport .=$data['way_recive_fax'].",";
				 }
				 if(isset($data['way_recive_email']))
				 {
				 	$receivereport .=$data['way_recive_email'].",";
				 }
				 if(isset($data['way_recive_none']))
				 {
				 	$receivereport .=$data['way_recive_none'].",";
				 }
				 
				 update_user_meta($data['user_id'],'report_recive_way',$receivereport);

			
				 if(isset($data['acc_cnt']))
				 {
					 $sql_data = "SELECT * from `wp_abd_usermeta` where `user_id`='".$data['user_id']."' AND `meta_key`='specimen_stages'";
					 $meta_results = $this->BlankModel->customquery($sql_data);
					 if(empty( $meta_results))
					 {
						$specimen_satges =implode(",",$data['acc_cnt']);
						add_user_meta($data['user_id'], 'specimen_stages', $specimen_satges);
					 }
					 else
					 {
						 $specimen_satges =implode(",",$data['acc_cnt']);
						 update_user_meta($data['user_id'],'specimen_stages',$specimen_satges);
					 }
				 }
				 

				 /*----------------------------END------------------------------------*/
				 /*--------------------------Update user table Ability------------------------*/
				 $ability_user_id = edit_table_data_curl_users('wp_abd_users',$data);
				 				 
				 /*--------------------------Update user table APS------------------------*/
				 $conditions = " ( `ID` = '".$data['user_id']."')";
		  		 $user_data = array('user_email' => $data['user_email'], 'user_login' =>$data['user_username'], 'user_url' => $data['user_url'],
		  						'user_nicename' => $data['user_nickname']);

		  		 $new_password = $data['new_password'];


				$confirm_password = $data['confirm_password'];
				if (isset($confirm_password) && !empty($confirm_password)) {
				 	$user_data['user_pass'] = md5(SECURITY_SALT.$new_password);
				}
				// print_r($user_data);
				// exit();
		  		 $programsedit = $this->BlankModel->editTableData('users', $user_data, $conditions);
				 /*-------------------------------END---------------------------------*/
				
		  		 $this->session->set_flashdata('succ', 'User Updated successfully');
			     header('location:'.base_url().'admin/user/');	
				 exit;
			}
		}
    }
}

  public function addUser()
  {
  
  	$sqlRoles = "SELECT * FROM `wp_abd_role_management`";
  	$executeRoles = $this->db->query($sqlRoles);
  	$roleArr = $executeRoles->result_array();


  	$sqlPartnerCompany = "SELECT * FROM `wp_abd_partners_company`";
	$executePartner = $this->db->query($sqlPartnerCompany);
	$resArrPartner = $executePartner->result_array();


    common_viewloader('User/addNewUser', array('roleList' => $roleArr,
												'partnerList' => $resArrPartner)); 	
  }

  public function createNewUser()
  {
    
  	if ($this->input->post()) {
  		$data = $this->input->post();

  		// $val = $data['partner_company'];
  		// print_r($val);
  		// exit();
		
  		$this->form_validation->set_rules('user_username','User Name','trim|required|max_length[50]');
  		$this->form_validation->set_rules('user_email','User Email','trim|required|valid_email|trim|xss_clean' );
  		$this->form_validation->set_rules('user_password','User Password','required');
  		$this->form_validation->set_rules('user_firstname','User First Name','trim|required|max_length[50]');
  		$this->form_validation->set_rules('user_role','User Role','required');
  		
  		if ($this->form_validation->run()== FALSE) {

			$this->addUser();
  		} else{

  			$user_login = $data['user_username'];
  			$user_email = $data['user_email'];

			$sqlRole = "SELECT * FROM `wp_abd_users` WHERE `user_login` = '$user_login' OR `user_email` = '$user_email'";

			$executeRole = $this->db->query($sqlRole);
			$val = $executeRole->result_array();

	
			if ($val) {

				$this->session->set_flashdata('user', 'Username Or User Email already exists');
		  	  	header('location:'.base_url().'admin/user/');
			} else{
			
			  	$data = $this->input->post();
			  	
			  	$password =   md5(SECURITY_SALT.$data['user_password']);
			    $display_name = $data['user_firstname'].' '. $data['user_lastname'];
			    $created_date = date("Y-m-d h:i:sa");
			    $table_data = array('user_login' =>$data['user_username'] ,'user_pass' => $password,'user_email' => $data['user_email'], 'display_name'=>$display_name ,'user_nicename'=> strtolower($data['user_firstname']), 'user_registered' => $created_date);

			     
			      $ability_user_id = add_table_data_curl_users('wp_abd_users', $table_data,$data);
			      $user_id = $this->BlankModel->addTableData('users', $table_data);

			    if($user_id AND $ability_user_id) {
			    	
					$role_type_array = array($data['user_role'] => '1');			   
					$role = serialize( $role_type_array);			   
					add_user_meta($user_id, 'first_name', $data['user_firstname']);
					add_user_meta($user_id, 'last_name', $data['user_lastname']);
					add_user_meta($user_id, 'wp_abd_capabilities', $role);

					add_user_meta($user_id, 'partners_company', $data['partner_company']);

					add_user_meta($user_id, 'nickname', strtolower($data['user_firstname']));
					add_user_meta($user_id, '_status', 'Active');
					$this->session->set_flashdata('succ', 'User Added successfully');
					header('location:'.base_url().'admin/user/');	
			  	 }
			  	 else {
					$this->session->set_flashdata('err', 'Something is worng. Please try again.');
					header('location:'.base_url().'admin/user/');	
					exit;
			  	}
		    }
		 }
	  } 
	  else{
		$this->session->set_flashdata('Err','Submission Failed');
		header('location:'.base_url().'admin/user/');
	    }
    }

  	function sendemail(){
		$userId = $this->input->post('user');	
		$sqlUserlist = "SELECT ID,user_email,user_login FROM wp_abd_users WHERE ID='".$userId."'";
		$exequerylist = $this->db->query($sqlUserlist);
		$resArrList = $exequerylist->result_array(); 
		
		$to        = $resArrList[0]['user_email'];
		$id        = $resArrList[0]['ID'];
		$user_name = $resArrList[0]['user_login'];

		$defaultPass= 'ADsaltlake';
		$password  = md5(SECURITY_SALT.$defaultPass);
		
		/*---------Update user password -----------------*/
		 $conditions = " ( `ID` = '".$id."')";
  		 $user_data = array('user_pass' => $password);
  		
  		 $programsedit = $this->BlankModel->editTableData('users', $user_data, $conditions);
		 update_user_meta($userId,'email_status','yes');

		/*------------------END-------------------------*/
		$user_message = "";
		$MailFormat= '<div>
		<table width="100%" border="0" cellpadding="2" cellspacing="2">
		<tr>
		<td align="left" valign="top">User Name:</td>
		<td align="left" valign="top">'.$user_name.'</td>
		</tr>
		<tr>
		<td align="left" valign="top">Password:</td>
		<td align="left" valign="top">'.$defaultPass.'</td>
		</tr>
		</table>
		</div>';
		
		$user_message .= "Dear ".$user_name.",<br><br> Please check your Login details below. <br/><br/>";
		$user_message .= $MailFormat;
		$user_message .= "Thank You<br>";
		$user_message .= "Ability Diagnostics Teams<br>";		
		$user_body     = mailBody($user_message);
		
		$from          = "mails@abilitydiagnostics.com";
		$subject       = "Ability Diagnostics Login Details";
		$result        = sendMail($to, $subject, $user_body, $from, "Ability Diagnostics Services");
		
		$admin_message = "Dear Administrator,<br><br> Please check below Login details for the user - ".$user_name.". <br/><br/>";
		$admin_message.= $MailFormat;
		$admin_message.= "Thank You<br>";
		$admin_message.= "Ability Diagnostics Teams<br>";
		$admin_body    =  mailBody($admin_message);
	
		$toAdmin = "info@abilitydiagnostics.com";	
		$subject = "Login Details";
		$result  = sendMail($toAdmin, $subject, $admin_body, $from, "Ability Diagnostics Services");
		
		if($result == true){
		  $msg = '1';
		}
		else{
		  $msg = "0";
		}
		echo $msg;
    }
 
 
   public function excel_sheet(){
	         
	header('Content-Type: text/csv; charset=utf-8');  
	header('Content-Disposition: attachment; filename=salesrepresentative.csv');  
	$output = fopen("php://output", "w");  

	fputcsv($output, array('ID', 'User Name','First Name', 'Last Name', 'Email')); 

	$sqlUser = "SELECT u1.ID,u1.user_login AS nickname, m1.meta_value AS firstname, m2.meta_value AS lastname, u1.user_email
	FROM wp_abd_users u1
	JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name')
	JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name')
	JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 
	AND m3.meta_value LIKE '%sales%' 
	GROUP BY u1.ID";

	$exequery = $this->db->query($sqlUser);
	$resArr = $exequery->result_array(); 

	foreach($resArr as $val)
	{	              
	 fputcsv($output, $val); 
	}  	        
	 fclose($output); 
	}
	
	function getUserLogDetails(){
		$data=array();
		$this->db->order_by('time_stamp','ASC');
		$uid=$this->input->post('uid');
		$this->db->where('user_id',$uid);
		$res = $this->db->get('wp_abd_user_log')->result_array();

		if(!empty($res)){
			$html='';
			for($i=0;$i<count($res);$i++){
				$name = get_user_meta_value($uid, 'first_name').' '.get_user_meta_value($uid, 'last_name');
				$html.='<tr class="odd gradeX"><td>'.($i+1).'</td>';
				$html.='<td>'.$res[$i]['user_login'].'</td>';
				$html.='<td>'.$name.'</td>';
				$html.='<td>'.date('Y-m-d',strtotime($res[$i]['time_stamp'])).'</td>';
				$html.='<td>'.date('H:i:s',strtotime($res[$i]['time_stamp'])).'</td>';
				$html.='<td>'.$res[$i]['site_view'].'</td></tr>';
			}
			$data['status']="1";
			$data['html']=$html;
		}else{
			$data['status']="0";
			$data['html']='<tr><td colspan="6">No Data Found</td></tr>';
		}
		


		echo json_encode($data);

	}
	    /*
		|--------------------------------------------------------------------------
		| curl delete User Table Aps Ability
		|--------------------------------------------------------------------		
		*/

	function deleteUserApsAbility($uid)
   {
       //$uid=$this->input->post("uid");
       
       $this->db->where("ID",$uid);
       $this->db->delete("wp_abd_users");
       
       $this->db->where("user_id",$uid);
       $this->db->delete("wp_abd_user_log");
       
       $this->db->where("user_id",$uid);
       $this->db->delete("wp_abd_usermeta");
       
       $api_url=$this->config->item('api_url').'ApsCurl/deleteUserApsAbility';
       $objarray=array("uid"=>$uid);

       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $api_url);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
       curl_setopt($ch, CURLOPT_HEADER, FALSE);
       curl_setopt($ch, CURLOPT_POST,TRUE);
       curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($objarray));
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       $response = curl_exec($ch);
       curl_close($ch);
       
       $result=json_decode($response,true);

       $this->session->set_flashdata('succ', 'User Successfully Deleted');
       header('location:'.base_url().'admin/user/');	
	   exit;
   }

 
}
?>