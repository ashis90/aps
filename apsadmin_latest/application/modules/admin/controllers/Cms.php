<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
class  Cms extends MY_Controller {

    function __construct() {
        parent::__construct();
          $this->session_checked($is_active_session = 1);
          $this->load->library('form_validation');
          // $this->load->helper('ckeditor_helper');
          // $this->load->library('CKEditor');
    }

	public function index() {
	 	$this->session_checked($is_active_session = 1);
	    $conditions = " (`site_page_status` ='Active')";
	  	$select_fields = '*';
	    $is_multy_result = 0;
	    $pageList = $this->BlankModel->getTableData('sitepages', $conditions, $select_fields, $is_multy_result);
	    common_viewloader('Cms/cms_mgr', array('pageList' => $pageList)); 
	}

	function add_page(){
	   $this->session_checked($is_active_session = 1);
	   common_viewloader('Cms/addPage');
	}

	function createNewPage(){
		if ($this->input->post()) {
			
			$this->form_validation->set_rules('site_page_name','Page Name','required');
			$this->form_validation->set_rules('site_page_title','Page Title','required');
			$this->form_validation->set_rules('site_page_desc','Page Description','required');
			if ($this->form_validation->run() == FALSE) {
				common_viewloader('Cms/addPage');
			} else{
				$data = array();
				$data = $this->input->post();

				$page_uploads = image_uploads('Sitecontentimage', $thumb_Size_width= '250', $thumb_Size_hight= '200', 'site_page_image');

				if (is_array($page_uploads)) {
					
					$page_image_name = $page_uploads['file_name'];

				} else{
					$page_image_name = '';
				}
					$date = date('Y-m-d h:i:sa');
					$page_data = array('site_page_name' => $data['site_page_name'],
							'site_page_title' => $data['site_page_title'], 
							'site_page_desc' => $data['site_page_desc'],
						 	'site_page_image' => $page_image_name,
						 	'site_page_status' => 'Active',
						 	'site_page_add_date' => $date,
						 	'site_page_type' => 'APS');

					$page_insert = $this->BlankModel->addTableData('wp_abd_sitepages', $page_data);
					if ($page_insert) {
						$this->session->set_flashdata('succs','Page Inserted successfully');
						header('location:'.base_url().'admin/cms');
					}
				}

		} else{
			$this->session->set_flashdata('Err','Submission Failed');
			header('locaion:'.base_url().'admin/cms');
		}
	}


	function pageEdit(){
	    $this->session_checked($is_active_session = 1);
		if($_GET['site_page_id']){
		$page_id = $_GET['site_page_id'];
		$conditions = " ( `site_page_id` = '".$page_id."' AND `site_page_status` = 'Active')";
	    $select_fields = '*';
	    $is_multy_result = 1;
		$page = $this->BlankModel->getTableData('sitepages', $conditions, $select_fields, $is_multy_result);
		
	   	common_viewloader('Cms/cms_mgredit', array('page' => $page)); 	
		}	
	}
 
	function pageEditing(){

	    $data = array();
	    $data = $this->input->post();	 
	    $logo_one="";
	    $get_img_conditions = " ( `site_page_id` = '".$data['site_page_id']."' AND `site_page_status` = 'Active')";
	    $select_fields = '*';
	    $is_multy_result = 1;
	    $details = $this->BlankModel->getTableData('sitepages', $get_img_conditions, $select_fields, $is_multy_result);
      
	  //--------profile image upload section-----------//
      
        if($_FILES['site_page_image']['name']==''){
          $logo_one = $details['site_page_image'];       
        }
        else
        {
			  $logo_one_uploads  = image_uploads('Sitecontentimage',$thumb_Size_width = '350', $thumb_Size_hight = '250', 'site_page_image');
			  if(is_array($logo_one_uploads)){
				$logo_one = $logo_one_uploads['file_name'];         
			  }   
			  if(!is_array($logo_one_uploads)){
				$this->session->set_flashdata('Err', $logo_one_uploads);
				header('location:'.base_url().'admin/cms'); 
				exit;        
			  }
        }
          
      //-------cover image upload section-------------//
      
      if(isset($data['site_page_desc']))
       {
          $description=$data['site_page_desc'];
       }
	   else
	   {
          $description="";
       }
       
    
	   if(isset($data['site_page_title']))
       {
          $site_page_title=$data['site_page_title'];
       }
	   else
	   {
          $site_page_title="";
       }
	     
		$conditions = " ( `site_page_id` = '".$data['site_page_id']."' AND `site_page_status` = 'Active')";
  		
		$page_data = array('site_page_name' => $data['site_page_name'],
							'site_page_desc' => html_entity_decode($data['site_page_desc']),
							'site_page_image'=>$logo_one,
							'site_page_title'=>$data['site_page_title'],
							//'site_page_type'=>$data['site_page_type']
						);
      
  		$programsedit = $this->BlankModel->editTableData('sitepages',$page_data, $conditions);
  		
		$this->session->set_flashdata('succ', 'Page Updated successfully');
	    header('location:'.base_url().'admin/cms/');
		exit;		   
	}

   
}
?>