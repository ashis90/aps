<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  

  if (!empty($user_data)) {
       
        $user_name = $user_data['user_login'];
        $user_id =  $user_data['ID'];
        $user_email = $user_data['user_email'];
 
        $user_website = $user_data['user_url'];
        $fname='';$lname='';$wp_abd_capabilities='';$nickname='';$mobile='';$address='';$fax='';$report_recive_way='';$report_recive_way=''; $company_logo='';
       
    if (!empty($user_id)) {
        $user_role = get_user_role($user_id);

   
        $fname = get_user_meta_value($user_id,'first_name',TRUE);
        $lname = get_user_meta_value($user_id,'last_name',TRUE);
        $wp_abd_capabilities = get_user_meta_value($user_id,'wp_abd_capabilities',TRUE);
        $partners_company = get_user_meta_value($user_id,'partners_company',TRUE);

        $nickname = get_user_meta_value($user_id,'nickname',TRUE);
        $mobile = get_user_meta_value($user_id,'_mobile',TRUE);
        $address = get_user_meta_value($user_id,'_address',TRUE);
        $spe_stage = get_user_meta_value($user_id,'specimen_stages',TRUE);
       
      if($user_role == 'physician' || $user_role == 'partner'){
          $fax = get_user_meta_value($user_id,'fax',TRUE);
      		$anthr_fax_number = get_user_meta_value($user_id,'anthr_fax_number',TRUE);
      }

      if($user_role == 'sales' || $user_role == 'regional_manager'){
          $ban = get_user_meta_value($user_id,'_ban',TRUE);
          $brn = get_user_meta_value($user_id,'_brn',TRUE);
          $nbaccount = get_user_meta_value($user_id,'_nbaccount',TRUE);
          $commission_percentage = get_user_meta_value($user_id,'_commission_percentage',TRUE);

          $assign_to = get_user_meta_value($user_id,'assign_to',TRUE);
          if (!empty($assign_to)) {
          $assign_to_value = get_user_meta_value($assign_to,'first_name',TRUE).' '.
                          get_user_meta_value($assign_to, 'last_name', TRUE);
          } else{
            $assign_to_value = '';
          }

          $llc = get_user_meta_value($user_id,'_llc',TRUE);
          $ein_no = get_user_meta_value($user_id,'_ein_no',TRUE);
        
      }
       
      if($user_role == 'physician'){
        $report_recive_way = get_user_meta_value($user_id,'report_recive_way',TRUE);
        $npi = get_user_meta_value($user_id,'npi',TRUE);
        $anthr_fax_number = get_user_meta_value($user_id,'anthr_fax_number',TRUE);
        $cell_phone = get_user_meta_value($user_id,'cell_phone',TRUE);
        $manager_contact_name = get_user_meta_value($user_id,'manager_contact_name',TRUE);
        $manager_cell_number = get_user_meta_value($user_id,'manager_cell_number',TRUE);
        $assign_specialty = get_user_meta_value($user_id,'assign_specialty',TRUE);
        $clinic_name = get_user_meta_value($user_id,'_clinic_name',TRUE);
        $clinic_addrs = get_user_meta_value($user_id,'clinic_addrs',TRUE);


        $added_by = get_user_meta_value($user_id,'added_by',TRUE);
        if (!empty($added_by)) {
          $added_by_value = get_user_meta_value($added_by,'first_name',TRUE).' '.
                          get_user_meta_value($added_by, 'last_name', TRUE);
        } else{
          $added_by_value = '';
        }
                         

        $combined_by = get_user_meta_value($user_id,'combined_by',TRUE);
        if (!empty($combined_by)) {
          $combined_by_value = get_user_meta_value($combined_by,'first_name',TRUE).' '.
                            get_user_meta_value($combined_by, 'last_name', TRUE);
        } else{
          $combined_by_value = '';
        }


        $assign_clinic = get_user_meta_value($user_id,'_assign_clinic',TRUE);
        if(!empty($assign_clinic)){
          $assign_clinic_value = get_user_meta_value($assign_clinic,'first_name',TRUE).' '. get_user_meta_value($assign_clinic, 'last_name', TRUE);
        } else{
          $assign_clinic_value= '';
        }


        $assign_partner = get_user_meta_value($user_id,'_assign_partner',TRUE);
        if(!empty($assign_partner)){
          $assign_partner_value = get_user_meta_value($assign_partner,'first_name',TRUE).' '. get_user_meta_value($assign_partner, 'last_name', TRUE);
        } 
        else{
          $assign_partner_value = '';
        }

      }
       
      if ($user_role == 'partner') {
       	$company_logo = get_user_meta_value($user_id,'company_logo',TRUE);
      }
    }
  }
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1> Users Management </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Users List</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
    <div class="row">
      <div class="col-xs-12">
       
          <div class="box-header">
            <?php
            if ($this->session->flashdata('email')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('email');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>
            <h4 class="title">User Edit</h4><br>
            <?php
            if ($this->session->flashdata('wrong')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('wrong');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>
          </div>
          <form action="<?php echo base_url().'admin/User/userEditing/';?>" method="POST" enctype="multipart/form-data" name="program_frm" id="program_frm">
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $_GET['user_id'];?>" />

            <div class="form-group col-md-6">
              <label for="exampleTextarea">User Role </label>
              <select name="user_role" id="user_role" class="form-control">
                <option value="">Select User Role</option>
                <?php 
                  foreach($roleList as $rolval)
                  {
                  ?>
                <option value="<?php echo $rolval['role_shortname'];?>" <?php if($rolval['role_shortname']==$user_role){?> selected="selected" <?php }?>><?php echo ucwords(str_replace('_', ' ', $rolval['role_name']));?></option>
                <?php 
                  }
                  ?>
              </select>
              <?php echo form_error('user_role','<div class="text-danger">','</div>'); ?>
            </div>

            <div class="form-group col-md-6" id="partner">
              <label for="exampleTextarea">Partners Company </label>
              <select name="partner_company" id="partner_company" class="form-control">
                <option value="">Select Partners Company</option>
                <?php 
                  foreach($partnerCompanyList as $companyPartner)
                  {
                  ?>
                  <option value="<?php echo $companyPartner['partner_id'];?>" <?php if($companyPartner['partner_id']==$partners_company){?> selected="selected" <?php }?>><?php echo $companyPartner['partner_name'];?></option>

                <?php 
                  }
                ?>
                
              </select>
              <?php echo form_error('partner_company','<div class="text-danger">','</div>'); ?>
            </div>

            <div class="form-group col-md-6">
              <label for="exampleTextarea"> User Name </label>
              <input type="text" id="user_username" name="user_username" value="<?php echo $user_name; ?>">
              <?php echo form_error('user_username','<div class="text-danger">','</div>'); ?>
            </div>


            <div class="form-group col-md-6">
              <label for="exampleTextarea"> First Name </label>
              <input type="text" id="user_firstname" name="user_firstname" placeholder="Enter User First name" value="<?php echo $fname; ?>">
              <?php echo form_error('user_firstname','<div class="text-danger">','</div>'); ?>
            </div>


            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Last Name </label>
              <input type="text" id="user_lastname" name="user_lastname" placeholder="Enter User Lastname" value="<?php echo $lname; ?>">
              <?php echo form_error('user_lastname','<div class="text-danger">','</div>'); ?>
            </div>

            <div class="form-group col-md-6">
              <label for="exampleTextarea"> New Password </label>
              <input type="password" id="new_password" class="form-control" name="new_password" placeholder="Enter Password">
              <?php echo form_error('user_mobile','<div class="text-danger">','</div>'); ?>

            </div>

            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Confirm Password </label>
              <input type="password" id="confirm_password" class="form-control" name="confirm_password" placeholder="Enter Confirm Password">
              <?php echo form_error('confirm_password','<div class="text-danger">','</div>'); ?>
            </div>

            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Nick Name </label>
              <input type="text" id="user_nickname" name="user_nickname" placeholder="Enter User Nickname" value="<?php echo $nickname; ?>">
            </div>

            <!-- <b>Contact Info:</b> -->
              <br/><br/> <br><br>
            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Email </label>
              <input type="text" id="user_email" name="user_email" placeholder="Enter User Email" value="<?php echo $user_email;?>">
              <?php echo form_error('user_email','<div class="text-danger">','</div>'); ?>
            </div>


            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Website </label>
              <input type="text" id="user_url" name="user_url" placeholder="Enter User Website" value="<?php echo $user_website; ?>">
            </div>


            <br/><br/> 
            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Mobile </label>
              <input type="text" id="mobile" name="user_mobile" placeholder="Enter User Mobile" value="<?php echo $mobile; ?>">
              <?php echo form_error('user_mobile','<div class="text-danger">','</div>'); ?>
            </div>


            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Address </label>
              <textarea name="user_address" rows="3" cols="80" placeholder="User address" class="form-control"><?php echo $address; ?></textarea>
            </div>

            <?php if($user_role == 'physician' ){ ?>
            
            <div class="form-group col-md-6">
              <label for="exampleTextarea">2nd Address </label>
              <textarea name="clinic_addrs" rows="3" cols="80" placeholder="User 2nd address" class="form-control"><?php echo $clinic_addrs; ?></textarea>
            </div>

          <?php } ?>

            <div class="clear"></div>
			

            <?php if($user_role =='physician' || $user_role =='partner'){ ?>
            <div class="form-group  col-md-6">
              <label for="exampleTextarea"> Fax </label>
              <input type="text" id="user_fax" name="user_fax" placeholder="Enter User Fax" value="<?php echo $fax; ?>">
              <?php echo form_error('user_mobile','<div class="text-danger">','</div>'); ?>
            </div>
            <?php }?>


            <?php if($user_role =='partner'){ ?>
            <div class="form-group col-md-6">
              <img src="<?php echo  $company_logo;?>" height="100px" width="100px">
              <label for="exampleTextarea"> Company logo </label>
              <!-- <input type="file" name="company_logo" id="company_logo" class="form-control"> -->
              <input type="file" name="company_logo" id="company_logo" class="form-control">
            </div>
            <?php } ?>

            <?php if($user_role == 'physician' ){ ?>

            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Another Fax number : </label>
              <input type="text" id="anthr_fax_number" name="anthr_fax_number" placeholder="Enter Another Fax" value="<?php echo $anthr_fax_number; ?>">
            </div>


            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Cell Phone : </label>
              <input type="text" id="cell_phone" name="cell_phone" placeholder="Enter Cell Phone" value="<?php echo $cell_phone; ?>">
            </div>


            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Office Manager Contact Name : </label>
              <input type="text" id="manager_contact_name" name="manager_contact_name" placeholder="Enter  Office Manager Contact Name" value="<?php echo $manager_contact_name; ?>">
            </div>  

            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Office Manager Cell Number : </label>
              <input type="text" id="manager_cell_number" name="manager_cell_number" placeholder="Enter  Office Manager Cell Number" value="<?php echo $manager_cell_number; ?>">
            </div>

            <div class="form-group col-md-6">
              <label for="exampleTextarea"> National Provider Identifier : </label>
              <input type="text" id="npi" name="npi" placeholder="Enter National Identifier" value="<?php echo $npi; ?>">
            </div>

            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Assign Physician to Clinic Profile: </label>
              <select name="assign_clinic" id="assign_clinic" class="form-control">
                 <option value="">Select Clinic</option>
                <?php foreach($clinicList as $clinic) {
                  ?>
                 <option value="<?php echo $clinic['ID'];?>" <?php if($clinic['firstname']." ".$clinic['lastname']==$assign_clinic_value){?> selected="selected" <?php }?>><?php echo $clinic['firstname']." ".$clinic['lastname']?></option>
                <?php
                  }
                ?>
              </select>
            </div>


            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Combine Physicians Account: </label>
              <select name="combined_by" id="combined_by" class="form-control">
                 <option value="">Select Combine Physicians Account</option>
                <?php foreach($combinePhysicianList as $combine) {  ?>
                  <option value="<?php echo $combine['ID'];?>" <?php if($combine['firstname']." ".$combine['lastname']==$combined_by_value){?> selected="selected" <?php }?>><?php echo $combine['firstname']." ".$combine['lastname']?>
                  </option>
                <?php
                  }
                ?>
              </select>
            </div>


            <div class="form-group col-md-6">
              <label for="exampleTextarea">Sales Representative : </label>
              <select name="added_by" id="added_by" class="form-control">
                 <option value="">Select Sales Representative</option>
                <?php foreach($salesList as $sales) {  ?>
                  <option value="<?php echo $sales['ID'];?>" <?php if($sales['firstname']." ".$sales['lastname']==$added_by_value){?> selected="selected" <?php }?>><?php echo $sales['firstname']." ".$sales['lastname']?>
                  </option>
                <?php
                  }
                ?>
              </select>
            </div> 


            <div class="form-group col-md-6">
              <label for="exampleTextarea">Clinic Name : </label>
              <input type="text" id="clinic_name" name="clinic_name" placeholder="Enter Clinic Name" value="<?php echo $clinic_name; ?>">
            </div>  

            
            <div class="form-group col-md-6">
              <label for="exampleTextarea">Assign Physician's Specialty: </label>
              <select name="assign_specialty" id="assign_specialty" class="form-control">
                 <option value="">Select Physician's Specialty</option>

                <?php foreach($assignSpecialityList as $speciality) {  ?>

                  <option value="<?php echo $speciality['meta_value'];?>" <?php if($speciality['meta_value']==$assign_specialty){?> selected="selected" <?php }?>><?php echo $speciality['meta_value']; ?>
                  </option>

                <?php
                  }
                ?>
              </select>
            </div> 

            <div class="form-group col-md-6">
              <label for="exampleTextarea"> Assign with Partner Company: </label>
              <select name="assign_partner" id="assign_partner" class="form-control">
                 <option value="">Select Partner</option>
                <?php foreach($partnerList as $partner) {
                  ?>

                  <option value="<?php echo $partner['ID'];?>" <?php if($partner['firstname']." ".$partner['lastname']== $assign_partner_value){?> selected="selected" <?php }?>><?php echo $partner['firstname']." ".$partner['lastname']?></option>

                <?php
                  }
                ?>
              </select>
            </div> 


            <!-- <b> Note :***Physicians only. </b> <br/><br/> -->
            <div class="form-group col-md-6">
              <?php
                $receiveArr = explode(',',$report_recive_way);
                ?>
              
              <label>Additional ways to get reports:</label>
              <input type="checkbox" name="way_recive_fax" value="Fax" id="way_recive_fax" <?php if(count($receiveArr)>0){if(in_array('Fax',$receiveArr)){?> checked="checked" <?php }}?>> Fax
              <input type="checkbox" name="way_recive_email" value="Email" id="way_recive_email" <?php if(count($receiveArr)>0){if(in_array('Email',$receiveArr)){?> checked="checked" <?php }}?>> Email
              <input type="checkbox" name="way_recive_none" value="None" id="way_recive_none" <?php if(count($receiveArr)>0){if(in_array('None',$receiveArr)){?> checked="checked" <?php }}?>> None
            </div>     
            <?php } ?>


            <!-- <div class="form-group">
              <label for="exampleTextarea"> Assign with Partner Company: </label>
              <select name="assign_partner" id="assign_partner" class="form-control">
                 <option value="">Select Partner</option>
                <?php foreach($partnerList as $partner) {
                  ?>

                  <option value="<?php echo $partner['ID'];?>" <?php if($partner['firstname']." ".$partner['lastname']==$assign_partner_value){?> selected="selected" <?php }?>><?php echo $partner['firstname']." ".$partner['lastname']?></option>

                <?php
                  }
                ?>
              </select>
            </div> -->


            <?php 
            if($user_role ==='laboratory' || $user_role ==='aps_sales_manager'){ ?>
            <div class="form-group col-md-6">
              <label for="exampleTextarea">Access Control:</label><br> 
              <?php $stage= explode(",",$spe_stage); ?>
              <input type="checkbox" name="acc_cnt[]" value="1" <?php if(in_array("1",$stage)) {  echo "checked"; } ?>>Delivery To Lab<br>
              <input type="checkbox" name="acc_cnt[]" value="2" <?php if(in_array("2",$stage)) {  echo "checked"; } ?>>Gross<br>
              <input type="checkbox" name="acc_cnt[]" value="3" <?php if(in_array("3",$stage)) {  echo "checked"; } ?>>Microtomy<br>
              <input type="checkbox" name="acc_cnt[]" value="4" <?php if(in_array("4",$stage)) {  echo "checked"; } ?>>QC Check<br>
            </div>
            <?php } ?>


<!-- --------------------------------Sales ---------------------------------------------->

            <?php
              if($user_role === 'sales' || $user_role === 'regional_manager' ){ ?>
                <div class="form-group col-md-6">
                  <label for="exampleTextarea"> Bank Routing Number : </label>
                  <input type="text" id="brn" name="brn" placeholder="Enter  Bank Routing Number" value="<?php echo $brn; ?>">
                </div>

                <div class="form-group col-md-6">
                  <label for="exampleTextarea"> Bank Account Number : </label>
                  <input type="text" id="ban" name="ban" placeholder="Enter Bank Account Number" value="<?php echo $ban; ?>">
                </div>


                <div class="form-group col-md-6">
                  <label for="exampleTextarea"> Name on Bank Account : </label>
                  <input type="text" id="nbaccount" name="nbaccount" placeholder="Enter Name on Bank Account" value="<?php echo $nbaccount; ?>">
                </div>


                <div class="form-group col-md-6">
                  <label for="exampleTextarea"> Commission percentage(%) : </label>
                  <input type="text" id="commission_percentage" name="commission_percentage" placeholder="Enter Commission percentage(%)" value="<?php echo $commission_percentage; ?>">
                </div>


                <div class="form-group col-md-6">
                  <label for="exampleTextarea"> Regional Manager: </label>
                  <select name="assign_to" id="assign_to" class="form-control">
                     <option value="">Select regional Manager</option>
                    <?php foreach($salesregionalList as $salesregional) {  ?>
                      <option value="<?php echo $salesregional['ID'];?>" <?php if($salesregional['firstname']." ".$salesregional['lastname']==$assign_to_value){?> selected="selected" <?php }?>><?php echo $salesregional['firstname']." ".$salesregional['lastname']?>
                      </option>
                    <?php
                      }
                    ?>
                  </select>
                </div>


                <div class="form-group col-md-6">
                  <label for="exampleTextarea"> Is your company an LLC or Corporation </label>
                  <?php
                    if ($llc == 'llc') { ?>
                      <div class="checkbox-llb">
                      <input class="form-check-input" type="radio" name="llc" id="llc" value="llc" checked> LLC
                      <input class="form-check-input" type="radio" name="llc" id="corporation" value="corporation"> Corporation
                    (if applicable)? : 
                  </div>

                  <?php  } elseif($llc == 'corporation'){  ?>
                    <div class="checkbox-llb">
                      <input class="form-check-input" type="radio" name="llc" id="llc" value="llc"> LLC
                      <input class="form-check-input" type="radio" name="llc" id="corporation" value="corporation" checked> Corporation
                    (if applicable)? : 
                  </div>
                  <?php } else{ ?>
                    <div class="checkbox-llb">
                      <input class="form-check-input" type="radio" name="llc" id="llc" value="llc"> LLC
                      <input class="form-check-input" type="radio" name="llc" id="corporation" value="corporation"> Corporation
                    (if applicable)? : 
                  </div>
                  <?php }   ?>
                </div>


                <div class="form-group col-md-6">
                    <div class="form-row agent_det_div" id="llc_open" style="display: none;">  
                    <label for="inputState">EIN number</label>
                    <input placeholder="EIN number" type="text" name="ein_no" id="ein_no" value="<?php echo $ein_no; ?>" class="form-control">
                    </div>
                </div>

            <?php  } ?>

<!-- --------------------------------Sales ---------------------------------------------->




            
            <div class="form-group col-md-6">
              <label for="exampleTextarea"> User status: </label>
              <select name="user_status" id="user_status" class="form-control">
                <option value="">Select Status</option>
                <option value="Active" <?php if( isset($user_data['status']) && $user_data['status']=='Active'){ echo "selected"; }?>>Active</option>
                <option value="Inactive" <?php if( isset($user_data['status']) && $user_data['status']=='Inactive'){ echo "selected"; }?>>Inactive</option>
              </select>
            </div>
            
            
			     <div class="clear"></div>


            <button type="submit" class="btn btn-primary ban-sbmt">Update User</button>
          </form>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.row -->
  </section>
</div>

<script type="text/javascript">
  $( document ).ready(function() { 
    $("#llc").click(function(){
      $("#llc_open").show();
    });
    $("#corporation").click(function(){
      $("#llc_open").hide();
    })

  });
</script>
