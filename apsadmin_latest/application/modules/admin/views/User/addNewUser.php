<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Users Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Usere List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Add New User</h4>
          </div>
          <form action="<?php echo base_url().'admin/user/createNewUser/';?>" method="POST" enctype="multipart/form-data" name="program_frm" id="program_frm">
            <div class="form-group">
              <label for="exampleTextarea"> User Name </label>
              <input type="text" id="user_username" name="user_username" autocomplete="off"  placeholder="Enter Username">
              <?php echo form_error('user_username','<div class="text-danger">','</div>'); ?>
            </div>
            <div class="form-group">
              <label for="exampleTextarea"> Email </label>
              <input type="email" id="user_email" name="user_email" class="form-control" placeholder="Enter User Email">
              <?php echo form_error('user_email','<div class="text-danger">','</div>'); ?>
            </div>
            <div class="form-group">
              <label for="exampleTextarea">Password </label>
              <input type="password" id="user_password" class="form-control" name="user_password" placeholder="Enter User password" value="" minlength="6">
              <?php echo form_error('user_password','<div class="text-danger">','</div>'); ?>
            </div>
            <div class="form-group">
              <label for="exampleTextarea"> First Name </label>
              <input type="text" id="user_firstname" name="user_firstname" placeholder="Enter User First name">
              <?php echo form_error('user_firstname','<div class="text-danger">','</div>'); ?>
            </div>
            <div class="form-group">
              <label for="exampleTextarea"> Last Name </label>
              <input type="text" id="user_lastname" name="user_lastname" placeholder="Enter User Lastname">
              <?php echo form_error('user_lastname','<div class="text-danger">','</div>'); ?>
            </div>
            
            <div class="form-group">
              <label for="exampleTextarea">Users Role </label>
              <select name="user_role" id="user_role" class="form-control">
                <option value="">Select User Role</option>
                <?php 
                  foreach($roleList as $rolval)
                  {
                    // $val = $rolval['role_name'];
                    // echo "<pre>";
                    // print_r($val);
                    // exit();                 
                  ?>
                <option value="<?php echo $rolval['role_shortname'];?>"><?php echo ucwords(str_replace('_', ' ', $rolval['role_name']));?> </option>
                <?php 
                  }
                ?>
                
              </select>
              <?php echo form_error('user_role','<div class="text-danger">','</div>'); ?>
            </div>




            <div class="form-group" style="display: none;" id="partner">
              <label for="exampleTextarea">Partners Company </label>
              <select name="partner_company" id="partner_company" class="form-control">
                <option value="">Select Partners Company</option>
                <?php 
                  foreach($partnerList as $partnerval)
                  {
                    // $val = $partnerval['partner_name'];
                    // echo "<pre>";
                    // print_r($val);
                    // exit();                 
                  ?>
                <option value="<?php echo $partnerval['partner_id'];?>"><?php echo $partnerval['partner_name'];?> </option>
                <?php 
                  }
                ?>
                
              </select>
              <?php echo form_error('user_role','<div class="text-danger">','</div>'); ?>
            </div>




            <button type="submit" class="btn btn-primary ban-sbmt">Add User</button>
          </form>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.row -->
  </section>
</div>
