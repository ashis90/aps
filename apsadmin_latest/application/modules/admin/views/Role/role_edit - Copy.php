<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Role Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Role List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Add New Role</h4>
          </div>
          <form action="<?php echo base_url().'admin/role/roleEditing/';?>" method="POST" enctype="multipart/form-data" name="role_frm" id="role_frm">
            <input type="hidden" name="role_id" value="<?php echo $role_data['role_id'];  ?>">
            <div class="form-group">
              <label for="exampleTextarea"> Role Name </label>
              <input type="text" id="role_name" name="role_name"  placeholder="Enter Role Name" value="<?php echo $role_data['role_name']; ?>">
              <?php echo form_error('role_name','<div class="text-danger">','</div>'); ?>
            </div>
            
            <div class="form-group">
              <label for="exampleTextarea"> Role Short Name </label>
              <input type="text" id="role_shortname" name="role_shortname" placeholder="Enter Short name" value="<?php echo $role_data['role_shortname'];  ?>">
              <?php echo form_error('role_shortname','<div class="text-danger">','</div>'); ?>
            </div>
            
            <button type="submit" class="btn btn-primary ban-sbmt">Update Role</button>
          </form>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.row -->
  </section>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript">
$(function(){


  $('#role_name').keyup(function()
  {
    var yourInput = $(this).val();
    // var test = $('#te').val();
    re = /[`~!@#$%^&*()|+\=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,'');
      $(this).val(no_spl_char);
      alert('Special Character is not allowed');
    } else{
        func();
    }
  });


  $('#role_shortname').keyup(function()
  {
    var yourInput = $(this).val();
    // var test = $('#te').val();
    re = /[`~!@#$%^&*()|+\=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,'');
      $(this).val(no_spl_char);
      alert('Special Character is not allowed');
    }
  });

});


function func() { 
  
    // var str = '/var/www/site/Brand new document.docx';

  // document.write( str.replace(/\s/g, '') );

  // var str = "B I O S T A L L"  
  var str = $("#role_name").val();
  str = str.replace(/\s/g,'_');  
  $("#role_shortname").val(str);

} 
</script>
