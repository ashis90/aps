<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Export Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Export Data</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header"></div>
          <!-------------------------------- Show Roles ------------------------------------>

          <h3>Select Role To Export</h3>
          <form action="<?php echo base_url().'admin/export/value/';?>" method="POST" enctype="multipart/form-data" name="program_frm" id="program_frm">
          <div class="lidot">
            <ul>
              <?php foreach($roleList as $rolval) { 
                 // echo "<pre>";
                 // print_r($rolval);
                 // exit();  
                  ?>
              <li>
                <input type="checkbox" name="role[]" value="<?php print_r($rolval['role_name']);  ?>"/>

                <?php echo $rolval['role_name']; ?> 
                  <p></p>
              </li>
              <?php } ?>

              <?php
                $all = 'all';
              ?>
              <li>
                <a href="<?php echo site_url('admin/user/index/').$all; ?>">
                  All
                  <p></p>
                </a>
              </li>
            </ul>
          </div>
          <!-------------------------------- Show Roles ------------------------------------>


          <!-------------------------------- Show Meta Key ------------------------------------>
          <div class="box-header"></div>
          <h3>Select Meta Key To Export</h3>
          <div class="lidot">
            <ul>
              <?php foreach($resUser as $userval) { ?>
                <?php 
                  if (($userval['COLUMN_NAME'] == 'ID') || 
                    ($userval['COLUMN_NAME'] == 'user_email') ||
                    ($userval['COLUMN_NAME'] == 'display_name') || 
                    ($userval['COLUMN_NAME'] == 'user_registered')) {
                ?>
              <li>
                <input type="checkbox" name="user[]" value="<?php print_r($userval['COLUMN_NAME']); ?>" /> 
                <?php 
                    print_r($userval['COLUMN_NAME']); ?> 
              </li>
                <?php    echo "<br><br>";
                }
                ?>
                  <p></p>
              <?php } ?>


              <?php foreach($metaList as $metaval) { ?>
              <li>
                <input type="checkbox" name="meta[]" value="<?php print_r($metaval['meta_key']); ?>" /> 
                <?php echo $metaval['meta_key']; ?>
                  <p></p>
              </li>
              <?php } ?>

            </ul>
          </div>
            <button type="submit" class="btn btn-primary ban-sbmt">Export</button>

          <!-------------------------------- Show Meta Key ------------------------------------>
        </form>

          
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
<style>
  .lidot ul {
  display: flex;
  list-style: none;
  flex-flow: row wrap;
  padding: 0;
  }
  .lidot ul li {
  margin: 7px;
  border: 1px solid #282828;
  padding: 5px;
  display: flex;
  align-items: center;
  }
  .lidot ul li a {
  display:flex;
  align-items:center;
  color:#000;
  }
  .lidot ul li p{
  margin-bottom:0;
  margin-left:6px;
  }
</style>
