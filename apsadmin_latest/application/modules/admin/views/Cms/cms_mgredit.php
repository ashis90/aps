<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Page Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Page List</li>
    </ol>
  </section>
     <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Page List </h4>
          </div>
        <form action="<?php echo base_url().'admin/cms/pageEditing/';?>" method="POST" name="program_frm" id="program_frm" enctype='multipart/form-data'>
         
        <input type="hidden" value="<?php echo $_REQUEST['site_page_id'];?>" name="site_page_id" />
		      <div class="form-group">
                <label for="exampleTextarea"> Page Name :</label>
                <input type="text" name="site_page_name" value="<?php echo $page['site_page_name'];?>"/>        
             </div>
             
			 <div class="form-group">
                <label for="exampleTextarea"> Page Title :</label>
                <input type="text" id="site_page_title" name="site_page_title" value="<?php echo $page['site_page_title'];?>"/>        
             </div>
    
          <div class="form-group">
            <label for="exampleTextarea"> Description:</label>
            <textarea class="tnytextarea" id="site_page_desc" name="site_page_desc" rows="10" cols="80" placeholder="Enter Description"><?php echo $page['site_page_desc'];?></textarea>            
          </div>
        
          <div class="form-group">
            <label for="image"> Image :</label>
           <input type="file" class="form-control-file" id="logo" aria-describedby="fileHelp" name="site_page_image">
           <?php
              if (($page['site_page_image']) != '') { ?>
               <a href="<?php echo base_url().'assets/uploads/Sitecontentimage/'.$page['site_page_image'];?>" class="fancy">
                   <div class="imf_dov1"><img src = "<?php echo base_url().'assets/uploads/thumb/'.$page['site_page_image'];?>" alt="<?php echo $page['site_page_image'];?>"  id="fancyLaunch"></div>
               </a>
              <?php  } ?>

          </div>

            <div class="text-center">
           <a href="<?php echo base_url();?>admin/cms/" class="btn btn-warning"> << Back</a>
          <button type="submit" class="btn btn-primary ban-sbmt" id="videos">Submit</button>
         </div>
        </form>
        </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
  
    <!-- /.row -->
  </section>
</div>
